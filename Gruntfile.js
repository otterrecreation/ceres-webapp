module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'compressed'
        },
        files: {                         // Dictionary of files
          'public/media/css/main.min.css': 'app/ember/scss/main.scss'       // 'destination': 'source'
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [ 'app/ember/**/*.js' ],
        dest: 'public/media/js/main.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.projectName %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        sourceMap: true, //provide a .map file with the minified file. we're nice like that
      },
      dist: {
        files: {
          'public/media/js/main.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
    watch: {
      styles: {
        files: ['app/ember/scss/**/*.scss'], // which files to watch
        tasks: ['sass:dist'],
        options: {
          nospawn: true
        }
      },
      scripts: {
          files: ['app/ember/**/*.js'], // which files to watch
          tasks: ['concat', 'uglify'],
          options: {
            nospawn: true
          }
      }
     },
     copy: {
          main: {
            files: [
              // includes files within path and its sub-directories
              { expand: true, src: ['./**'], dest: '../<%= pkg.productionName %>' }
            ]
          }
      },
      sync: {
          main: {
            files: [{
              src: [
                '**' /* Include everything */
              ],
              dest: '../<%= pkg.productionName %>'
            }],
            pretend: false, // Don't do any IO (if set to true). Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
            verbose: true // Display log messages when copying files
          }
      },
      //just an example right now. used to replace any of the setting information
      replace: {
          example: {
            src: ['text/*.txt'],             // source files array (supports minimatch)
            dest: 'build/text/',             // destination directory or file
            replacements: [{
              from: 'Red',                   // string replacement
              to: 'Blue'
            }, {
              from: /(f|F)(o{2,100})/g,      // regex replacement ('Fooo' to 'Mooo')
              to: 'M$2'
            }, {
              from: 'Foo',
              to: function (matchedWord) {   // callback replacement
                return matchedWord + ' Bar';
              }
            }]
          }
      },
      
      bower: {
        install: {
            options: {
                layout: "byComponent",
                targetDir: "public/media/packages"
            }
        }
      }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-bower-task');

  grunt.registerTask('prepare-production', ['sync', 'replace']); //after files are transferred over we can then replace any pieces we need in the code...
  grunt.registerTask('default', ['sass', 'concat', 'uglify']);

};
