<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | OAUTH TYPE
    |--------------------------------------------------------------------------
    |
    | Used to determine which kind of OAuth to use. When using 3-legged and
    | trying to obtain use credentials, The system will be redirected outside
    | to the API where the user will enter their credentials. 
    |
    | Supported: "2-legged", "3-legged" (default is 2-legged because I'm lazy)
    |
    */
    'type' => '2-legged',

    /*
    |--------------------------------------------------------------------------
    | API BASE & TOKEN ENDPOINT
    |--------------------------------------------------------------------------
    |
    | The base url for the API.
    |
    | Supported: "URL"
    | In this case because the API lives in the same place, we can use the URL
    | class for convenience
    |
    */
    'base' => URL::to('api'),
    'token_endpoint' => URL::to('api/token'),
    'redirect_uri' => URL::to('/'),
    
    /*
    |--------------------------------------------------------------------------
    | CLIENT ID & SECRET
    |--------------------------------------------------------------------------
    |
    | Most OAuth APIs require passing a client_id and client_secret when
    | making requests to the API.
    |
    */
    'id' => 'ceres_webapp',
    'secret' => '123456',

);
