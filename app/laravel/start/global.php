<?php

define( 'ROOT_URL', substr( URL::to('/'), Helper::strposX(  URL::to('/'), '/', 3 ) ) . '/' );
define( 'EMBER_VIEWS', __DIR__ . '/../../ember' );
define( 'UPLOAD_DESTINATION', dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public/media/files');
View::addNamespace('ember', EMBER_VIEWS);

//dd(storage_path());
/*================

	BLADE SETTINGS (Change so Handlebars will still work)

================*/

Blade::setContentTags('<%', '%>'); 		// for variables and all things Blade
Blade::setEscapedContentTags('<%%', '%%>'); 	// for escaped data

/*======================

	OAUTH SERVER SIDE

======================*/
App::singleton('OAuthServer', function() {
	$dbname = Config::get('database.connections.mysql.database');
	$dbhost = Config::get('database.connections.mysql.host');
	$dbuser = Config::get('database.connections.mysql.username');
	$dbpass = Config::get('database.connections.mysql.password');

	$storage = new LaravelPdo\LaravelPdo(array(
			'dsn' => "mysql:dbname=$dbname;host$dbhost", 
			'username' => $dbuser, 
			'password' => $dbpass
	));

	$server = new OAuth2\Server($storage); //allows implicit auth type as well
	//set the scope
	$server->setScopeUtil(App::make('OAuthScope'));

	$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
	$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
	$server->addGrantType(new OAuth2\GrantType\UserCredentials($storage));
	$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage, array(
		'always_issue_new_refresh_token' => true,
	)));

	return $server;
});

//define the scopes of the application
App::singleton('OAuthScope', function() {
	// // configure your available scopes
	// $defaultScope = 'postonwall';
	$supportedScopes = array(
	  'basic',
	  'usercreate',
	  'userdelete',
	  'userview',
	);
	$memory = new OAuth2\Storage\Memory(array(
	  // 'default_scope' => $defaultScope,
	  'supported_scopes' => $supportedScopes
	));
	return new OAuth2\Scope($memory);
});

App::singleton('OAuthToken', function() {
	$token = App::make('OAuthServer')->getAccessTokenData(OAuth2\Request::createFromGlobals());

	//now we can grab the user if the token has any user data
	if($token['user_id']) {
        if( $user = User::find((int) $token['user_id']) ) {
            $token['user'] = $user;
        }
	}

	return $token;
});

/*======================

	OAUTH CLIENT SIDE

======================*/

//just a way to only have to create this in one place
App::singleton('OAuthClient', function() {
	return new OAuthClient(Config::get('oauthclient.id'), Config::get('oauthclient.secret'));
});

//the following simply grab the tokens
App::singleton('OAuthUserToken', function() {

	$client = App::make('OAuthClient');

	//We are assuming that the user access_token is no longer available and they are logging in for the first time
	if(Input::has('username') && Input::has('password') ) {
		$user = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
		);

		$response = $client->getAccessToken(Config::get('oauthclient.token_endpoint'), 'password', $user);
        
		if ( !empty($response['result']['error']) ) {
			return Response::json(array('messages' => array($response['result']['error_description'])), 422);
		}
        
        //set the usertoken in the session
        $token = $response['result'];
        $token['expires_in'] = $token['expires_in'] + time();
        Session::set('ember.usertoken', $token);
        
        //we need to set the current user and actually retrieve the user so here it goes
        $data = array('access_token' => $token['access_token'], 'with_permissions' => 'true');
        $response = $client->fetch(Config::get('oauthclient.base') . '/account', $data, 'GET');
        
        if($response['code'] === 200) {
            Session::set('ember.currentuser', $response['result']['user']);
        } else {
            return Response::json(array('messages' => array('unable to retrieve your account')), $response['code']);
        }

		return Response::json( $response['result']['user'] );

	} else {
		return Response::json(array('messages' => array('username and password required')), 422);
	}
});

App::singleton('OAuthRefreshToken', function() {
	$client = App::make('OAuthClient');

	if(Input::has('refresh_token')) {
		$refresh = array(
			'refresh_token' => Input::get('refresh_token'),
		);

		$response = $client->getAccessToken(Config::get('oauthclient.token_endpoint'), 'refresh_token', $refresh);

		return $response['result'];
	} else {
		return array('error' => array('refresh_token is required in the request'));
	}
});

App::singleton('OAuthClientToken', function() {
	$client = App::make('OAuthClient');
	$response = $client->getAccessToken(Config::get('oauthclient.token_endpoint'), 'client_credentials', array());
	if ( !empty($response['error']) ) {
		//TODO: better error handling
		return App::abort(404);
	}

	return $response['result'];
});

//make sure that every cart request that goes through the proxy is setup properly with the session
App::singleton('InitializeCart', function() {
    
    $client = App::make('OAuthClient');
    
    $requestUrl = Config::get('oauthclient.base') . '/carts';
    
    $token = App::make('OAuthToken'); //is the user logged in?
		
    if(!Session::has('ember.usertoken')) {
    
        if(!Cookie::get('cart_token')) {

            $data = Input::except('refresh_token');

            $requestUrl .= strpos($requestUrl, '?') === false ? '?' : '&';
            $requestUrl .= 'access_token=' . Input::get('access_token');

            if(array_key_exists('access_token', $data)) {
                unset($data['access_token']); //access_token can only be passed one way
            }

            //create a cart because it doesn't exist
            $response = $client->fetch($requestUrl, array(), 'POST');

//            dd($response['result']);

            Cookie::queue('cart_token', $response['result']['cart']['token'], 2628000); //because this is a new cart we know that it hasn't been paid yet

        } else {

            $currentCart = array();
            $data['cart_token'] = Cookie::get('cart_token');
            $data['access_token'] = Input::get('access_token');
            $response = $client->fetch($requestUrl, $data, 'GET');

//            dd($response);
            
            if(!isset($response['result']['cart']) || count($response['result']['cart']) == 0) {

                $data = Input::except('refresh_token');

                $requestUrl .= strpos($requestUrl, '?') === false ? '?' : '&';
                $requestUrl .= 'access_token=' . Input::get('access_token');

                if(array_key_exists('access_token', $data)) {
                    unset($data['access_token']); //access_token can only be passed one way
                }

                //create a cart because it doesn't exist
                $response = $client->fetch($requestUrl, array(), 'POST');

//                dd($response);

                Cookie::queue('cart_token', $response['result']['cart']['token'], 2628000);
            }

        }
        
        $currentCart = array();
        $currentCart['cart_token'] = Cookie::get('cart_token');

        Input::merge($currentCart); //because there is no user we know that they need to grab a cart by the id
        
    } else {
    
        $currentCart = array();
        $data['access_token'] = Input::get('access_token');
        
        if(Cookie::get('cart_token')) {
            $data['cart_token'] = Cookie::get('cart_token');
        }
        
//        dd($data);
        
        $response = $client->fetch($requestUrl, $data, 'GET');
        
//        dd($response);
        
        if(!isset($response['result']['cart']) || count($response['result']['cart']) == 0) {

            $data = Input::except('refresh_token');

            $requestUrl .= strpos($requestUrl, '?') === false ? '?' : '&';
            $requestUrl .= 'access_token=' . Input::get('access_token');

            if(array_key_exists('access_token', $data)) {
                unset($data['access_token']); //access_token can only be passed one way
            }

            //create a cart because it doesn't exist
            $response = $client->fetch($requestUrl, array(), 'POST');
        }
        
        if(Cookie::get('cart_token')) {
        
            Input::merge(array('cart_token' => Cookie::get('cart_token')));
            
        }
    
    }
    
});

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
