<?php

//NOTE: order of routes DOES matter as far as I can tell

/***************************************
    API Proxy (for requests from Emberjs)
****************************************/
Route::group(array('prefix' => 'api-proxy', 'before' => 'handletoken'), function() {
    
    //ember will actually go to this page to logout and reload
    Route::get('logout', function() {
        Session::forget('ember'); //remove all ember session data
        return Redirect::to('/');
    });
    
    Route::post('login', function() {
        return App::make('OAuthUserToken'); //it will try to set the token to a user_token
    });
    
    //every api resource request (besides token handling)
    Route::any('{object?}/{id?}', function($object = null, $id = null) {
        $requestObject = is_null($id) ? '/' . $object : '/' . $object . '/' . $id;

        $client = App::make('OAuthClient');
        
        //we need to intercept any attempt to the cartItems first
        if( $object == 'cartItems' || $object == 'carts' || $object == 'checkout' ) {
            App::make('InitializeCart');
        }
        
        //https://github.com/bshaffer/oauth2-server-php/issues/155
        $requestUrl = Config::get('oauthclient.base') . $requestObject;
        $data = Input::except('refresh_token');
        if(strtolower(Request::method()) !== 'get') {
            
            $requestUrl .= strpos($requestUrl, '?') === false ? '?' : '&';
            $requestUrl .= 'access_token=' . Input::get('access_token');
            
            if(array_key_exists('access_token', $data)) {
                unset($data['access_token']); //access_token can only be passed one way
            }
        }
		
//		dd($data);
        
        $response = $client->fetch($requestUrl, $data, Request::method());

        //to handle when a new user is created so we can store them in the session and log them in server side...
        if($object == 'users' && Request::isMethod('post') && $response['code'] === 200) {
//            Input::merge($data['user']); //ember sends user data
            App::make('OAuthUserToken'); //just to set the session of the current user
        }
		
//		if($response['code'] > 200) {
//			return $response['result'];
//		}
        
        return Response::json($response['result'], $response['code']);
	});
    
});

/***************************************
    Actual API
****************************************/
Route::group(array('prefix' => 'api'), function() {
    
    Route::post('token', function() {
        App::make('OAuthServer')->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    });
 
	/***************************************
    	Users Module
	 ****************************************/
	Route::resource('users', 'UserController');
	Route::resource('userPermissions', 'UserPermissionsController');
	Route::resource('userRoles', 'UserRolesController');
	Route::resource('userAddresses', 'UserAddressesController');
	Route::resource('userBillingInfos', 'UserBillingInfoController');
	
	/***************************************
    	Inventory Module
	 ****************************************/
	Route::resource('inventories', 'InventoryController');
	Route::resource('inventoryCategories', 'InventoryCategoryController');
	Route::resource('inventoryInstances', 'InventoryInstanceController');
	Route::resource('inventoryStatuses', 'InventoryStatusController');
	Route::resource('inventoryConditions', 'InventoryConditionController');
    
    Route::group(array('before' => 'oauth.clienttoken'), function() {
        
        Route::resource('users', 'UserController', array('only' => array('store')));
        Route::resource('inventories', 'InventoryController', array('only' => array('index', 'show')));
        Route::resource('inventoryCategories', 'InventoryCategoryController', array('only' => array('index', 'show')));
        Route::resource('inventoryInstances', 'InventoryInstanceController', array('only' => array('index', 'show')));
        Route::resource('inventoryStatuses', 'InventoryStatusController', array('only' => array('index', 'show')));
        Route::resource('inventoryConditions', 'InventoryConditionController', array('only' => array('index', 'show')));
        
        
        
        /***************************************
        Inventory Ordersrs / Restock Module
        ****************************************/
        Route::resource('inventoryOrders','InventoryOrderController');
        Route::resource('inventoryOrderVendors','InventoryOrderVendorsController');
        Route::resource('inventoryOrderItems','InventoryOrderItemsController');
        Route::resource('inventoryOrderStatuses','InventoryOrderStatusesController');
        //                  Name Below       Controller Name
        //               'ember model name','laravel controller'
        //Route::resource('tableNameThing','ThingNameController'); //Teplate blank
        
    });
        
    
    Route::resource('goods', 'GoodsController', array('only' => array('index', 'show')));
    Route::resource('goodCategories', 'GoodCategoryController', array('only' => array('index', 'show')));
    
    Route::resource('rentals', 'RentalsController', array('only' => array('index', 'show')));
    Route::resource('rentalCategories', 'RentalCategoryController', array('only' => array('index', 'show')));
    
    /***************************************
    	Events Module
	 ****************************************/
    Route::resource('events', 'EventsController');
    
    /***************************************
        Services Module
     ****************************************/
    Route::resource('services', 'ServicesController');
    Route::resource('servicesSettings', 'ServicesSettingsController'); 

    /***************************************
    	Cart Module
	 ****************************************/
	Route::resource('carts', 'CartController');
    Route::resource('cartItems', 'CartItemsController');
    Route::post('carts/checkout', 'CartController@checkout');
    
    /***************************************
    	Blog Module
	 ****************************************/
	Route::resource('blogs', 'BlogsController');
    
    /***************************************
    	Core Module
	 ****************************************/
    Route::resource('files', 'MediaController');
    Route::resource('slideshows', 'SlideshowController');
    Route::resource('slides', 'SlidesController');
    Route::resource('pages', 'PagesController');
    Route::resource('settings', 'SettingsController');
    
    /***************************************
    	Special Routes
	 ****************************************/
	
	Route::group(array('before' => 'oauth.clienttoken'), function() { 
		
		/* if you need to have a clienttoken at least */ 
		
	});
	
    Route::group(array('before' => 'oauth.usertoken'), function() {
        
        Route::any('account', function() {
            
            $token = App::make('OAuthToken');
            $userController = new UserController();
            
            switch( strtolower( Request::method() ) ) {
                case 'get': return $userController->show($token['user_id']);
                    break;
                case 'update': return $userController->update($token['user_id']);
                    break;
                case 'delete': return $userController->destroy($token['user_id']);
                    break;
                default: //method not allowed?
                    break;
            }
            
        });
		
	 });
    
});


/***************************************
    STATIC (Non-Ember handling) PAGES
****************************************/
    
    

/****************************************
    Ember handled pages. Note: Ember cannot have urls with [root]/api/, or [root]/api-proxy/
*****************************************/
Route::get('/{param1?}/{param2?}/{param3?}/{param4?}/{param5?}/{param6?}', function() {
    
    $view = View::make('ember::index');
    
    Helper::initializeView($view);
    
    return $view;
    
});
