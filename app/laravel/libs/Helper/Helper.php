<?php

namespace Helper;

class Helper {
    
    /**
     * Find the position of the Xth occurrence of a substring in a string
     * @param $haystack
     * @param $needle
     * @param $number integer > 0
     * @return int
     */
    public static function strposX($haystack, $needle, $x) {
        if($x == '1'){
            return strpos($haystack, $needle);
        } else if ($x > '1'){
            return strpos($haystack, $needle, self::strposX($haystack, $needle, $x - 1) + strlen($needle));
        } else {
            return -1;
        }
    }
    
    public static function initializeView(&$view) {
        
        if(\Session::has('ember.currentuser')) {
            $currentUser = \Session::get('ember.currentuser');
            $view->with('currentUser', json_encode($currentUser)); //all we need is the user and the id (to be able to access their account)
        }
        
//        $pages = \Page::where('is_core', '=', 0)->get();
//        $view->with('pages', $pages);
        
        $view->with('settings', self::getSettings());
        
        $iterator = new \DirectoryIterator(EMBER_VIEWS . '/views');
//        dd(self::getBlades($iterator));
        $view->with('includes', self::getBlades($iterator));
    }
    
    public static function getSettings() {
        $settings = \Setting::all();
        
        $objectSettings = new \stdClass();
        foreach($settings as $setting) {
            $objectSettings->{$setting->name} = empty($setting->string_value) ? $setting->number_value : $setting->string_value;
        }
        
        return $objectSettings;
    }
    
    public static function getBlades($iterator, $rootDirectory = '', $previousDirectory = '', $viewLocation = 'ember::views.', $views = array()) {
        
        $currentDirectory = substr(strrchr($iterator->getPath(), DIRECTORY_SEPARATOR ), 1);
        
        if($previousDirectory === '') {
            $rootDirectory = substr(strrchr($iterator->getPath(), DIRECTORY_SEPARATOR ), 1);
        }
        
        if($currentDirectory !== $rootDirectory) {
            $viewLocation .= $currentDirectory . '.';
        }
        
        foreach ($iterator as $fileinfo) {
            
            if($fileinfo->isDot()) continue;
            
            if($fileinfo->isFile() && $currentDirectory !== $rootDirectory) {
                //now check if it's a php file and a blade file to boot.
                if($fileinfo->getExtension() === 'php') {
                    
                    //we know it's a blade file
                    if(strpos($fileinfo->getFilename(), 'blade.php') !== false) {

                        $filename = explode('.', $fileinfo->getFilename());
                        $views[] = $viewLocation . $filename[0];

                    }
                    
                }
            }
            
            if($fileinfo->isDir()) {
                
                $new_iterator = new \DirectoryIterator($iterator->getPathname());
                $views = self::getBlades($new_iterator, $rootDirectory, $currentDirectory, $viewLocation, $views);
                
            }
            
        }
        
        return $views;
        
    }
    
}