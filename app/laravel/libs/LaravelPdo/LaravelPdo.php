<?php

namespace LaravelPdo;

use OAuth2\Storage\Pdo;
use Illuminate\Support\Facades\Hash;

//simply used to override the way that user credentials are checked (now we can do it the laravel way!)
class LaravelPdo extends Pdo {
	protected function checkPassword($user, $password)
    {
        return Hash::check($password, $user['password']);
    }

    /* OAuth2\Storage\ClientCredentialsInterface */
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        $stmt = $this->db->prepare(sprintf('SELECT * from %s where client_id = :client_id', $this->config['client_table']));
        $stmt->execute(compact('client_id'));
        $result = $stmt->fetch();

        // this way the client secret can be hashed too
        return $result && Hash::check($client_secret, $result['client_secret']);
    }

    public function getUser($username)
    {
        $stmt = $this->db->prepare($sql = sprintf('SELECT * from %s where username=:username', $this->config['user_table']));
        $stmt->execute(array('username' => $username));

        if (!$userInfo = $stmt->fetch()) {
            return false;
        }

        //overwritten so that user_id is set from ther oauth_users id instead of the default username
        return array_merge(array(
            'user_id' => $userInfo['id']
        ), $userInfo);
    }
}
