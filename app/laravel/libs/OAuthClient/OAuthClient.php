<?php

namespace OAuthClient;
use OAuth2Client\Client;

//extended because the client was throwing an error when trying to pass crap
class OAuthClient extends Client {
    public function fetch($protected_resource_url, $parameters = array(), $http_method = self::HTTP_METHOD_GET, array $http_headers = array(), $form_content_type = self::HTTP_FORM_CONTENT_TYPE_MULTIPART) {
		
		$data = null;
		
        $parameters = $this->http_build_query_for_curl($parameters, $data); //flatten the array with keys!
		
//		dd($data);
		
        return parent::fetch($protected_resource_url, $data, $http_method, $http_headers, $form_content_type);
    }

    //this function will flatten out the array with keys intact because emberjs passes objects. the original client can't handle that.
    protected function flatten(array $array) {
        $return = array();
        array_walk_recursive($array, function($a,$b) use (&$return) { $return[$b] = $a; });
        return $return;
    }
	
	//an improved way of flattening the responses
	protected function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null )
	{
	    if ( is_object( $arrays ) ) {
	        $arrays = get_object_vars( $arrays );
	    }

	    foreach ( $arrays as $key => $value ) {
	        $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
	        if ( is_array( $value ) OR is_object( $value )  ) {
	            $this->http_build_query_for_curl( $value, $new, $k );
	        } else {
	            $new[$k] = $value;
	        }
	    }
	}
}
