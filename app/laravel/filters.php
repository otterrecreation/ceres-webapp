<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

Route::filter('handletoken', function() {
    
    $access_token = '';
    
    if( Session::has('ember.usertoken') && Session::get('ember.usertoken.expires_in') > time() ) {
            
        $access_token = Session::get('ember.usertoken.access_token');
        
    } else if( Session::has('ember.usertoken.refresh_token') ) {
        
        //use the refresh token
        Input::merge( array('refresh_token' => Session::get('ember.usertoken.refresh_token')) );
        Session::forget('ember.usertoken'); //usertoken no longer good
        
        $token = App::make('OAuthRefreshToken');

        if( !array_key_exists('error', $token) ) {
            
            $token['expires_in'] = $token['expires_in'] + time();
            Session::set('ember.usertoken', $token);
            $access_token = $token['access_token'];
            
        } else {
            
            $token = App::make('OAuthClientToken');
            if(isset($token['expires_in'])) {
                $token['expires_in'] = $token['expires_in'] + time();
                Session::set('ember.clienttoken', $token);
                $access_token = $token['access_token'];
            }
            
        }
        
    } else if( Session::has('ember.clienttoken') && Session::get('ember.clienttoken.expires_in') > time() ) {
            
        $access_token = Session::get('ember.clienttoken.access_token');
    
    } else {
        
        $token = App::make('OAuthClientToken');
        if(isset($token['expires_in'])) {
            $token['expires_in'] = $token['expires_in'] + time();
            Session::set('ember.clienttoken', $token); //client token needs to be reset
            $access_token = $token['access_token'];
        }
    
    }
    
    if( !isset($access_token) ) {
        return Response::json(array('messages' => array('api requests must pass a valid access_token')), 499);
    }
    
    Input::merge(array('access_token' => $access_token)); //insert the access_token into the request
    
});

//VERIFY THAT THE TOKEN HAS A USER IN IT
Route::filter('oauth.usertoken', function() {
	$bridgedRequest  = OAuth2\HttpFoundationBridge\Request::createFromRequest(Request::instance());
	$bridgedResponse = new OAuth2\HttpFoundationBridge\Response();

	$server = App::make('OAuthServer');

	if( !$server->verifyResourceRequest($bridgedRequest, $bridgedResponse) ) {
//        dd($bridgedResponse);
        return Response::json(array('messages' => array('Invalid token passed')), 498);
	}

	$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());
    
    if(is_null($token['user_id'])) {
        return Response::json(array('messages' => array('No user associated with request')), 401); //keep in mind this isn't exactly used per the specs (no WWW-Authenticate header is sent in this response)   
    }
});

//VERIFY THAT THE TOKEN IS VALID
Route::filter('oauth.clienttoken', function() {
    
	$bridgedRequest  = OAuth2\HttpFoundationBridge\Request::createFromRequest(Request::instance());
	$bridgedResponse = new OAuth2\HttpFoundationBridge\Response();
    
	$server = App::make('OAuthServer');

	if( !$server->verifyResourceRequest($bridgedRequest, $bridgedResponse) ) {
        return Response::json(array('messages' => array('Invalid token passed')), 498);
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
