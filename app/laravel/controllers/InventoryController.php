<?php

class InventoryController extends BaseController {

    public function index() {
        
        if( ($currentUser = $this->getCurrentUser()) ) {
            //check for permissions of the user with that
            if($currentUser->can('view_admin')) {
                $inventory = Inventory::all();
            } else {
                $inventory = Inventory::where('publicVisible', '=', 1)->get();
            }
        } else {
            $inventory = Inventory::where('publicVisible', '=', 1)->get();
        }
        
        foreach($inventory as &$individualInventory) {
            $this->bindHasManyRelationship('inventoryInstance', $individualInventory);
        }
        
        return Response::json(array('inventory' => $inventory));
        
    }
    
    public function show($id) {
        
        if(($currentUser = $this->getCurrentUser())) {
            //check for permissions of the user with that
            if($currentUser->can('view_admin')) {
                $inventory = Inventory::find($id);
            } else {
                $inventory = Inventory::find($id)->where('publicVisible', '=', 1)->get();
            }
        } else {
            $inventory = Inventory::find($id)->where('publicVisible', '=', 1)->get();
        }
        
        $this->bindHasManyRelationship('inventoryInstance', $inventory);
        
        return Response::json(array('inventory' => $inventory));
    }
    
    public function store() {
        
        $validator = Validator::make(Input::all(), Inventory::$rules);
        
        if($validator->passes()) {
            
            $inventory                      = new Inventory();
            $inventory->name                = Input::get('name');
            $inventory->description         = Input::get('description');
            $inventory->price               = Input::get('price');
            $inventory->type                = Input::get('type');
            if(Input::has('file')) {
                $inventory->file = Input::get('file');
            }
            
            if($inventory->type === 'rental') {
                $inventory->pricePer = Input::get('pricePer');
            }
            
            if($inventory->type === 'good') {
                $inventory->count = Input::get('count', 0);
            } 
            
            $inventory->alertAt             = Input::get('alertAt', 0);
            $inventory->inventoryCategory   = Input::get('inventoryCategory');
            $inventory->publicVisible       = Input::get('publicVisible');
            
            $inventory->save();
            
            if($inventory->type !== 'good') {
                $this->createInstances($inventory->id, Input::get('count', 0));
            }
            
            $this->bindHasManyRelationship('inventoryInstance', $inventory);
            
            return Response::json(array('inventory' => $inventory));
            
        } else {
            return Response::json(array('messages' => 'check your input'), 422);
        }
        
    }
    
    public function update($id) {
        
        //TODO: make requirements more stringent
        $rules = array(
            'name' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if($validator->passes()) {
            
            $inventory                      = Inventory::find($id);
            $inventory->name                = Input::get('name');
            $inventory->description         = Input::get('description');
            $inventory->price               = Input::get('price');
            
            if(Input::has('file')) {
                $inventory->file = Input::get('file');
            }
            
            if($inventory->type === 'rental') {
                $inventory->pricePer = Input::get('pricePer');
            }
            
            if($inventory->type === 'good') {
                $inventory->count               = Input::get('count', 0);
            }
            
            $inventory->alertAt             = Input::get('alertAt', 0);
            $inventory->inventoryCategory   = Input::get('inventoryCategory');
            $inventory->publicVisible       = Input::get('publicVisible');
            
            $inventory->save();
            
            $this->bindHasManyRelationship('inventoryInstance', $inventory);
            
            return Response::json(array('inventory' => $inventory));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
    }
    
    public function destroy($id) {
        $inventory = Inventory::find($id);
        $inventory->delete();
        
        return Response::json(array('messages' => 'inventory item removed successfully'));
    }
    
    protected function createInstances($inventoryId, $count) {
        $now = Carbon::now('utc')->toDateTimeString();
                
        $instancesToInsert = array();
        for($i = 0; $i < $count; $i++) {
            $instancesToInsert[] = array(
                'inventory' => $inventoryId, 
                'inventoryStatus' => 1, 
                'inventoryCondition' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            );
        }

        if(count($instancesToInsert) > 0) {
            InventoryInstance::insert($instancesToInsert);
        }
    }

}