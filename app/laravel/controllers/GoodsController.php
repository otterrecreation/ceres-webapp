<?php

class GoodsController extends BaseController {

    public function index() {
        
        if(Input::has('random') && Input::get('random')) {
            $goods = Inventory::where('publicVisible', '=', 1)->where('type', '=', 'good')->whereNotNull('file')->orderBy(DB::raw('RAND()'))->get();
        } else {
            $goods = Inventory::where('publicVisible', '=', 1)
                              ->where('type', '=', 'good')
                              ->get();
        }

        
        foreach($goods as &$good) {
            $good->goodCategory = $good->inventoryCategory;
            unset($good->inventoryCategory);
        }
        
        return Response::json(array('good' => $goods));
        
    }
    
    public function show($id) {
        
        if(is_numeric($id)) {
            $good = Inventory::find($id)->where('publicVisible', '=', 1)->where('type', '=', 'good')->first();
        } else {
            $good = Inventory::where('publicVisible', '=', 1)->where('type', '=', 'good')->whereNotNull('file')->orderBy(DB::raw('RAND()'))->first();
        }
        
        if($good) {
            $good->goodCategory = $good->inventoryCategory;
            unset($good->inventoryCategory);
        }
        
        return Response::json(array('good' => $good));
    }

}