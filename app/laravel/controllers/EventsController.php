<?php

class EventsController extends BaseController {
    public function __construct() {
        $this->beforeFilter('oauth.clienttoken', array('except' => array('store', 'update', 'destroy')));
        $this->beforeFilter('oauth.usertoken', array('only' => array('store', 'update', 'destroy')));
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$events = EventModel::all();
        return Response::json(array('event' => $events));
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

        //SANITIZE DATES BEFORE VALIDATION
        Input::merge( array('start' => date( 'Y-m-d h:i:s', strtotime( Input::get('start') ) ) ) );
        Input::merge( array('end' => date( 'Y-m-d h:i:s', strtotime( Input::get('end') ) ) ) );
        Input::merge( array('registrationDeadline' => date( 'Y-m-d h:i:s', strtotime( Input::get('registrationDeadline') ) ) ) );
        Input::merge( array('publicAt' => date( 'Y-m-d h:i:s', strtotime( Input::get('publicAt') ) ) ) );

		$validator = Validator::make(Input::all(), EventModel::$rules);

        if($validator->passes()) {
            $event                          = new EventModel;
            $event->alertAt                 = Input::get('alertAt');
            $event->capacity                = Input::get('capacity');
            $event->description             = Input::get('description');
            $event->end                     = Input::get('end');
            $event->location                = Input::get('location');
            $event->name                    = Input::get('name');
            $event->price                   = Input::get('price');
            $event->publicAt                = Input::get('publicAt');
            $event->publicVisible           = Input::get('publicVisible');
            $event->registrationDeadline    = Input::get('registrationDeadline');
            $event->start                   = Input::get('start');
            
            if(Input::has('file')) {
                $event->file = Input::get('file');
            }
            
            $event->save();
            return Response::json(array('event' => $event));
        } else {
            return Response::json($validator->messages(), 422);
        }
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        if(is_numeric($id)) {
            $event = EventModel::find($id);
        } else {
            $event = null;
        }
        return Response::json(array('event' => $event));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {

        //SANITIZE DATES BEFORE VALIDATION
        Input::merge( array('start' => date( 'Y-m-d h:i:s', strtotime( Input::get('start') ) ) ) );
        Input::merge( array('end' => date( 'Y-m-d h:i:s', strtotime( Input::get('end') ) ) ) );
        Input::merge( array('registrationDeadline' => date( 'Y-m-d h:i:s', strtotime( Input::get('registrationDeadline') ) ) ) );
        Input::merge( array('publicAt' => date( 'Y-m-d h:i:s', strtotime( Input::get('publicAt') ) ) ) );
        
        $validator = Validator::make(Input::all(), EventModel::$rules);

        if($validator->passes()) {
            $event = EventModel::find($id);
            $event->alertAt                 = Input::get('alertAt');
            $event->capacity                = Input::get('capacity');
            $event->description             = Input::get('description');
            $event->end                     = date('Y-m-d h:i:s', strtotime(Input::get('end')));
            $event->location                = Input::get('location');
            $event->name                    = Input::get('name');
            $event->price                   = Input::get('price');
            $event->publicAt                = date('Y-m-d h:i:s', strtotime(Input::get('publicAt')));
            $event->publicVisible           = Input::get('publicVisible');
            $event->registrationDeadline    = date('Y-m-d h:i:s', strtotime(Input::get('registrationDeadline')));
            $event->start                   = date('Y-m-d h:i:s', strtotime(Input::get('start')));
            
            if(Input::has('file')) {
                $event->file = Input::get('file');
            }
            
            $event->save();
            return Response::json(array('event' => $event));
        } else {
            return Response::json($validator->messages(), 422);
        }
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$event = EventModel::find($id);
        $event->delete();
        return Response::json(array('messages' => array('Event has been removed')));
	}
}
