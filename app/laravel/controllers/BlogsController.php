<?php

class BlogsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
        $blogs = Blog::orderBy('created_at', 'DESC')->get(); //we don't want to pull the default pages because those are reserved for the custom modules
        foreach($blogs as $key => &$blog) {
            $blog->previousBlog = isset($blogs[$key + 1]) ? $blogs[$key + 1]->id : null;
            $blog->nextBlog = isset($blogs[$key - 1]) ? $blogs[$key - 1]->id : null;
        }
        
        return Response::json(array('blog' => $blogs));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//these need to be initialized with seeds because we aren't letting users create new pages
        $validator = Validator::make(Input::all(), Blog::$rules);
        
        if($validator->passes()) {
            
            $blog = new Blog;
            $blog->title = Input::get('title', '');
             //the url that the page will use...
            $blog->is_active = Input::get('is_active');
            $blog->page_content = Input::get('page_content', ''); //right now this is somewhat dangerous but we'll just assume that the content is ok and non-melicous...
            $blog->author = $this->getCurrentUser()->id;
            $blog->last_modified_by = $this->getCurrentUser()->id;
            $blog->save();
            
            $blog->slug = $blog->id . '-' . preg_replace('/[^a-z0-9]+/', '-', strtolower($blog->title));
            
            $blog->save();
            
            $this->getPreviousBlog($blog);
            $this->getNextBlog($blog);
            
            return Response::json(array('blog' => $blog));
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $blog = Blog::find($id);
        
        $this->getPreviousBlog($blog);
        $this->getNextBlog($blog);
        
        return Response::json(array('blog' => $blog));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), Blog::$rules);
        
        if($validator->passes()) {
            
            $blog = Blog::find($id);
            $blog->title = Input::get('title', 'untitled');
            $blog->is_active = Input::get('is_active');
            $blog->page_content = Input::get('page_content'); //right now this is somewhat dangerous but we'll just assume that the content is ok and non-melicous...
            $blog->last_modified_by = $this->getCurrentUser()->id;
            $blog->save();
            
            $this->getPreviousBlog($blog);
            $this->getNextBlog($blog);
            
            return Response::json(array('blog' => $blog));
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $blog = Blog::find($id);
        $blog->delete();
        
        return Response::json(array('messages' => array('blog deleted successfully')));
	}
    
    protected function getNextBlog(&$blog) {
        
        $nextBlog = Blog::where('created_at', '>', $blog->created_at )->orderBy('created_at', 'ASC')->first();
        $blog->nextBlog = $nextBlog ? $nextBlog->id : null;
        
    }
    
    protected function getPreviousBlog(&$blog) {
        
        $previousBlog = Blog::where('created_at', '<', $blog->created_at )->orderBy('created_at', 'DESC')->first();
        $blog->previousBlog = $previousBlog ? $previousBlog->id : null;
        
    }

}
