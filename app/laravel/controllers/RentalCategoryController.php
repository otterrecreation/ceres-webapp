<?php

class RentalCategoryController extends BaseController {

    public function index() {
        
        $inventoryCategories = InventoryCategory::all();
        
        return Response::json(array('rentalCategory' => $inventoryCategories));
    }
    
    public function show($id) {
        $category = InventoryCategory::find($id);
        
        return Response::json(array('rentalCategory' => $category));
    }

}