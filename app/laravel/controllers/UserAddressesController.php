<?php

class UserAddressesController extends BaseController {

	protected $user_token;
	
	public function __construct() {
		$this->beforeFilter('oauth.usertoken'); //all requests must be on behalf of an authenticated user
		$this->user_token = App::make('OAuthToken'); //after the filter has been verified. set the current user...
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//SHOULD ONLY RETURN INFO FOR A GIVEN USER...
		$addresses = UserAddress::where('user', '=', $this->user_token['user_id'])->get();
		
		return Response::json(array('userAddress' => $addresses));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), UserAddress::$rules);
		
		if($validator->passes()) {
			
			$address = new UserAddress();
			$address->user = $this->user_token['user_id'];
			$address->recipient_name = Input::get('recipient_name');
			$address->address_line_1 = Input::get('address_line_1');
			$address->address_line_2 = Input::get('address_line_2');
			$address->city = Input::get('city');
			$address->state_province = Input::get('state_province');
			$address->zip = Input::get('zip');
			$address->country = Input::get('country');
			
			$address->save();
			
			return Response::json(array('userAddress' => $address));
			
		} else {
			return Response::json($validator->messages(), 422);
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//SHOULD MAKE SURE THAT THE USER IS EITHER THE RIGHT USER OR HAS PROPER PERMISSIONS
		$address = UserAddress::find($id);
		
		return Response::json(array('userAddress' => $address));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(), UserAddress::$rules);
		
		if($validator->passes()) {
			
			$address = UserAddress::find($id);
			$address->recipient_name = Input::get('recipient_name');
			$address->address_line_1 = Input::get('address_line_1');
			$address->address_line_2 = Input::get('address_line_2');
			$address->city = Input::get('city');
			$address->state_province = Input::get('state_province');
			$address->zip = Input::get('zip');
			$address->country = Input::get('country');
			
			$address->save();
			
			return Response::json(array('userAddress' => $address));
			
		} else {
			return Response::json($validator->messages(), 422);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$address = UserAddress::find($id);
		$address->delete();
		
		return Response::json(array('messages' => 'removed address successfully'));
	}


}
