<?php

class InventoryOrderStatusesController extends BaseController {

    public function index() {   // get all vendors Plural
        
        $statuses = InventoryOrderStatus::all ();// get all vendors from DB to var $vendors
        return Response::json(array('inventoryOrderStatus' => $statuses));
    }
    
    
    public function show($id) { // get instance of vedor Singular
        
        $status = InventoryOrderStatus::find ($id);// get instance vendor from DB to var $vendors
        return Response::json(array('inventoryOrderStatus' => $status));
    }
    
    
    public function store() {   // creat instance of vendor
        
        $status = new InventoryOrderStatus;
        
        $status->status    = Input::get('status');
        
        $status-> save(); // save above data to DB
    
        return Response::json(array('inventoryOrderStatus' => $status));
    }
    
    
    public function update($id) {   // update DB

        $status =  InventoryOrderStatus::find($id);
        
        $status->status = Input::get('status');
        
        $status->save(); // save above data to DB
    
        return Response::json(array('inventoryOrderStatus' => $status));
    }
    
    
    public function destroy($id) { // remove/delet from DB, could be hard or soft delete.
        
        $status =  InventoryOrderStatus::find($id);
        
        $status->delete();
    }

}