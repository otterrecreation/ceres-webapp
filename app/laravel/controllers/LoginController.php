<?php

class LoginController extends BaseController {
	//so we can register filters for parts of the controller
	public function __construct() {
		//check the form
		$this->beforeFilter('csrf', array('only' => 'postIndex'));
	}

	//requested the login form
	public function getIndex() {
		//Note:: just expimenting with a way to do HMVC! Seems to work well! (need to check about the filters it calls though...)
		if(Auth::check()) {
            return Redirect::to('/');
        } else {
            return View::make('users.create');
        }
	}

	//submitted the login form
	public function postIndex() {
        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))) {
            return Redirect::to('/')->with('message', 'You are now logged in!');
        } else {
            return Redirect::to('login')
                ->with('message', 'Your username/password combination was incorrect')
                ->withInput();
        }
	}
}