<?php

class InventoryInstanceController extends BaseController {

    public function index() {
        
        $inventoryInstances = InventoryInstance::all();
        
        return Response::json(array('inventoryInstance' => $inventoryInstances));
    }
    
    public function show($id) {
        $inventoryInstance = InventoryInstance::find($id);
        
        return Response::json(array('inventoryInstance' => $inventoryInstance));
    }
    
    public function store() {
        $inventoryInstance = new InventoryInstance();
        $inventoryInstance->inventory = Input::get('inventory');
        $inventoryInstance->inventoryStatus = 1;
        $inventoryInstance->inventoryCondition = 1;
        $inventoryInstance->save();
        
        return Response::json(array('inventoryInstance' => $inventoryInstance));
    }
    
    public function update($id) {
        $inventoryInstance = InventoryInstance::find($id);
        $inventoryInstance->inventoryStatus = Input::get('inventoryStatus');
        $inventoryInstance->inventoryCondition = Input::get('inventoryCondition');
        $inventoryInstance->save();
        
        return Response::json(array('inventoryInstance' => $inventoryInstance));
    }
    
    public function destroy($id) {
        $inventoryInstance = InventoryInstance::find($id);
        $inventoryInstance->delete();
        
        return Response::json(array('messages' => 'category removed successfully'));
    }

}