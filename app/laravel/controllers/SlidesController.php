<?php

class SlidesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $slide = Slide::all();
        
        return Response::json(array('slide' => $slide));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validator = Validator::make(Input::all(), Slide::$rules);
        
        if($validator->passes()) {
            
            $slide = new Slide;
            $slide->title = Input::get('title');
            $slide->caption = Input::get('caption');
            $slide->file = Input::get('file');
            $slide->slideshow = Input::get('slideshow');
            $slide->save();
            
            return Response::json(array('slide' => $slide));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $slide = Slide::find($id);
        
        return Response::json(array('slide' => $slide));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'title' => 'required',
            'file' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if($validator->passes()) {
            
            $slide = Slide::find($id);
            $slide->title = Input::get('title');
            $slide->file = Input::get('file');
            $slide->caption = Input::get('caption');
            $slide->save();
            
            return Response::json(array('slide' => $slide));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$slide = Slide::find($id);
        $slide->delete();
        
        return Response::json(array('messages' => array('item removed successfully')));
	}


}
