<?php

class ServicesController extends BaseController {

    public function index() {

	$token = App::make('OAuthToken');
        
        if(isset($token['user_id'])) {
            //check for permissions of the user with that
            $currentUser = User::find($token['user_id']);
            if($currentUser->can('view_admin')) {
                $services = Services::all();
            } else {
                $services = Services::where('publicVisible', '=', 1)->get();
            }
        } else {
            $services = Services::where('publicVisible', '=', 1)->get();
        }
        
        return Response::json(array('service' => $services));
        
    }
    
    public function show($id) {
        $token = App::make('OAuthToken');
        
        if(isset($token['user_id'])) {
            //check for permissions of the user with that
            $currentUser = User::find($token['user_id']);
            if($currentUser->can('view_admin')) {
                $services = Services::find($id);
            } else {
                $services = Services::find($id)->where('publicVisible', '=', 1)->get();
            }
        } else {
            $services = Services::find($id)->where('publicVisible', '=', 1)->get();
        }
        
        return Response::json(array('service' => $services));

    }
    public function store() {
        
        $validator = Validator::make(Input::all(), Services::$rules);
        
        if($validator->passes()) {
            
            $services                      = new Services();
            $services->name                = Input::get('name');
            $services->price               = Input::get('price');
            $services->description         = Input::get('description');
            $services->hours               = Input::get('hours');
            $services->publicVisible       = Input::get('publicVisible');
            
            $services->save();
            
            return Response::json(array('service' => $services));
            
        } else {
            return Response::json(array('messages' => 'check your input'), 422);
        }
        
    }
    
    public function update($id) {
        
        $rules = array(
            'name' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if($validator->passes()) {
            
            $services                      = Services::find($id);
            $services->name                = Input::get('name');
            $services->description         = Input::get('description');
            $services->price               = Input::get('price');
            $services->hours               = Input::get('hours');
            $services->publicVisible       = Input::get('publicVisible');
            
            $services->save();
            
            return Response::json(array('service' => $services));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
    }
    
    public function destroy($id) {
        $services = Services::find($id);
        $services->delete();
        
        return Response::json(array('messages' => 'services removed successfully'));
    }

}
?>	