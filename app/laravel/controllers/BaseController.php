<?php

class BaseController extends Controller {
	
	protected $currentUser;
	
	public function __construct() {
		
		$this->beforeFilter('oauth.clienttoken', array('only' => array('index', 'show')));
		$this->beforeFilter('oauth.usertoken', array('only' => array('store', 'update', 'destroy')));
		
		$token = App::make('OAuthToken');
		
		if(isset($token['user_id'])) {
			$this->currentUser = User::find($token['user_id']);
		}
	}
	
	//allows us to retrieve the current user instead of having to call this kind of logic over and over
	protected function getCurrentUser() {
		
		if(isset($this->currentUser)) {
			return $this->currentUser;
		}
		
		$token = App::make('OAuthToken');
		
		if(isset($token['user_id'])) {
			return User::find($token['user_id']);
		}
		
		return false;
		
	}
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    
    //a generic way to bind a has many relationship with the parents ids of the children (the ember data way)
    protected function bindHasManyRelationship($hasMany, &$object) {
        $ids = array();
        
        foreach($object->{$hasMany} as $objectToBind) {
            $ids[] = $objectToBind->id;
        }
        
        unset($object->{$hasMany});
        $object->{$hasMany} = $ids;
    }
}

