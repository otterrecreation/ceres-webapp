<?php

class SettingsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{   
        $settings = Setting::all();
        
        return Response::json(array('setting' => $settings));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//for now we don't need this because they should seeded in the beginning
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $setting = Setting::find($id);
        
        return Response::json(array('setting' => $setting));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//this is where most of the interaction with settings will exist
        $validator = Validator::make(Input::all(), Setting::$rules);
        
        if($validator->passes()) {
        
            $setting = Setting::find($id);
            $setting->string_value = Input::get('string_value');
            $setting->number_value = Input::get('number_value');
            $setting->save();
            
            return Response::json(array('setting' => $setting));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
