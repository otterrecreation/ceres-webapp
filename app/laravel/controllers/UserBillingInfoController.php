<?php

class UserBillingInfoController extends BaseController {
	
	public function __construct() {
	
		$this->beforeFilter('oauth.usertoken');
		//set the api key for subsequent calls to this controller
		\Stripe\Stripe::setApiKey( Config::get('stripe.secret_key') );
		
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$billingInfo = UserBillingInfo::all(); //should we limit what can be queried here?
		
		return Response::json(array('userBillingInfo' => $billingInfo));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Input::has('stripeToken')) 
		{
			//1. we need to grab the current user
			$user = $this->getCurrentUser(); //in the future we can grab whatever user as long as the admin has permissions to do so...
			
			//2. check that the user isn't already a stripe customer
			if(strlen($user->stripe_customer_id)) 
			{
				$customer = \Stripe\Customer::retrieve($user->stripe_customer_id); //attempt to retrieve stipe customer
				$newCardInfo = $customer->sources->create( array( 'card' => Input::get('stripeToken') ) );
			}
			else
			{
				$customer = \Stripe\Customer::create( array(
			  		'email' => $user->username,
			  		'description' => $user->name . ' added their card from Ceres',
			  		'card' => Input::get('stripeToken'), // obtained with Stripe.js
				) );
				
				$user->stripe_customer_id = $customer->id;
				$user->save(); //set the customer id on the user because it hasn't been done before
				
				$newCardInfo = $customer->sources->retrieve($customer->default_source);
			}
			
			if( strlen($newCardInfo->id) ) 
			{
				$billingInfo 				 = new UserBillingInfo();
				$billingInfo->user			 = $user->id;
				$billingInfo->name			 = $newCardInfo->name;
				$billingInfo->stripe_card_id = $newCardInfo->id; //$table->string('stripe_card_id');
				$billingInfo->last4 		 = $newCardInfo->last4; //$table->integer('last4');
				$billingInfo->brand 		 = $newCardInfo->brand; //$table->string('brand');
				$billingInfo->funding 		 = $newCardInfo->funding; //$table->string('funding');
				$billingInfo->exp_month 	 = $newCardInfo->exp_month; //$table->integer('exp_month');
				$billingInfo->exp_year 		 = $newCardInfo->exp_year; //$table->integer('exp_year');
				
				$billingInfo->save();
				
				return Response::json(array('userBillingInfo' => $billingInfo));
			} 
			else 
			{
				return Response::json(array('messages' => 'unfortunately the card could not be created. please try again'), 422);
			}
			
		} 
		else 
		{
			return Response::json(array('messages' => 'you must pass a stripe token'), 422);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//SHOULD MAKE SURE THAT THE USER IS EITHER THE RIGHT USER OR HAS PROPER PERMISSIONS?
		$billingInfo = UserBillingInfo::find($id);
		
		return Response::json(array('userBillingInfo' => $billingInfo));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'name' => 'required',
			'exp_month' => 'required',
			'exp_year' => 'required',
		);
		
		$validator = Validator::make(Input::all(), $rules);
		
		if($validator->passes()) {
			
			$billinginfo = UserBillingInfo::find($id);
			$user = $this->getCurrentUser();
		
//			dd(Input::all());

//			dd($user->stripe_customer_id);

			$customer = \Stripe\Customer::retrieve($user->stripe_customer_id);
			$card = $customer->sources->retrieve($billinginfo->stripe_card_id);

	//		dd($card);

			$card->name 	 = Input::get('name');
			$card->exp_month = Input::get('exp_month');
			$card->exp_year  = Input::get('exp_year');
			$card->save();

			$billinginfo->name 		= Input::get('name');
			$billinginfo->exp_month = Input::get('exp_month');
			$billinginfo->exp_year 	= Input::get('exp_year');
			$billinginfo->save();
		
			return Response::json(array('userBillingInfo' => $billinginfo));
			
		} else {
			return Response::json($validator->messages, 422);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
