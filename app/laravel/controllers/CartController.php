<?php

class CartController extends BaseController {

    public function __construct() {
    
        $this->beforeFilter('oauth.clienttoken');
    
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        
        if($this->getCurrentUser()) {
            
            $cart = Cart::where('user', '=', $this->getCurrentUser()->id)
                        ->first();
            
            //first...let's see if 
            if(Input::has('cart_token') && $cart) {

                $cartItems = Cart::where('token', '=', Input::get('cart_token'))
                                 ->where('user', '=', null)
                                 ->first()->cartItems;
                
//                return Response::json(array($cartItems));
                
                if($cartItems) {
                    //now bind the cartitems to the newly logged in user...
                    foreach($cartItems as $cartItem) {
                        $cartItem->cart = $cart->id;
                        $cartItem->save();
                    }
                }

            }
            
            if($cart) {
                $carts = array($cart);
            } else {
                $carts = array();
            }
        } else {
            $carts = Cart::where('token', '=', Input::get('cart_token'))
                         ->where('user', '=', null)
                         ->get();
        }
        
        foreach($carts as &$cart) {
            
            if(!$cart->user && $this->getCurrentUser()) {
                $cart->user = $this->getCurrentUser()->id;
                $cart->save(); //update it to have a user associated with it
            }
            
            $this->bindHasManyRelationship('cartItems', $cart);
        }
        
        
        return Response::json(array('cart' => $carts));
        
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$cart = new Cart();
        
        if( ($currentUser = $this->getCurrentUser()) ) {
            $cart->user = $currentUser->id;
        }
        
        $cart->save();
        
        if(!$this->getCurrentUser()) {
        
            //we know that a non-authenticated user is requesting to make a cart so we'll give them a 'single use token'
            $cart->token = Hash::make($cart->id);
            $cart->save();
            
        }
        
        $this->bindHasManyRelationship('cartItems', $cart);
        
        return Response::json(array('cart' => $cart));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$cart = Cart::find($id);
        
        $this->bindHasManyRelationship('cartItems', $cart);
        
        return Response::json(array('cart' => $cart));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
	}
    
    public function checkout() 
    {
        return Response::json(array('messages' => array('successful!')));
    }


}
