<?php

class InventoryStatusController extends BaseController {

    public function index() {
        
        $inventoryStatus = InventoryStatus::all();
        
        return Response::json(array('inventoryStatus' => $inventoryStatus));
    }
    
    public function show($id) {
        $inventoryStatus = InventoryStatus::find($id);
        
        return Response::json(array('inventoryStatus' => $inventoryStatus));
    }
    
    public function store() {
        $inventoryStatus = new InventoryStatus();
        $inventoryStatus->status = Input::get('status');
        $inventoryStatus->save();
        
        return Response::json(array('inventoryStatus' => $inventoryStatus));
    }
    
    public function update($id) {
        $inventoryStatus = InventoryStatus::find($id);
        $inventoryStatus->status = Input::get('status');
        $inventoryStatus->save();
        
        return Response::json(array('inventoryStatus' => $inventoryStatus));
    }
    
    public function destroy($id) {
        $inventoryStatus = InventoryStatus::find($id);
        $inventoryStatus->delete();
        
        return Response::json(array('messages' => 'condition removed successfully'));
    }

}