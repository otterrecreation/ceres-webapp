<?php

class PagesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $pages = Page::all(); //we don't want to pull the default pages because those are reserved for the custom modules
        return Response::json(array('page' => $pages));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//these need to be initialized with seeds because we aren't letting users create new pages
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        if(Input::has('slug')) {
            $page = Page::where('slug', '=', Input::get('slug'))->first();
        } else {
            $page = Page::find($id);
        }
        
        return Response::json(array('page' => $page));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
        		//
        $validator = Validator::make(Input::all(), Page::$rules);
        
        if($validator->passes()) {
            
            $page = Page::find($id);
            $page->title = Input::get('title');
            $page->show_title = Input::get('show_title');
            $page->slug = preg_replace('/[^a-z0-9]+/', '-', strtolower($page->title)); //the url that the page will use...
            $page->is_active = Input::get('is_active');
            
            if(Input::has('slideshow')) {
                $page->slideshow = Input::get('slideshow');
            } else {
                $page->slideshow = null;
            }
            
            $page->page_content = Input::get('page_content'); //right now this is somewhat dangerous but we'll just assume that the content is ok and non-melicous...
            $page->save();
            
            return Response::json(array('page' => $page));
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
