<?php

class PermissionController extends BaseController {

	public function getIndex() {
		//let's grab all of the roles and their permissions
		$roles = Role::all();

		//let's grab the roles and set them
		foreach ($roles as $key => $role) {
			$roles[$key]->perms = $role->perms;
			//now remove the pivot section since it's pointless
			foreach($roles[$key]->perms as $k => $v) {
				unset($roles[$key]->perms[$k]->pivot);
			}
		}

		return Message::make($roles);
	}

	/*
		Here's how the input should be formatted:
		roles[role_id][permission_id] //the first index is going to be the id of the role. The second will be the id of the permission. 1 means it will be turned out. And 0 means it will not.
	 */
	public function postIndex() {
		//let's update all of the roles that are defined
		if(Input::has('roles')) {
			// dd(Input::get('roles'));
			foreach(Input::get('roles') as $role_id => $permissions) {
				if( $role = Role::find($role_id) ) {
					$permission_ids = array(); //so we don't get an error
					foreach($permissions as $permission_id => $permission) {
						if( Permission::find($permission_id) && (int)$permission ) {
							$permission_ids[] = $permission_id;
						}
					}

					if( count($permission_ids) > 0 ) {
						$role->perms()->sync($permission_ids);
					} else {
						$role->perms()->sync(array()); //they want to remove the permissions
					}
					$role->save();
				} else {
					return Message::make('Can\'t find role');
				}
			}
			return $this->getIndex();
		} else {
			return Message::make('Input must have roles passed to it');
		}
	}
}