<?php

class RentalsController extends BaseController {

    public function index() {
        
        if(Input::has('random') && Input::get('random')) {
            
            $goods = Inventory::where('publicVisible', '=', 1)->where('type', '=', 'rental')->whereNotNull('file')->orderBy(DB::raw('RAND()'))->get();
            
        } else {
            $goods = Inventory::where('publicVisible', '=', 1)
                              ->where('type', '=', 'rental')
                              ->get();
        }
        
        foreach($goods as &$good) {
            $good->rentalCategory = $good->inventoryCategory;
            unset($good->inventoryCategory);
        }
        
        return Response::json(array('rental' => $goods));
        
    }
    
    public function show($id) {
        
        $good = Inventory::find($id)->where('publicVisible', '=', 1)->where('type', '=', 'rental')->first();
        
        if($good) {
            $good->rentalCategory = $good->inventoryCategory;
            unset($good->inventoryCategory);
        }
        
        return Response::json(array('rental' => $good));
    }

}