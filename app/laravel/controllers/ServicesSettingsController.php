<?php

class ServicesSettingsController extends BaseController {

	public function index() {

		$servicesSetting = ServicesSetting::first();

		return Response::json(array('servicesSettings' => $servicesSetting));
	}

	public function update($id) {
	        
	        $validator = Validator::make(Input::all(), ServicesSetting::$rules);
	        
	        if($validator->passes()) {
	            
	 	        $servicesSetting                           = ServicesSetting::find($id);
		        $servicesSetting->availableServices        = Input::get('servicesSetting.availableServices');
		        $servicesSetting->canReserve               = Input::get('servicesSetting.canReserve');
	            
	            $servicesSetting->save();
	            
	            return Response::json(array('servicesSettings' => $servicesSetting));
	            
	        } else {
	            return Response::json($validator->messages(), 422);
	        }
	    }

	    public function show($id) {
	    	$servicesSetting = ServicesSetting::find($id);

	    	return Response::json(array('servicesSettings' => $servicesSetting));
	    }


}
?>