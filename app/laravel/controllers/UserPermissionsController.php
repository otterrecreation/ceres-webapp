<?php

class UserPermissionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$permissions = Permission::all();
		
		foreach($permissions as &$permission) {
			$this->bindHasManyRelationship('roles', $permission);
		}
		
		return Response::json(array('userPermission' => $permissions));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'roles' => 'required',
		);
		
		$validator = Validator::make(Input::all(), $rules);
		
//		return Response::json(Input::all());
		
		//
		//let's update all of the roles that are defined
		if($validator->passes()) {
			// dd(Input::get('roles'));
			foreach(Input::get('roles') as $role_id => $permissions) {
				if( $role = Role::find($role_id) ) {
					$permission_ids = array(); //so we don't get an error
					foreach($permissions as $permission_id => $permission) {
						if( Permission::find($permission_id) && (int)$permission ) {
							$permission_ids[] = $permission_id;
						}
					}

					if( count($permission_ids) > 0 ) {
						$role->perms()->sync($permission_ids);
					} else {
						$role->perms()->sync(array()); //they want to remove the permissions
					}
					$role->save();
				} else {
					return Response::json('Can\'t find role', 404);
				}
			}
			
			return Response::json(array('permissions updated successfully'));
			
		} else {
			return Response::json($validator->messages(), 422);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$permission = Permission::find($id);
		
		$this->bindHasManyRelationship('roles', $permission);
		
		return Response::json(array('userPermission' => $permission));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
