<?php

class InventoryOrderVendorsController extends BaseController {

    public function index() {   // get all vendors
        
        $vendors = InventoryOrderVendor::all ();// get all vendors from DB to var $vendors
        return Response::json(array('inventoryOrderVendor' => $vendors));
    }
    
    
    public function show($id) { // get instance of vedor
        
        $vendor = InventoryOrderVendor::find ($id);// get instance vendor from DB to var $vendors
        return Response::json(array('inventoryOrderVendor' => $vendor));
    }
    
    
    public function store() {   // creat instance of vendor
        
        $vendor = new InventoryOrderVendor;
        
        $vendor->vendor_name    = Input::get('vendor_name');
        $vendor->rep_name       = Input::get('rep_name');
        $vendor->phone          = Input::get('phone');
        $vendor->ext            = Input::get('ext');
        $vendor->account_number = Input::get('account_number');
        
        $vendor-> save(); // save above data to DB
    
        return Response::json(array('inventoryOrderVendor' => $vendor));
    }
    
    
    public function update($id) {   // update DB

        $vendor =  InventoryOrderVendor::find($id);
        
        $vendor->vendor_name    = Input::get('vendor_name');
        $vendor->rep_name       = Input::get('rep_name');
        $vendor->phone          = Input::get('phone');
        $vendor->ext            = Input::get('ext');
        $vendor->account_number = Input::get('account_number');
        
        $vendor->save(); // save above data to DB
    
        return Response::json(array('inventoryOrderVendor' => $vendor));
    }
    
    
    public function destroy($id) { // remove/delet from DB, could be hard or soft delete.
        
        $vendor =  InventoryOrderVendor::find($id);
        
        $vendor->delete();
    }

}