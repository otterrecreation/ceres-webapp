<?php

class AuthController extends BaseController {
    public function getLogout() {
        Session::forget('oauth.usertoken');
        return Redirect::to('/'); //back to ember handling stuff
    }

    public function getLogin() {
        if(Session::has('oauth.usertoken') && Session::get('oauth.usertoken.expires_in') > time()) {
            return Redirect::to('/'); //TODO: allow them to go where they were trying to before getting pushed to login...
        } else {
            return View::make('auth.login');
        }
    }

    public function postLogin() {
        if(($response = App::make('OAuthUserToken')) !== true) {
            return Redirect::to('login')->withErrors(array('messages' => array($response)));
        } else {
            return Redirect::to('/');
        }
    }
    
    public function getSignup() {
        
    }
    
    public function postSignup() {
        
    }
}