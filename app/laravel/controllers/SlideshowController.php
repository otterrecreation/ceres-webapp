<?php

class SlideshowController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        if(Input::has('slug')) {
            $page = Page::where('slug', '=', Input::get('slug'))->first();
            
            if($page) {
                $slideshow = Slideshow::find($page->slideshow);
                
                if($slideshow) {
                    $this->bindHasManyRelationship('slides', $slideshow);
                    $this->bindHasManyRelationship('pages', $slideshow);
                    return Response::json(array('slideshow' => $slideshow));   
                } else {
                    return Response::json(array('no slideshow'), 404);
                }
            }
        }
        
        $slideshows = Slideshow::all();
        
        foreach($slideshows as &$slideshow) {
            $this->bindHasManyRelationship('slides', $slideshow);
            $this->bindHasManyRelationship('pages', $slideshow);
        }
        
        return Response::json(array('slideshow' => $slideshows));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), Slideshow::$rules);
        
        if($validator->passes()) {
            $slideshow = new Slideshow;
            $slideshow->title = Input::get('title');
            $slideshow->save();
            
            $this->bindHasManyRelationship('slides', $slideshow);
            $this->bindHasManyRelationship('pages', $slideshow);
            
            return Response::json(array('slideshow' => $slideshow));
        } else {
            return Response::json($validator->messages, 422);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        if(is_numeric($id)) {
            $slideshow = Slideshow::find($id);
        }
        
        $this->bindHasManyRelationship('slides', $slideshow);
        $this->bindHasManyRelationship('pages', $slideshow);
        
        return Response::json(array('slideshow' => $slideshow));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
        $validator = Validator::make(Input::all(), Slideshow::$rules);
        
        if($validator->passes()) {
            $slideshow = Slideshow::find($id);
            $slideshow->title = Input::get('title');
            $slideshow->save();
            
            $this->bindHasManyRelationship('slides', $slideshow);
            $this->bindHasManyRelationship('pages', $slideshow);
            
            return Response::json(array('slideshow' => $slideshow));
        } else {
            return Response::json($validator->messages, 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $slideshow = Slideshow::find($id);
        
        //disassociate all pages associated with this slideshow
        foreach($slideshow->pages as $page) {
            $page->slideshow = null;
            $page->save();
        }
        
        $slideshow->delete();
	}


}
