<?php

class RentalController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rentals = Rental::all();
        return Response::json( array('rentals' => $rentals) );
	}

	public function store() {
		$validator = Validator::make(Input::all(), Rental::$createRules);

		if($validator->passes()) {
			$rental = new Rental;
			$rental->name = Input::get('name');
			$rental->description = Input::get('description', '');
			$rental->count = Input::get('count');

			$rental->save();

			return Response::json( array('rental' => $rental) );

		} else {
			return Response::json($validator->failed(), 422);
		}
	}

	public function show($id) {
		$rental = Rental::find($id);
		return Response::json( array('rental' => $rental) );
	}
    
    //ADDITIONAL METHODS THAT DON'T WORK WITH THE Route::resource type of routes
    public function anyAvailability() {
        $checkoutRules = array(
            'item_id' => 'required|numeric',
            'check_out' => 'required',
            'check_in' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $checkoutRules);
        
        if($validator->passes()) {
            $itemId = Input::get('item_id');
        
            $checkOutDate = strtotime( Input::get('check_out') );
            $checkInDate = strtotime( Input::get('check_in') );
            
            //first, let's check that the item exists. don't make assumptions (we also need to compare the count of the item for availability next to the schedule)
            
            $rentalItem = Rental::findOrFail($itemId); //we probably need a better way to fail this. I'm just lazy right now
        
            // TODO: make sure the timestamp filter is filtering properly
        
            $schedules = DB::table('rental_schedule')
                         ->select(DB::raw('id,
                                           count,
                                           UNIX_TIMESTAMP(check_out) AS check_out_timestamp,
                                           UNIX_TIMESTAMP(check_in) AS check_in_timestamp'))
                         ->where('rental_id', $itemId)
                         ->whereRaw("(($checkOutDate >= UNIX_TIMESTAMP(check_out) AND $checkOutDate <= UNIX_TIMESTAMP(check_in)) 
                                        OR
                                     ($checkInDate >= UNIX_TIMESTAMP(check_out) AND $checkInDate <= UNIX_TIMESTAMP(check_in))
                                        OR
                                     ($checkOutDate <= UNIX_TIMESTAMP(check_out) AND $checkInDate <= UNIX_TIMESTAMP(check_in)))")
                         ->get();
        
//            dd(DB::getQueryLog());
            
            //check this first...
            if($rentalItem->count < Input::get('count')) {
                return Redirect::to('rentals/' . $itemId)->withErrors( array('messages' => array('We don\'t have the amount  that you requested.')));
            }
            
            return $this->checkSchedule($schedules, $rentalItem);     
        } else {
            dd(array('errors'));
        }
    }
    
    private function checkSchedule($schedules, $rentalItem) {
        Session::forget('rental');
        $checkOutDate = strtotime( Input::get('check_out') );
        $checkInDate = strtotime( Input::get('check_in') );
        
        if (count($schedules)) {
            
            $max = 0;
            
            //TODO: determine the granularity that we need to evaluate this (minutes? hours? days?)
            for ($i = $checkOutDate, $j = $checkInDate; $i <= $j ; $i++, $j--) {
                
                $checkOutSum = 0;
                $checkInSum = 0;
                foreach( $schedules as $schedule ) {
                    //this means that $i is within the schedules timeframe
                    if($i >= $schedule->check_out_timestamp && $i <= $schedule->check_in_timestamp) {
                        $checkOutSum += (int) $schedule->count;
                    }
                    
                    if($j >= $schedule->check_out_timestamp && $j <= $schedule->check_in_timestamp) {
                        $checkInSum += (int) $schedule->count;
                    }
                }
                
                if($checkOutSum > $max) {
                    $max = $checkOutSum;
                }
                
                if($checkInSum > $max) {
                    $max = $checkInSum;
                }
            }
            
            if( ($rentalItem->count - $max) >= Input::get('count') ) {
                //YYYY-MM-DD HH:MI:SS
                
                Session::set('check_out', date('Y-m-d H:i:s', $checkOutDate));
                Session::set('check_in', date('Y-m-d H:i:s', $checkInDate));
                Session::set('rental_id', $rentalItem->id);
                Session::set('count', Input::get('count'));
                
                return Redirect::to('rentals/confirm')->with('messages', array('You can proceed with this reservation'));
            } else {
                return Redirect::to('rentals/' . $rentalItem->id)->withErrors(array('messages' => array('You can\'t rent this item.')));
            }
            
//            dd(array('not sure if you can rent this...: ' . $max));
        } else {
            //YYYY-MM-DD HH:MI:SS
            
            Session::set('check_out', date('Y-m-d H:i:s', $checkOutDate));
            Session::set('check_in', date('Y-m-d H:i:s', $checkInDate));
            Session::set('rental_id', $rentalItem->id);
            Session::set('count', Input::get('count'));
            
            return Redirect::to('rentals/confirm')->with('messages', array('You can rent this item'));
        } 
    }
    
    public function getConfirm() {
        return View::make('rentals.confirm')->with('messages', Session::get('messages'));
    }
    
    public function postConfirm() {
        if(Session::has('rental')) {
            $rentalItem = Session::get('rental');
            DB::table('rental_schedule')->insert($rentalItem);
            
            return Redirect::to('rentals')->with('messages', array('Rental item successfully reserved.'));
        }
        
        return Redirect::to('rentals')->withErrors(array('messages' => array('error with something')));
    }
}