<?php

class InventoryCategoryController extends BaseController {

    public function index() {
        
        $inventoryCategories = InventoryCategory::all();
        
        foreach($inventoryCategories as &$category) {
            $this->bindHasManyRelationship('inventory', $category);
        }
        
        return Response::json(array('inventoryCategory' => $inventoryCategories));
    }
    
    public function show($id) {
        $category = InventoryCategory::find($id);
        
        $this->bindHasManyRelationship('inventory', $category);
        
        return Response::json(array('inventoryCategory' => $category));
    }
    
    public function store() {
        $validator = Validator::make(Input::all(), InventoryCategory::$rules);
        
        if($validator->passes()) {
            $category = new InventoryCategory();
            $category->category = Input::get('category');
            
            $category->save();
            
            $this->bindHasManyRelationship('inventory', $category);
            
            return Response::json(array('inventoryCategory' => $category));
        } else {
            return Response::json(array('messages' => 'check your input'), 422);
        }
    }
    
    public function update($id) {
        $validator = Validator::make(Input::all(), InventoryCategory::$rules);
        
        if($validator->passes()) {
            $category = InventoryCategory::find($id);
            $category->category = Input::get('category');
            
            $category->save();
            
            $this->bindHasManyRelationship('inventory', $category);
            
            return Response::json(array('inventoryCategory' => $category));
        } else {
            return Response::json(array('messages' => 'check your input'), 422);
        }
    }
    
    public function destroy($id) {
        $category = InventoryCategory::find($id);
        $category->delete();
        
        return Response::json(array('messages' => 'category removed successfully'));
    }

}