<?php

class InventoryOrderItemsController extends BaseController {

    public function index() {   // get all 
        
        $items = InventoryOrderItem::all ();// get all vendors from DB to var $vendors
        return Response::json(array('inventoryOrderItem' => $items));
    }
    
    
    public function show($id) { // get instance of 
        
        $item = InventoryOrderItem::find ($id);// get instance vendor from DB to var $vendors
        return Response::json(array('inventoryOrderItem' => $item));
    }
    
    
    public function store() {   // creat instance of 
        
        $item = new InventoryOrderItem;
        
        $item->item_cost    = Input::get('item_cost');
        $item->qty_ordered  = Input::get('qty_ordered');
        $item->qty_received = Input::get('qty_received');
        $item->inventory_id = Input::get('inventory_id');
        $item->order_id     = Input::get('order_id');
        $item->intended_for = Input::get('intended_for');
        $item->vendor_id    = Input::get('vendor_id');
        
        $item->save(); // save above data to DB
    
        return Response::json(array('inventoryOrderItem' => $item));
    }
    
    
    public function update($id) {   // update DB

        $item =  InventoryOrderItem::find($id);
        
        $item->item_cost    = Input::get('item_cost');
        $item->qty_ordered  = Input::get('qty_ordered');
        $item->qty_received = Input::get('qty_received');
        $item->inventory_id = Input::get('inventory_id');
        $item->order_id     = Input::get('order_id');
        $item->intended_for = Input::get('intended_for');
        $item->vendor_id    = Input::get('vendor_id');
        
        $item->save(); // save above data to DB
    
        return Response::json(array('inventoryOrderItem' => $item));
    }
    
    
    public function destroy($id) { // remove/delet from DB, could be hard or soft delete.
        
        $item =  InventoryOrderItem::find($id);
        
        $item->delete();
    }

}