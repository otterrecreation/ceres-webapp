<?php

class CartItemsController extends BaseController {

    
    public function __construct() {
        $this->beforeFilter('oauth.clienttoken');
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if( ($currentUser = $this->getCurrentUser()) ) {
            
            $cart = Cart::where('user', '=', $currentUser->id)->first();
            
            if($cart) {
                
                $cartItems = $cart->cartItems;
            
                if($cartItems) {

                    return Response::json(array('cartItem' => $cartItems));

                }
                
            }
            
            
            
        } else if (Input::has('cart_token')) {
            
            $cart = Cart::where('token', '=', Input::get('cart_token'))->first();
            
            if($cart) {
                
                $cartItems = $cart->cartItems;
                
                if($cartItems) {
                    return Response::json(array('cartItem' => $cartItems));
                }
            }
            
        }
        
        return Response::json(array('cartItem' => array()));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'table_name' => 'required',
            'table_id' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
        
        if($validator->passes()) {
        
            if( ($currentUser = $this->getCurrentUser()) ) {
                $cart_id = Cart::where('user', '=', $currentUser->id)->first()->id;
            } else {
                $cart_id = Cart::where('token', '=', Input::get('cart_token'))->first()->id;
            }
            
            $cartItem = new CartItem;
            $cartItem->paid = 0;
            $cartItem->cart = $cart_id; //embedded by the cookie request
            $cartItem->table_name = Input::get('table_name');
            $cartItem->table_id = Input::get('table_id');
            
            $model = DB::table(Input::get('table_name'))
                       ->where('id', Input::get('table_id'))
                       ->first();
            
            $cartItem->price = $model->price;
            $cartItem->item_name = $model->name;
            
            $cartItem->save();
            
            return Response::json(array('cartItem' => $cartItem));
            
        } else {
        
            return Response::json($validator->messages(), 422);
        
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $cartItems = CartItem::where('id', '=', $id)
                             ->where('paid', '=', 0)
                             ->get();
        if($cartItems) {
            return Response::json(array('cartItem' => $cartItems));
        }
        
        return Response::json(array('cartItem' => null), 404);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$orderItem = CartItem::find($id);
        
        $orderItem->delete();
        
        return Response::json(array('messages' => array('item removed from cart')));
	}


}
