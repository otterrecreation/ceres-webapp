<?php

class MediaController extends BaseController {

    protected $filesDirectory = 'media/files'; //location relative to the public folder that the files will be uploaded to
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$files = Media::all();
        
        return Response::json(array('file' => $files));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        if(Input::hasFile('file')) 
        {
            //handle the file upload here
            $file      = Input::file('file');
            
            if( ($isImage = $this->isImage( $file->getRealPath()) ) ) //right now we are only allowing images into the system for upload
            {
                $destination = realpath(UPLOAD_DESTINATION);
                $name        = time() . '-' . $file->getClientOriginalName();
                $imageSize   = getimagesize($file->getRealPath());
                $extension   = $file->getClientOriginalExtension();
                $title       = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file->getClientOriginalName());
                $mimeType    = $file->getMimeType();
                $bytes       = $file->getSize();
                $size        = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
                $factor      = floor((strlen($bytes) - 1) / 3);
                $size        = sprintf("%.2f", $bytes / pow(1024, $factor)) . @$size[$factor];
                
                $file->move($destination, $name);
                
                $file          = new Media();
                $file->name    = $name;
                $file->title   = $title;
                $file->src     = $this->filesDirectory . DIRECTORY_SEPARATOR . $file->name; //all uploads go here. note that if the UPLOAD_DESTINATION constant is changed then this will need to be changed as well
                $file->isImage = $isImage;
                $file->width   = $imageSize[0];
                $file->height  = $imageSize[1];
                $file->size    = $size;

                //we can create a thumbnail if the file uploaded is an image
                if($file->isImage) 
                {
                    $thumbnailDestination = realpath(UPLOAD_DESTINATION);
                    $thumbnailSrc = $this->filesDirectory . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . 'thumbnail-' . $file->name;
                    $fileName = $destination . DIRECTORY_SEPARATOR . $file->name;
                    $image = new abeautifulsite\SimpleImage($fileName); //pull in the newly saved file
                    $image->thumbnail(300, 300); // crop the best fitting 1:1 ratio (300x300) and resize to 300x300 pixel
                    $image->save($thumbnailDestination . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . 'thumbnail-' . $file->name); //save the image in the thumbnails folder

                    $file->thumbnailSrc = $thumbnailSrc;
                }

                $file->mime = $mimeType;
                $file->extension = $extension;
                $file->save();

                return Response::json(array('file' => $file));
            }
        }
        
        return Response::json(array('messages' => array('please check the file you attempted to upload')), 422);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
        $file = Media::find($id);
        
        return Response::json(array('file' => $file));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{   
		$validator = Validator::make(Input::all(), Media::$rules);
        
        if($validator->passes()) {
            
            $file = Media::find($id);
            $file->title = Input::get('title');
            $file->altText = Input::get('altText');
            $file->description = Input::get('description');
            $file->save();
            
            return Response::json(array('file' => $file));
            
        } else {
            return Response::json($validator->messages(), 422);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$file = Media::find($id);
        
//        dd($file);
        
        $absolutePath = realpath(UPLOAD_DESTINATION) . DIRECTORY_SEPARATOR;
        
        unlink($absolutePath . $file->name);
        
        if($file->isImage) 
        {
            //delete thumbnail as well
            unlink($absolutePath . 'thumbnails' . DIRECTORY_SEPARATOR . 'thumbnail-' . $file->name);
            
        }
        
        $file->delete();
        
        return Response::json(array('messages' => array('file deleted successfully')));
        
	}
    
    protected function isImage($file)
    {   
        return getimagesize($file) !== false;
    }


}
