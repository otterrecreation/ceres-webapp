<?php

class InventoryOrderController extends BaseController {

    public function index() {   // get all 
        
        $orders = InventoryOrder::all ();// get all  from DB to var $____
        
        foreach($orders as &$order) {
            $this->bindHasManyRelationship('inventoryOrderList', $order);
        }
        
        return Response::json(array('inventoryOrder' => $orders));
    }
    
    
    public function show($id) { // get instance of 
        
        $order = InventoryOrder::find ($id);// get instance vendor from DB to var $______
        return Response::json(array('inventoryOrder' => $order));
    }
    
    
    public function store() {   // creat instance of 
        
        $order = new InventoryOrder;
        
        $order->po_number           = Input::get('po_number');
        $order->shipping_cost       = Input::get('shipping_cost');
        $order->total_cost          = Input::get('total_cost');
        $order->ordered_at          = Input::get('ordered_at');
        $order->fulfilled_at        = Input::get('fulfilled_at');
        $order->notes               = Input::get('notes');
        $order->vendor              = Input::get('vendor');
        $order->order_status        = Input::get('order_status');
        $order->ordered_by          = Input::get('ordered_by');
        $order->entered_by          = Input::get('entered_by');
        $order->last_modified_by    = Input::get('last_modified_by');
        
        
        $order-> save(); // save above data to DB
        
        $this->bindHasManyRelationship('inventoryOrderList', $order);
    
        return Response::json(array('inventoryOrder' => $order));
    }
    
    
    public function update($id) {   // update DB

        $order =  InventoryOrder::find($id);
        
        $order->po_number           = Input::get('po_number');
        $order->shipping_cost       = Input::get('shipping_cost');
        $order->total_cost          = Input::get('total_cost');
        $order->ordered_at          = Input::get('ordered_at');
        $order->fulfilled_at        = Input::get('fulfilled_at');
        $order->notes               = Input::get('notes');
        $order->vendor              = Input::get('vendor');
        $order->order_status        = Input::get('order_status');
        $order->ordered_by          = Input::get('ordered_by');
        $order->entered_by          = Input::get('entered_by');
        $order->last_modified_by    = Input::get('last_modified_by');
        
        
        $order->save(); // save above data to DB
        
        $this->bindHasManyRelationship('inventoryOrderList', $order);
    
        return Response::json(array('inventoryOrder' => $order));
    }
    
    
    public function destroy($id) { // remove/delet from DB, could be hard or soft delete.
        
        $order =  InventoryOrder::find($id);
        
        $order->delete();
    }

}