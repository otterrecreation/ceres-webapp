<?php

class UserController extends BaseController {
	
	public function __construct() {
		$this->beforeFilter('oauth.usertoken', array('except' => 'store'));
		$this->beforeFilter('oauth.clienttoken', array('only' => 'store'));
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		
		foreach($users as &$user) {
			
			if(Input::get('with_permissions', null) === 'true') {
			
				$this->attachPermissions($user);
				
			}
			
			
			$this->attachRole($user);
			$this->bindHasManyRelationship('addresses', $user);
			$this->bindHasManyRelationship('billingInfo', $user);
		}
        
		return Response::json( array('users' => $users) );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), User::$rules);

		if ($validator->passes()) 
		{
			$user = new User;
			$user->name = Input::get('name');
			$user->username = Input::get('username');
			$user->password = Hash::make(Input::get('password'));
			
			$user->save();
			//now let's add roles to the user
			//1. it would be good to check for permissions of the user making the request
			
			//2. if user is not permitted to create_users (not authenticated either) then we can apply the default role on signup
			//	 i. for now that role is simply the user role (which has no permissions attached to it)
			$defaultRole = Role::where('name', '=', 'User')->first();
			$user->attachRole($defaultRole);
               
			$this->attachPermissions($user);
			$this->attachRole($user);
			$this->bindHasManyRelationship('addresses', $user);
			$this->bindHasManyRelationship('billingInfo', $user);
            
            return Response::json( array('user' => $user) );
		} else {
			return Response::json( $validator->messages(), 422 );
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $user = User::find($id);
        
		if(Input::get('with_permissions', null) === 'true') {
            $this->attachPermissions($user);
        }
		
		$this->attachRole($user);
		$this->bindHasManyRelationship('addresses', $user);
		$this->bindHasManyRelationship('billingInfo', $user);
        
		return Response::json( array('user' => $user) );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'name' => 'required',
		);
		
		$validator = Validator::make(Input::all(), $rules);

		//checking to make sure the input is valid
		if ($validator->passes()) 
		{

			$user = User::find($id);

			$user->name = Input::get('name', $user->name);
			$user->password = Input::has('password') ? Hash::make(Input::get('password')) : $user->password;
            
            $currentUser = $this->getCurrentUser();
            
            if($currentUser->can('edit_users') && Input::has('role')) {
                $user->roles()->detach();
                $user->roles()->sync(array(Input::get('role'))); //now we can update the role. only super admins can change this...
            }
            
			$user->save();
            
            $this->attachPermissions($user);
			$this->attachRole($user); //although entrust makes roles a many to many relationship we are using it as a many to one
			$this->bindHasManyRelationship('addresses', $user);
			$this->bindHasManyRelationship('billingInfo', $user);

			return Response::json( array('user' => $user) );
		} 
		else 
		{   
			return Response::json( Input::all(), 422);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        User::destroy($id); //TODO: find a way to check that the user was successfully deleted
        return Response::json( array('Account removed. Sorry to see you go.') );
	}
    
    //helper for attaching all of the permissions a user has
    public function attachPermissions(&$user) {
        
        $permissions = array();
        foreach($user->roles as &$role) {
            unset($role->pivot);
            foreach($role->perms as &$perm) {
                //do nothing
                unset($perm->pivot);
                $permissions[] = $perm->name;
            }
        }
        
        unset($user->roles);
        $user->permissions = array_unique($permissions);
    }
    
    public function attachRole(&$user) {
        //we need to make roles become role
        $userRole = null;
        foreach($user->roles as $role) {
            $userRole = $role->id;
        }
        
        unset($user->roles); //so we aren't carrying around an array of roles
        
        $user->role = $userRole;
    }
}