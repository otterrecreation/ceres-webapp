<?php

class InventoryConditionController extends BaseController {

    public function index() {
        
        $inventoryConditions = InventoryCondition::all();
        
        return Response::json(array('inventoryCondition' => $inventoryConditions));
    }
    
    public function show($id) {
        $inventoryCondition = InventoryCondition::find($id);
        
        return Response::json(array('inventoryCondition' => $inventoryCondition));
    }
    
    public function store() {
        $inventoryCondition = new InventoryCondition();
        $inventoryCondition->condition = Input::get('condition');
        $inventoryCondition->save();
        
        return Response::json(array('inventoryCondition' => $inventoryCondition));
    }
    
    public function update($id) {
        $inventoryCondition = InventoryCondition::find($id);
        $inventoryCondition->condition = Input::get('condition');
        $inventoryCondition->save();
        
        return Response::json(array('inventoryCondition' => $inventoryCondition));
    }
    
    public function destroy($id) {
        $inventoryCondition = InventoryCondition::find($id);
        $inventoryCondition->delete();
        
        return Response::json(array('messages' => 'condition removed successfully'));
    }

}