<?php

class GoodCategoryController extends BaseController {

    public function index() {
        
        $inventoryCategories = InventoryCategory::all();
        
        return Response::json(array('goodCategory' => $inventoryCategories));
    }
    
    public function show($id) {
        $category = InventoryCategory::find($id);
        
        return Response::json(array('goodCategory' => $category));
    }

}