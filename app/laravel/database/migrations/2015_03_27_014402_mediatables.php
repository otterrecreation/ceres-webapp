<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mediatables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('media', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('size');
            $table->string('title');
            $table->string('altText');
            $table->string('description');
            $table->string('width');
            $table->string('height');
            $table->string('src');
            $table->string('thumbnailSrc')->nullable();
            $table->string('extension');
            $table->string('mime');
            $table->tinyInteger('isImage')->default(0);
            $table->timestamps();
        });
        
        Schema::create('slideshows', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });
        
        Schema::create('slides', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->string('caption');
            $table->integer('file')->unsigned();
            $table->foreign('file')
                  ->references('id')->on('media')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('slideshow')->unsigned();
            $table->foreign('slideshow')
                  ->references('id')->on('slideshows')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('slides');
        Schema::drop('slideshows');
        Schema::drop('media');
	}

}
