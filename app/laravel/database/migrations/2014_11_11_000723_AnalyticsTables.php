<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnalyticsTables extends Migration 
{
	public function up()
	{
		Schema::create('analytics', function($table) {
			$table->increments('id');
			$table->integer('user_id')
				  ->unsigned();
			$table->foreign('user_id')
				  ->references('id')
				  ->on('rentals')
				  ->onDelete('cascade');
			$table->datetime('check_out');
			$table->datetime('check_in');
			$table->timestamps();
		});
	}
	public function down()
	{
		Schema::drop('analytics');
	}

}
