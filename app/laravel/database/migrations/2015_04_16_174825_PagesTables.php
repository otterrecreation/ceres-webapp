<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('pages', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('is_home')->default(0);
            $table->tinyInteger('show_title');
            $table->string('slug');
            $table->tinyInteger('is_active');
            $table->text('page_content');
            $table->integer('slideshow')->unsigned()->nullable()->default(null);
            $table->foreign('slideshow')
                  ->references('id')->on('slideshows')
                  ->onUpdate('cascade')
                  ->onDelete('set null');
            $table->timestamps();
        });
        
        Schema::create('blogs', function($table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug');
            $table->tinyInteger('is_active');
            $table->text('page_content');
            $table->integer('author')->unsigned();
            $table->foreign('author')
                  ->references('id')->on('oauth_users');
            $table->integer('last_modified_by')->unsigned();
            $table->foreign('last_modified_by')
                  ->references('id')->on('oauth_users');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('pages');
        Schema::drop('blogs');
	}

}
