<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RentalTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rentals', function($table) {
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->integer('count');
			$table->timestamps();
		});

		Schema::create('rental_schedule', function($table) {
			$table->increments('id');
			$table->integer('rental_id')->unsigned();
			$table->timestamp('check_out');
			$table->timestamp('check_in');
            $table->integer('count');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rental_schedule');
		Schema::drop('rentals');
	}

}
