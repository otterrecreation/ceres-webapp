<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InventorySchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('inventory_categories', function($table) {
            $table->increments('id');
            $table->string('category');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('inventory', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description', 500);
            $table->decimal('price', 10, 2);
            $table->string('pricePer');
            $table->string('type');
            $table->integer('count')->unsigned();
            $table->integer('alertAt')->unsigned();
            $table->tinyInteger('publicVisible')->default(1);
            $table->softDeletes();
            $table->timestamps();
            
            $table->integer('inventoryCategory')->unsigned();
            $table->foreign('inventoryCategory')
                  ->references('id')->on('inventory_categories')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            
            $table->integer('file')->unsigned()->nullable();
            $table->foreign('file')
                  ->references('id')->on('media')
                  ->onUpdate('cascade')
                  ->onDelete('set null');
        });
        
        Schema::create('inventory_status', function($table) {
            $table->increments('id');
            $table->string('status');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('inventory_condition', function($table) {
            $table->increments('id');
            $table->string('condition');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('inventory_instances', function($table) {
            $table->increments('id');
            $table->integer('inventory')->unsigned();
            $table->foreign('inventory')
                  ->references('id')->on('inventory')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('inventoryStatus')->unsigned();
            $table->foreign('inventoryStatus')
                  ->references('id')->on('inventory_status')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->integer('inventoryCondition')->unsigned()->nullable();
            $table->foreign('inventoryCondition')
                  ->references('id')->on('inventory_condition')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->tinyInteger('publicVisible')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('inventory_instances');
        Schema::drop('inventory');
        Schema::drop('inventory_categories');
        Schema::drop('inventory_condition');
        Schema::drop('inventory_status');
	}

}
