<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OAuthTables extends Migration {

    /**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('oauth_clients', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('client_id')->unique();
            $table->string('client_secret');
            $table->string('redirect_uri');
            $table->string('grant_types')->nullable();
            $table->string('scope')->nullable();
            $table->string('user_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('oauth_access_tokens', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('access_token')->unique();
            $table->string('client_id');
            $table->string('user_id')->nullable();
            $table->timestamp('expires');
            $table->string('scope')->nullable();
            $table->timestamps();
        });

        Schema::create('oauth_authorization_codes', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('authorization_code')->unique();
            $table->string('client_id');
            $table->string('user_id')->nullable();
            $table->timestamp('expires');
            $table->string('redirect_uri')->nullable();
            $table->string('scope')->nullable();
            $table->timestamps();
        });

        Schema::create('oauth_refresh_tokens', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('refresh_token')->unique();
            $table->string('client_id');
            $table->string('user_id')->nullable();
            $table->timestamp('expires');
            $table->string('scope')->nullable();
            $table->timestamps();
        });

        Schema::create('oauth_users', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('name');
			$table->string('stripe_customer_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('oauth_scopes', function ($table) {
            $table->increments('id')->unsigned();
            $table->text('scope');
            $table->boolean('is_default');
            $table->timestamps();
        });

        Schema::create('oauth_jwt', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('client_id');
            $table->string('subject')->nullable();
            $table->string('public_key');
            $table->timestamps();
        });
		
		
		//USER BILLING INFO
		Schema::create('user_billing_info', function($table) {
			$table->increments('id')->unsigned();
			$table->string('stripe_card_id');
			$table->string('name');
			$table->integer('last4');
			$table->string('brand');
			$table->string('funding');
			$table->integer('exp_month');
			$table->integer('exp_year');
			
			$table->integer('user')->unsigned();
			$table->foreign('user')
				  ->references('id')->on('oauth_users')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
			$table->timestamps();
			$table->softDeletes();
		});
		
		//USER ADDRESSES
		Schema::create('user_addresses', function($table) {
			$table->increments('id')->unsigned();
			$table->string('recipient_name');
			$table->string('address_line_1');
			$table->string('address_line_2');
			$table->string('city');
			$table->string('state_province');
			$table->string('country');
			$table->string('zip');
			$table->integer('user')->unsigned();
			$table->foreign('user')
				  ->references('id')->on('oauth_users')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oauth_clients');
		Schema::drop('oauth_access_tokens');
		Schema::drop('oauth_authorization_codes');
		Schema::drop('oauth_refresh_tokens');
		Schema::drop('user_addresses');
		Schema::drop('user_billing_info');
		Schema::drop('oauth_users');
		Schema::drop('oauth_scopes');
		Schema::drop('oauth_jwt');
	}

}
