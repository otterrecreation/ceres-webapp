<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CartTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('cart', function($table) {
            $table->increments('id')->unsigned();
            $table->string('token')->nullable(); //a way for non-authenticated users to have a cart
            $table->integer('user')->unsigned()->nullable(); //just in case they decide to log in or create an account, we can immediately associate their cart with the user... //whether the user paid for the cart yet
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('cart_items', function($table) {
            $table->increments('id')->unsigned();
            $table->string('table_name');
            $table->integer('table_id')->unsigned();
            $table->string('item_name');
            $table->tinyInteger('paid')->default(0); //track whether or not the item has been paid for...
            $table->decimal('price', 10, 2);
            $table->integer('quantity')->default(1); //how many of an item they want (the multiplier)
            $table->integer('cart')->unsigned();
            $table->foreign('cart')
                  ->references('id')->on('cart')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('cart_items');
        Schema::drop('cart');
	}

}
