<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestockTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() // What to install when you migrate
	{
		
         Schema::create('inventory_order_statuses', function($table) {
            $table->increments('id');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
        
        
        // Vendor Data Table 
        Schema::create('inventory_order_vendors', function($table) {
            $table->increments('id');
            $table->string('vendor_name');
            $table->string('rep_name');
            $table->string('phone');
            $table->string('ext');
            $table->string('account_number');
            $table->timestamps();
            $table->softDeletes();
        });
        
        
        Schema::create('inventory_orders', function($table) {
            $table->increments('id');
            $table->string('po_number');
            $table->decimal('shipping_cost');
            $table->decimal('total_cost');
            $table->dateTime('ordered_at');
            $table->dateTime('fulfilled_at');
            $table->string('notes', 1000);
            
            $table->integer('vendor')->unsigned()->nullable();
            $table->foreign('vendor')
				  ->references('id')->on('inventory_order_vendors')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');            

            $table->integer('order_status')->unsigned()->nullable();
            $table->foreign('order_status')
				  ->references('id')->on('inventory_order_statuses')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->integer('ordered_by')->unsigned()->nullable();
            $table->foreign('ordered_by')
				->references('id')->on('oauth_users')
				->onUpdate('cascade')
				->onDelete('cascade');
            
            $table->integer('entered_by')->unsigned()->nullable();
            $table->foreign('entered_by')
				  ->references('id')->on('oauth_users')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->integer('last_modified_by')->unsigned()->nullable();
            $table->foreign('last_modified_by')
				  ->references('id')->on('oauth_users')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();  // adds soft delete potential to this table

        });
        

        Schema::create('inventory_order_items', function($table) {
            $table->increments('id');
            $table->string('item_cost');
            $table->string('qty_ordered');
            $table->string('qty_received');
            
            
            $table->integer('inventory_id')->unsigned()->nullable();
            $table->foreign('inventory_id')
				  ->references('id')->on('inventory')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')
				  ->references('id')->on('inventory_orders')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->integer('intended_for')->unsigned()->nullable();
            $table->foreign('intended_for')
				  ->references('id')->on('oauth_users')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
            
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')
				  ->references('id')->on('inventory_order_vendors')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();

        });
        
        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() 
        // what to delete when you reset or roll back. Will delete in order. Delete Foreign Keys first.
	{
		//     
        Schema::drop('inventory_order_items');
        Schema::drop('inventory_orders');
        Schema::drop('inventory_order_statuses');
        Schema::drop('inventory_order_vendors');
    }

}
