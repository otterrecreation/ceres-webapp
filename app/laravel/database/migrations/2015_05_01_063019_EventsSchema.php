<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('events', function($table) {
			$table->increments('id');
            // The number of registrants at which an alert will show for an event.
            $table->integer('alertAt')->unsigned()->nullable();
            // The maximum amount of participants who can register for an event.
            $table->integer('capacity')->unsigned()->nullable();
            // The description of an event.
            $table->string('description')->nullable();
            // The end date/time of an event.
            $table->dateTime('end')->nullable();
            // The physical location of an event.
            $table->string('location')->nullable();
            // The name of an event.
            $table->string('name')->nullable();
            // The price that a customer will pay at purchase.
            $table->decimal('price', 10, 2)->nullable();
            // The date an event becomes visible to the public.
            $table->dateTime('publicAt')->nullable();
			// The overriding decider of an event's public visibility.
			$table->boolean('publicVisible')->nullable();
            // The date/time by which a customer can register for an event.
            $table->dateTime('registrationDeadline')->nullable();
            // The start date/time of an event.
            $table->dateTime('start')->nullable();
            $table->integer('file')->unsigned()->nullable();
            $table->foreign('file')
                  ->references('id')->on('media')
                  ->onUpdate('cascade')
                  ->onDelete('set null');
			$table->timestamps();
            $table->softDeletes();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('events');
	}

}
