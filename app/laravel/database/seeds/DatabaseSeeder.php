<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('ServicesSeeder');
        $this->call('ServicesSettingsSeeder');
        $this->call('InventoryCategorySeeder');
        $this->call('InventoryStatusSeeder');
        $this->call('InventoryConditionSeeder');
        $this->call('OAuthClientsSeeder');
		$this->call('OAuthUsersSeeder');
        $this->call('InventorySeeder');
        $this->call('InventoryOrderStatusSeeder');
        $this->call('InventoryOrderVendorSeeder');
        $this->call('InventoryOrderSeeder');
        $this->call('InventoryOrderItemSeeder');
        $this->call('PagesSeeder'); //setup the default routes for the front page
        $this->call('SettingsSeeder');
	}
}

class ServicesSeeder extends Seeder {
    public function run()
    {

    }

}

class InventoryCategorySeeder extends Seeder {
    public function run()
    {
        
        $category = new InventoryCategory();
        $category->category = 'uncategorized';
        $category->save();
        
    }
}

class InventoryStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_status')->delete();
        
        $status = new InventoryStatus();
        $status->status = 'in';
        $status->save();

        $status = new InventoryStatus();
        $status->status = 'reserved';
        $status->save();
        
        $status = new InventoryStatus();
        $status->status = 'out';
        $status->save();
        
    }
}

class InventorySeeder extends Seeder {
    public function run()
    {
        DB::table('inventory')->delete();
        
//        $item = new Inventory();
//        $item->name = 'SB66';
//        $item->description = 'Yellow Size:M FS';
//        $item->price = '999.00';
//        $item->type = 'good';
//        $item->count = 1;
//        $item->alertAt = 0;
//        $item->publicVisible = 1;
//        $item->inventoryCategory = 1;
//        $item->save();
//        
//        
//        $item = new Inventory();
//        $item->name = 'Socks';
//        $item->description = 'wool socks for hiking';
//        $item->price = '9.90';
//        $item->pricePer = 'hour';
//        $item->type = 'rental';
//        $item->count = 1;
//        $item->alertAt = 0;
//        $item->publicVisible = 1;
//        $item->inventoryCategory = 1;
//        $item->save();
//        
//        
//        $item = new Inventory();
//        $item->name = 'shoes';
//        $item->description = 'awesome shoes';
//        $item->price = '39.00';
//        $item->pricePer = 'hour';
//        $item->type = 'rental';
//        $item->count = 2;
//        $item->alertAt = 0;
//        $item->publicVisible = 1;
//        $item->inventoryCategory = 1;
//        $item->save();
        
    }
}

class InventoryConditionSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_condition')->delete();
        
        $condition = new InventoryCondition();
        $condition->condition = 'working';
        $condition->save();
        
        $condition = new InventoryCondition();
        $condition->condition = 'broken';
        $condition->save();
        
    }
}

class OAuthClientsSeeder extends Seeder
{
    public function run()
    {
        DB::table('oauth_clients')->delete();

        DB::table('oauth_clients')->insert(array(
            'client_id' => Config::get('oauthclient.id'),
            'client_secret' => Hash::make(Config::get('oauthclient.secret')),
            'redirect_uri' => Config::get('oauthclient.redirect_uri'),
        ));
    }
}

class OAuthUsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('oauth_users')->delete();
        DB::table('roles')->delete();
        DB::table('permissions')->delete();

        //setup the permissions
        $edit_user = new Permission;
        $edit_user->name = 'edit_users';
        $edit_user->display_name = 'Edit Users';
        $edit_user->save();
        

        $view_analytics = new Permission;
        $view_analytics->name = 'view_analytics';
        $view_analytics->display_name = 'View Analytics';
        $view_analytics->save();
        
        $view_orders = new Permission;
        $view_orders->name = 'view_orders';
        $view_orders->display_name = 'View Orders';
        $view_orders->save();

        $edit_permissions = new Permission;
        $edit_permissions->name = 'edit_permissions';
        $edit_permissions->display_name = 'Edit Permissions';
        $edit_permissions->save();
        
        $view_admin = new Permission;
        $view_admin->name = 'view_admin';
        $view_admin->display_name = 'View Admin';
        $view_admin->save();
        
        //set up the roles
        $super = new Role;
        $super->name = 'Super Admin';
        $super->save();
        $super->perms()->sync(array($edit_user->id, 
                                    $view_analytics->id, 
                                    $edit_permissions->id, 
                                    $view_admin->id, 
                                    $view_orders->id
                                   ));
        
        
        $admin = new Role;
		$admin->name = 'Admin';
        $admin->save();

        $employee = new Role;
        $employee->name = 'Employee';
        $employee->save();

        $user = new Role;
		$user->name = 'User';
        $user->save();

        $user = new User;
        $user->username = 'OCC';
        $user->password = Hash::make('occ');
        $user->name = 'Otter Cycle Center';
        $user->save();
        $user->attachRole($super);
        
        $user = new User;
        $user->username = 'espi4055';
        $user->password = Hash::make('bike');
        $user->name = 'Carlos Espinoza';
        $user->save();
        $user->attachRole($super);
        
        // Testing account for Micah
        $user = new User;
        $user->username = 'miriye@csumb.edu';
        $user->password = Hash::make('123456');
        $user->name = 'Micah Iriye';
        $user->save();
        $user->attachRole($super);

        // Testing account for Greg
        $user = new User;
        $user->username = 'ggreenleaf@csumb.edu';
        $user->password = Hash::make('000000');
        $user->name = 'Greg Greenleaf';
        $user->save();
        $user->attachRole($super);
        
        // Testing user account
        $user = new User;
        $user->username = 'admin';
        $user->password = Hash::make('admin');
        $user->name = 'Generic Admin';
        $user->save();
        $user->attachRole($super);
        
    }
}

class ServicesSettingsSeeder extends Seeder {
    public function run() {
        DB::table('service_settings')->delete();

        $servicesSetting = new ServicesSetting();
        $servicesSetting->availableServices = 5;
        $servicesSetting->canReserve = 1;
        $servicesSetting->save();
    }
}

/************************START***********************************
Restock seeders
****************************************************************/ 
class InventoryOrderStatusSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_order_statuses')->delete();
        
//        $status = new InventoryOrderStatus();
//        $status->status = 'Open';
//        $status->save();
//
//        $status = new InventoryOrderStatus();
//        $status->status = 'Ordered';
//        $status->save();
//        
//        $status = new InventoryOrderStatus();
//        $status->status = 'Fulfilled';
//        $status->save();
    }
}


class InventoryOrderVendorSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_order_vendors')->delete();
        
//        $vendor = array( array(
//            'vendor_name' => 'KHS',
//            'rep_name' => 'Eric White',
//            'phone' => '800-559-8989',
//            'ext' => '279',
//            'account_number' => 'KHS11223'
//        ));    
//        DB::table('inventory_order_vendors')->insert($vendor);
//        
//        $vendor = array( array(
//            'vendor_name' => 'Cyclone',
//            'rep_name' => 'NONE',
//            'phone' => '800-111-1132',
//            'ext' => 'XXX',
//            'account_number' => 'CYCLN11223'
//        ));
//        DB::table('inventory_order_vendors')->insert($vendor);
    }
}


class InventoryOrderSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_orders')->delete();
        
//        $order = new InventoryOrder();      
//        $order->po_number='123-xxx';
//        $order->shipping_cost='12.00';
//        $order->total_cost='755.87';
//        $order->ordered_at='';
//        $order->fulfilled_at='';
//        $order->notes='shop order with other stuff included: notes';
//        $order->vendor='1';
//        $order->order_status='1';
//        $order->ordered_by='1';
//        $order->entered_by='2';
//        $order->last_modified_by='2';
//        $order->save();
//        
//        $order = new InventoryOrder();      
//        $order->po_number='123-xxx';
//        $order->shipping_cost='12.00';
//        $order->total_cost='755.87';
//        $order->ordered_at='';
//        $order->fulfilled_at='';
//        $order->notes='shop order with other stuff included: notes';
//        $order->vendor='2';
//        $order->order_status='2';
//        $order->ordered_by='1';
//        $order->entered_by='2';
//        $order->last_modified_by='3';
//        $order->save();
    }
}


class InventoryOrderItemSeeder extends Seeder {
    public function run()
    {
        DB::table('inventory_order_items')->delete();
        
//        $item = new InventoryOrderItem();
//        $item->item_cost='2.99';
//        $item->qty_ordered='5';
//        $item->qty_received='7';
//        $item->inventory_id='1';
//        $item->order_id='1';
//        $item->intended_for='1';
//        $item->vendor_id='1';
//
//        $item->save();
//        
//        $item = new InventoryOrderItem();
//        $item->item_cost='23.99';
//        $item->qty_ordered='3';
//        $item->qty_received='2';
//        $item->inventory_id='2';
//        $item->order_id='1';
//        $item->intended_for='2';
//        $item->vendor_id='2';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='0.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='3';
//        $item->order_id='1';
//        $item->intended_for='3';
//        $item->vendor_id='1';
//
//        $item->save();
//        
//        $item = new InventoryOrderItem();
//        $item->item_cost='2.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='1';
//        $item->order_id='1';
//        $item->intended_for='1';
//        $item->vendor_id='1';
//
//        $item->save();
//        
//        $item = new InventoryOrderItem();
//        $item->item_cost='25.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='2';
//        $item->order_id='2';
//        $item->intended_for='2';
//        $item->vendor_id='2';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='9.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='3';
//        $item->order_id='2';
//        $item->intended_for='3';
//        $item->vendor_id='1';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='4.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='2';
//        $item->order_id='2';
//        $item->intended_for='2';
//        $item->vendor_id='2';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='222.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='3';
//        $item->order_id='2';
//        $item->intended_for='3';
//        $item->vendor_id='1';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='2.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='2';
//        $item->order_id='2';
//        $item->intended_for='2';
//        $item->vendor_id='2';
//
//        $item->save();
//
//        $item = new InventoryOrderItem();
//        $item->item_cost='1020.99';
//        $item->qty_ordered='5';
//        $item->qty_received='';
//        $item->inventory_id='3';
//        $item->order_id='2';
//        $item->intended_for='3';
//        $item->vendor_id='2';
//
//        $item->save();

    }
}
/*/////////////////////////////////////////////////////////////
Restock seeders
//////////////////////////////END////////////////////////////*/ 




class PagesSeeder extends Seeder
{
    public function run()
    {
        DB::table('pages')->delete();
        
        $pages = array(
            'home',
            'rentals',
            'shop',
            'events',
            'services',
            'blog',
            'contact',
        );
        
        //save each of these pages
        foreach($pages as $page) {
            $newPage = new Page();
            $newPage->title = ucwords($page);
            $newPage->slug = $page;
            if($page === 'home') {
                $newPage->is_home = 1;
            }
            $newPage->is_active = 1;
            $newPage->save();
        }
    }
}

class SettingsSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->delete();
        
        $settings = array(
            //General settings
            'core_site_name' => array('Test Name', null),
            'core_site_logo' => array(null, 1),
            //Contact settings
            'core_phone' => array('555-555-5555', null),
            'core_fax' => array('555-555-5556', null),
            'core_email' => array('info@ceres.com', null),
            'core_address' => array('100 Campus Center Seaside, CA 93955', null),
            //Hours settings
            'core_hours_monday' => array('9am - 5pm', null),
            'core_hours_tuesday' => array('9am - 5pm', null),
            'core_hours_wednesday' => array('9am - 5pm', null),
            'core_hours_thursday' => array('9am - 5pm', null),
            'core_hours_friday' => array('9am - 5pm', null),
            'core_hours_saturday' => array('9am - 5pm', null),
            'core_hours_sunday' => array('9am - 5pm', null),
            //Payment Gateway settings
            'core_publishable_key' => array('pk_test_UoK2T7qiQj0ETXKc7l4SM35V', null),
            'core_secret_key' => array('sk_test_KGQ7tvlApHYJX5nqIuFKEo3I', null),
            //Google settings
            'core_google_api_key' => array('', null),
            //Frontend settings
            'core_frontend_is_active' => array(null, 1),
        );
        
        //save each of these pages
        foreach($settings as $name => $value) {
            $setting = new Setting();
            $setting->name = $name;
            $setting->string_value = $value[0];
            $setting->number_value = $value[1];
            $setting->save();
        }
    }
}
