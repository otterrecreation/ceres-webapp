<?php

class Slide extends Eloquent {
    
    public static $rules = array(
        'title' => 'required',
        'file' => 'required',
        'slideshow' => 'required',
    );
    
    protected $table = 'slides';
    
}