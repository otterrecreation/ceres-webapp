<?php

class ServicesSetting extends Eloquent {
    
    public static $rules = array(
        'servicesSetting.availableServices' => 'required',
        'servicesSetting.canReserve' => 'required',
    );
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'service_settings';
}