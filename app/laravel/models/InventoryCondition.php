<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class InventoryCondition extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
		'condition' 	=> 'required|unique:inventory_categories',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_condition';
    
    protected $hidden = array('deleted_at');
    
    public function inventoryInstances() {
        return $this->hasMany('InventoryInstance', 'inventoryCondition');
    }
}