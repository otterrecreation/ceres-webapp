<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait; // part of soft delete

class InventoryOrderStatus extends Eloquent {
	
    use SoftDeletingTrait;  // part of soft delete
    
	public static $rules = array(
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_order_statuses';
    protected $hidden = array('deleted_at'); // Soft delete, removed from page but not DB.
    
    
    public function status() {
        return $this->belongsTo('InventoryOrder','order_status');
    }
}