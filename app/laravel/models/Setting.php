<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Setting extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array();
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';
    
    protected $hidden = array('deleted_at');
}