<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Services extends Eloquent {
	
    use SoftDeletingTrait;
    
    public static $rules = array(
        'name' => 'required',
        'description' => 'required',
        'hours' => 'required',
    );
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'services';
    
    protected $hidden = array('deleted_at');
}