<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class InventoryStatus extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
		'status' 	=> 'required|unique:inventory_categories',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_status';
    
    protected $hidden = array('deleted_at');
    
    public function inventoryInstances() {
        return $this->hasMany('InventoryInstance', 'inventoryStatus');
    }
}