<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class EventModel extends Eloquent {
    use SoftDeletingTrait;
    public static $rules = array(
        'name'                  => 'required',
        'description'           => 'required',
        'start'                 => 'required|date_format:Y-m-d h:i:s',
        'end'                   => 'required|date_format:Y-m-d h:i:s',
        'registrationDeadline'  => 'required|date_format:Y-m-d h:i:s'
    );
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'events';
    protected $hidden = array('deleted_at');
}