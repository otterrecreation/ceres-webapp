<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class InventoryCategory extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
		'category' => 'required|unique:inventory_categories,category',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_categories';
    
    protected $hidden = array('deleted_at');
    
    public function inventory() {
        return $this->hasMany('Inventory', 'inventoryCategory');
    }
}