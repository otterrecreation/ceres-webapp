<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Cart extends Eloquent {

    use SoftDeletingTrait;
    
    protected $table = 'cart';  
    
    protected $hidden = array('deleted_at');
    
    public function cartItems() {
        return $this->hasMany('CartItem', 'cart');
    }
    
}