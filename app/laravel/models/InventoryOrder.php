<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait; // part of soft delete

class InventoryOrder extends Eloquent {
	
    use SoftDeletingTrait;  // part of soft delete
    
	public static $rules = array(
		'po_number' 	=> 'required',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_orders';
    protected $hidden = array('deleted_at'); // Soft delete, removed from page but not DB.
    
    
    public function vendor() {
        return $this->belongsTo('InventoryOrderVendor', 'vendor');
    }

    public function status() {
        return $this->belongsTo('InventoryOrderStatus', 'order_status');
    }
    
    public function orderedBy() {
        return $this->belongsTo('User', 'ordered_by');
    }
    
    public function enteredBy() {
        return $this->belongsTo('User', 'entered_by');
    }
    
    public function lastModifiedBy() {
        return $this->belongsTo('User', 'last_modified_by');
    }    
    
    public function inventoryOrderList() {
        return $this->hasMany('InventoryOrderItem', 'order_id');
    }
    
}