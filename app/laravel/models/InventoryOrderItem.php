<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait; // part of soft delete

class InventoryOrderItem extends Eloquent {
	
    use SoftDeletingTrait;  // part of soft delete
    
	public static $rules = array(
        
        'item_cost' => 'required',
    );

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_order_items';
    protected $hidden = array('deleted_at'); // Soft delete, removed from page but not DB.
    
    

    
    public function order() {
        return $this->belongsTo('InventoryOrder','order_id');
    }
    
    public function user() {
        return $this->belongsTo('User', 'intended_for');
    }
    
    public function vendor() {
        return $this->belongsTo('InventoryOrderVendor', 'vendor_id');
    }
    
        public function inventory() {
        return $this->belongsTo('Inventory', 'inventory_id');
    }
}