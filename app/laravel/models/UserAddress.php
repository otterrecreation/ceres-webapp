<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class UserAddress extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
        'address_line_1'	=> 'required',
		'city'				=> 'required',
		'zip'				=> 'required',
		'state_province'	=> 'required',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_addresses';
    
    protected $hidden = array('deleted_at');
    
    public function user() {
        return $this->belongsTo('User');
    }
}