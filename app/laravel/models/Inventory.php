<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Inventory extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
		'name' 	=> 'required',
		'type' 	=> 'required',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory';
    
    protected $hidden = array('deleted_at');
    
    public function inventoryCategory() {
        return $this->belongsTo('InventoryCategory', 'inventoryCategory');
    }
    
    public function inventoryInstance() {
        return $this->hasMany('InventoryInstance', 'inventory');
    }
}