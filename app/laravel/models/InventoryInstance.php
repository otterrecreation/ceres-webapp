<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class InventoryInstance extends Eloquent {
	
    use SoftDeletingTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_instances';
    
    protected $hidden = array('deleted_at');
    
    public function inventory() {
        return $this->belongsTo('Inventory');
    }
}