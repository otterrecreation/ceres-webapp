<?php

class Media extends Eloquent {
    
    public static $rules = array(
        'title' => 'required',
    );
    
    protected $table = 'media';
    
}