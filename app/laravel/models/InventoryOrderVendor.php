<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait; // part of soft delete

class InventoryOrderVendor extends Eloquent {
	
    use SoftDeletingTrait;  // part of soft delete
    
	public static $rules = array(
		'verndor_name' => 'required',
		'rep_name' 	  => 'required',
		'phone'       => 'required',
		'account_number'   => 'required',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inventory_order_vendors';
    protected $hidden = array('deleted_at'); // Soft delete, removed from page but not DB.
    
    
    public function vendor() {
        return $this->belongsTo('InventoryOrder','vendor');
    }
}