<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface {

	use UserTrait, RemindableTrait, HasRole, SoftDeletingTrait;

	//Validation rules
	public static $rules = array(
		'name'                  => 'required|min:4',
		'username'              => 'required|email|unique:oauth_users,username',
		'password'              => 'required|alpha_num|between:6,20|confirmed',
		'password_confirmation' => 'required|alpha_num|between:6,20',
	);

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'oauth_users';

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
     protected $hidden = array('password', 'remember_token', 'deleted_at', 'created_at', 'updated_at');
	
	 public function addresses() {
	 	return $this->hasMany('UserAddress', 'user');
	 }
	
	 public function billingInfo() {
	 	return $this->hasMany('UserBillingInfo', 'user');
	 }
}