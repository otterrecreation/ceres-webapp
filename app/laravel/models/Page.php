<?php

class Page extends Eloquent {
    
    public static $rules = array(
        'title' => 'required',
        'show_title' => 'required',
        'is_active' => 'required',
    );
    
    protected $table = 'pages';
    
}