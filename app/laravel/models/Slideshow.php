<?php

class Slideshow extends Eloquent {
    
    public static $rules = array(
        'title' => 'required',
    );
    
    protected $table = 'slideshows';
    
    public function slides() {
        return $this->hasMany('Slide', 'slideshow');
    }
    
    public function pages() {
        return $this->hasMany('Page', 'slideshow');
    }
    
}