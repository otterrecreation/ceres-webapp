<?php

use Zizaco\Entrust\EntrustRole;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Role extends EntrustRole
{
    use SoftDeletingTrait;
    
    protected $hidden = array('deleted_at');
}