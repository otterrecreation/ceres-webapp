<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class UserBillingInfo extends Eloquent {
	
    use SoftDeletingTrait;
    
	public static $rules = array(
		'stripe_id'	=> 'required',
	);
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_billing_info';
    
    protected $hidden = array('deleted_at');
    
    public function user() {
        return $this->belongsTo('User');
    }
}