<?php

class Blog extends Eloquent {
    
    public static $rules = array(
        'is_active' => 'required',
    );
    
    protected $table = 'blogs';
    
}