App.ApplicationRoute = Ember.Route.extend({
    //allows us to set the title in the tab of the browser
    model: function() {
        return this.store.find('setting');
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('pages').set('model', this.store.find('page'));
        this.controllerFor('cart').set('model', this.store.find('cartItem'));
        
    },
    
    promptLogin: function(transition) {
        $(function() {
            $('#login-dropdown').dropdown('toggle');
        });
        
        var applicationController = this.controllerFor('application');
        applicationController.set('attemptedTransition', transition);
        this.transitionTo('home');
    },

    actions: {
        refresh: function() {
            this.refresh();
        },
        
        //open the admin up
        openAdmin: function() {
            this.send('closeAdmin');
            
            window.adminSide = window.open(rootURL + 'admin');
        },
        
        closeAdmin: function() {
            if(typeof window.adminSide !== 'undefined') {
                console.log('the frontend side has already been opened');
                
                if(typeof window.adminSide.close === 'function') {
                    window.adminSide.close();
                }
            } else {
                
                if(!Ember.isEmpty(window.opener)) {
                    console.log('the frontend side opened the admin side');
                    window.adminSide = window.opener;
                    window.adminSide.close();
                }
            }
        },
        
        frontendLogout: function() {
            
            this.send('closeAdmin');
            
            window.location = rootURL + 'api-proxy/logout'; //now that we've closed the children window we can logout
            
        },
        
        //catch all errors
        error: function(reason, transition) {
          
            switch(reason.status) {
                case 401:
                    //user is not logged in
                    this.promptLogin(transition);

                    break;
                case 403:

                    //token is good (could be client token) but the user is not allowed
                    this.redirectToHome(reason);

                    break;
                case 404:

                    //resource not found


                    break;
                case 405:

                    //the method used for the request is not allowed for the route requested


                    break;
                case 407:

                    //the api-proxy


                    break;
                case 440:

                    //login session has expired


                    break;
                case 498:

                    //the token must be an invalid one.


                    break;
                case 499:

                    //no token was submitted


                    break;
                default:

                    //unhandled error here


                    break;
            }
            
        }
    }
});