App.ApplicationView = Ember.View.extend({
	templateName: 'application',
	
	didInsertElement: function() {
		
	}
});

App.ModalView = Ember.View.extend(Ember.Evented, {
    
    routeOnClose: 'admin.inventory',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute(self.get('routeOnClose'));
        });
        
        this.get('controller').on('modalActionDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('modalActionDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
});

App.SlideoutView = Ember.View.extend({
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
});