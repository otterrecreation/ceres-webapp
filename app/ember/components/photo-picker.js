App.PhotoPickerComponent = Ember.Component.extend({
    
    photoPicked: function() {
//        console.log('photo picked changed');
        return this.get('targetObject.photoPicked');
    }.property('targetObject.photoPicked'),
    
    selectedMedia: function() {
        
        var files = this.get('media');
        var photoPicked = this.get('targetObject.photoPicked');
        
        if(typeof photoPicked !== 'undefined') {
            files.forEach(function(file) {
                if(photoPicked.get('id') == file.get('id')) {
                    file.set('isSelected', true);
                } else {
                    file.set('isSelected', false);
                }
            });
        }
        
        return files;
        
    }.property('media', 'targetObject.photoPicked'),
    
    click: function(e) {
        //make sure to only pass the photoPicked action when it's an input that is selected
        if(typeof Ember.$(e.target).attr('name') !== 'undefined') {
//            console.log(e.target);
            var file_id =  Ember.$(e.target).val();
            this.get('targetObject').set('photoPicked', this.get('targetObject.store').find('file', file_id)); 
        }
    }
});