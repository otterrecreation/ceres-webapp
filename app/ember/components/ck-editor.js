App.CkEditorComponent = Ember.Component.extend({
    
    autoSaveInterval: 2, //in seconds
    autosave: true,
    
    didInsertElement: function() {
        
        var component = this;
        var autoSaveInterval = this.get('autoSaveInterval') * 1000;
        
//        CKEDITOR.plugins.add( 'emberImageSelect', {
//            init: function( editor ) {
//                // Plugin logic goes here...
//                editor.addCommand( 'emberImageSelectDialog', new CKEDITOR.dialogCommand( 'emberImageSelectDialog' ) );
//            }
//        });
        
        var ckEditor = CKEDITOR.replace('ck-editor-component', {
//            extraPlugins : 'emberImageSelect',
//            toolbar : [
//                ['Source', '-', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
//                ['About','-','emberImageSelect']
//            ],
            on: {
                instanceReady: function() {
                    
                    var self = this;
                    // How to autosave
                    if(component.get('autosave')) {
                        var buffer = CKEDITOR.tools.eventsBuffer( autoSaveInterval, function() {
                            component.set('value', self.getData());
                        });         

                        this.on( 'change', buffer.input );
                    } else {
                        this.on( 'change', function() {
                            component.set('value', self.getData());
                        });
                    }
                }
            }
        });
        
        
    }
});