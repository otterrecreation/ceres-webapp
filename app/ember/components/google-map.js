App.GoogleMapComponent = Ember.Component.extend({
    
    willInsertElement: function() {
        if(Ember.isEmpty(this.get('address')) && (Ember.isEmpty(this.get('lag')) || Ember.isEmpty('lng'))) {
            
            this.set('shouldRender', false);
        } else {
            
            this.set('shouldRender', true);
        }
    },
    
    didInsertElement: function() {
        
        if(this.get('shouldRender')) {
            var self = this;
            var $googleMap = Ember.$('#' + this.elementId);
                $googleMap.addClass('google-map');

            if(this.get('class')) {
                $googleMap.addClass(this.get('class'));
            }

            //let's make sure that we have included google maps
            if(typeof google !== 'undefined') {

                if(this.get('latLng') || (this.get('lat') && this.get('lng'))) {
                    if(!this.get('latLng')) {
                        this.set('latLng', { lat: this.get('lat'), lng: this.get('lng') });
                    }
                    
                    this.send('initializeMap');
                } else {
                    //we need to asynchronously
                    this.send('initializeMapWithAddress');
                }
            }
        }
    },
    
    actions: { 
        
        initializeMap: function() {
            var mapOptions = {
                center: this.get('latLng'),
                zoom: this.getWithDefault('zoom', 13)
            };

            var map = new google.maps.Map(document.getElementById(this.elementId), mapOptions);

            //add a marker
            var marker = new google.maps.Marker({
                position: this.get('latLng'),
                map: map,
                title: this.getWithDefault('title', '')
            });  
        },

        initializeMapWithAddress: function() {

            var self = this;
            
            var geocoder = new google.maps.Geocoder();

            geocoder.geocode(
                { address: this.get('address')},
                function(results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {
                        self.set('latLng', results[0].geometry.location);
                        self.send('initializeMap');
                    }
                }
            );

        }
    }
    
});