App.BootstrapDatetimepickerComponent = Ember.Component.extend({
    
    classNames: ['input-group', 'date'],
    
    stepping: 5,
    sideBySide: false,
    allowInputToggle: true, //normally datetimepicker isn't set to true. i don't agree with this default
    minDate: false, 
    maxDate: false,
    
    didInsertElement: function() {
        
        var self = this;
        
        Ember.$('#' + this.get('elementId')).datetimepicker({
            stepping: self.get('stepping'),
            sideBySide: self.get('sideBySide'),
            minDate: self.get('minDate'),
            maxDate: self.get('maxDate')
        });
        
        Ember.$('#' + self.get('elementId') + ' input').click(function(event){
           Ember.$('#' + self.get('elementId')).data("DateTimePicker").show();
        });
    }
});