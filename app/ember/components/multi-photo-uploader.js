App.MultiPhotoUploaderComponent = Ember.Component.extend({
    
    didInsertElement: function() {
        var self = this;
        //initialize the dropzone
        Dropzone.options.adminMediaDropzone = false; //don't autoload dropzone. update it programatically
        
        //only allow image files
        var mediaDropzone = new Dropzone('#admin-media-dropzone', { url: baseURL + '/api-proxy/files', acceptedFiles: 'image/*' });
        
        mediaDropzone.on('success', function(response) {
            console.log(JSON.parse(response.xhr.response).file);
            self.get('targetObject.store').push('file', JSON.parse(response.xhr.response).file);
        });
        
        mediaDropzone.on('error', function(response) {
            console.log(response);
        });
    }
    
});