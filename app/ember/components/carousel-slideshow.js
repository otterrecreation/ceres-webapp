App.CarouselSlideshowComponent = Ember.Component.extend({
    
    slideHeight: 200,
    backgroundSize: 'cover',
    
    //allows us to format the bootstrap slideshow properly
    indexedSlides: function() {
        var slides = this.get('slides');
        slides.forEach(function(slide, i) {
            console.log(i);
            slide.set('index', i);
            if(i === 0) {
                slide.set('isFirst', true);
            } else {
                slide.set('isFirst', false);
            }
        });
        
        return slides;
        
    }.property('slides'),
    
    didInsertElement: function() {
        
        var self = this;
        
        window.Carousel = Ember.$('.carousel').carousel();  //initialize the homepage slideshow
        
    }
});