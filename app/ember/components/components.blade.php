<script type="text/x-handlebars" id="components/bootstrap-modal">
    <div class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">{{title}}</h4>
          </div>
          <div class="modal-body">
            {{yield}}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" {{action 'ok'}}>Ok</button>
          </div>
        </div>
      </div>
    </div>
</script>

<script type="text/x-handlebars" id="components/switch-box">
    <div class="onoffswitch">
        <label class="onoffswitch-label">
        <input type="checkbox" class="onoffswitch-checkbox" {{bind-attr checked=checked}}>
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
    </div>
</script>


<script type="text/x-handlebars" id="components/gravatar-image">
    <img {{bind-attr class="class"}} {{bind-attr src="gravatarUrl"}}>
</script>


<script type="text/x-handlebars" id="components/multi-photo-uploader">
    <div id="admin-media-dropzone" {{bind-attr class=":dropzone class"}}></div>
</script>

<script type="text/x-handlebars" id="components/single-photo-uploader">
    
    <div class="dropdown">
        <div id="dropzone-selector" class="dropzone dropzone-single-photo dropdown-toggle" data-toggle="dropdown">
            {{#if value.absoluteThumbnailSrc}}
            {{log 'has absolute src'}}
            <img id="dropzone-preview" class="img-responsive" {{ bind-attr src="value.absoluteThumbnailSrc" }} />
            {{ else }}
            {{log 'does not have src'}}
            <img class="img-responsive" src="<% URL::to('/') %>/media/images/photo_placeholder.png" />
            {{/if}}
            <div class="dz-default dz-message">
                <span><strong>Select a photo</strong></span>
            </div>
        </div>
        <ul class="dropdown-menu" role="menu">
            <li class="text-center"><button id="dropzone-clickable" class="btn btn-primary">Upload a new photo</button></li>
            {{#if selectedMedia}}
            <li class="divider"></li>
            <li class="dropdown-header">Select a photo</li>
            <li>
                <div class="photo-picker">
                {{#each file in selectedMedia }}
                    <label>
                        <input name="photo-picker" type="radio" {{bind-attr value=file.id}} {{bind-attr checked=file.isSelected}}>
                        <img class="img-responsive" 
                             {{bind-attr src="file.absoluteThumbnailSrc"}} 
                             {{bind-attr title="file.title"}} 
                             {{bind-attr alt="altText"}} />
                    </label>
                {{/each}}
                </div>
            </li>
            {{/if}}
        </ul>
    </div>
    
</script>


<script type="text/x-handlebars" id="components/photo-picker">
    <div class="photo-picker">
    {{#each file in selectedMedia }}
        <label>
            <input name="photo-picker" type="radio" {{bind-attr value=file.id}} {{bind-attr checked=file.isSelected}}>
            <img class="img-responsive" 
                 {{bind-attr src="file.absoluteThumbnailSrc"}} 
                 {{bind-attr title="file.title"}} 
                 {{bind-attr alt="altText"}} />
        </label>
    {{else}}
        <p class="text-center">No images</p>
    {{/each}}
    </div>
</script>

<script type="text/x-handlebars" id="components/media-img">
    {{#if file.absoluteSrc}}
    <img {{bind-attr src="file.absoluteSrc"}} {{bind-attr alt="file.altText"}} {{bind-attr title="file.title"}} {{bind-attr class="class"}} />
    {{ else }}
        {{#if placeholder }}
            <img {{ bind-attr src="placeholder" }} {{ bind-attr class="class" }} />
        {{ else }}
            <img src="<% URL::to('/') %>/media/images/photo_placeholder.png" alt="Photo placeholder" title="Photo placeholder" {{bind-attr class="class"}} />
        {{/if}}
    {{/if}}
</script>

<script type="text/x-handlebars" id="components/media-img-thumb">
    {{#if file.absoluteThumbnailSrc}}
    <img {{bind-attr src="file.absoluteThumbnailSrc"}} {{bind-attr alt="file.altText"}} {{bind-attr title="file.title"}} {{bind-attr class="class"}}>
    {{ else }}
        {{#if placeholder }}
            <img {{ bind-attr src="placeholder" }} {{ bind-attr class="class" }} />
        {{ else }}
            <img src="<% URL::to('/') %>/media/images/photo_placeholder.png" alt="Photo placeholder" title="Photo placeholder" {{bind-attr class="class"}} />
        {{/if}}
    {{/if}}
</script>


<script type="text/x-handlebars" id="components/carousel-slideshow">
    {{#if slides}}
        <div id="homepage-slideshow" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            {{#each slide in indexedSlides}}
            <li data-target="#homepage-slideshow" {{ bind-attr data-slide-to="slide.index" }} {{bind-attr class="slide.isFirst:active"}}></li>
            {{/each}}
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            {{#each slide in indexedSlides}}
            <div {{bind-attr class=":item slide.isFirst:active"}}>
              <div {{ bind-style background="slide.file.backgroundSrc" }}>
                  {{#if slide.caption}}
                  <div class="carousel-caption">
                      {{#if slide.title}}
                      <h2>{{ slide.title }}</h2>
                      {{/if}}
                      <p class="lead">{{ slide.caption }}</p>
                  </div>
              {{/if}}
              </div>
            </div>
            {{/each}}
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#homepage-slideshow" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#homepage-slideshow" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    {{/if}}
</script>


<script type="text/x-handlebars" id="components/ck-editor">
    {{ textarea id="ck-editor-component" value=value }}
</script>


<script type="text/x-handlebars" id="components/bootstrap-datetimepicker">
    {{input type='text' class="form-control" value=value }}
    <span class="input-group-addon">
        <span class="fa fa-calendar-o"></span>
    </span>
</script>