App.SinglePhotoUploaderComponent = Ember.Component.extend({
    
    selectedMedia: function() {
        
        var files = this.get('media');
        var photoPicked = this.get('value');
//        console.log(photoPicked);
        
        if(!Ember.isNone(photoPicked)) {
            files.forEach(function(file) {
                if(photoPicked.get('id') == file.get('id')) {
                    file.set('isSelected', true);
                } else {
                    file.set('isSelected', false);
                }
            });
        }
        
        return files;
        
    }.property('media', 'value'),
    
    click: function(e) {
        //make sure to only pass the photoPicked action when it's an input that is selected
        if(!Ember.isNone(Ember.$(e.target).attr('name'))) {
//            console.log(e.target);
            var file_id =  Ember.$(e.target).val();
//            console.log(this.get('targetObject.store').find('file', file_id));
            var self = this;
            this.get('targetObject.store').find('file', file_id).then(function(file) {
                self.set('value', file);
            });
        }
    },
    
    didInsertElement: function() {
        
        $('.dropdown-menu a.removefromcart').click(function(e) {
            e.stopPropagation();
        });
        
        var self = this;
        //initialize the dropzone
        Dropzone.options.dropzoneSelector = false; //don't autoload dropzone. update it programatically
        
        //only allow image files
        var mediaDropzone = new Dropzone(
            '#dropzone-selector', 
            { 
                maxFiles: 1,
                url: baseURL + '/api-proxy/files', 
                acceptedFiles: 'image/*',
                clickable: '#dropzone-clickable',
                thumbnail: function() {},
                addedfile: function(file) {},
                thumbnail: function(file, dataUrl) {},
                uploadprogress: function(file, progress, bytesSent) {}
            }
        );
        
        //prevent more than one file from being loaded at a time in the preview
        mediaDropzone.on('addedfile', function() {
            if(this.files[1] != null) {
                this.removeFile(this.files[0]);
            }
        });
        
        mediaDropzone.on('success', function(response) {
            console.log(response);
            console.log(JSON.parse(response.xhr.response).file);
            var file = self.get('targetObject.store').push('file', JSON.parse(response.xhr.response).file);
            self.set('value', file);
        });
        
        mediaDropzone.on('error', function(response) {
            console.log(response);
        });
    }
    
});
