//EMBER MODEL SETTINGS AND INITIAL SETUP
var attr = DS.attr,
    hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;


App.ApplicationSerializer = DS.RESTSerializer.extend({
    serializeIntoHash: function(hash, type, record, options) {
        Ember.merge(hash, this.serialize(record, options));
    }
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
    host: window.baseURL,
    namespace: 'api-proxy',

    ajax: function(url, type, hash) {
      if (Ember.isEmpty(hash)) hash = {};
      if (Ember.isEmpty(hash.data)) hash.data = {};

      //so we can spoof the request method so laravel knows how to interpret it
      if(type !== 'GET' && type !== 'POST') {
          hash.data._method = type;
          type = 'POST';
      }

      Ember.Logger.debug('DEBUG: Method type:', type);
      Ember.Logger.debug('DEBUG: Hash information: ', hash);

      return this._super(url, type, hash);
    },

    ajaxError: function(jqXHR) {

        console.log(jqXHR);
        var defaultAjaxError = this._super(jqXHR);
        if (jqXHR) {
            switch(jqXHR.status) {
                case 401:
                return jqXHR;
            }
        }
        return defaultAjaxError;
    }
});

//comment when not testing-------------------
/*App.ApplicationAdapter = DS.FixtureAdapter;
App.ApplicationAdapter = DS.FixtureAdapter.extend({
    queryFixtures: function(fixtures, query, type) {
        console.log('called from query fixtures');
        return fixtures.filter(function(fixture) {
            for(key in query) {
                if(fixture[key] !== query[key]) { return false; }
            }
            console.log(query);
            return true;
        });
    }
});*/

//help to override the faulty DS model
DS.Model.reopenClass({

    becameError: function() {
      alert('there was an error!');
    },

    becameInvalid: function(errors) {
      alert("Record was invalid because: #{errors}");
    }
});