<script type="text/x-handlebars" id='_main_menu'>
    <div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                {{#link-to 'home' class="navbar-brand"}}
                    {{#if site_logo}}
                        {{ img-media-thumb file=site_logo }}
                    {{ else }}

                        {{#if siteName.string_value }}
                            {{ siteName.string_value }}
                        {{ else }}
                            Home
                        {{/if}}

                    {{/if}}
                {{/link-to}}
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".main-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <ul class="nav navbar-collapse navbar-nav main-navbar-collapse collapse" aria-expanded="false">
                {{partial 'main_menu_items'}}
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
                {{#link-to 'cart' tagName='li'}}
                    <a href="">
                        <small>
                            <span class="badge badge-danger">{{ controllers.cart.model.length }}</span>
                            <i class="fa fa-shopping-cart"></i> Cart
                        </small>
                    </a>
                {{/link-to}}
            {{#if currentUser}}
                <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><small><i class="fa fa-user"></i> {{ currentUser.username }} <span class="caret"></span></small></a>
                  <ul class="dropdown-menu" role="menu">
                    {{#link-to 'account' tagName='li'}}<a href="#">Account</a>{{/link-to}}
                    {{#if canViewAdmin }}
                      <li><a href="#" {{action 'openAdmin'}}>Admin</a></li>
					{{/if}}
                    <li class="divider"></li>
                    <li><a href="#" {{ action 'frontendLogout' }}>Logout</a></li>
                  </ul>
                </li>
            {{else}}
                {{#link-to 'login' tagName='li'}}
                    <a href="#">
                        <small>
                            Login
                        </small>
                    </a>
                {{/link-to}}
            {{/if}}
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</script>

<script type="text/x-handlebars" id="_main_menu_items">
   
    {{#each page in pages}}
        {{#unless page.is_home}}
            {{#if page.is_active}}
                {{#link-to page.slug tagName='li'}}<a href="#">{{ page.title }}</a>{{/link-to}}
            {{/if}}
        {{/unless}}
    {{/each}}
    
@if(isset($pages))
@forelse($pages as $page)
    {{#link-to '<% $page->slug %>' tagName='li'}}<a href="#"><% $page->title %></a>{{/link-to}}
@empty
@endforelse    
@endif
</script>

<script type="text/x-handlebars" id='_admin_menu'>
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="#" {{action 'openFrontend'}}>Ceres <i class="fa fa-external-link"></i></a>
            </li>
            <li class="sidebar-profile">
                {{#link-to 'admin.account'}}{{gravatar-image class="img-responsive img-circle" email=currentUser.username size="200"}}{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.dashboard'}}<i class="fa fa-dashboard"></i> Dashboard{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.inventory'}}<i class="fa fa-cubes"></i> Inventory{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.inventoryOrders'}}<i class="fa fa-truck"></i> Restock{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.events'}}<i class="fa fa-calendar-o"></i> Events{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.services'}}<i class="fa fa-list"></i> Services{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.blog'}}<i class="fa fa-copy"></i> Blog{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.media'}}<i class="fa fa-picture-o"></i> Media{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.slideshows'}}<i class="fa fa-archive"></i> Slideshows{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.users'}}<i class="fa fa-users"></i> Users{{/link-to}}
            </li>
            <li>
                {{#link-to 'admin.settings'}}<i class="fa fa-cog"></i> Settings{{/link-to}}
            </li>
            <li>
                <a href="#" {{ action 'adminLogout' }}>Logout</a>
            </li>
        </ul>
    </div>
</script>

<script type="text/x-handlebars" id="_footer">
    <footer id="footer" class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                      <p class="text-muted">{{ siteName.string_value }} © {{ thisYear }} all rights reserved</p>  
                </div>
                <div class="col-xs-8">
                   <ul class="text-muted pull-right">
                    {{#link-to 'home' tagName='li'}}<a href="#">Home</a>{{/link-to}}
                    {{partial 'main_menu_items'}}
                   </ul>
                </div>
            </div>
        </div>
    </footer>
</script>

<script type="text/x-handlebars" id="loading">

    <div class="loading">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-red-only">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div><div class="gap-patch">
                <div class="circle"></div>
              </div><div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
        </div>
    </div>

</script>

<script type="text/x-handlebars" id="admin/loading">

    <div class="loading">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-red-only">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div><div class="gap-patch">
                <div class="circle"></div>
              </div><div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
        </div>
    </div>

</script>


