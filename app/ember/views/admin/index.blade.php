<script type="text/x-handlebars" id="admin">
    <button class="btn btn-default visible-xs visible-sm" {{action "toggleAdminMenu"}}><i class="fa fa-bars"></i></button>
    {{ outlet }}
</script>
