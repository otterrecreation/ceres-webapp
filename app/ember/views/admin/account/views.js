App.AdminAccountBillinginfoCreateView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountAddressCreateView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountBillinginfoEditView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountAddressEditView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});