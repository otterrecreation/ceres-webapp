<script type="text/x-handlebars" id="admin/account/billinginfo/create">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Add a new card
            </h3>
          </div>
        <?/***********************************************
        *   Add form for inventory, broken up in a table
        *
        ***********************************************/?>
        <form id="form-add-billing-info" class="form" {{action 'addBillinginfo' on='submit'}}>  
          <div class="modal-body">                      
		   	   <div class="row">
		   	   		<div class="payment-errors"></div>
		   	   </div>
		   	<div class="form-group">
		   	   <div class="row">
		   	   		<div class="col-xs-12">
		   	   		    <label>Cardholder</label>
		   	   			<input type="text" data-stripe="name" class="form-control" />
		   	   		</div>
		   	   </div>
		   	</div>
		   	<div class="form-group">
			   <div class="row">
						<div class="col-xs-8">
						    <label>Card Number</label>
							<input type="text" data-stripe="number" class="form-control" />
						</div>
						<div class="col-xs-4">
						    <label>CVC</label>
							<input type="text" data-stripe="cvc" class="form-control" />
						</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
						<div class="col-xs-6">
						    <label>Exp. Month</label>
							<input type="text" data-stripe="exp_month" class="form-control" />
						</div>
						<div class="col-xs-6">
						    <label>Exp. Year</label>
							<input type="text" data-stripe="exp_year" class="form-control" />
						</div>

			    </div>
			</div>
                      
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form> 
        </div>
      </div>
    </div>	
	
</script>
