App.AdminAccountRoute = Ember.Route.extend({
    
    model: function() {
        
		var currentUser = this.controllerFor('application').get('currentUser');
		return this.store.find('user', currentUser.id);
        
    }
    
});

App.AdminAccountAddressEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userAddress', params.address_id);
	}
});

App.AdminAccountBillinginfoEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userBillingInfo', params.billing_id);
	}
});