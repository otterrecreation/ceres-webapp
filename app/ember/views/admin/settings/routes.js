App.AdminSettingsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('setting');  
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('pages').set('model', this.store.find('page'));
    },
    
    actions: {
        updateGeneral: function() {
            var controller = this.get('controller');
            
            var data = controller.getProperties('coreSiteName');
            
            this.send('saveSettings', data);
            
        },
        
        updateContact: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('corePhone',
                                                'coreFax',
                                                'coreEmail',
                                                'coreAddress');
            
            this.send('saveSettings', data);
        },
        
        updateHours: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('coreHoursMonday',
                                                'coreHoursTuesday',
                                                'coreHoursWednesday',
                                                'coreHoursThursday',
                                                'coreHoursFriday',
                                                'coreHoursSaturday',
                                                'coreHoursSunday');
            
            this.send('saveSettings', data);
        },
        
        updatePayment: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('corePublishableKey',
                                                'coreSecretKey');
            
            this.send('saveSettings', data);
        },
        
        updateGoogle: function() {
            var  controller = this.get('controller');
            var data = controller.getProperties('coreGoogleApiKey');
            
            this.send('saveSettings', data);
        },
        
        saveSettings: function(data) {
            for(var datum in data) {
                //data[datum] should be model instances
                data[datum].save().then(function() {}, function(response) {
                    data[datum].rollback();
                });
            }
            
            if(typeof window.frontendSide !== 'undefined') {
                this.send('closeFrontend'); //we want to close it so that it can be refreshed properly...
            }
        }
    }
    
});