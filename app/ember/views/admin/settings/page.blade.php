<script type="text/x-handlebars" id="admin/settings/page">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header modal-emphasis">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Edit Page: {{ title }}
            </h3>
          </div>
       
          <div class="modal-body">
                        
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="button" class="btn btn-primary" {{ action 'saveItem' }}>Save</button>
          </div>
        </div>
      </div>
    </div>
</script>