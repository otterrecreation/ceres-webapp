App.AdminSettingsController = Ember.ArrayController.extend({
    
    needs: ['media', 'pages'],
    media: Ember.computed.alias('controllers.media'),
    pages: Ember.computed.alias('controllers.pages'),
    
    //GRAB EACH SETTING TO BE MODIFIED
    
    //General
    coreSiteName: function() { return this.get('model').filterBy('name', 'core_site_name').get('firstObject') }.property('model'),
    coreSiteLogo: function() { return this.get('model').filterBy('name', 'core_site_logo').get('firstObject') }.property('model'),
    
    //Contact Information
    corePhone: function() { return this.get('model').filterBy('name', 'core_phone').get('firstObject') }.property('model'),
    coreFax: function() { return this.get('model').filterBy('name', 'core_fax').get('firstObject') }.property('model'),
    coreEmail: function() { return this.get('model').filterBy('name', 'core_email').get('firstObject') }.property('model'),
    coreAddress: function() { return this.get('model').filterBy('name', 'core_address').get('firstObject') }.property('model'),
    
    //Hours of Operation
    coreHoursMonday: function() { return this.get('model').filterBy('name', 'core_hours_monday').get('firstObject') }.property('model'),
    coreHoursTuesday: function() { return this.get('model').filterBy('name', 'core_hours_tuesday').get('firstObject') }.property('model'),
    coreHoursWednesday: function() { return this.get('model').filterBy('name', 'core_hours_wednesday').get('firstObject') }.property('model'),
    coreHoursThursday: function() { return this.get('model').filterBy('name', 'core_hours_thursday').get('firstObject') }.property('model'),
    coreHoursFriday: function() { return this.get('model').filterBy('name', 'core_hours_friday').get('firstObject') }.property('model'),
    coreHoursSaturday: function() { return this.get('model').filterBy('name', 'core_hours_saturday').get('firstObject') }.property('model'),
    coreHoursSunday: function() { return this.get('model').filterBy('name', 'core_hours_sunday').get('firstObject') }.property('model'),
    
    //Payment Gateway
    corePublishableKey: function() { return this.get('model').filterBy('name', 'core_publishable_key').get('firstObject') }.property('model'),
    coreSecretKey: function() { return this.get('model').filterBy('name', 'core_secret_key').get('firstObject') }.property('model'),
    
    //Google API
    coreGoogleApiKey: function() { return this.get('model').filterBy('name', 'core_google_api_key').get('firstObject') }.property('model'),
    
    //FONTEND
    coreFrontendIsActive: function() { return this.get('model').filterBy('name', 'core_frontend_is_active').get('firstObject') }.property('model'),
    
    actions: {
        toggleActiveFrontend: function(value, object) {
            object.set('number_value', value).save().then(function() {}, function() { object.rollback(); });
        },
        
        toggleActivePage: function(value, object) {
            object.set('is_active', value).save().then(function() {}, function() { object.rollback(); }); 
        }
    }
    
});

App.AdminSettingsPageController = Ember.ObjectController.extend(Ember.Evented, {

});