App.AdminSettingsView = Ember.View.extend({
    
    didInsertElement: function() {
        
        $masonryContainer = Ember.$('.masonry-settings');
        $masonryContainer.imagesLoaded(function() {
            $masonryContainer.masonry({
              columnWidth: '.item',
              itemSelector: '.item'
            });
        });
    },
    
    willClearRender: function() {
        
    }
    
});

App.AdminSettingsPageView = App.ModalView.extend({ routeOnClose: 'admin.settings' });