<script type="text/x-handlebars" id="admin/settings">
    <h1>Settings</h1>         
    <div class="masonry-settings row">
       <div class="item col-xs-12 col-sm-6 col-md-4">
            <div class="panel well col-xs-12">
               <h3>General</h3>
                <br>
                <div class="form-group">
                    <label>Site name</label>
                    {{input type="text" value=coreSiteName.string_value class="form-control"}}
                </div>
                <div class="form-group">
                    <label>Site logo</label>
                    {{ single-photo-uploader media=media value=siteLogo }}
                </div>
                <div class="form-group">
                    <button class="btn btn-primary pull-right" {{ action 'updateGeneral' }}>Save</button>
                   <div class="clearfix"></div>
                </div>
           </div>
        </div>
        <div class="item col-xs-12 col-sm-6 col-md-4">
           <div class="panel well col-xs-12">
               <h3>Contact Information</h3>
               <p><em>Information to be displayed on the contact page.</em></p>
               <br>
               <div class="form-group">
                   <label>Phone</label>
                    {{input type="text" value=corePhone.string_value class="form-control"}}
               </div>
               <div class="form-group">
                   <label>Fax</label>
                    {{input type="text" value=coreFax.string_value class="form-control"}}
               </div>
               <div class="form-group">
                   <label>Email</label>
                    {{input type="text" value=coreEmail.string_value class="form-control"}}
               </div>
               <div class="form-group">
                    <label>Address</label>
                    {{input type="text" value=coreAddress.string_value class="form-control"}}
               </div>
               <div class="form-group">
                   <button class="btn btn-primary pull-right" {{ action 'updateContact' }}>Save</button>
                   <div class="clearfix"></div>
               </div>
            </div>
        </div>
        <div class="item col-xs-12 col-sm-6 col-md-4">
            <div class="panel well col-xs-12">
                <h3>Hours of Operation</h3>
                <p><em>Leave spaces blank for days you will not be operating. </em></p>
                <br>
                <div class="form">
                    <div class="form-group">
                        <div class="row">
                           <div class="col-xs-12">
                                <label>Monday</label>
                                {{input type="text" value=coreHoursMonday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Tuesday</label>
                                {{input type="text" value=coreHoursTuesday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Wednesday</label>
                                {{input type="text" value=coreHoursWednesday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Thursday</label>
                                {{input type="text" value=coreHoursThursday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Friday</label>
                                {{input type="text" value=coreHoursFriday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Saturday</label>
                                {{input type="text" value=coreHoursSaturday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>Sunday</label>
                                {{input type="text" value=coreHoursSunday.string_value class="form-control"}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                           <div class="col-xs-12">
                               <button class="btn btn-primary pull-right" {{ action 'updateHours' }}>Save</button>
                               <div class="clearfix"></div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="item col-xs-12 col-sm-6 col-md-4">
            <div class="panel well col-xs-12">
                <h3>Payment Gateway</h3>
                <p><em>Stripe is a simple and secure system for taking payments. No payment processing occurs on your server. Setting up a Stripe account is free at <a href="https://dashboard.stripe.com/register" target="_blank">dashboard.stripe.com/register</a></em></p>
                <br>
                <div class="form-group">
                    <label>Stripe Publishable Key</label>
                    {{input type="text" value=corePublishableKey.string_value class="form-control"}}
                </div>
                <div class="form-group">
                    <label>Stripe Secret Key</label>
                    {{input type="text" value=coreSecretKey.string_value class="form-control"}}
                </div>
                <div class="form-group">
                    <div class="row">
                       <div class="col-xs-12">
                           <button class="btn btn-primary pull-right" {{ action 'updatePayment' }}>Save</button>
                           <div class="clearfix"></div>
                       </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="item col-xs-12 col-sm-6 col-md-4">
            <div class="panel well col-xs-12">
                <h3>Frontent Pages
                    <div class="pull-right">
                        <small>Frontend: </small>
                        {{#custom-select value=coreFrontendIsActive.number_value class="form-control form-override" action='toggleActiveFrontend' object=coreFrontendIsActive }}
                            {{#custom-option value=1}}Active{{/custom-option}}
                            {{#custom-option value=0}}Inactive{{/custom-option}}
                        {{/custom-select}}
                    </div>
                </h3>
                <p><em>Enable / Disable certain features from displaying to users on your site. You can also completely disable the frontend.</em></p>
                <br>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-hoverable table-middle">
                            <tr>
                                <th>Page</th>
                                <th class="text-right">Active</th>
                            </tr>
                            {{#each page in pages }}
                                <tr>
                                    <td>{{ page.title }}</td>
                                    <td class="text-right">
                                        {{#custom-select value=page.is_active class="form-control form-override" action='toggleActivePage' object=page}}
                                            {{#custom-option value=1}}Active{{/custom-option}}
                                            {{#custom-option value=0}}Inactive{{/custom-option}}
                                        {{/custom-select}}
                                    </td>
                                </tr>
                            {{/each}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="item col-xs-12 col-sm-6 col-md-4">
            <div class="panel well col-xs-12">
                <h3>Google Services</h3>
                <br>
                <div class="form-group">
                    <label>Google API Key</label>
                    {{input type="text" value=coreGoogleApiKey.string_value class="form-control"}}
                </div>
                <div class="form-group">
                    <div class="row">
                       <div class="col-xs-12">
                           <button class="btn btn-primary pull-right" {{ action 'updateGoogle' }}>Save</button>
                           <div class="clearfix"></div>
                       </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>      
    
    {{ outlet }}
</script>