App.Setting = DS.Model.extend({
    name: attr('string'),
    string_value: attr('string'),
    number_value: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date')
});