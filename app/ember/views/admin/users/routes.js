App.AdminUsersRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('user');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('userRoles').set('model', this.store.find('userRole'));
    }
    
});

App.AdminUsersUserRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('user', params.user_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});

App.AdminUsersPermissionsRoute = Ember.Route.extend({
	
    model: function() {
        return this.store.find('userPermission');
    },
	
	setupController: function(controller, model) {
		controller.set('model', model);
	}
    
});