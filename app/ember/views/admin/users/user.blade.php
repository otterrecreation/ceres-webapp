<script type="text/x-handlebars" id="admin/users/user">
    <div class="slideout">
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}><i class="fa fa-close"></i></button>
                <button class="btn btn-success" {{ action 'updateUser' }}>Save</button>
                <span class="text-muted">{{ userState }}</span>
                <button class="btn btn-danger pull-right" {{ action 'deleteUser' }}><i class="fa fa-trash"></i></button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                   <div class="form-group">
                       <div class="row">
                            <div class="col-xs-5">
                                {{gravatar-image class="img-responsive" email=username size="200"}}
                            </div>
                            <div class="col-xs-7">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                        {{ input type="text" 
                                           value=username
                                           disabled="disabled"
                                           class="form-control" }}      
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                        {{ input type="text"
                                           value=name
                                           disabled="disabled"
                                           class="form-control" }} 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Role</label>
                                            {{ view "select"
                                               content=userRoles
                                               optionValuePath="content.id"
                                               optionLabelPath="content.name"
                                               selection=role.content
                                               class="form-control" }}      
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                    <hr>
                    <div class="row">
                     	<div class="col-xs-12">
							<h3>Shipping Addresses</h3>
							<table class="table">
								<tr>
									<th><small>recipient</small></th>
									<th><small>address</small></th>
								</tr>
								{{#each address in addresses }}
								<tr>
									<th>
										<strong>{{ address.recipient_name }}</strong>
									</th>
									<td>
										<p>{{ address.address_line_1 }}&nbsp;{{ address.address_line_2 }}</p>
										<p>{{ address.city }}, {{ address.state_province }} {{ address.zip }} {{ address.country }}</p>
									</td>
								</tr>
								{{ else }}
								<tr>
									<td colspan="2"><p class="lead text-center">No saved addresses</p></td>
								</tr>
								{{/each}}
							</table>
						</div>
                    </div>
                    <div class="row">
                     	<div class="col-xs-12">
							<h3>Billing Information</h3>
							<table class="table">
								<tr>
									<th>Cardholder</th>
									<th>Number</th>
									<th>Expiration</th>
									<th>Type</th>
								</tr>
								{{#each bill in billingInfo }}
								<tr>
									<th>
										<strong>{{ bill.name }}</strong>
									</th>
									<td>
										<p>&bull;&bull;&bull;&bull;&nbsp;&bull;&bull;&bull;&bull;&nbsp;&bull;&bull;&bull;&bull;&nbsp;{{ bill.last4 }}</p>
									</td>
									<td>
										<p>{{ bill.exp_month }} / {{ bill.exp_year }}</p>
									</td>
									<td><p>{{ bill.funding }}</p></td>
								</tr>
								{{ else }}
								<tr>
									<td colspan="4"><p class="lead text-center">No saved billing information</p></td>
								</tr>
								{{/each}}
							</table>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>