<script type="text/x-handlebars" id="admin/users">
    <h1>Users {{#link-to 'admin.users.permissions' class="btn btn-default"}}<i class="fa fa-cog"></i>{{/link-to}}
    </h1>
    <br>          
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group" role="group">
            {{#link-to 'admin.users' (query-params role='') class='btn btn-default' tagName='button'}}All{{/link-to}}
              
            {{#each role in userRoles }}
              	{{#link-to 'admin.users' (query-params role=role.id) class='btn btn-default' tagName='button'}}{{ role.name }}{{/link-to}}
            {{/each}}
            </div>
        </div>
        <div class="col-md-4 col-md-offset-2">
            <form class="form" {{ action 'search' on="submit" }}>
                <div class="form-group">
                   <div class="input-group">
                       
                        {{ input type="text" 
                           value=searchQuery
                           class="form-control"
                           placeholder="Search Users" }}
                       <div class="input-group-btn">
                           <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                       </div>
                   </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-spaced">
                <tr>
                    <th><a href="#" {{ action 'sortBy' 'name' }}>name</a></th>
                    <th><a href="#" {{ action 'sortBy' 'username' }}>username</a></th>
                    <th><a href="#" {{ action 'sortBy' 'permissions' }}>role</a></th>
                </tr>
                {{#each user in filteredResults}}
                    {{#link-to 'admin.users.user' user tagName='tr' }}
                        <td>
                            {{gravatar-image class="img-responsive" email=user.username size="200"}}
                            {{ user.name }}
                        </td>
                        <td>{{ user.username }}</td>
                        <td>
                        	{{ user.role.name }}
                        </td>
                    {{/link-to}}
                {{else}}
                <tr>
                    <td colspan="8">
                        <p class="lead text-center">No users</p>
                    </td>
                </tr>
                {{/each}}
            </table>
        </div>
    </div>
    
    {{ outlet }}
    
</script>