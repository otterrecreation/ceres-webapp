App.User = DS.Model.extend({
    //properties
    name: attr('string'),
    username: attr('string'),
    password: attr('string'),
    password_confirmation: attr('string'),
	stripe_customer_id: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    addresses: hasMany('user-address', { async: true }),
	billingInfo: hasMany('user-billing-info', { async: true }),
	role: belongsTo('user-role', { async: true })
});

App.UserAddress = DS.Model.extend({
    //properties
	recipient_name: attr('string'),
    address_line_1: attr('string'),
    address_line_2: attr('string'),
	city: attr('string'),
    state_province: attr('string'),
    zip: attr('string'),
	country: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    user: belongsTo('user', { async: true })
});

App.UserBillingInfo = DS.Model.extend({
	stripe_card_id: attr('string'),
	name: attr('string'),
	last4: attr('number'),
	brand: attr('string'),
	funding: attr('string'),
	exp_month: attr('number'),
	exp_year: attr('number'),
	created_at: attr('date'),
    updated_at: attr('date'),
	
	//TODO: determine if we need to have address info as well...
	
	//relationships
	user: belongsTo('user', { async: true })
});

App.UserRole = DS.Model.extend({
	name: attr('string'),
	created_at: attr('date'),
    updated_at: attr('date'),
	
	//relationships
	users: hasMany('user', { async: true }),
	perms: hasMany('user-permission', { async: true })
});

App.UserPermission = DS.Model.extend({
	name: attr('string'),
	display_name: attr('string'),
	created_at: attr('date'),
    updated_at: attr('date'),
    
	
	//relationships
	roles: hasMany('user-role', { async: true })
});