App.AdminUsersController = Ember.ArrayController.extend({
	
	needs: ['userRoles'],
    userRoles: Ember.computed.alias('controllers.userRoles'),
	
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['role', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    role: null,                 // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    filteredResults: function() {
        
        var role = this.get('role');    // retrieves type variable to filter by
        var search = this.get('search');
        var searchableProperties = ['name', 'username'];
        var users = this.get('arrangedContent');  // creats array of models to be filtered.
        
        if (role) {
			console.log(users);
			
			users = users.filter(function(user) {
				return user.get('role.id') === role;
			});
			
        }
        
        if(search) {
            users = users.filter(function(user) { 
				
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(user.get(searchableProperties[i]));
                    if(user.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return users;
    }.property('role', 'search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
        
    },
    
    

});

App.AdminUsersUserController = Ember.ObjectController.extend({
    
   needs: ['adminUsers'],
   userRoles: Ember.computed.alias('controllers.adminUsers.userRoles'),
    
   pricePerUnits: ['hour', 'day'],
    
   publicVisibility:  [{value: 1, label: 'Public'}, {value: 0, label: 'Private'}], // Whether or not the the item should be show to the frontend or not
   isItemGood: function() {
        return this.get('model.type') === 'good';
   }.property('model.type'),
    
   isItemRental: function() {
        return this.get('model.type') === 'rental';
   }.property('model.type'),
    
   isItemAsset: function() {
        return this.get('model.type') === 'asset';
   }.property('model.type'),
    
   userState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'user ' + state[2];
   }.property('model.currentState.stateName'),
    
   actions: {
        updateUser: function() {
            
            var self = this;
            var item = this.get('model');
            
            if(this.get('newCategory')) {
                
                var newCategory = this.store.createRecord('inventoryCategory', { category: this.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    
                    item.set('inventoryCategory', category);
                    item.save().then(function(results) {
                        self.set('newCategory', ''); //clear the new category value now that it's been added to the dropdown
                        console.log('update was successful');
                    }, function(results) {
                        item.rollback(); //save didn't work...don't persist any of the changes to ember
                    });
                    
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
                
            } else {
                
                item.set('inventoryCategory', this.get('inventoryCategory.content'));    
                item.save().then(function(results) {
                    console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
            }
            
        },
       
        backToUsers: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteUser: function() {
            console.log('delete inventory...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this inventory item?");
            if (confirmDelete) {
                this.send('backToUsers', function() {
                    var record = self.get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.users');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;
            
            this.send('backToUsers', function() {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.users');
            });
        }
   }
});

App.AdminUsersPermissionsController = Ember.ArrayController.extend({
    
    needs: ['adminUsers'],
    userRoles: Ember.computed.alias('controllers.adminUsers.userRoles'),
    
	rolesPermissionsMap: function() {
        
		var allPermissions = this.get('model');
		var allRoles = this.get('userRoles');
        
        console.log(allRoles);
        
		var map = Ember.A();
		
		allPermissions.forEach(function(permission) {
			
			var permissionMap = {};
			permissionMap.permission_name = permission.get('display_name');
			permissionMap.permission_map = Ember.A();
            
			allRoles.forEach(function(role) {
                
				var permission_map = {};
				
				permission_map.disabled = role.get('name') === 'Super Admin';
				permission_map.nameAttr = "roles[" + role.get('id') + "][" + permission.get('id') + "]";
				permission_map.checked = role.get('perms').some(function(rolePermission) {
					return rolePermission.get('id') === permission.get('id');	
				});
				
				permissionMap.permission_map.push(permission_map);
				
			});
			
			map.push(permissionMap);

		});
        
		return map;
		
	}.property('userRoles.@each'),
	
	actions: {
		
		updatePermissions: function() {
			
			var permissionsMap = this.get('rolesPermissionsMap');
			
			var data = "";
			
			permissionsMap.forEach(function(permission) {
				permission.permission_map.forEach(function(role) {
					data += role.nameAttr + "=" + ( + role.checked ) + "&";
				});
			});
			
			Ember.$.post(rootURL + 'api-proxy/userPermissions', data).then(function(response) {
				console.log(response);
				console.log('permissions updated successfully');
			}, function(response) {
				console.log(response);
				console.log('failed to update permissions');
			});
		}
		
	}
	
});

App.UserRolesController = Ember.ArrayController.extend();
App.UserPermissionsController = Ember.ArrayController.extend();
App.UserAddressesController = Ember.ArrayController.extend();
App.UserBillablesController = Ember.ArrayController.extend();