<script type="text/x-handlebars" id="admin/users/permissions">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Permissions
            </h3>
          </div>
        
        <form id="permissions-form" class="form" {{action 'updatePermissions' on='submit'}}>  
          <div class="modal-body">
			  <p>Permissions are used for access to and editing of the admin side. Use caution when granting access for the "User" role.</p>                   
          		<table class="table table-borderless">
          			<tr>
          				<th>&nbsp;</th>
						{{#each role in userRoles }}
          				<th class="text-center">{{ role.name }}</th>
						{{/each}}
         				<th class="text-center">Guest</th>
          			</tr>
          			
          			{{#each permission in rolesPermissionsMap }}
          			<tr>
          				<th>{{ permission.permission_name }}</th>
						{{#each role in permission.permission_map }}
          				<td class="text-center">
							{{ input type="checkbox" 
								   	 name=role.nameAttr
								     disabled=role.disabled
								     checked=role.checked }}
						</td>
						{{/each}}
         				<td class="text-center">
							{{ input type="checkbox"
        							 disabled="disabled" }}
         				</td>
          			</tr>
					{{/each}}

          		</table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Done</button>
            <button type="submit" class="btn btn-primary" {{ action 'updatePermissions' }}>Save</button>
          </div>
        </form> 
        </div>
      </div>
    </div>
</script>