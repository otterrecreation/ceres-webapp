<script type="text/x-handlebars" id="admin/media/item">
    <div class="slideout">
        <div class="slideout-preview hidden-xs hidden-sm">
            <div class="container-fluid">
                 <br>
                {{ media-img file=model class="img-responsive pull-center" }}
            </div>
        </div>
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}><i class="fa fa-close"></i></button>
                <button class="btn btn-success" {{ action 'saveItem' }}>Save</button>
                <span class="text-muted">{{ itemState }}</span>
                <button class="btn btn-danger pull-right" {{ action 'deleteItem' }}><i class="fa fa-trash"></i></button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                       <div class="row">
                            <div class="col-sm-5">
                                <img class="img-responsive" {{bind-attr title="title"}} {{bind-attr src=absoluteThumbnailSrc }} {{bind-attr alt=altText}} />
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    {{input type="text" value=title placeholder="title" class='form-control'}}
                                </div>
                                <div class="form-group">
                                    {{input type="text" value=altText placeholder="alt text" class='form-control'}}
                                </div>
                                <div class="form-group">
                                    <strong>Name: </strong> {{ name }}<br>
                                    <strong>Dimensions: </strong>{{ width }} x {{ height }} <br>
                                    <strong>Size: </strong> {{ size }}
                                </div>
                            </div>
                       </div>
                       <hr>
                       <div class="row">
                           <div class="col-sm-12">
                               <div class="form-group">
                                   {{textarea value=description class='form-control' placeholder="description"}}
                               </div>
                           </div>
                       </div>
                </div>
            </div>
        </div>
    </div>
</script>