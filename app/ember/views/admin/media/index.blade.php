<script type="text/x-handlebars" id="admin/media">
   
   <h1>Media {{#link-to 'admin.media.upload' tagName='button' class='btn btn-primary'}}<i class="fa fa-plus"></i>{{/link-to}}</h1>
   
    {{#each media in model}}
        {{#link-to 'admin.media.item' media tagName='div' class='thumbnail-media panel'}}
            <div class="thumbnail-actions">
                <button class="btn btn-danger pull-right" {{ action 'deleteMedia' media }}><i class="fa fa-trash"></i></button>
                <div class="clearfix"></div>
            </div>

            {{#if media.isImage }}
                <img class="img-responsive" {{bind-attr title="media.name"}} {{bind-attr src="media.absoluteThumbnailSrc"}} {{bind-attr alt="media.altText"}} />
            {{ else }}
                {{ media.extension }}
            {{/if}}
            <div class="thumbnail-title">
               {{ media.title }}
           </div>
        {{/link-to}}
    {{ else }}
       <p class="lead text-center">No media uploaded yet</p>
    {{/each}}
    
    {{ outlet }}
    
</script>