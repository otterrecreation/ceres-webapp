App.File = DS.Model.extend({
    name: attr('string'),
    title: attr('string'),
    src: attr('string'),
    description: attr('string'),
    width: attr('string'),
    height: attr('string'),
    size: attr('string'),
    thumbnailSrc: attr('string'),
    extension: attr('string'),
    mime: attr('string'),
    altText: attr('string'),
    description: attr('string'),
    isImage: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //computed properties
    absoluteSrc: function() {
        return rootURL + '/' + this.get('src');
    }.property('src'),
    
    backgroundSrc: function() {
        return 'url(' + rootURL + '/' + this.get('src') + ') center center';
    }.property('src'),
    
    absoluteThumbnailSrc: function() {
        return rootURL + '/' + this.get('thumbnailSrc');
    }.property('thumbnailSrc'),
    
    backgroundThumbnailSrc: function() {
        return 'url(' + rootURL + '/' + this.get('thumbnailSrc') + ') center center';
    }.property('thumbnailSrc')
});