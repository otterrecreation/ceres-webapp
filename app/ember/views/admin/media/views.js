App.AdminMediaUploadView = Ember.View.extend({
    
    didInsertElement: function() {
        
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.media');
        });
    },
    
    actions: {
        
        closeModal: function() {
            Ember.$('#myModal').modal('hide');
        }
        
    }
    
});

App.AdminMediaItemView = Ember.View.extend({
    
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
    
});