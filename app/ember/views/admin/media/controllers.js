App.AdminMediaItemController = Ember.ObjectController.extend({
    itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
    }.property('model.currentState.stateName'),
});