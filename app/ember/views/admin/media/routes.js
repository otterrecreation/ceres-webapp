App.AdminMediaRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('file');
    },
    
    actions: {
        
        deleteMedia: function(item) {
            
            var confirmDelete = confirm('Are you sure you want to delete this file?');
            
            if(confirmDelete) {
                item.deleteRecord();
                if(item.get('isDeleted')) {
                    item.save().then(function(results) {
                        //item deleted successfully. no action necessary
                    }, function(results) {
                        item.rollback();
                    });
                }
            }
            
        },
        
        deleteSlideshow: function(item) {
            
            var confirmDelete = confirm('Are you sure you want to delete this slideshow?');
            
            if(confirmDelete) {
                item.deleteRecord();
                if(item.get('isDeleted')) {
                    item.save().then(function(results) {
    //                            console.log(results);
                    }, function(results) {
                        item.rollback();
                    });
                }
            }
        }
        
    }
    
});

App.AdminMediaItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('file', params.media_id);
    },
    
    actions: {
    
        deleteItem: function() {
            
            var self = this;
            
            confirmDelete = confirm('Are you sure you want to delete this file?');
            
            if(confirmDelete) {
                this.send('backToMedia', function() {
                    var item = self.currentModel; //delete the inventory record
                    item.deleteRecord();
                    if(item.get('isDeleted')) {
                        item.save().then(function(results) {
                            self.transitionTo('admin.media');
                        }, function(results) {
                            item.rollback();
                        });
                    }
                });
            }
        },
        
        saveItem: function() {
            
            var self = this;
            
            var item = this.currentModel;
            item.save().then(function(results) {
                console.log('update was successful');
            }, function(results) {
                console.log(results);
                item.rollback();
            });
            
        },
        
        backToMedia: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        doneEditing: function() {
            var self = this;
            
            this.send('backToMedia', function() {
                if(self.currentModel.get('isDirty')) {
                    self.currentModel.rollback(); //undo anything that hasn't been saved?
                }
                self.transitionTo('admin.media');
            });
        }
        
    }
    
});

App.AdminMediaCreateslideshowRoute = Ember.Route.extend({
    actions: {
        saveSlideshow: function() {
            var data = this.get('controller').getProperties('title');
            var item = this.store.createRecord('slideshow', data);
            item.save().then(function() {}, function(results) {
                item.rollback();
            });
        }
    }
});

App.AdminMediaSlideshowRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('slideshow', params.slideshow_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('media', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var data = {};
            var controller = this.get('controller');
            
            data.file = controller.get('newSlide');
            data.title = controller.get('newSlideTitle');
            data.caption = controller.get('newSlideCaption');
            data.slideshow = controller.get('model');;
            
            var item = this.store.createRecord('slide', data);
            
            item.save().then(function(results) {
                //item saved successfully
                console.log(results);
            }, function(results) {
                item.destroyRecord(); //backpedal
            });
        }
    }
});