<script type="text/x-handlebars" id="admin/media/upload">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Upload Media
            </h3>
          </div> 
          <div class="modal-body">                      
              {{multi-photo-uploader class="dropzone-borderless"}}
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" {{ action 'closeModal' target='view' }}>Done</button>
          </div>
        </div>
      </div>
    </div>
</script>