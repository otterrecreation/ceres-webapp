//needs to be defined later. after the application controller is defined
App.VerifyAccessRoute = Ember.Route.extend({
    
    permissions: [],
    
    userHasPermissions: function() {
        var requiredPermissions = this.get('permissions');
        var currentUser = this.controllerFor('application').get('currentUser');
        
        if(typeof currentUser.permissions !== 'undefined') {
            
            var userPermissions = currentUser.permissions;
            
            return requiredPermissions.every(function(requiredPermission) {
                return userPermissions.some(function(userPermission) {
                    return userPermission === requiredPermission;
                });
            });
            
        }
        
        return false;
    },
    
    beforeModel: function(transition) {
        
        if(this.userHasPermissions()) {
            Ember.Logger.debug('user has valid permissions');
            return true;
        } else {
            
            var loginController = this.generateController('login');
            loginController.set('attemptedTransition', transition);
            
            $.growl.error({ title: "Error", message: "Redirect to home due to lack of permissions to that URL" });
            Ember.Logger.debug('user does not have valid permissions');
            
            this.transitionTo('login');
//            return false;
        }
    },
    
});

App.AdminRoute = App.VerifyAccessRoute.extend({
    
    permissions: ['view_admin'], //use some basic abilities needed for ALL admin routes
    
    activate: function() {
        this.controllerFor('application').set('isAdminRoute', true);
    },
    
    deactivate: function() {
        this.controllerFor('application').set('isAdminRoute', false);
    },
    
    actions: {
    
        openFrontend: function() {
            this.send('closeFrontend');
            
            window.frontendSide = window.open(rootURL);
        },
        
        closeFrontend: function() {
            if(typeof window.frontendSide !== 'undefined') {
                console.log('the frontend side has already been opened');
                
                if(typeof window.frontendSide.close === 'function') {
                    window.frontendSide.close();
                }
            } else {
                
                if(!Ember.isEmpty(window.opener)) {
                    console.log('the frontend side opened the admin side');
                    window.frontendSide = window.opener;
                    window.frontendSide.close();
                }
            }
        },
        
        adminLogout: function() {
            
            this.send('closeFrontend');
            
            window.location = rootURL + 'api-proxy/logout'; //now that we've closed the children window we can logout
            
        },
        
        //simply slides the admin menu in and out
        toggleAdminMenu: function() {
            $("#wrapper").toggleClass("toggled");
        }
    
    },
	
	renderTemplate: function() {
		this._super();
    	this.render({outlet: 'admin'});
  	}
});