<script type="text/x-handlebars" id="admin/services/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                New Service
            </h3>
          </div>
        <?/***********************************************
        *   Add form for services, broken up in a table
        *
        ***********************************************/?>
        <form class="form" {{action 'addService' on='submit'}}>  
          <div class="modal-body">                      
                    <div class="form-group">
                       <div class="row">
                                <div class="col-xs-6">
                                    <label class="control-label">Name</label>
                                    {{ input type="text" 
                                       value=name
                                       class="form-control" }}
                                </div>
                                <div class="col-xs-3">
                                    <label class="control-label">Price</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">$</div>
                                      {{ input type="text" 
                                         value=price
                                         class="form-control" }}
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <label class="control-label">Hours</label>
                                    <div class="input-group">
                                      {{ input type="text" 
                                         value=hours
                                         class="form-control" }}
                                    </div>     
                                </div>
                                <div class="col-xs-3">
                                    <label class="control-label">Visibility</label>
                                     {{switch-box checked=publicVisible }}
                                </div>
                       </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                               <label class="control-label">Description</label>
                                {{ textarea
                                   value=description 
                                   class="form-control"
                                   rows=6 }}  
                            </div>
                        </div>
                    </div>   
                    <hr>                                       
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary" {{ action 'saveService' }}>Save</button>
          </div>
        </form> 
        </div>
      </div>
    </div>
</script>