App.AdminServicesServiceView = Ember.View.extend(Ember.Evented, {

	templateName:'admin/services/service',

	didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }

});

App.AdminServicesCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/services/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.services');
        });
        
        this.get('controller').on('addServiceDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addServiceDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminServicesSettingsView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/services/settings',

    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.services');
        });
        this.get('controller').on('saveServiceSettingsDidSucceed', function() {
            self.send('closeModal');
        });    
    },

    willClearRender: function() {
        this.get('controller').off('saveServiceSettingsDidSucceed', function() {
            self.send('closeModal');
        });
    },

    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }

});    