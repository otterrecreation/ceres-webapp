App.AdminServicesRoute = Ember.Route.extend({

    model: function() {
        return this.store.find('service');
    }

});

App.AdminServicesServiceRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('service', params.services_id);
    },
    
    setupController: function(controller, model) {
        
        var self = this;
        
        controller.set('model', model);      
    }
    
});

App.AdminServicesCreateRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        controller.reset(); //reset the values when called
    }
    
});

App.AdminServicesSettingsRoute = Ember.Route.extend({

    model: function() {
        return this.store.find('servicesSetting', 1);
    },

    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});