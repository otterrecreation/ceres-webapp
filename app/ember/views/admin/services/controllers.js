App.AdminServicesController = Ember.ArrayController.extend({

    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['search'],
    search: null,
    searchQuery: null,

    filteredServices: function() {
            
            var search = this.get('search');
            var searchableProperties = ['name', 'description'];
            var items = this.get('arrangedContent');  // creats array of models to be filtered.
            
            if(search) {
                items = items.filter(function(item) {
                    var trueCount = 0;
                    
                    for(var i = 0; i < searchableProperties.length; i++) {
                        console.log(item.get(searchableProperties[i]));
                        if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                            trueCount++;
                        }
                    }
                
                    return trueCount > 0;
                });
            }
            
            return items;
        }.property('search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.

    actions: {

        search: function() {
            this.set('search', this.get('searchQuery'));
        },

        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
    }

});

App.AdminServicesServiceController = Ember.ObjectController.extend({

    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],

    actions: {

        backToServices: function(callback) 
        {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteServices: function() 
        {
            console.log('delete services...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this service?");
            if (confirmDelete) {
                this.send('backToServices', function() {
                    var record = self.get('model'); //delete the services record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.services');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() 
        {
            var self = this;
            
            this.send('backToServices', function() 
            {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.services');
            });
        },

        updateServices: function() 
        {        
            var self = this;
            var item = this.get('model');

            item.save().then(function(results) {
                console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
        }
    }
});

App.AdminServicesCreateController = App.AdminServicesServiceController.extend(Ember.Evented, {
    reset: function() {
        this.setProperties({
            name: '',
            price: '',
            description: '',
            hours: ''
        });
    },

    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],

    actions: {
        saveService: function() {
            
            var self = this;
            //grab all relavent data for creating a new service
            var data = this.getProperties('name',
                                          'price',
                                          'description',
                                          'hours',
                                          'publicVisible');
                
            self.send('saveServiceWithData', data);
        },
        
        saveServiceWithData: function(data) {
            var self = this;
            var item = this.store.createRecord('service', data);
            
            item.save().then(function(item) {
                //now attach the item to the category
                self.trigger('addServiceDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('inventory saved unsuccessfully');
                console.log(results);
            });
        }
    }  

});

App.AdminServicesSettingsController = Ember.ObjectController.extend({

    reserveability:  [{value: 0, label: 'In Person Only'}, {value: 1, label: 'Online Reservable'}],

    actions: {
        saveServiceSettings: function() {
            
            var self = this;
            var item = this.get('model');
            item.save().then(function(response) { console.log(response); }, function(response) { console.log(response); });
            //grab all relavent data for creating a new service
            // var data = this.getProperties('availableServices',
            //                               'canReserve');
                
             self.send('saveServiceSettingsWithData', data);
        },

        saveServiceSettingsWithData: function(data) {
            var self = this;
            var item = this.store.updateRecord('service_settings', data);
            
            item.save().then(function(item) {
                //now attach the item to the category
                self.trigger('saveServiceSettingsDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('settings saved unsuccessfully');
                console.log(results);
            });
        }
    }
});

App.AdminServicesRequestedController = Ember.ArrayController.extend({

    needs: ['cartItem'],
// will filter based on if a service and paid
    filteredRequestedServices: function() {

    }

});        