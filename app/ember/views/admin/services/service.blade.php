<script type="text/x-handlebars" id="admin/services/service">
    <div class="slideout">
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}><i class="fa fa-close"></i></button>
                <button class="btn btn-success" {{ action 'updateServices' }}>Save</button>
                <button class="btn btn-danger pull-right" {{ action 'deleteServices' }}><i class="fa fa-trash"></i></button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                    <div class="form-group">
                       <div class="row">
                                <div class="col-xs-8">
                                    <label class="control-label">Name</label>
                                    {{ input type="text" 
                                       value=name
                                       class="form-control" }}
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label">Price</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">$</div>
                                      {{ input type="text" 
                                         value=price
                                         class="form-control" }}
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label class="control-label">Hours</label>
                                    <div class="input-group">
                                      {{ input type="text" 
                                         value=hours
                                         class="form-control" }}
                                    </div>     
                                </div>
                                <div class="col-xs-3">
                                    <label class="control-label">Visibility</label>
                                      {{switch-box checked=publicVisible }}
                                </div>
                       </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                               <label class="control-label">Description</label>
                                {{ textarea
                                   value=description 
                                   class="form-control"
                                   rows=6 }}  
                            </div>
                        </div>
                    </div>   
                    <hr>
                </div>
            </div>
        </div>
    </div>
</script>