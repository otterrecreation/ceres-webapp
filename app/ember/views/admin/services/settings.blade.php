<script type="text/x-handlebars" id="admin/services/settings">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Service Settings
            </h3>
          </div>
        <form class="form" {{action 'saveServiceSettings' on='submit'}}>  
          <div class="modal-body">                      
                    <div class="form-group">
                       <div class="row">
                            <div class="col-xs-3">
                            {{ log model }}
                                    <label class="control-label">Available Services</label>
                                    <div class="input-group">
                                      {{ input type="text" 
                                         value=availableServices
                                         class="form-control" }}
                                    </div>     
                            </div>
                            <div class="col-xs-3">
                                    <label class="control-label">Reservable</label>
                                    {{switch-box checked=canReserve}}
                            </div>

                       </div>
                    </div>
                    <hr>       

          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary" {{ action 'saveServiceSettings' }}>Save Changes</button>
          </div>
        </form>
        </div>
      </div>
    </div>

</script>