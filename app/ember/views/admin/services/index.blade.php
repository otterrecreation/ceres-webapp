<script type="text/x-handlebars" id="admin/services">
  
	<h1>Services
  {{#link-to 'admin.services.create' class="btn btn-primary"}}<i class="fa fa-plus"></i>{{/link-to}}
  {{#link-to 'admin.services.settings' class="btn btn-default"}}<i class="fa fa-cog"></i>{{/link-to}}
  </h1> 

    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="nav active"><a href="#A" data-toggle="tab">Services</a></li>
        <li class="nav"><a href="#B" data-toggle="tab">Service Requests</a></li>
    </ul>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade in active" id="A">
                <div class="col-md-4 col-md-offset-2">
                    <form class="form" {{ action 'search' on="submit" }}>
                        <div class="form-group">
                           <div class="input-group">
                               
                                {{ input type="text" 
                                   value=searchQuery
                                   class="form-control"
                                   placeholder="Search Services" }}
                               <div class="input-group-btn">
                                   <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                               </div>
                           </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <tr>
                                <th><small><a href="#" {{ action 'sortBy' 'name' }}>name</a></small></th>
                                <th><small><a href="#" {{ action 'sortBy' 'price' }}>price</a></small></th>
                                <th><small>hours</small></th>
                            </tr>        
                                    {{#each service in filteredServices}}
                                        {{#link-to 'admin.services.service' service tagName='tr'}} 
                                                <td>{{ service.name }}</td>
                                                <td>{{ currency service.price }}</td>
                                                <td>{{ service.hours }}</td>
                                        {{/link-to}}
                                    {{/each}}
                        </table> 
                    </div>                                  
                </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="B">
            <h1>Service Requests</h1>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <tr>
                                <th><small><a href="#">user</a></small></th>
                                <th><small><a href="#">service</a></small></th>
                            </tr>        
                                    {{#each request in filteredRequestedServices}}
                                                <td>{{ request.item_name }}</td>
                                    {{/each}}
                        </table> 
                    </div>                                  
                </div>

        </div>
    </div>    

    {{ outlet }}

 </script> 