App.Page = DS.Model.extend({
    title: attr('string'),
    slug: attr('string'),
    show_title: attr('number'),
    is_active: attr('number'),
    is_home: attr('number'),
    page_content: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    slideshow: belongsTo('slideshow', { async: true })
});

App.Blog = DS.Model.extend({
    title: attr('string'),
    slug: attr('string'),
    is_active: attr('number'),
    page_content: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    previousBlog: attr('number'),
    nextBlog: attr('number'),
    
    author: belongsTo('user', { async: true }),
    last_modified_by: belongsTo('user', { async: true })
});
