<script type="text/x-handlebars" id="admin/blog/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header modal-emphasis">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Create a new blog
            </h3>
          </div>
          <div class="modal-body">
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-12">
                          <label>Blog Title</label>
                          {{ input type="text" value=title class="form-control" }}
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-12">
                          {{ ck-editor value=page_content autosave=false }}
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <span class="text-muted">blog {{ itemState }}</span>
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Done</button>
            <button type="button" class="btn btn-primary" {{ action 'togglePublish' }}>{{#if is_active }}Unplublish{{ else }}Publish{{/if}}</button>
          </div>
        </div>
      </div>
    </div>
</script>