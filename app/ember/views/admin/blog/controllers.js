App.AdminBlogController = Ember.ArrayController.extend({
    sortProperties: ['created_at'],
    sortAscending: false,
});

App.AdminBlogCreateController = Ember.ObjectController.extend(Ember.Evented, {
    
    item: null,
    
    init: function() {
        this._super();
        
        var self = this;
        
        //setup the autosaving feature...
        this.set('autosaveInterval', setInterval(function() {
            
            var item = self.get('item');
            
            if(Ember.isEmpty(item)) {
                var data = self.getProperties('title', 'is_active', 'page_content');
                var item = self.store.createRecord('blog', data);
                    item.save();
            } else {
                item.set('title', self.get('title'));
                item.set('is_active', self.get('is_active'));
                item.set('page_content', self.get('page_content'));
                item.save();
            }
            
            self.set('item', item);
            console.log('saving...');
        }, 2000));
    },
    
    is_active: false //by default the blog should be set to be published
    
});

App.AdminBlogItemController = Ember.ObjectController.extend(Ember.Evented, {
    
    init: function() {
        this._super();
        
        var self = this;
        
        //setup the autosaving feature...
        this.set('autosaveInterval', setInterval(function() {
            console.log('saving...');
            self.get('model').save();
        }, 2000));
    },
    
});