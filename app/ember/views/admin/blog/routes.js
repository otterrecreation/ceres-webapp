App.AdminBlogRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('blog');
    }
    
});

App.AdminBlogCreateRoute = Ember.Route.extend({
    
    actions: {
        togglePublish: function() {
            var controller = this.get('controller');
            controller.set('is_active', !controller.get('is_active'));
        },
        
        willTransition: function(transition) {
            console.log('stop saving');
            //save one more time just in case
            clearInterval( this.get('controller').get('autosaveInterval') );
        }
    }
    
});

App.AdminBlogItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('blog', params.blog_id);
    },

    actions: {
        togglePublish: function() {
            var controller = this.get('controller');
            controller.set('is_active', !controller.get('is_active'));
            controller.get('model').save();
        },
        
        willTransition: function(transition) {
            console.log('stop saving');
            clearInterval( this.get('controller').get('autosaveInterval') );
        }
    }
    
});