App.AdminBlogCreateView = App.ModalView.extend({
    routeOnClose: 'admin.blog'
});

App.AdminBlogItemView = App.ModalView.extend({
    routeOnClose: 'admin.blog'
});