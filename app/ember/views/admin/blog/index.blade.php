<script type="text/x-handlebars" id="admin/blog">
    <h1>Blog {{#link-to 'admin.blog.create' class="btn btn-primary"}}<i class="fa fa-plus"></i>{{/link-to}}</h1>
    
    {{#if arrangedContent }}
    <table class="table table-spaced table-hoverable">
        <tr>
            <th>title</th>
            <th>author</th>
            <th>last edited by</th>
            <th>created on</th>
            <th>last modified</th>
        </tr>
        {{#each blog in arrangedContent}}
            {{#link-to 'admin.blog.item' blog tagName='tr'}}
                <td>
                    <strong>
                        {{#if blog.title }}
                            {{ blog.title }}
                        {{ else }}
                            <span class="text-muted">Untitled</span>
                        {{/if}}
                    </strong>
                </td>
                <td>
                    {{ blog.author.name }}
                </td>
                <td>
                    {{ blog.last_modified_by.name }}
                </td>
                <td class="text-muted">{{ moment blog.created_at }}</td>
                <td class="text-muted">{{ moment blog.updated_at }}</td>
            {{/link-to}}
        {{/each}}
    </table>
    {{  else }}
        <p class="lead text-center">No blog posts yet</p>
    {{/if}}
        
    {{ outlet }}
</script>