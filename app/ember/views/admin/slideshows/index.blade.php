<script type="text/x-handlebars" id="admin/slideshows">
    
    <h1>Slideshows {{#link-to 'admin.slideshows.create' tagName='button' class='btn btn-primary'}}<i class="fa fa-plus"></i>{{/link-to}}</h1>
     {{#if model }}
      <table class="table table-spaced">
      <tr>
          <th>Title</th>
          <th>Assigned Pages</th>
          <th>Slides</th>
      </tr>
      {{#each slideshow in model}}
        {{#link-to 'admin.slideshows.slideshow' slideshow tagName='tr'}}
          <td>
              {{log slideshow.slides.firstObject.file }}
              {{media-img-thumb file=slideshow.slides.firstObject.file class="img-responsive" }}<strong>{{ slideshow.title }}</strong>
          </td>
          <td>
              {{#each page in slideshow.pages }}
                  <span class="badge">{{ page.title }}</span>
              {{ else }}
              <span class="text-muted">None</span>
              {{/each}}
          </td>
          <td>{{ slideshow.slides.length }}</td>
        {{/link-to}}
      {{/each}}
      </table>
    {{ else }}
     <p class="lead text-center">No slideshows yet</p>
    {{/if}}
      {{ outlet }}
</script>