<script type="text/x-handlebars" id="admin/slideshows/slideshow">
    <div class="slideout">
        <div class="slideout-preview hidden-xs hidden-sm">
            {{ carousel-slideshow slides=slides }}
        </div>
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}><i class="fa fa-close"></i></button>
                <button class="btn btn-success" {{ action 'updateSlideshow' }}>Save</button>
                <span class="text-muted">{{ itemState }}</span>
                <button class="btn btn-danger pull-right" {{ action 'deleteSlideshow' }}><i class="fa fa-trash"></i></button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                     <div class="form-group">
                         <div class="row">
                             <div class="col-xs-12">
                                 <label>Title</label>
                                 {{input type="text" value=title class="form-control"}}
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col-xs-12">
                                 <label>Pages</label>
                                 {{select-2
                                        content=allPages
                                        value=pagesToSync
                                        placeholder="Assign slideshow to pages"
                                        optionLabelPath="title"
                                        multiple=true
                                        allowClear=true
                                  }}
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">
                             <div class="col-xs-12">
                                 <h3>Slides {{#link-to 'admin.slideshows.slideshow.createslide' class='btn btn-primary'}}<i class="fa fa-plus"></i>{{/link-to}}</h3>
                                 <p><em>Slides that have a different height will make the slideshow resize with each slide. It is recommended that all slides have the same height.</em></p>
                                 {{#if slides}}
                                 <div class="row">
                                     <div class="col-xs-12">
                                         <p>&nbsp;</p>
                                         <table class="table table-middle">
                                             <tr>
                                                 <th>Slide</th>
                                                 <th class="text-center">Edit</th>
                                                 <th class="text-right">Remove</th>
                                             </tr>
                                             {{#each slide in slides}}
                                                 <tr>
                                                     <td>
                                                         {{media-img-thumb file=slide.file class='img-responsive'}}
                                                         {{ slide.title }}
                                                     </td>
                                                     <td class="text-center">{{#link-to 'admin.slideshows.slideshow.slide' slide }}edit{{/link-to}}</td>
                                                     <td class="text-right">
                                                       <a href="#" {{ action 'deleteSlide' slide }}>
                                                           <i class="fa fa-trash"></i>
                                                       </a>
                                                     </td>
                                                 </tr>
                                             {{/each}}
                                         </table>
                                     </div>
                                 </div>
                                 {{ else }}
                                 <p class="text-center">{{ title }} doesn't have any slides yet</p>
                                 {{/if}}
                             </div>
                         </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    
    {{ outlet }}
</script>