App.AdminSlideshowsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('slideshow');
    } 
});

App.AdminSlideshowsCreateRoute = Ember.Route.extend({
    actions: {
        saveSlideshow: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('title');
            
            var item = this.store.createRecord('slideshow', data);
            item.save().then(function() { 
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});

App.AdminSlideshowsSlideshowRoute = Ember.Route.extend(Ember.Evented, {
    model: function(params) {
        return this.store.find('slideshow', params.slideshow_id);
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        controller.set('pagesToSync', model.get('pages.content.content'));
        
        //dependencies
        this.controllerFor('pages').set('model', this.store.find('page'));
    },
    
    actions: {
        
        addPage: function(value) {
            value.set('slideshow', this.get('controller').get('model')).save();
        },
        
        backToSlideshow: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteSlideshow: function() {
            console.log('delete slideshow...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this slideshow?");
            if (confirmDelete) {
                this.send('backToSlideshow', function() {
                    var record = self.get('controller').get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionTo('admin.slideshows');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        updateSlideshow: function() {
            
            var self = this;
            var item = this.get('controller').get('model');
            
            item.save().then(function(results) { 
                console.log('update was successful');
            }, function(results) {
                item.rollback(); //save didn't work...don't persist any of the changes to ember
            });
            
        },
       
       
       deleteSlide: function(instance) {
           var confirmDelete = confirm("Are you sure you want to delete this slide?");
           if (confirmDelete) {
               
               var self = this;
               
               instance.deleteRecord();
               
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                        Ember.$.growl({ title: 'Note', message: 'Refresh your page or reopen the slideshow to reload the slideshow' });
//                        window.location.reload(); //hard refresh. TODO: make this not such an ugly solution
                    }, function(results) {
                            instance.rollback();
                    });
                }
            }
       },
        
        doneEditing: function() {
            var self = this;
            var item = this.get('controller').get('model');
            
            this.set('photoPicked', null);
            
            this.send('backToSlideshow', function() {
                item.rollback(); //undo anything that hasn't been saved
                self.transitionTo('admin.slideshows');
            });
        }
        
    }
});

App.AdminSlideshowsSlideshowCreateslideRoute = Ember.Route.extend({
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var controller = this.get('controller');
            var slideshow = this.modelFor('adminSlideshowsSlideshow');
            var data = {
                slideshow: slideshow,
                title: controller.get('newSlideTitle'),
                caption: controller.get('newSlideCaption'),
                file: controller.get('newSlide')
            };
            
            var item = this.store.createRecord('slide', data);
            item.save().then(function() {
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});

App.AdminSlideshowsSlideshowSlideRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('slide', params.slide_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var controller = this.get('controller');
            var item = controller.get('model');
            
            item.save().then(function() {
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});