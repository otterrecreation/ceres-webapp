App.Slideshow = DS.Model.extend({
    title: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),    
    
    //relationship
    slides: hasMany('slide', { async: true }),
    pages: hasMany('page', { async: true }),
    
    //computed properties
    collapseHeadingId: function() {
        return 'collapse-heading-' + this.get('id');
    }.property('id'),
    
    collapseId: function() {
        return 'collapse-' + this.get('id');
    }.property('id'),
    
    hashedId: function() {
        return '#' + this.get('collapseId');
    }.property('collapsedId')
});


App.Slide = DS.Model.extend({
    title: attr('string'),
    caption: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    slideshow: belongsTo('slideshow', { async: true }),
    file: belongsTo('file', { async: true })
});