App.AdminSlideshowsCreateController = Ember.ObjectController.extend(Ember.Evented, {

});

App.AdminSlideshowsSlideshowController = Ember.ObjectController.extend({
    
   needs: ['pages'],
   allPages: Ember.computed.alias('controllers.pages'),
   pagesToSync: null,
    
   itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
   }.property('model.currentState.stateName'),
    
//   pagesToSync: function() {
//        return this.get('model.pages.content.content');
//   }.property('model.pages.content.content'),
    
   syncPages: function() {
       
       var pagesToSync = this.get('pagesToSync');
       var allPages = this.get('allPages');
       var slideshow = this.get('model');
       
       allPages.forEach(function(page) {
            if(pagesToSync.some(function(pageToSync) {
                return page.get('id') === pageToSync.get('id');
            })) {
                console.log('set ' + page.get('title') + ' to HAVE a slideshow');
                page.set('slideshow', slideshow).save();
            } else if(!Ember.isEmpty(page.get('slideshow')) && page.get('slideshow').get('id') === slideshow.get('id')) {
                console.log('set ' + page.get('title') + ' to NOT HAVE a slideshow');
                page.set('slideshow', null).save();
            }
            
       });
       
   }.observes('pagesToSync')
    
});

App.AdminSlideshowsSlideshowCreateslideController = Ember.ObjectController.extend(Ember.Evented, {
    needs: ['media'],
    media: Ember.computed.alias('controllers.media')
});

App.AdminSlideshowsSlideshowSlideController  = Ember.ObjectController.extend(Ember.Evented, {
    needs: ['media'],
    media: Ember.computed.alias('controllers.media')
});