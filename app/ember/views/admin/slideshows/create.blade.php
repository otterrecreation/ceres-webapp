<script type="text/x-handlebars" id="admin/slideshows/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                New Slideshow
            </h3>
          </div>
          <div class="modal-body">                      
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-12">
                          <label>Title</label>
                          {{ input type="text" value=title class="form-control" }}
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary" {{ action 'saveSlideshow' }}>Create Slideshow</button>
          </div>
        </div>
      </div>
    </div>
</script>