<script type="text/x-handlebars" id="admin/slideshows/slideshow/createslide">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Add Slide to {{ title }} Slideshow
            </h3>
          </div> 
          <div class="modal-body">                      
              <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Photo</label>
                            {{ single-photo-uploader media=media value=newSlide }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">   
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Title</label>
                                    {{ input type="text" value=newSlideTitle class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                               
                                <div class="row">
                                <div class="col-xs-12">
                                    <label>Caption</label>
                                    {{ textarea
                                       value=newSlideCaption 
                                       class="form-control"
                                       rows=4 }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" {{ action 'closeModal' target='view' }}>Done</button>
            <button type="button" class="btn btn-primary" {{ action 'saveItem' }}>Add Photo</button>
          </div>
        </div>
      </div>
    </div>
</script>


<script type="text/x-handlebars" id="admin/slideshows/slideshow/slide">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Edit Slide: {{ title }}
            </h3>
          </div> 
          <div class="modal-body">                      
              <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Photo</label>
                            {{ single-photo-uploader media=media value=file }}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">   
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Title</label>
                                    {{ input type="text" value=title class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                               
                                <div class="row">
                                <div class="col-xs-12">
                                    <label>Caption</label>
                                    {{ textarea
                                       value=caption 
                                       class="form-control"
                                       rows=4 }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" {{ action 'closeModal' target='view' }}>Done</button>
            <button type="button" class="btn btn-primary" {{ action 'saveItem' }}>Save</button>
          </div>
        </div>
      </div>
    </div>
</script>