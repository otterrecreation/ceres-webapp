<script type="text/x-handlebars" id="admin/events">
    <h1>Events {{#link-to 'admin.events.create' class="btn btn-primary"}}<i class="fa fa-plus"></i>{{/link-to}}</h1>
    <br>
    <div class="row">
        <div class="col-md-4 col-md-offset-8">
            <form class="form" {{ action 'search' on="submit" }}>
                <div class="form-group">
                    <div class="input-group">
                        {{
                            input type="text"
                            value=searchQuery
                            class="form-control"
                            placeholder="Search Events"
                        }}
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{#if filteredEvents }}
                <table class="table table-spaced">
                    <tr>
                        <th><a href="#" {{ action 'sortBy' 'name' }}>name</a></th>
                        <th><a href="#" {{ action 'sortBy' 'location' }}>location</a></th>
                        <th><a href="#" {{ action 'sortBy' 'start' }}>start</a></th>
                        <th><a href="#" {{ action 'sortBy' 'end' }}>end</a></th>
                        <th><a href="#" {{ action 'sortBy' 'price' }}>price</a></th>
                        <th><a href="#" {{ action 'sortBy' 'capacity' }}>capacity</a></th>
                    </tr>
                {{#each event in filteredEvents}}
                    {{#link-to 'admin.events.event' event tagName='tr'}}
                        <td>
                            {{ media-img-thumb file=event.file class="img-responsive" }}
                            <strong>{{ event.name }}</strong>
                        </td>
                        <td>{{ event.location }}</td>
                        <td>{{ moment event.start }}</td>
                        <td>{{ moment event.end }}</td>
                        <td>{{ currency event.price }}</td>
                        <td>{{ event.capacity }}</td>
                    {{/link-to}}
                {{/each}}
                </table>
            {{ else }}
                <p class="lead text-center">No events</p>
            {{/if}}
        </div>
    </div>
    {{ outlet }}
</script>