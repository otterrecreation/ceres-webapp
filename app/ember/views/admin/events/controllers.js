App.AdminEventsController = Ember.ArrayController.extend({
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['type', 'search'],    // Creates url Query ../?type='_____' will result in a search for type
    type: null,                         // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    filteredEvents: function() {
        var search = this.get('search');
        var searchableProperties = ['name', 'description'];
        var events = this.get('arrangedContent');  // creates array of models to be filtered.
        if(search) {
            events = events.filter(function(event) {
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(event.get(searchableProperties[i]));
                    if(event.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
                return false;
            });
        }
        return events;
    }.property('search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
    }
});

App.AdminEventsEventController = Ember.ObjectController.extend({
    
    needs: ['media'],
    media: Ember.computed.alias('controllers.media'),
    
    actions: {
        updateEvent: function() {
            var self = this;
            var item = this.get('model');
            
            item.set('start', new Date(this.get('start')));
            item.set('end', new Date(this.get('end')));
            item.set('registrationDeadline', new Date(this.get('registrationDeadline')));
            item.set('publicAt', new Date(this.get('publicAt')));
            
            item.save().then(function() {}, function(results) {
                console.log(results);
                item.rollback();
            });
            
        },
        addInstances: function() {
            var instanceCount = this.get('instanceCount');
            for(var i = 0; i < instanceCount; i++) {
                var newInstance = this.store.createRecord('eventsInstance', { events: this.get('model') });
                newInstance.save().then(function(results) {
                    console.log(results);
                }, function(results) {
                    console.log(results);
                    newInstance.destroyRecord();
                });
            }
        },
        deleteInstance: function(instance) {
            var confirmDelete = confirm("Are you sure you want to delete this event?");
            if (confirmDelete) {
                instance.deleteRecord();
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                    }, function(results) {
                        instance.rollback();
                    });
                }
            }
        },
        backToEvents: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        deleteEvent: function() {
            console.log('delete event...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this event?");
            if (confirmDelete) {
                this.send('backToEvents', function() {
                    var record = self.get('model'); //delete the event record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.events');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;

            this.send('backToEvents', function() {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.events');
            });
        }
    }
});

App.AdminEventsCreateController = App.AdminEventsEventController.extend(Ember.Evented, {
    
    needs: ['media'],
    media: Ember.computed.alias('controllers.media'),
    
    publicVisible: true,
    //function to reset all of the values in the modal when loaded...
    reset: function() {
        this.setProperties({
            alertAt: '',
            capacity: '',
            description: '',
            end: '',
            location: '',
            name: '',
            price: '',
            publicAt: '',
            registrationDeadline: '',
            start: ''
        });
    },
    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],
    actions: {
        saveEvent: function () {
            var self = this;
            //grab all relevant data for creating a new event
            var data = this.getProperties(
                'alertAt',
                'capacity',
                'description',
                'end',
                'location',
                'name',
                'file',
                'price',
                'publicVisible'
            );

            data.start = Ember.isEmpty(this.get('start')) ? null : new Date(this.get('start'));
            data.end = Ember.isEmpty(this.get('end')) ? null : new Date(this.get('end'));
            data.registrationDeadline = Ember.isEmpty(this.get('registrationDeadline')) ? null : new Date(this.get('registrationDeadline'));
            data.publicAt = Ember.isEmpty(this.get('publicAt')) ? null : new Date(this.get('publicAt'));

            console.log("Grabbing data from form...");
            self.send('saveEventWithData', data);
        },
        saveEventWithData: function(data) {
            console.log("Attempting to create a new event...");
            var self = this;
            var event = this.store.createRecord('event', data);
            event.save().then(function (event) {
                //now attach the item to the category
                console.log("Creating a new event succeeded.");
                self.trigger('addEventDidSucceed');
            }, function (results) {
                //TODO: this is where we want to highlight crap
                event.destroyRecord(); //failed. therefore we want to delete it from the store
                console.log("Creating a new event failed:\n" + results);
            });
        }
    }
});