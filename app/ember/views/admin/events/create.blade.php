<script type="text/x-handlebars" id="admin/events/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">New Event</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-0">
                                {{ single-photo-uploader value=file media=media }}
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Name</label>
                                            {{
                                                input type="text"
                                                value=name
                                                class="form-control"
                                            }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                {{
                                                    input type="text"
                                                    value=price
                                                    class="form-control"
                                                    title="e.g., 19.99"
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Capacity</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                {{
                                                    input type="text"
                                                    value=capacity
                                                    class="form-control"
                                                    title="e.g., 10"
                                                }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Location</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                {{
                                                    input type="text"
                                                    value=location
                                                    class="form-control"
                                                    title="e.g., 123 Somewhere St., Marina, CA"
                                                }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Starts At</label>
                                {{ bootstrap-datetimepicker value=start }}
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Ends At</label>
                                {{ bootstrap-datetimepicker value=end }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label">Description</label>
                                {{
                                    textarea
                                    value=description
                                    class="form-control"
                                    rows=6
                                }}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Registration Deadline</label>
                                {{ bootstrap-datetimepicker value=registrationDeadline }}
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">When will the event be visible?</label>
                                {{ bootstrap-datetimepicker value=publicAt }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" {{ action 'saveEvent' }}>Save</button>
                </div>
            </div>
        </div>
    </div>
</script>