<script type="text/x-handlebars" id="admin/events/event">
    <div class="slideout">
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}>
                    <i class="fa fa-close"></i>
                </button>
                <button class="btn btn-success" {{ action 'updateEvent' }}>Save</button>
                <button class="btn btn-danger pull-right" {{ action 'deleteEvent' }}>
                    <i class="fa fa-trash"></i>
                </button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                {{ single-photo-uploader value=file media=media }}
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Event name</label>
                                            {{
                                                input type="text"
                                                value=name
                                                class="form-control"
                                            }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                {{
                                                    input type="text"
                                                    value=price
                                                    class="form-control"
                                                    title="e.g., 19.99"
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Capacity</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                {{
                                                    input type="text"
                                                    value=capacity
                                                    class="form-control"
                                                    title="e.g., 10"
                                                }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Location</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                                                {{
                                                    input type="text"
                                                    value=location
                                                    class="form-control"
                                                    title="e.g., 123 Somewhere St., Marina, CA"
                                                }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Start</label>
                                {{ bootstrap-datetimepicker value=start }}
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">End</label>
                                {{ bootstrap-datetimepicker value=end }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label">Description</label>
                                {{
                                    textarea
                                    value=description
                                    class="form-control"
                                    rows=6
                                }}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label">Registration Deadline</label>
                                {{ bootstrap-datetimepicker value=registrationDeadline }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label">When will the event be public-visible?</label>
                                {{ bootstrap-datetimepicker value=publicAt }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>