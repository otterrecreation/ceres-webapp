App.EventsBase = DS.Model.extend({
    alertAt:                attr('number'),     // The number of registrants at which an alert will show for an event
    capacity:               attr('number'),     // The maximum amount of participants who can register for an event
    created_at:             attr('date'),       // The date an event was added to database, date created
    description:            attr('string'),     // The description of the event
    end:                    attr('date'),       // The end date/time of an event
    location:               attr('string'),     // The physical location of the event
    name:                   attr('string'),     // The name of event
    price:                  attr('number'),     // The price that a customer will pay at purchase
    publicAt:               attr('date'),       // The date an event becomes visible to the public
    publicVisible:          attr('boolean'),    // The overriding decider of an event's public visibility.
    registrationDeadline:   attr('date'),       // The date/time by which a customer can register for an event
    start:                  attr('date'),       // The start date/time of an event
    updated_at:             attr('date'),       // The date an event was last updated, date modified/updated
    
    file: belongsTo('file', { async: true }),
    
    //COMPUTED PROPERTIES FOR SORTING AND DISPLAYING
    //registrantCount: function() { return this.get('count'); }.property('count'),

    alertClass: function(registrantCount) {
        var dangerCount = isNaN(parseInt(this.get('alertAt'))) ? 0 : parseInt(this.get('alertAt'));
        var warningCount = dangerCount == 0 ? parseInt(this.get('alertAt')) / 3 : dangerCount / 2;
        //var registrantCount = parseInt(this.get('count'));

        if(registrantCount >= dangerCount) {
            return 'label-danger';
        } else if(registrantCount >= warningCount) {
            return 'label-warning';
        } else {
            return 'label-success';
        }

    }.property('alertAt', 'registrantCount')
});

App.Event = App.EventsBase.extend({});