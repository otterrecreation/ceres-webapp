App.AdminEventsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('event');
    }
});

App.AdminEventsEventRoute = Ember.Route.extend({

    model: function(params) {
        return this.store.find('event', params.event_id);
    },

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    }

});

App.AdminEventsCreateRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        
        controller.reset(); //reset the values when called
    }

});