App.AdminEventsEventView = Ember.View.extend({
    templateName: 'admin/events/event',
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
});

App.AdminEventsCreateView = Ember.View.extend(Ember.Evented, {
    templateName: 'admin/events/create',
    didInsertElement: function() {
        var self = this;
        Ember.$('#myModal').modal('show');
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.events');
        });
        this.get('controller').on('addEventDidSucceed', function() {
            self.send('closeModal');
        });
    },
    willClearRender: function() {
        this.get('controller').off('addEventDidSucceed', function() {
            self.send('closeModal');
        });
    },
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
});