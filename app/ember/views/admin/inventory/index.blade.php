<script type="text/x-handlebars" id="admin/inventory">
    <h1>Inventory {{#link-to 'admin.inventory.create' class="btn btn-primary"}}<i class="fa fa-plus"></i>{{/link-to}}
    </h1>
    <br>          
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group" role="group">
              {{#link-to 'admin.inventory' (query-params type='') class='btn btn-default' tagName='button'}}All{{/link-to}}
              {{#link-to 'admin.inventory' (query-params type='good') class='btn btn-default' tagName='button'}}Goods{{/link-to}}
              {{#link-to 'admin.inventory' (query-params type='rental') class='btn btn-default' tagName='button'}}Rentals{{/link-to}}
              {{#link-to 'admin.inventory' (query-params type='asset') class='btn btn-default' tagName='button'}}Assets{{/link-to}}
            </div>
        </div>
        <div class="col-md-4 col-md-offset-2">
            <form class="form" {{ action 'search' on="submit" }}>
                <div class="form-group">
                   <div class="input-group">
                       
                        {{ input type="text"
                           value=searchQuery
                           class="form-control"
                           placeholder="Search Inventory" }}
                       <div class="input-group-btn">
                           <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                       </div>
                   </div>
                </div>
            </form>
        </div>
    </div>
                  
<?/***********************************************
*   Display data from database below
*
***********************************************/?>
    <div class="row">
        <div class="col-md-12">
            {{#if filteredInventory}}
            <table class="table table-spaced">
                <tr>
                    <th><a href="#" {{ action 'sortBy' 'name' }}>name</a></th>
                    <th><a href="#" {{ action 'sortBy' 'type' }}>type</a></th>
                    <th><a href="#" {{ action 'sortBy' 'categoryName' }}>category</a></th>
                    <th><a href="#" {{ action 'sortBy' 'sku' }}>sku</a></th>
                    <th><a href="#" {{ action 'sortBy' 'price' }}>price</a></th>
                    <th><a href="#" {{ action 'sortBy' 'itemCount' }}>count</a></th>
                </tr>
                {{#each item in filteredInventory}}
                    {{#link-to 'admin.inventory.item' item tagName='tr' }}
                        
                        <td>
                            {{media-img-thumb file=item.file class="img-responsive"}}
                            {{ item.name }}
                        </td>
                        <td>{{ item.type }}</td>
                        <td>{{ item.inventoryCategory.category }}</td>
                        <td>{{ item.id }}</td>
                        <td>{{ currency item.price }}{{#if item.pricePer }}<span class="text-muted"> / {{ item.pricePer }}</span> {{/if}}</td>
                        <td><span {{bind-attr class=":label item.alertClass"}}>{{ item.itemCount }}</span></td>
                    {{/link-to}}
                {{/each}}
            </table>
            {{ else }}
                <p class="lead text-center">No inventory items</p>
            {{/if}}
        </div>
    </div>
    
    {{ outlet }}
    
</script>