App.AdminInventoryController = Ember.ArrayController.extend({
    
    //other controllers
    needs: ['inventoryCategories'],
    inventoryCategories: Ember.computed.alias('controllers.inventoryCategories'),
    
    inventoryTypes: ['good', 'rental', 'asset'],
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['type', 'search'],    // Creates url Query ../?type='_____' will result in a search for type
    type: null,                         // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    
    filteredInventory: function() {
        
        var type = this.get('type');    // retrieves type variable to filter by
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'id'];
        var items = this.get('arrangedContent');  // creats array of models to be filtered.
        
        if (type) {
            items = items.filterBy('type', type);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
    }.property('type', 'search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
        
    },

});



App.AdminInventoryItemController = Ember.ObjectController.extend({
    
   //other dependencies
   needs: ['inventoryCategories', 'media'],
   inventoryCategories: Ember.computed.alias('controllers.inventoryCategories'),
   media: Ember.computed.alias('controllers.media'),
    
   currentCategory: null,
    
   pricePerUnits: ['hour', 'day'],
    
   isItemGood: function() {
        return this.get('type') === 'good';
   }.property('type'),
    
   isItemRental: function() {
        return this.get('type') === 'rental';
   }.property('type'),
    
   isItemAsset: function() {
        return this.get('type') === 'asset';
   }.property('type'),
    
   itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
   }.property('model.currentState.stateName'),
    
   actions: {
        updatePublicVisible: function(value) {
            this.set('publicVisible', value);
        },

        changeInventoryCategory: function(value, object) {
            this.set('inventoryCategory', value);  
        }
   }
});




App.AdminInventoryCreateController = App.AdminInventoryItemController.extend(Ember.Evented, {
    
    inventoryTypes: ['good', 'rental', 'asset'], //legal inventory types for the dropdown
    type: 'good', //default inventory type for the dropdown
    
    pricePer: 'hour', //default pricePer value for the dropdown
    publicVisible: true,
    
    inventoryCategory: function() {
        return this.get('inventoryCategories').get('firstObject');
    }.property('inventoryCategories'),
    
    //function to reset all of the values in the modal when loaded...
    reset: function() {
        this.setProperties({
            name: '',
            price: '',
            newCategory: '',
            description: '',
            count: '',
            alertAt: ''
        });
    },
    
    actions: {
        updatePublicVisible: function(value) {
            this.set('publicVisible', value);
        },
        
        changeType: function(value) {
            console.log(value);
            this.set('type', value);
        },
        
        changeInventoryCategory: function(value, object) {
            console.log(value);
            this.set('inventoryCategory', object);  
        }
    }
});