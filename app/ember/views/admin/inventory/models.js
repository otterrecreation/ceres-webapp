App.InventoryBase = DS.Model.extend({
    //Name	Catagory ID	Description	SKU #	Price	Type	Count
    name:           attr('string'), // Name of item.
    description:    attr('string'), // Description of item.
    sku:            attr('string'), // ---still debatable---
    price:          attr('number'), // Price that a customer will pay at purchase.
    type:           attr('string'), // One of three; 'asset' 'rental' 'good'
    pricePer:       attr('string'), // One of two; 'hour' 'day'
    count:          attr('number'), // Quantity on hand of item.
    alertAt:        attr('number'), // Quantity of stock at which an aller will be displayed for a 'good'.
    publicVisible:  attr('number'), // Whether or not the the item should be show to the frontend or not
    created_at:     attr('date'),   // Date item was added to database, date created.
    updated_at:     attr('date'),   // Date item was last updated, date modified/updated.
    file:           belongsTo('file', { async: true }),
    /*************************************************************
    * Logic for getting an inventory quantity. If type is a 'good'
    * then grab count for that imtem, else look for quantity of
    * instantces.
    *************************************************************/
    //COMPUTED PROPERTIES FOR SORTING AND DISPLAYING SUB-ITEM DATA
    
    itemCount: function() {
        
        if(this.get('type') === 'good') {
            return this.get('count');
        } else {
            return this.get('inventoryInstance.length');
        }
        
    }.property('type', 'count', 'inventoryInstance.length'),
    
    alertClass: function() {
        
        var dangerCount = isNaN(parseInt(this.get('alertAt'))) ? 0 : parseInt(this.get('alertAt'));
        var warningCount = dangerCount == 0 ? 3 : dangerCount * 2;
        var itemCount = parseInt(this.get('itemCount'));
        
        if(dangerCount >= itemCount) {
            return 'label-danger';
        } else if(warningCount >= itemCount) {
            return 'label-warning';
        } else {
            return 'label-success';
        }
        
    }.property('alertAt', 'itemCount')
});

App.Inventory = App.InventoryBase.extend({

    //RELATIONSHIPS
    inventoryCategory: belongsTo('inventory-category', { async: true }),
    inventoryInstance: hasMany('inventory-instance', { async: true })
    
});


//INVENTORY
App.InventoryStatus = DS.Model.extend({
    status: attr('string'),
    inventoryInstance: hasMany('inventory-instance', {async: true}),
    created_at: attr('date'),
    updated_at: attr('date')
});


App.InventoryCondition = DS.Model.extend({
    condition: attr('string'),
    inventoryInstance: hasMany('inventory-instance', {async: true}),
    created_at: attr('date'),
    updated_at: attr('date')
});

App.InventoryCategoryBase = DS.Model.extend({
    category: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date')
});

App.InventoryCategory = App.InventoryCategoryBase.extend({
    inventory: hasMany('inventory', {async: true})
});


App.InventoryInstance = DS.Model.extend({
    publicVisible:  attr('number'), // Whether or not the the item should be show to the frontend or not
    inventory: belongsTo('inventory', { async: true }),
    inventoryStatus: belongsTo('inventory-status', { async: true }),
    inventoryCondition: belongsTo('inventory-condition', { async: true }),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    serial: function() {
        return this.get('inventory.id') + '-' + this.get('id');
    }.property('inventory.id', 'id')
});