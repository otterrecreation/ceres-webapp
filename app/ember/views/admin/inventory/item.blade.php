<script type="text/x-handlebars" id="admin/inventory/item">
    <div class="slideout">
        <div class="slideout-content">
            <div class="slideout-actions">
                <button class="btn btn-default" aria-hidden="true" {{ action 'doneEditing' }}><i class="fa fa-close"></i></button>
                <button class="btn btn-success" {{ action 'updateInventory' }}>Save</button>
                <span class="text-muted">{{ itemState }}</span>
                <button class="btn btn-danger pull-right" {{ action 'deleteInventory' }}><i class="fa fa-trash"></i></button>
            </div>
            <div class="slideout-body">
                <br>
                <div class="form">
                     <div class="form-group">
                         <div class="row">
                             {{#if isItemAsset}}
                             <div class="col-xs-6">
                                 <label class="control-label">SKU</label>
                                 {{ input type="text" 
                                    value=id
                                    disabled="disabled"
                                    class="form-control" }}
                             </div>
                             <div class="col-xs-6">
                                 <label class="control-label">Type</label>
                                 {{ input type="text" 
                                    value=type
                                    disabled="disabled"
                                    class="form-control" }}
                             </div>
                             {{ else }}
                             <div class="col-xs-4">
                                 <label class="control-label">SKU</label>
                                 {{ input type="text" 
                                    value=id
                                    disabled="disabled"
                                    class="form-control" }}
                             </div>
                             <div class="col-xs-4">
                                 <label class="control-label">Type</label>
                                 {{ input type="text" 
                                    value=type
                                    disabled="disabled"
                                    class="form-control" }}
                             </div>
                             <div class="col-xs-4">
                                 <label class="control-label">Visibility</label>
                                 {{#custom-select value=publicVisible action='updatePublicVisible' class="form-control"}}
                                    {{#custom-option value=1}}Shown{{/custom-option}}
                                    {{#custom-option value=0}}Hidden{{/custom-option}}
                                {{/custom-select}}
                             </div>
                             {{/if}}
                         </div>
                     </div>
                     <hr>
                     <div class="form-group">
                         <div class="row">
                           <div class="col-xs-5">
                                {{ single-photo-uploader media=media value=file }}
                           </div>
                           <div class="col-xs-7">
                               <div class="form-group">
                                   <div class="row">
                                       <div class="col-xs-12">
                                           <label class="control-label">Name</label>
                                           {{ input type="text" 
                                              value=name
                                              class="form-control" }}
                                       </div>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <div class="row">
                                       {{#if isItemRental }}
                                        <div class="col-xs-7">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                              <div class="input-group-addon">$</div>
                                              {{ input type="text" 
                                                 value=price
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                        <div class="col-xs-5">
                                            <label class="control-label">Per</label>
                                            {{ view "select"
                                               value=pricePer
                                               content=pricePerUnits
                                               class="form-control" }}
                                        </div>
                                       {{else}}
                                       <div class="col-xs-12">
                                           <label class="control-label">Price</label>
                                            <div class="input-group">
                                              <div class="input-group-addon">$</div>
                                              {{ input type="text" 
                                                 value=price
                                                 class="form-control" }}
                                            </div>
                                       </div>
                                       {{/if}}
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                       <div class="row">
                           <div class="col-xs-5">
                                <label class="control-label">Existing Category</label>
                                {{#custom-select value=currentCategory.content class="form-control" action="changeInventoryCategory" }}
                                   {{#each category in inventoryCategories }}
                                       {{#custom-option value=category}}{{ category.category }}{{/custom-option}}
                                   {{/each}}
                                {{/custom-select}}
                           </div>
                           <div class="col-xs-2 text-center">
                               <p>&nbsp;</p>
                               <p><strong>&ndash; or &ndash;</strong></p>
                           </div>
                           <div class="col-xs-5">
                               <label class="control-label">New Category</label>
                                {{ input type="text" 
                                   value=newCategory
                                   class="form-control" }}
                           </div>
                       </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                               <label class="control-label">Description</label>
                                {{ textarea
                                   value=description 
                                   class="form-control"
                                   rows=6 }}  
                            </div>
                        </div>
                    </div>   
                    <hr>
                    {{#if isItemGood }}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">Item Count</label>
                                {{ input type="text"
                                   value=count 
                                   class="form-control" }}
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Alert at</label>
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></div>
                                  {{ input type="text" 
                                     value=alertAt
                                     class="form-control" }}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{else}}
                    <div class="row">
                        <div class="col-xs-5">
                            <h2>Sub-items </h2>
                        </div>
                        <div class="col-xs-4">
                           <form class="form" {{ action 'addInstances' on='submit' }}>
                            <div class="form-group">
                               <label>Add Sub-items</label>
                               <div class="input-group">
                                    {{ input type="text" 
                                       value=instanceCount
                                       class="form-control"
                                       placeholder="count" }}
                                   <div class="input-group-btn">
                                       <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span></button>
                                   </div>
                               </div>
                            </div>
                            </form>
                        </div>
                        <div class="col-xs-3">
                            <label class="control-label">Alert at</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></div>
                              {{ input type="text" 
                                 value=alertAt
                                 class="form-control" }}
                            </div>
                        </div>
                    </div>                       
                    <table class="table table-hover">
                        <tr>
                            <th>Serial</th>
                            <th>Condition</th>
                            <th colspan="2">Status</th>
                        </tr>
                        {{#each instance in inventoryInstance }}
                        <tr>
                            <td>{{ instance.serial }}</td>
                            <td>{{ instance.inventoryCondition.condition }}</td>
                            <td>{{ instance.inventoryStatus.status }}</td>
                            <td class="text-right">
                               <a href="#" {{ action 'deleteInstance' instance }}>
                                   <i class="fa fa-trash"></i>
                               </a>
                            </td>
                        </tr>
                        {{ else }}
                        <tr>
                            <td colspan="3">
                                <p class="lead text-center">
                                    No sub-items
                                </p>
                            </td>
                        </tr>
                        {{/each}}
                    </table>
                    {{/if}}
                </div>
            </div>
        </div>
    </div>
</script>