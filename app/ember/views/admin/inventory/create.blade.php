<script type="text/x-handlebars" id="admin/inventory/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header modal-emphasis">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                New Inventory Item
                
                {{#custom-select value=type action="changeType" class="form-control form-override" }}
                    {{#custom-option value='good'}}Good{{/custom-option}}
                    {{#custom-option value='rental'}}Rental{{/custom-option}}
                    {{#custom-option value='asset'}}Asset{{/custom-option}}
                {{/custom-select}}
            </h3>
          </div>
        <?/***********************************************
        *   Add form for inventory, broken up in a table
        *
        ***********************************************/?> 
          <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-6 col-xs-offset-3 col-sm-offset-0 col-sm-5">
                                {{single-photo-uploader media=media value=file}}
                            </div>
                            <div class="col-xs-12 col-sm-7">
                               {{#if isItemGood }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Name</label>
                                            {{ input type="text" 
                                               value=name
                                               class="form-control" }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                              <div class="input-group-addon">$</div>
                                              {{ input type="text" 
                                                 value=price
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="control-label">Visibility</label>
                                            {{#custom-select value=publicVisible action='updatePublicVisible' class="form-control"}}
                                                {{#custom-option value=1}}Shown{{/custom-option}}
                                                {{#custom-option value=0}}Hidden{{/custom-option}}
                                            {{/custom-select}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{#if isItemGood }}
                                            <label class="control-label">Item Count</label>
                                            {{ else }}
                                            <label class="control-label">Sub-item Count</label>
                                            {{/if}}
                                            {{ input type="text"
                                               value=count 
                                               class="form-control" }}
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Alert at</label>
                                            <div class="input-group">
                                              <div class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></div>
                                              {{ input type="text" 
                                                 value=alertAt
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               {{/if}}
                               {{#if isItemRental }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Name</label>
                                            {{ input type="text" 
                                               value=name
                                               class="form-control" }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                              <div class="input-group-addon">$</div>
                                              {{ input type="text" 
                                                 value=price
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="control-label">Per</label>
                                            {{ view "select"
                                               value=pricePer
                                               content=pricePerUnits
                                               class="form-control" }}
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="control-label">Visibility</label>
                                            {{#custom-select value=publicVisible action='updatePublicVisible' class="form-control"}}
                                                {{#custom-option value=1}}Shown{{/custom-option}}
                                                {{#custom-option value=0}}Hidden{{/custom-option}}
                                            {{/custom-select}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{#if isItemGood }}
                                            <label class="control-label">Item Count</label>
                                            {{ else }}
                                            <label class="control-label">Sub-item Count</label>
                                            {{/if}}
                                            {{ input type="text"
                                               value=count 
                                               class="form-control" }}
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Alert at</label>
                                            <div class="input-group">
                                              <div class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></div>
                                              {{ input type="text" 
                                                 value=alertAt
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               {{/if}}
                               {{#if isItemAsset }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Name</label>
                                            {{ input type="text" 
                                               value=name
                                               class="form-control" }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                   <div class="row">
                                        <div class="col-xs-12">
                                            <label class="control-label">Price</label>
                                            <div class="input-group">
                                              <div class="input-group-addon">$</div>
                                              {{ input type="text" 
                                                 value=price
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            {{#if isItemGood }}
                                            <label class="control-label">Item Count</label>
                                            {{ else }}
                                            <label class="control-label">Sub-item Count</label>
                                            {{/if}}
                                            {{ input type="text"
                                               value=count 
                                               class="form-control" }}
                                        </div>
                                        <div class="col-xs-6">
                                            <label class="control-label">Alert at</label>
                                            <div class="input-group">
                                              <div class="input-group-addon"><i class="fa fa-exclamation-triangle"></i></div>
                                              {{ input type="text" 
                                                 value=alertAt
                                                 class="form-control" }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               {{/if}}
                            </div>
                        </div>
                    <hr>
                    <div class="form-group">
                       <div class="row">
                           <div class="col-xs-12 col-sm-5">
                                <label class="control-label">Existing Category</label>
                               {{#custom-select value=inventoryCategory class="form-control" action="changeInventoryCategory" object=inventoryCategory }}
                                   {{#each category in inventoryCategories }}
                                       {{#custom-option value=category}}{{ category.category }}{{/custom-option}}
                                   {{/each}}
                               {{/custom-select}}
                           </div>
                           <div class="col-xs-12 col-sm-2 text-center">
                               <p>&nbsp;</p>
                               <p><strong>&ndash; or &ndash;</strong></p>
                           </div>
                           <div class="col-xs-12 col-sm-5">
                               <label class="control-label">New Category</label>
                                {{ input type="text" 
                                   value=newCategory
                                   class="form-control" }}
                           </div>
                       </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                               <label class="control-label">Description</label>
                                {{ textarea
                                   value=description 
                                   class="form-control"
                                   rows=6 }}  
                            </div>
                        </div>
                    </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="button" class="btn btn-primary" {{ action 'saveItem' }}>Save</button>
          </div>
        </div>
      </div>
    </div>
</script>