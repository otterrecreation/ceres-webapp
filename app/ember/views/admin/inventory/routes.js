App.AdminInventoryRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('inventory');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
    }
});

App.AdminInventoryItemRoute = Ember.Route.extend({

    model: function(params) {
        return this.store.find('inventory', params.inventory_id);
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        controller.set('currentCategory', model.get('inventoryCategory'));
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
        
    },
    
    actions: {
        updateInventory: function() {
            
            var self = this;
            var controller = this.get('controller');
            var item = controller.get('model');
            
            console.log(item);
            
            if(controller.get('newCategory')) {
                
                var newCategory = this.store.createRecord('inventoryCategory', { category: controller.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    
                    item.set('inventoryCategory', category);
                    item.save().then(function(results) {
                        self.set('newCategory', ''); //clear the new category value now that it's been added to the dropdown
                        console.log('update was successful');
                    }, function(results) {
                        item.rollback(); //save didn't work...don't persist any of the changes to ember
                    });
                    
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
                
            } else {
                
                item.set('inventoryCategory', controller.get('inventoryCategory'));    
                item.save().then(function(results) {
                    console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
            }
            
        },
       
       
        addInstances: function() {
            var controller = this.get('controller');
            var instanceCount = controller.get('instanceCount');
            for(var i = 0; i < instanceCount; i++) {
                var newInstance = this.store.createRecord('inventoryInstance', { inventory: controller.get('model') });
                newInstance.save().then(function(results) {
                    console.log(results);
                }, function(results) {
                    console.log(results);
                    newInstance.destroyRecord();
                });
            }
        },
       
       
       deleteInstance: function(instance) {
           var confirmDelete = confirm("Are you sure you want to delete this sub-item?");
           if (confirmDelete) {
               instance.deleteRecord();
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                    }, function(results) {
                            instance.rollback();
                    });
                }
            }
       },
       
        backToInventory: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteInventory: function() {
            
            var controller = this.get('controller');
            console.log('delete inventory...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this inventory item?");
            if (confirmDelete) {
                this.send('backToInventory', function() {
                    var record = controller.get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.replaceWith('admin.inventory');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;
            var controller = this.get('controller');
            
            this.send('backToInventory', function() {
                controller.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionTo('admin.inventory');
            });
        }
    }
    
});

App.AdminInventoryCreateRoute = Ember.Route.extend({
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        
        this.controllerFor('media');
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
        
        controller.reset(); //reset the values when called
    },
    
    actions: {
        saveItem: function() {
            
            var self = this;
            var controller = this.get('controller');
            //grab all relavent data for creating a new inventory item
            var data = controller.getProperties('type', 
                                                'name',
                                                'price',
                                                'description',
                                                'publicVisible',
                                                'count',
                                                'alertAt',
                                                'file');
            
            console.log(this.get('inventoryCategory'));
            
            if(data.type === 'rental') {
                data.pricePer = this.get('pricePer');
            }
            
            //create a new category or grab the existing one based on the categoryId
            if(this.get('newCategory')) {
                console.log('new category');
                var newCategory = this.store.createRecord('inventoryCategory', { category: controller.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    data.inventoryCategory = category;
                    self.send('saveItemWithData', data);
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
            } else {
                if(!this.get('inventoryCategory')) {
                    data.inventoryCategory = controller.get('inventoryCategories').get('firstObject');
                } else {
                    data.inventoryCategory = controller.get('inventoryCategory');
                }
                
                self.send('saveItemWithData', data);
            }
        },
        
        saveItemWithData: function(data) {
            var self = this;
            var item = this.store.createRecord('inventory', data);
            var controller = this.get('controller');
            
            item.save().then(function(item) {
                //now attach the item to the category
                controller.trigger('modalActionDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('inventory saved unsuccessfully');
                console.log(results);
            });
        }
    }
    
});