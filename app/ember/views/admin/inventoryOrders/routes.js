App.AdminInventoryOrdersRoute = Ember.Route.extend({
        
    model: function() {
        // the model will be pulled from the order model in models.js file
        return this.store.find('inventoryOrder'); 
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});


App.AdminInventoryOrderRoute = Ember.Route.extend({
    
    model: function(params) {

    // filter array inventory, by publicVisable set to 1(true) 
        return this.store.find('inventoryOrder',  params.inventory_orders_id );  
    },
    
    setupController: function(controller, model) {
                
        controller.set('model', model);
//        controller.set('inventoryOrderItems', this.store.find('inventoryOrderItem'));
  
        
    }
});


App.AdminInventoryOrderItemsRoute = Ember.Route.extend({
    
    
    model: function(params) {
        return this.store.find('inventoryOrderItem', params.inventoryOrder_id );
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
});

