<script type="text/x-handlebars" id="admin/inventoryOrders/inventoryOrder">
    <h1>Order Details
    </h1>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><strong>PO# : </strong> {{ po_number }}</h1>
        </div>
        <div class="panel-body">
            <table>
                <tr><td>id: </td>               <td>{{id}} </td></tr>
                <tr><td>po_number: </td>        <td>{{po_number}} </td></tr>
                <tr><td>shipping_cost: </td>    <td>{{shipping_cost}} </td></tr>
                <tr><td>total_cost: </td>       <td>{{total_cost}} </td></tr>
                <tr><td>ordered_at: </td>       <td>{{ordered_at}} </td></tr>
                <tr><td>fulfilled_at: </td>     <td>{{fulfilled_at}} </td></tr>
                <tr><td>created_at: </td>       <td>{{created_at}} </td></tr>
                <tr><td>updated_at: </td>       <td>{{updated_at}} </td></tr>
                <tr><td>received_at: </td>      <td>{{received_at}} </td></tr>
                <tr><td>notes: </td>            <td>{{notes}} </td></tr>
                <tr><td>vendor: </td>           <td>{{vendor.vendor_name}} </td></tr>
                <tr><td>order_status: </td>     <td>{{order_status.status}} </td></tr>
                <tr><td>ordered_by: </td>       <td>{{ordered_by.name}} </td></tr>
                <tr><td>entered_by: </td>       <td>{{entered_by.name}} </td></tr>
                <tr><td>last_modified_by: </td> <td>{{last_modified_by.name}} </td></tr>
                <tr><td>last_modified_by: </td> <td>{{inventoryOrderList}} </td></tr>
            </table>

            <br>      
            
            <table calss="table">
                <tr>
                    <th><strong>Ordered Items</strong></th>
                </tr>
                <tr>
                    <th>Item Cost</th>
                    <th>Qty Ordered</th>
                    <th>Qty Recieved</th>
                    <th>Item Name</th>
                    <th>Item Desc.</th>
                    <th>Ordered For</th>
                    <th>Vendor Name</th>
                </tr>
                    {{log 'below'}}
                    {{log inventoryOrderList}}
                    {{log inventoryOrderItem}}
                    {{log 'above'}}
                    {{#each item in inventoryOrderList}}
                        <td>{{item.item_cost}}</td>
                        <td>{{item.qty_ordered}}</td>
                        <td>{{#if item.orderItemReceived}}
                                {{item.qty_received}}
                            {{else}}
                                Not Entered
                            {{/if}}
                        </td>                
                        <td>{{item.inventory_id.name}}</td>
                        <td>{{item.inventory_id.description}}</td>
                        <td>{{item.intended_for.name}}</td>
                        <td>{{item.vendor_id.vendor_name}}</td>
                        
                    {{else}}
                <br>
                        <th><strong>Order Is Empty</strong></th>
                    {{/each}}
                    
                <tr>  
                    <td><p>{{ description }}</p></td>
                </tr>
            </table>
            
            
        </div>
    </div>
</script>