App.AdminRestockOrderView = Ember.View.extend({
    
    templateName: 'admin/restock/order',
    
    didInsertElement: function() {
        console.log('in thing stuff');
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
    
});

App.AdminRestockCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/restock/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.restock');
        });
        
        this.get('controller').on('addItemDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addItemDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});