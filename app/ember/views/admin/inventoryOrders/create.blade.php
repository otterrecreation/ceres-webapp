<script type="text/x-handlebars" id="admin/restock/create">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                New Order 
                {{log 'New restock'}}
            </h3>
          </div>
        <?/***********************************************
        *   Add form for inventory, broken up in a table
        *
        ***********************************************/?>
        <form class="form" {{action 'addItem' on='submit'}}>  
          <div class="modal-body">                      
                    <div class="form-group">
                        
                    </div>
                    <hr>
                    <div class="form-group">
                       <div class="row">
                           <div class="col-xs-5">
                                <label class="control-label">Existing Category</label>
                                
                           </div>
                           <div class="col-xs-2 text-center">
                               <p>&nbsp;</p>
                               <p><strong>&ndash; or &ndash;</strong></p>
                           </div>
                           <div class="col-xs-5">
                               <label class="control-label">New Category</label>
                                {{ input type="text"
                                   value=newCategory
                                   class="form-control" }}
                           </div>
                       </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                        </div>
                    </div>   
                    <hr>                                       
                    <div class="form-group">
                        <div class="row">
                        </div>
                    </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary" {{ action 'saveItem' }}>Save</button>
          </div>
        </form> 
        </div>
      </div>
    </div>
</script>