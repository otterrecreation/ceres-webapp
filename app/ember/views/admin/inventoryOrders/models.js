App.InventoryOrder = DS.Model.extend({   
    
    po_number:      attr('string'), //
    shipping_cost:  attr('string'), //
    total_cost:     attr('string'), //
    ordered_at:     attr('date'),   //
    fulfilled_at:   attr('date'),   //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   //
    notes:          attr('string'), //
    
    //Relationships
    vendor:             belongsTo('inventory-order-vendor', { async: true }),
    order_status:       belongsTo('inventory-order-status', { async: true }),
    ordered_by:         belongsTo('user', { async: true }),
    entered_by:         belongsTo('user', { async: true }),
    last_modified_by:   belongsTo('user', { async: true }),
    
    inventoryOrderList: hasMany('inventory-order-item', { async: true }),

});


App.InventoryOrderStatus = DS.Model.extend({  
    
    created_at: attr('date'),   //
    updated_at: attr('date'),   //
    status:     attr('string'),
    
    //Relationships
    
});


App.InventoryOrderVendor = DS.Model.extend({  
    
    vendor_name:    attr('string'), //
    rep_name:       attr('string'), //
    phone:          attr('string'), //
    ext:            attr('string'), //
    account_number: attr('string'), //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   //
    
    //Relationships
    
});


App.InventoryOrderItem = DS.Model.extend({  
    
    item_cost:      attr('string'), //
    qty_ordered:    attr('string'), //
    qty_received:   attr('string'), //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   // 
    
    //Relationships
    inventory_id:   belongsTo('inventory', { async: true }), //
    order_id:       belongsTo('inventory-order', { async: true }), //
    intended_for:   belongsTo('user', { async: true }), //
    vendor_id:      belongsTo('inventory-order-vendor', { async: true }), //
    
    //Computed property
    orderItemReceived: function() {
        var isEntered = false;
        var itemCount = (this.get('qty_received'));

        
        if ( itemCount == '')
        {
            isEntered = false;
            
        } else {isEntered = true;}
        
        return isEntered;
        
    }.property('qty_received'),

});


