<script type="text/x-handlebars" id="admin/inventoryOrders">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Inventory Orders
                </div>
                <div class="panel-body">
                    <div class="list-group">
                    {{#each order in model }}
                        {{#link-to 'admin.inventoryOrders.inventoryOrder' order class="list-group-item"}} 
                            PO# {{ order.po_number }}
                            {{log order}}
                        {{/link-to}}
                    {{else}}
                        Empty Orders
                    {{/each}}   

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            {{ outlet }}
        </div>
    </div>
</script>



