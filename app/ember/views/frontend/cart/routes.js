App.CartRoute = Ember.Route.extend({

    model: function() {
    
        return this.store.find('cartItem'); //we are grabbing the cart so we can then pull all of the items and do the realy stuff
    
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        if( !Ember.isEmpty(window.currentUser) ) {
            console.log('currentUser exists');
            controller.set('currentUser', this.store.find('user', window.currentUser.id));
        }
    },
    
    processNewAddress: function() {
        var controller = this.get('controller');
        
        
        
        return true;
    },
    
    processNewCard: function() {
        
    },
    
    actions: {
    
        removeItem: function(item) {
            item.deleteRecord();
            if(item.get('isDeleted')) {
                item.save().then(function(results) {
//                  console.log(results);
                }, function(results) {
                    console.log(results);
                    item.rollback();
                });
            }
        },
        
        anonymousCheckout: function() {
            console.log('checkout for anonymous user');
        },
        
        userCheckout: function() {
            
            var controller = this.get('controller');
            controller.set('isCheckingOut', true);
            
            var processNewAddress = false;
            var processNewCard = false;
            
            //time to process the address
            if( controller.get('recipient_name') ||
                controller.get('address_line_1') || 
                controller.get('address_line_2') || 
                controller.get('city') ||
                controller.get('state_province') ||
                controller.get('zip') ||
                controller.get('country') ) {

                if( !controller.get('recipient_name') ||
                    !controller.get('address_line_1') || 
                    !controller.get('address_line_2') || 
                    !controller.get('city') ||
                    !controller.get('state_province') ||
                    !controller.get('zip') ||
                    !controller.get('country') ) {
                
                    Ember.$.growl.error({ title: 'Error', message: 'The address you entered was not valid. Please check that you entered all of the fields'});
                    controller.set('isCheckingOut', false);
                    return; //stop. just stop.
                    
                } else {
                
                    processNewAddress = true;
                
                }
                
            } else if ( !controller.get('existingAddress') ) {
                Ember.$.growl.error({ title: 'Error', message: 'Please enter or choose an address'});
                controller.set('isCheckingOut', false);
                return; //stop. just stop.
            }
            
            //time to process the card...
            if( controller.get('card_holder') ||
                controller.get('card_number') ||
                controller.get('cvc') || 
                controller.get('exp_month') ||
                controller.get('exp_year') ) {
                
                if( !controller.get('card_holder') ||
                    !controller.get('card_number') ||
                    !controller.get('cvc') || 
                    !controller.get('exp_month') ||
                    !controller.get('exp_year') ) {
                
                    Ember.$.growl.error({ title: 'Error', message: 'We could not process your address correctly.'});
                    controller.set('isCheckingOut', false);
                    return; //stop. just stop.
                } else {
                    processNewCard = true;
                }
                
            } else if( !controller.get('existingCard') ) {
                Ember.$.growl.error({ title: 'Error', message: 'Please enter or choose payment information'});
                controller.set('isCheckingOut', false);
                return; //stop. just stop.
            }
            
            //now we can attempt to checkout.
            this.send('checkout');
            
        },
        
        saveNewAddress: function() {
            
            var controller = this.get('controller');
            
            var self = this;
			var data = controller.getProperties('recipient_name',
										        'address_line_1',
										        'address_line_2',
										        'city',
										        'state_province',
										        'zip',
										        'country');
			
			var userAddress = this.store.createRecord('userAddress', data);
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
                
                controller.set('existingAddress', userAddress);
                controller.resetAddress();
                
                Ember.$.growl.notice({ title: 'Success', message: 'New address added successfully' });
                
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.destroyRecord();
			});
        },
        
        checkout: function() {
        
        },
        
        flipCard: function() {
            Ember.$('.flipped-container .flipped-card').toggleClass('flipped');
        }
    
    }
    
});