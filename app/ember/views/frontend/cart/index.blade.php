<script type="text/x-handlebars" id="cart">
   <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="flipped-container">
                    <div class="flipped-card">
                        <div class="flipped-front flipped-face">
                                   <h1 class="text-center"><i class="fa fa-shopping-cart"></i> Cart</h1>
                                    {{#if model}}
                                    <table class="table table-cart panel well">
                                        <tr>
                                            <th>Item</th>
                                            <th>Count</th>
                                            <th class="text-right">Price</th>
                                        </tr>
                                        {{#each item in model }}
                                        <tr>
                                            <td>{{ item.item_name }}</td>
                                            <td>{{ item.count }}</td>
                                            <td class="text-right">{{ currency item.price }} <a href="#" {{ action 'removeItem' item }}<i class="fa fa-times"></i></a></td>
                                        </tr>
                                        {{/each}}
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                <strong>Total: {{ currency totalPrice }}</strong>&nbsp;&nbsp;<button class="btn btn-sm btn-success" {{ action 'flipCard' }}>Proceed to Checkout</button>
                                            </td>
                                        </tr>
                                    </table>
                                    {{else}}
                                    <div class="row panel well">
                                        <p>&nbsp;</p>
                                        <p class="lead text-center">Nothing in your cart yet. {{#link-to 'shop' class="btn btn-primary"}}Shop{{/link-to}}</p>
                                        </div>
                                    {{/if}}
                                    <br>
                                </div>
                        <div class="flipped-back flipped-face">
                           <h1 class="text-center"><i class="fa fa-credit-card"></i> Checkout</h1>
                            <div class="panel well">
                                <div class="container-fluid">
                                {{#if currentUser }}
                                    <h3>Shipping Info</h3>
                                <form class="checkout-form" {{action 'userCheckout' on='submit'}}>
                                    <div class="row vertical-align">
                                        <div class="col-xs-4">
                                            {{#custom-select value=existingAddress class="form-control"}}
                                                {{#custom-option value=0}}Choose an address{{/custom-option}}
                                                {{#each address in currentUser.addresses}}
                                                    {{#custom-option value=address}}{{ address.address_line_1 }} {{ address.address_line_2 }} {{ address.city }}, {{ address.zip }}{{/custom-option}}
                                                {{/each}}
                                            {{/custom-select}}
                                        </div>
                                        <!--  -->
                                        <div class="col-xs-2"><p class="text-center">&mdash; OR &mdash;</p></div>
                                        <!--  -->
                                        <div class="col-xs-6">
                                            <div class="row">

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        {{input type="text" value=recipient_name placeholder="name" class="form-control"}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        {{input type="text" value=address_line_1 placeholder="address line 1" class="form-control"}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        {{input type="text" value=address_line_2 placeholder="address line 2" class="form-control"}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="form-group">
                                                        {{input type="text" value=city placeholder="city" class="form-control"}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="form-group">
                                                        {{input type="text" value=zip placeholder="zip" class="form-control"}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        {{input type="text" value=state_province placeholder="state" class="form-control"}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        {{input type="text" value=country placeholder="country" class="form-control"}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label>
                                                        Save new address {{input type="checkbox" checked=saveAddress}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Billing Info</h3>
                                    <div class="row vertical-align">
                                        <div class="col-md-4">
                                            {{#custom-select value=existingBilling class="form-control"}}
                                                {{#custom-option value=0}}Choose a card{{/custom-option}}
                                                {{#each billing in currentUser.billingInfo}}
                                                    {{#custom-option value=billing}}{{ billing.name }} -  &bull;&bull;&bull;&bull;&nbsp;&bull;&bull;&bull;&bull;&nbsp;{{ billing.last4 }}{{/custom-option}}
                                                {{/each}}
                                            {{/custom-select}}
                                        </div>
                                        <div class="col-xs-2 text-center">&mdash; OR &mdash;</div>
                                        <div class="col-xs-6">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        {{input type="text" value=card_holder placeholder="Cardholder" class="form-control"}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-xs-8">
                                                    <div class="form-group">
                                                        {{input type="text" value=card_number placeholder="card #" class="form-control"}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="form-group">
                                                        {{input type="text" value=cvc placeholder="cvc" class="form-control"}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        {{input type="text" value=exp_month placeholder="exp. month" class="form-control"}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                        {{input type="text" value=exp_year placeholder="exp. year" class="form-control"}}
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>
                                                            Save new card {{input type="checkbox" checked=saveCard}}
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-sm btn-default" {{ action 'flipCard' }} {{bind-attr disabled=isCheckingOut}}>Back to cart</button>
                                        <button class="btn btn-sm btn-success" {{ action 'userCheckout' }} {{bind-attr disabled=isCheckingOut}}>{{#if isCheckingOut }}Processing Checkout{{ else }}Checkout{{/if}}</button>
                                    </div>
                                </form>
                                {{ else }}

                                    <h3>Shipping Info</h3>
                                <form class="checkout-form" {{action 'anonymousCheckout' on='submit'}}>
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                {{input type="text" placeholder="name" class="form-control"}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                {{input type="text" placeholder="address line 1" class="form-control"}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                {{input type="text" placeholder="address line 2" class="form-control"}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                {{input type="text" placeholder="city" class="form-control"}}
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                {{input type="text" placeholder="zip" class="form-control"}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                {{input type="text" placeholder="state" class="form-control"}}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                {{input type="text" placeholder="country" class="form-control"}}
                                            </div>
                                        </div>
                                    </div>

                                    <h3>Billing Info</h3>
                                    <div class="row">

                                        <div class="col-xs-8">
                                            <div class="form-group">
                                                {{input type="text" placeholder="card #" class="form-control"}}
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                {{input type="text" placeholder="cvc" class="form-control"}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                {{input type="text" placeholder="exp. month" class="form-control"}}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                {{input type="text" placeholder="exp. year" class="form-control"}}
                                            </div>
                                        </div>

                                    </div>
                                <div class="text-right">
                                    <button class="btn btn-sm btn-default" {{ action 'flipCard' }} {{bind-attr disabled=isCheckingOut}}>Back to cart</button>
                                    <button class="btn btn-sm btn-success" {{ action 'anonymousCheckout' }} {{bind-attr disabled=isCheckingOut}}>{{#if isCheckingOut }}Processing Checkout{{ else }}Checkout{{/if}}</button>
                                </div>
                                </form>
                                {{/if}}
                                <div class="clearfix"></div>
                                <br>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>