App.CartController = Ember.ArrayController.extend({
    
    resetAddress: function() {
        this.setProperties({
            recipient_name: '',
            address_line_1: '',
            address_line_2: '',
            city: '',
            state_province: '',
            zip: '',
            country: ''
        });
    },
    
    isCheckingOut: false,
    
    totalPrice: function() {
    
        var total = 0;
        
        this.get('model').forEach(function(cart) {
            total += cart.get('price');
        });
        
        return total;
    
    }.property('@each.price', '@each.quantity')
    
});