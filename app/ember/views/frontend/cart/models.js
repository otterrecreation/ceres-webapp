App.CartItem = DS.Model.extend({
    
    table_name: attr('string'),
    table_id: attr('number'),
    item_name: attr('string'),
    quantity: attr('number'),
    paid: attr('number'),
    price: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date'),
    //relationships
    cart: attr('number')
    
});

//just so we don't break fixtures
App.CartItem.reopenClass({
    FIXTURES: [
        {
            id: 'fixture-1',
            table_name: 'inventory',
            item_name: 'item 1',
            quantity: 1,
            paid: 0,
            price: 50,
            cart: 1
        }
    ]
});