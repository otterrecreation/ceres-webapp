<script type="text/x-handlebars" id="blog">
    <div class="container">
        <h1>Blog</h1>
        {{#each blog in model}}
            {{#link-to 'singleBlog' blog tagName='div' class='blog panel panel-default hoverable' }}
                <div class="panel-heading">
                    <i class="fa fa-user"></i>{{ blog.author.name }}&nbsp;&nbsp;&nbsp;<i class="fa fa-calendar-o"></i> {{ moment blog.created_at }}
                </div>
                <div class="container-fluid">
                   <div class="row">
                       <div class="col-xs-12">
                             <h1>{{ blog.title }}</h1>
                            {{{ blog.page_content }}}  
                       </div>
                   </div>
                </div>
            {{/link-to }}
        {{ else }}
            <p class="lead text-center">No blog posts yet</p>
        {{/each}}
    </div>
</script>