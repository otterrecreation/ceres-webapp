<script type="text/x-handlebars" id="singleBlog">
    <div class="container">
        <h1>{{ title }}</h1>
        {{{ page_content }}}
    </div>
</script>