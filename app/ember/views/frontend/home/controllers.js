App.HomeController = Ember.ArrayController.extend({
    needs: ['currentPage', 'randomRental', 'randomGood', 'randomEvent'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    randomRental: Ember.computed.alias('controllers.randomRental'),
    randomGood: Ember.computed.alias('controllers.randomGood'),
    randomEvent: Ember.computed.alias('controllers.randomEvent')
});