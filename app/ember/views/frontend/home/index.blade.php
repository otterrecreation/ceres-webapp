<script type="text/x-handlebars" id="home">
    {{ carousel-slideshow slides=currentPage.slideshow.slides }}
    <div class="container">
        <br>
        <!--SHOP MEDIUM AND UP-->
        <div class="row hidden-xs hidden-sm">
               <div class="col-md-9">
                   <h2 class="featurette-heading">Shop</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'shop' class="btn btn-danger btn-lg"}}Shop <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div>
               <div class="col-md-3">
                   {{ media-img-thumb file=randomGood.firstObject.file placeholder="<% URL::to('/') %>/media/images/shop_placeholder.jpg" class="img-circle img-responsive dropshadow"}}
               </div> 
        </div>
        
        <!--SHOP SMALL AND EXTRA SMALL-->
        <div class="row visible-xs visible-sm">
               <div class="col-xs-8 col-xs-offset-2">
                   {{ media-img-thumb file=randomGood.firstObject.file placeholder="<% URL::to('/') %>/media/images/shop_placeholder.jpg" class="img-circle img-responsive"}}
               </div>
               <div class="col-xs-12 text-center">
                   <h2 class="featurette-heading">Shop</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'shop' class="btn btn-danger btn-lg"}}Shop <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div> 
        </div>

        <hr> 
        
        <!--RENTAL MEDIUM AND UP-->
        <div class="row hidden-xs hidden-sm">
               <div class="col-md-3">
                   {{ media-img-thumb file=randomRental.firstObject.file placeholder="<% URL::to('/') %>/media/images/rent_placeholder.jpg" class="img-circle img-responsive dropshadow"}}
               </div>
               <div class="col-md-9 text-right">
                   <h2 class="featurette-heading">Rentals</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'rentals' class="btn btn-primary btn-lg"}}Rent <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div>               
        </div>
         
         <!--RENTAL SMALL AND EXTRA SMALL-->
         <div class="row visible-xs visible-sm">
               <div class="col-xs-8 col-xs-offset-2">
                   {{ media-img-thumb file=randomRental.firstObject.file placeholder="<% URL::to('/') %>/media/images/rent_placeholder.jpg" class="img-circle img-responsive pull-center"}}
               </div>
               <div class="col-xs-12 text-center">
                   <h2 class="featurette-heading">Rentals</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'rentals' class="btn btn-primary btn-lg"}}Rent <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div>               
        </div>

         <hr>  

       <!--EVENTS MEDIUM AND UP-->
        <div class="row hidden-xs hidden-sm">
               <div class="col-md-9">
                   <h2 class="featurette-heading">Events</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'events' class="btn btn-success btn-lg"}}Sign up <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div>
               <div class="col-md-3">
                   {{ media-img-thumb file=randomEvent.firstObject.file placeholder="<% URL::to('/') %>/media/images/event_placeholder.jpg" class="img-circle img-responsive dropshadow"}}
               </div>
        </div>
        
        <!--EVENTS SMALL AND EXTRA SMALL-->
        <div class="row visible-xs visible-sm">
              <div class="col-xs-8 col-xs-offset-2">
                   {{ media-img-thumb file=randomEvent.firstObject.file placeholder="<% URL::to('/') %>/media/images/event_placeholder.jpg" class="img-circle img-responsive pull-center"}}
               </div>
               <div class="col-xs-12 text-center">
                   <h2 class="featurette-heading">Events</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum, ligula ut euismod pharetra, mauris felis pulvinar est, eu lacinia risus eros non tortor. Ut imperdiet efficitur ligula. Mauris dapibus ipsum ac eros lacinia, non fermentum massa fermentum. Suspendisse laoreet tellus erat, non hendrerit dui aliquet id. Mauris interdum efficitur sapien. Nulla interdum aliquam vehicula.</p>
                    <p class="text-center">
                        {{#link-to 'events' class="btn btn-success btn-lg"}}Sign up <i class="fa fa-chevron-right"></i>{{/link-to}}
                    </p>
               </div>
        </div>
        <br>
    </div>
</script>