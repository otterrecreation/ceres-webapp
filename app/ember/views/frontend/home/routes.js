App.HomeRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 1));
        this.controllerFor('randomRental').set('model', this.store.find('rental', { random: true }));
        this.controllerFor('randomGood').set('model', this.store.find('good', { random: true }));
        this.controllerFor('randomEvent').set('model', this.store.find('event', { random: true }));
    }
});