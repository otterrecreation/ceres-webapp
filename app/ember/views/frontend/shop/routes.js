App.ShopRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('good');  // filter array inventory, by publicVisable set to 1(true) 
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 3));
        this.controllerFor('goodCategories').set('model', this.store.find('goodCategory'));
    }
    
});

App.ShopItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('good',  params.inventory_id );  // filter array inventory, by publicVisable set to 1(true) 
    }
    
});