<script type="text/x-handlebars" id="shop">
  {{ carousel-slideshow slides=currentPage.slideshow.slides }}
  <div class="container">
        <div class="row">
           <div class="col-md-4">
               <h1>Shop</h1>
           </div>
           <h1 class="col-md-8 text-right">
               <div class="form form-inline">
                  <form {{action 'search' on='submit'}}>
                       {{ view "select"
                          selection=goodCategory
                          content=goodCategories
                          optionValuePath="content.id"
                          optionLabelPath="content.category"
                          class="form-control"
                          prompt="All Categories" }}
                       <div class="input-group">
                            {{ input type="text" 
                               value=searchQuery
                               class="form-control"
                               placeholder="Search Shop" }}
                           <div class="input-group-btn">
                               <button type="submit" class="btn btn-primary" {{ action 'search' }}><span class="fa fa-search"></span></button>
                           </div>
                       </div>
                   </form>
               </div>
           </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{#each inventory in filteredInventory}}
                    {{#link-to 'shopItem' inventory tagName='div' class='thumbnail-media panel'}}
                        <div class="thumbnail-actions text-left">
                            <button class="btn btn-danger pull-right"><i class="fa fa-shopping-cart"></i></button>
                            <div class="clearfix"></div>
                        </div>

                        {{media-img-thumb file=inventory.file class='img-responsive'}}
                        <div class="thumbnail-title">
                           {{ inventory.name }} <br> {{currency inventory.price}} 
                       </div>
                    {{/link-to}}
                {{ else }}
                    <p class="lead text-center">No shopping items</p>
                {{/each}}
            </div>
        </div>
    </div>
</script>

<script type="text/x-handlebars" id="shopItem">
   <div class="container">
    <div class="row">
       <div class="col-md-12">
           <h1>{{#link-to 'shop'}}Shop{{/link-to}} <span class="text-muted">/</span> {{ name }}
               <button class="btn btn-danger btn-sm pull-right" {{action 'addToCart'}}>Add to cart <i class="fa fa-shopping-cart"></i></button>
               <div class="clearfix"></div>
           </h1>
       </div>
    </div>
    <div class="panel well">
        <div class="panel-body">
           
            <div class="row">
                <div class="col-sm-3">
                    {{ media-img file=file class="img-responsive" }}
                </div>
                <div class="col-sm-9">
                   <div class="row">
                            <div class="col-sm-12">
                                <p><strong>Price:</strong> {{ currency price }} </p>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="lead">{{ description }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    </div>
</script>