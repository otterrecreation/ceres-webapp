App.Good = App.InventoryBase.extend({
    goodCategory: belongsTo('good-category', { async: true })
}); //create another instance of inventory with a different endpoint for the api

App.GoodCategory = App.InventoryCategory.extend({
    goods: hasMany('good', {async: true})
});

;