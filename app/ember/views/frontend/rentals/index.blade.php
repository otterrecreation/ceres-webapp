<script type="text/x-handlebars" id="rentals">
   {{ carousel-slideshow slides=currentPage.slideshow.slides }}
   <div class="container">
        <div class="row">
           <div class="col-md-4">
               <h1>Rentals</h1>
           </div>
           <h1 class="col-md-8 text-right">
               <div class="form form-inline">
                   <form {{action 'search' on='submit'}}>
                   {{ view "select"
                      selection=rentalCategory
                      content=rentalCategories
                      optionValuePath="content.id"
                      optionLabelPath="content.category"
                      class="form-control"
                      prompt="All Categories" }}
                   <div class="input-group">
                        {{ input type="text" 
                           value=searchQuery
                           class="form-control"
                           placeholder="Search Rentals" }}
                       <div class="input-group-btn">
                           <button type="submit" class="btn btn-primary" {{action 'search'}}><span class="fa fa-search"></span></button>
                       </div>
                   </div>
                   </form>
               </div>
           </h1>
        </div>
        <div class="container-fluid">
            {{#each inventory in filteredInventory}}
                {{#link-to 'rentalItem' inventory tagName='div' class='thumbnail-media panel'}}
                    <div class="thumbnail-actions text-left">
                        
                        <div class="clearfix"></div>
                    </div>

                    {{media-img-thumb file=inventory.file class='img-responsive'}}
                    <div class="thumbnail-title">
                       {{ inventory.name }} <br> {{currency inventory.price}} 
                   </div>
                {{/link-to}}
            {{ else }}
                <p class="lead text-center">No rentals</p>
            {{/each}}
        </div>
    </div>
</script>


<script type="text/x-handlebars" id="rentalItem">
  <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h1>{{#link-to 'rentals'}}Rentals{{/link-to}} <span class="text-muted">/</span> {{ name }}
                   {{#link-to 'rentalItem.reserve' this class="btn btn-danger btn-sm pull-right" }}Reserve <i class="fa fa-calendar-o"></i>{{/link-to}}
                    <div class="clearfix"></div>
               </h1>
           </div>
        </div>
        <div class="panel well">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-3">
                        {{ media-img file=file class="img-responsive" }}
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12">
                                <p><strong>Price:</strong> {{ currency price }} <span class="text-muted">/ {{ pricePer }}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="lead">{{ description }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{ outlet }}
</script>