<script type="text/x-handlebars" id="rentalItem/reserve">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header modal-emphasis">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Make a Reservation
            </h3>
          </div>
          <div class="modal-body">
               <div class="form-group">
                   <div class="row">
                       <div class="col-xs-12">
                           <label>How many do you want?</label>
                           {{ input type="text" value=count class="form-control" default=1 }}
                       </div>
                   </div>
               </div>
               <div class="form-group">
                   <div class="row">
                       <div class="col-xs-12">
                           <label>Pick up on</label>
                            {{ bootstrap-datetimepicker }}
                       </div>
                   </div>
               </div>
               <div class="form-group">
                   <div class="row">
                       <div class="col-xs-12">
                           <label>Return on</label>
                            <div class="form-group">
                                {{ bootstrap-datetimepicker }}      
                            </div>
                       </div>
                   </div>
               </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="button" class="btn btn-primary" {{ action 'checkReservation' this }}>Reserve</button>
          </div>
        </div>
      </div>
    </div>
</script>