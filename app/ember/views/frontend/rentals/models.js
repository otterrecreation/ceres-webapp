App.Rental = App.InventoryBase.extend({

    //RELATIONSHIPS
    rentalCategory: belongsTo('rental-category', { async: true })
    
}); //create another instance of inventory with a different endpoint for the api

App.RentalCategory = App.InventoryCategoryBase.extend({
    rentals: hasMany('rental', { async: true })
});

;