App.RentalsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('rental');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 2));
        this.controllerFor('rentalCategories').set('model', this.store.find('rentalCategory'));
    }
    
});

App.RentalItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('rental',  params.inventory_id );  // filter array inventory, by publicVisable set to 1(true) 
    }
    
});

App.RentalItemReserveRoute = Ember.Route.extend({
    
    actions: {
        
        checkReservation: function(item) {
            
            
            
        }
        
    }
    
});