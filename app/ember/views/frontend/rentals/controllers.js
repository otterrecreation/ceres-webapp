App.RentalsController = Ember.ArrayController.extend({

    
    //dependencies
    needs: ['rentalCategories', 'currentPage'],
    rentalCategories: Ember.computed.alias('controllers.rentalCategories'),
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    sortProperties: ['name'],

    queryParams: ['category', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    category: null,                 // this is blank variable will store type to filter by
    tableName: 'inventory',
    
    filteredInventory: function() {
        //this.set('type', 'rental');
        
        var category = this.get('rentalCategory.id');    // retrieves type variable to filter by
        this.set('category', category); //just to show it in the url
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'sku'];
        var items = this.get('model');  // creats array of models to be filtered.
        
        if (category) {
            items = items.filterBy('rentalCategory.id', category);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
        
    }.property('rentalCategory', 'search', 'model'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        }
    
    }
    
    
});

App.RentalItemController = Ember.ObjectController.extend({
    
    needs: ['cart'],
    
    tableName: 'inventory',
    
    actions: {
    
        addToCart: function() {
            
            var self = this;
            
//            console.log(this.get('controllers.cart').get('model').get('firstObject'));
            
            var item = this.get('model');
            
            if(item.get('count') > 0) {
                
                var data = {};
                    data.price = this.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = this.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                    $.growl.notice({ title: "Item Added", message: "Thank you for adding this item to your cart!" });
                }, function(response) {
                    $.growl.error({ title: "Item Not Added", message: "Unfortunately there was a problem adding this item to your cart. Please try again later" });
                });
                
            } else {
                $.growl.error({ title: "Error", message: "Unfortunately there are no more available. Please try ordering it again later." });
            }
            
        }
        
    }
});

App.RentalItemReserveController = Ember.ObjectController.extend(Ember.Evented, {
    
});