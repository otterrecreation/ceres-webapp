App.LoginRoute = Ember.Route.extend({
    actions: {
        login: function(data) {

            if(typeof data === 'undefined') {
                data = this.get('controller').getProperties('username', 'password');
            }
            
            var loginController = this.get('controller');

            Ember.$.post('api-proxy/login', data).then(function(response) {

                var attemptedTransition = loginController.get('attemptedTransition');
                
                if(attemptedTransition) {
                    console.log(attemptedTransition);
                    var attemptedUrl = attemptedTransition.intent.url;
                    
                    if(attemptedUrl.indexOf('/') === 0) {
                        attemptedUrl = attemptedUrl.substring(1);
                    }
                    
                    loginController.set('attemptedTransition', null);
                    
                    window.location = rootURL + attemptedUrl;
                    
                } else {
                    window.location = rootURL;   
                }
                
//                var attemptedTransition = self.get('attemptedTransition');
//                if (attemptedTransition) {
//                    attemptedTransition.retry();
//                    self.set('attemptedTransition', null);
//                } else {
//                    // Redirect to 'home' by default.
//                    self.transitionToRoute('home');
//                }
            }, function(response) {
                console.log(response);
                $.growl.error({ title: "Error", message: "Please check your username and password" });
            });
        }
    }
});