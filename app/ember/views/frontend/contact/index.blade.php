<script type="text/x-handlebars" id="contact">
    {{ carousel-slideshow slides=currentPage.slideshow.slides }}
    <div class="container">
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
               {{#if siteName.string_value }}
                    <h1>{{ siteName.string_value }}</h1>
                {{ else }}
                    <h1>Contact</h1>
                {{/if}}
                <div class="lead">
                    <address>
                        {{#if address.string_value }}
                        {{ address.string_value }}<br><br>
                        {{/if}}
                        {{#if phone.string_value}}
                        <abbr title="Phone">P:</abbr> {{ phone.string_value }}<br>
                        {{/if}}
                        {{#if fax.string_value }}
                        <abbr title="Fax">F:</abbr> {{ fax.string_value }}<br>
                        {{/if}}
                        {{#if email.string_value}}
                        <a {{bind-attr href=email.string_value }}>{{ email.string_value }}</a>
                        {{/if}}
                    </address>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8">
                {{ google-map address=address.string_value class="contact-map panel" }}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 col-sm-5 col-md-3">
                <div class="panel well container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Hours</h3>
                            <table class="table table-borderless">
                                <tr>
                                    <td><strong>Monday</strong></td><td class="text-right">{{#if hoursMonday.string_value }}{{ hoursMonday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Tuesday</strong></td><td class="text-right">{{#if hoursTuesday.string_value }}{{ hoursTuesday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Wednesday</strong></td><td class="text-right">{{#if hoursWednesday.string_value }}{{ hoursWednesday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Thursday</strong></td><td class="text-right">{{#if hoursThursday.string_value }}{{ hoursThursday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Friday</strong></td><td class="text-right">{{#if hoursFriday.string_value }}{{ hoursFriday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Saturday</strong></td><td class="text-right">{{#if hoursSaturday.string_value }}{{ hoursSaturday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Sunday</strong></td><td class="text-right">{{#if hoursSunday.string_value }}{{ hoursSunday.string_value }}{{ else }}<em>CLOSED</em>{{/if}}</td>
                                </tr>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>