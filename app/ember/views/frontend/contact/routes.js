App.ContactRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('setting');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 7));
    }
    
});