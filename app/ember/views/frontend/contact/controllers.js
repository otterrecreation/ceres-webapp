App.ContactController = Ember.ArrayController.extend({
    
    needs: ['currentPage'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    //basic contact information
    siteName: function() { return this.get('model').filterBy('name', 'core_site_name').get('firstObject'); }.property('model'),
    phone: function() { return this.get('model').filterBy('name', 'core_phone').get('firstObject'); }.property('model'),
    fax: function() { return this.get('model').filterBy('name', 'core_fax').get('firstObject'); }.property('model'),
    email: function() { return this.get('model').filterBy('name', 'core_email').get('firstObject'); }.property('model'),
    address: function() { return this.get('model').filterBy('name', 'core_address').get('firstObject'); }.property('model'),
    
    //Hours of Operation
    hoursMonday: function() { return this.get('model').filterBy('name', 'core_hours_monday').get('firstObject') }.property('model'),
    hoursTuesday: function() { return this.get('model').filterBy('name', 'core_hours_tuesday').get('firstObject') }.property('model'),
    hoursWednesday: function() { return this.get('model').filterBy('name', 'core_hours_wednesday').get('firstObject') }.property('model'),
    hoursThursday: function() { return this.get('model').filterBy('name', 'core_hours_thursday').get('firstObject') }.property('model'),
    hoursFriday: function() { return this.get('model').filterBy('name', 'core_hours_friday').get('firstObject') }.property('model'),
    hoursSaturday: function() { return this.get('model').filterBy('name', 'core_hours_saturday').get('firstObject') }.property('model'),
    hoursSunday: function() { return this.get('model').filterBy('name', 'core_hours_sunday').get('firstObject') }.property('model'),
    
});