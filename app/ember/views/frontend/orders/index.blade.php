<script type="text/x-handlebars" data-template-name="orders">
   <div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Orders
                </div>
                <div class="panel-body">
                    <div class="list-group">

                    {{#each orders in model }}
                        {{#link-to 'order' orders class="list-group-item"}} 
                            PO# {{ orders.poId }}
                        {{/link-to}}
                    {{/each}}

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            {{ outlet }}
        </div>
    </div>
    </div>
</script>

<!--
    id:             attr('string'), //
    poId:           attr('string'), // 
    vendorId:       attr('string'), // 
    description:    attr('string'), //
    orderBy:        attr('string'), //
    receiveBy:      attr('string'), //
    editBy:         attr('string'), //
    verifyBy:       attr('string'), //
    created_at:     attr('date'),   // Date order was added to database, date created.
    received_at:    attr('date'),   // Date order was received, date modified/updated.
    placed_at:      attr('date'),   // Date order was placed/submitted to vendor updated, date modified/updated   
    updated_at:     attr('date'),   // Date order was last updated, date modified/updated.
-->

<script type="text/x-handlebars" data-template-name="order">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><strong>PO# : </strong> {{ poId }}</h1>
        </div>
        <div class="panel-body">
            <table>
                <tr><td>id: </td>            <td>{{id}}          </td></tr>
                <tr><td>poId: </td>          <td>{{poId}}        </td></tr>
                <tr><td>vendorId: </td>      <td>{{vendorId}}    </td></tr>
                <tr><td>description: </td>   <td>{{description}} </td></tr>
                <tr><td>orderBy: </td>       <td>{{orderBy}}     </td></tr>
                <tr><td>receiveBy: </td>     <td>{{receiveBy}}   </td></tr>
                <tr><td>editBy: </td>        <td>{{editBy}}      </td></tr>
                <tr><td>verifyBy: </td>      <td>{{verifyBy}}    </td></tr>
                <tr><td>created_at: </td>    <td>{{created_at}}  </td></tr>
                <tr><td>received_at: </td>   <td>{{received_at}} </td></tr>
                <tr><td>placed_at: </td>     <td>{{placed_at}}   </td></tr>
                <tr><td>updated_at: </td>    <td>{{updated_at}}  </td></tr>
                {{#each item in orderItems}}
                    {{item.id}}
                {{/each}}
            </table>

            <tabe calss="table">
                <tr>
                    <th><strong>Description</strong></th>
                </tr>
                <tr>
                    <td><p>{{ description }}</p></td>
                </tr>
            </table>
            
        </div>
    </div>
</script>