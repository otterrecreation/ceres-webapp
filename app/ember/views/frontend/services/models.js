App.Service = DS.Model.extend({

	name: 		   attr('string'),
	description:   attr('string'),
	price: 		   attr('number'),
    hours:          attr('number'),
    publicVisible:  attr('number'),
	created_at:    attr('date'),  
	updated_at:    attr('date')

});

App.ServicesSetting = DS.Model.extend({

    availableServices:    attr('number'),
    canReserve:           attr('number'),
    created_at:           attr('date'),  
    updated_at:           attr('date')

});

App.Service.reopenClass({
    FIXTURES: [
        { id: 1,
            name: 'Tune Up',
            description: 'New break cables, break housing, shifter cables, shifter housing, and chain (if necessary)',
            price: 40.00,
            created_at: '',
            updated_at: ''},
            { id: 2,
            name: 'Hit Man',
            description: 'does it need one?',
            price: 1000.00,
            created_at: '',
            updated_at: ''}
    ]
});