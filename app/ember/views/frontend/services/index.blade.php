<script type="text/x-handlebars" id="services">
	<h1>Services</h1>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Services
                </div>
                <div class="panel-body">
                    <div class="list-group">
                    {{#each services in model}}
                         {{#link-to 'service' services class="list-group-service"}} 
                            <li>{{ services.name }}</li>
                         {{/link-to}}
                    {{/each}}    
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            {{ outlet }}
        </div>
    </div>
</script>


<script type="text/x-handlebars" id="service">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">{{ name }}</h1>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <th>Description</th>
                    <th>Estimated Time</th>
                    <th>Price</th>
                    <th>Reserve</th>
                </tr>
                <tr>
                    <td><p>{{ description }}</p></td>
                    <td>{{ hours }}</td>
                    <td>{{ currency price }}</td>
                    {{#if isCurrentUser }}
                    <td>
                        <button type="submit" class="btn btn-primary" {{action 'addToCart'}}>Reserve</button>
                    </td>
                    {{else}}
                    <td>
                        Please Sign In
                    </td>
                    {{/if}}
                </tr>  
            </table>
        </div>
    </div>
</script>