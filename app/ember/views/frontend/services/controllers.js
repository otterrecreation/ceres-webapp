App.ServicesController = Ember.ArrayController.extend({

    public: 1

});	

App.ServiceController = Ember.ObjectController.extend({

	needs: ['application', 'cart'],

	isCurrentUser: function() {
		return this.get('controllers.application.currentUser');
	}.property('controllers.application.currentUser'),

	tableName: 'services',

	actions: {
    
        addToCart: function() {
            
            var self = this;
            
//            console.log(this.get('controllers.cart').get('model').get('firstObject'));
            
            var item = this.get('model');
                
                var data = {};
                    data.price = this.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = this.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                }, function(response) {
                    console.log(response)
                }); 
        }
        
    }

});	