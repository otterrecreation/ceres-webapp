<script type="text/x-handlebars" id="account/address/create">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title" id="myModalLabel">
                Add a new address
            </h3>
          </div>
        <?/***********************************************
        *   Add form for inventory, broken up in a table
        *
        ***********************************************/?>
        <form class="form" {{action 'addAddress' on='submit'}}>  
          <div class="modal-body">                      
			<div class="form-group">
	   	   		<div class="form-group">
				   <div class="row">
							<div class="col-xs-12">
							    <label>Recipient Name</label>
								{{ input type="text" 
								   value=recipient_name
								   class="form-control" }}
							</div>
					</div>
				</div>
		   	   <div class="form-group">
				   <div class="row">
							<div class="col-xs-12">
							    <label>Address Line 1</label>
								{{ input type="text" 
								   value=address_line_1
								   class="form-control" }}
							</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
							<div class="col-xs-12">
							    <label>Address Line 2</label>
								{{ input type="text" 
								   value=address_line_2
								   class="form-control" }}
							</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
							<div class="col-xs-6">
							    <label>City</label>
								{{ input type="text" 
								   value=city
								   class="form-control" }}
							</div>
							<div class="col-xs-3">
							    <label>State</label>
								{{ input type="text" 
								   value=state_province
								   class="form-control" }}
							</div>
							<div class="col-xs-3">
							    <label>ZIP</label>
								{{ input type="text" 
								   value=zip
								   class="form-control" }}
							</div>
			    	</div>
			    </div>
			    <div class="form-group">
			    	<div class="row">
							<div class="col-xs-4">
							    <label>Country</label>
								{{ input type="text" 
								   value=country
								   class="form-control" }}
							</div>

					</div>
				</div>
			</div>
                      
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" {{ action 'closeModal' target='view' }}>Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form> 
        </div>
      </div>
    </div>
	
</script>
