<script type="text/x-handlebars" id="account">
    <div class="container">
        <h1>{{ name }}'s Account</h1>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 panel well">
                    <form {{ action 'updateUser' on="submit" }}>
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Username</label>
                                    {{ input type="text"
                                       disabled="disabled"
                                       value=username
                                       class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Name</label>
                                    {{ input type="text"
                                       value=name
                                       class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>New Password <span class="text-muted">- leave blank to keep current password</span></label>
                                    {{ input type="password"
                                       value=password
                                       class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>New Password Confirmation</label>
                                    {{ input type="password"
                                       value=password_confirmation
                                       class="form-control" }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <h3>Shipping Addresses</h3>
        {{#each address in addresses }}
            {{#link-to 'account.address.edit' address}}
            <div class="envelope pull-left">
                <div class="stamp"><div></div></div>
                <div class="address">
                    <h4 class="text-center">{{ address.recipient_name }}</h4>
                    <p class="text-center">{{ address.address_line_1 }}&nbsp;{{ address.address_line_2 }}</p>
                    <p class="text-center">{{ address.city }}, {{ address.state_province }} {{ address.zip }} {{ address.country }}</p>
                </div>
            </div>
            {{/link-to}} 
        {{/each}}

        {{#link-to 'account.address.create' class="envelope envelope-new pull-left" }}
            <h1 class="text-center"><i class="fa fa-plus"></i></h1>
            <p class="text-center">Add a new address</p>
        {{/link-to}}
        <div class="clearfix"></div>

        <h3>Billing Information</h3>

        {{#each bill in billingInfo }}
            {{#link-to 'account.billinginfo.edit' bill}}
            <div {{ bind-attr class=":card :pull-left bill.brand" }}>
                <div class="brand"></div>
                <div class="number"><h3>&bull;&bull;&bull;&bull;&nbsp;&bull;&bull;&bull;&bull;&nbsp;&bull;&bull;&bull;&bull;&nbsp;{{ bill.last4 }}</h3></div>
                <div class="card_holder"><h4>{{ bill.name }}</h4></div>
                <div class="expiration"><h5>Exp. {{ bill.exp_month }}&nbsp;/&nbsp;{{ bill.exp_year }}</h5></div>
            </div>
            {{/link-to}}
        {{/each}}

        {{#link-to 'account.billinginfo.create' class="card card-new pull-left" }}
            <h1 class="text-center"><i class="fa fa-plus"></i></h1>
            <p class="text-center">Add a new card</p>
        {{/link-to}}

        <div class="clearfix"></div>
        <br>
        {{ outlet }}
    </div>
</script>
