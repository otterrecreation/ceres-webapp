App.AccountRoute = Ember.Route.extend({
    
    model: function() {
        var self = this;
        
		var currentUser = this.controllerFor('application').get('currentUser');
		
		return this.store.find('user', currentUser.id);
		
//        return Ember.$.get(window.rootURL + 'api-proxy/account').then(function(results) {
//            return results;
//        }, function(results) {
//            self.transitionTo('home'); //TODO: have error message when they try to get there
//        });
    },
    
    actions: {
    
        updateAccount: function() {
            var self = this;
            
            
            
        }
    
    }
    
});

App.AccountAddressEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userAddress', params.address_id);
	}
});

App.AccountBillinginfoEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userBillingInfo', params.billing_id);
	}
});