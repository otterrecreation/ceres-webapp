App.AccountController = Ember.ObjectController.extend({
	needs: ['userAddresses'],
	
	reset: function() {
		this.setProperties({
			password: '',
			password_confirmation: ''
		});
	},
	
	actions: {
		
		updateUser: function() {
			var self = this;
			var user = this.get('model');
			
			user.save().then(function(results) {
				console.log(results);
				console.log('user info updated successfully');
				self.reset();
			}, function(results) {
				console.log(results);
				user.rollback(); //if  saving didn't work
			});
			
		}
		
	}
});

App.AccountAddressCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addAddress: function() {
		
			var self = this;
			var data = this.getProperties('recipient_name',
										  'address_line_1',
										  'address_line_2',
										  'city',
										  'state_province',
										  'zip',
										  'country');
			
			var userAddress = this.store.createRecord('userAddress', data);
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('addAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.destroyRecord();
			});
		
		}
	
	}
	
});

App.AccountBillinginfoCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addBillinginfo: function(form) {
			
			var self = this;
			var $form = Ember.$('#form-add-billing-info');
			
			// Disable the submit button to prevent repeated clicks
    		var $submitForm = $form.find('button[type="submit"]').prop('disabled', true);
			
			Stripe.card.createToken($form, function(status, response) {
				  if (response.error) {
					// Show the errors on the form
					$form.find('.payment-errors').text(response.error.message);
					$submitForm.prop('disabled', false);
				  } else {
					// response contains id and card, which contains additional card details
					var token = response.id;
					
					var data = { stripeToken: token };
					Ember.$.post(rootURL + '/api-proxy/userBillingInfos', data).then(function(response) {
						self.store.push('userBillingInfo', response.userBillingInfo); //push to store to render on the view
						self.trigger('addBillingInfoDidSucceed');
					}, function(response) {
						$form.find('.payment-errors').text('error saving card. please try again');
						$submitForm.prop('disabled', false);
						console.log('error saving the new credit card information ' + response);
					});
					  
				  }
				
			});
			
		}
	
	}
	
});

App.AccountAddressEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		updateAddress: function() {
		
			var self = this;
			var userAddress = this.get('model');
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.rollback();
			});
		
		}
	
	}
	
});

App.AccountBillinginfoEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	formatted4: function() {
		return '•••• •••• •••• ' + this.get('last4');
	}.property('last4'),
	
	actions: {
	
		updateBillinginfo: function() {
			
			var self = this;
			var billingInfo = this.get('model');
			
			billingInfo.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateBillingInfoDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				billingInfo.rollback();
			});
			
		}
	
	}
	
});