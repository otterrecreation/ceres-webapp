App.AccountBillinginfoCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/billinginfo/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountAddressCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/address/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountBillinginfoEditView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/billinginfo/edit',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountAddressEditView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/address/edit',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});