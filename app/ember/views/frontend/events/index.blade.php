<script type="text/x-handlebars" id="events">
    {{ carousel-slideshow slides=currentPage.slideshow.slides }}
    <div class="container">
        <div class="row">
           <div class="col-md-4">
               <h1>Events</h1>
           </div>
           <h1 class="col-md-8 text-right">
               <div class="form form-inline">
                  <form {{action 'search' on='submit'}}>
                       <div class="input-group">
                            {{ input type="text" 
                               value=searchQuery
                               class="form-control"
                               placeholder="Search Events" }}
                           <div class="input-group-btn">
                               <button type="submit" class="btn btn-primary" {{ action 'search' }}><span class="fa fa-search"></span></button>
                           </div>
                       </div>
                   </form>
               </div>
           </h1>
        </div>
        <div class="row">
           <div class="col-md-12">
            {{#if filteredEvents}}
                {{#each event in filteredEvents}}
                    {{#link-to 'event' event tagName='div' class="panel well hoverable"}}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-8 col-xs-offset-2 col-sm-offset-0 col-sm-6 col-md-3">
                                    <div class="date-icon absolute">
                                        <p>{{moment event.start 'D'}}<span>{{ moment event.start 'MMM' }}</p>
                                    </div>
                                    {{ media-img-thumb file=event.file class="img-responsive" }}
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-7">
                                    <h3>{{ event.name }} <small><span class="text-muted">{{ moment event.start 'h:mm a' }} - {{ moment event.end 'h:mm a' }}</span></small></h3>
                                    <p class="lead">{{ event.description }}</p>
                                    
                                </div>
                            </div>
                        </div>
                    {{/link-to}}
                {{/each}}
            {{ else }}
                <p class="lead text-center">No events</p>
            {{/if}}   
           </div>
        </div>
    </div>
</script>


<script type="text/x-handlebars" id="event">
   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h1>{{#link-to 'events'}}Events{{/link-to}} <span class="text-muted"> / </span> {{ name }} <button class="btn btn-danger btn-sm pull-right" {{action 'addToCart'}}>Add to cart <i class="fa fa-cart"></i></button>
                    <div class="clearfix"></div></h1>
           </div>
        </div>
        <div class="panel panel-default well">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="date-icon absolute">
                            <p>{{moment start 'D'}}<span>{{ moment start 'MMM' }}</p>
                            </div>
                            {{ media-img-thumb file=file class="img-responsive" }}
                        </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="lead">{{ description }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{#if location}}
        <h3><i class="fa fa-map-marker"></i> {{ location }}</h3>
        <div class="panel" style="height: 400px;">
            {{ google-map address=location height='100%' }}
        </div>
        {{/if}}
    </div>
</script>