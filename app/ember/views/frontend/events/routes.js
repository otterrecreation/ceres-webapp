App.EventsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('event');  // filter array inventory, by publicVisable set to 1(true)
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 4));
    }
    
});

App.EventRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('event', params.event_id);
    }
    
});