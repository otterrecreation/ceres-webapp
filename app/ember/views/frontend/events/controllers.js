App.EventsController = Ember.ArrayController.extend({

    needs: ['currentPage'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    sortProperties: ['name'],

    queryParams: ['category', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    category: null,                 // this is blank variable will store type to filter by
    public: 1,                  // select public view items.
    
    filteredEvents: function() {
        //this.set('type', 'rental');
        
        var category = this.get('eventCategory.id');    // retrieves type variable to filter by
        this.set('category', category); //just to show it in the url
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'sku'];
        var items = this.get('model');  // creats array of models to be filtered.
        
        if (category) {
            items = items.filterBy('eventCategory.id', category);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
        
    }.property('eventCategory', 'search', 'model'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        }
    
    }
    
    
});