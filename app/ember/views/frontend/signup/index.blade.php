<script type="text/x-handlebars" id="signup">
   <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <form {{ action "signup" on="submit" }}>
                <h1 class="text-center">Sign Up</h1>
                <div class="row">
                    <div class="col-md-12 panel well">
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Name</label>
                                    {{ input type="text" class="form-control" value=name }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Email</label>
                                    {{ input type="text" class="form-control" value=username }}  
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                               <div class="col-md-12">
                               <label>Password</label>
                               {{ input type="password" class="form-control" value=password }}  
                               </div>
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                               <div class="col-md-12">
                                   <label>Confirm Password</label>
                                   {{ input type="password" class="form-control" value=password_confirmation }}
                               </div>
                           </div>
                        </div>
                        <div class="form-group text-right">
                            {{ input type="submit" class="btn btn-primary" value="signup" }} or {{#link-to 'login'}}login{{/link-to}}
                        </div>  
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>
