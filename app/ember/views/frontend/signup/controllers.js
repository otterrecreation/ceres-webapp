App.SignupController = Ember.ArrayController.extend({

    needs: ['application'],
    
    reset: function() {
        this.setProperties({
            name: '',
            username: '',
            password: '',
            password_confirmation: '',
        });
    },

    actions: {

        signup: function() {
            var self = this,
                data = this.getProperties('name', 'username', 'password', 'password_confirmation'),
                user = this.store.createRecord('user', data);

            user.save().then(function(response) {
                var applicationController = self.get('controllers.application');
                applicationController.set('currentUser', response.get('data'));
                self.transitionToRoute('home');
                
            }, function(response) {
                Ember.Logger.debug('user signup error:', response);
                self.set('errorMessage', response.messages);
            });
        }

    }

});
