App.ApplicationController = Ember.ArrayController.extend({
    
    needs: ['cart', 'pages'],
    cart: Ember.computed.alias('controllers.cart'),
    pages: Ember.computed.alias('controllers.pages'),
    
    siteName: function() { 
        //now we can set the document title as well...
        var coreSiteName = this.get('model').filterBy('name', 'core_site_name').get('firstObject');
        
        //TODO: put this in a better spot to make the document title be dynamic based on this value changing
        if(!Ember.isEmpty(coreSiteName)) {
            document.title = coreSiteName.get('string_value');
        } else {
            document.title = 'Ceres Manager';
        }
        
        return coreSiteName; 
    
    }.property('model'),
    
    cartCount: function() {
        return this.get('cart.model.cartItems.length'); //just to get the count
    }.property('cart.model.cartItems'),
    
    currentUser: window.currentUser,
    
	canViewAdmin: function() {
		
		var permissions = this.get('currentUser.permissions');
		
		if(permissions) {
			//permissions exists so make sure one of them is view_admin for the current user
			return permissions.some(function(permission) {
				return permission === 'view_admin';
			});
		}
		
		return false;
		
	}.property('currentUser')

    
});



//Controllers not tied to any routes
App.InventoryCategoriesController = Ember.ArrayController.extend();
App.MediaController = Ember.ArrayController.extend();
App.PagesController = Ember.ArrayController.extend();
App.CurrentPageController = Ember.ObjectController.extend();
App.RentalCategoriesController = Ember.ArrayController.extend();
App.GoodCategoriesController = Ember.ArrayController.extend();

App.RandomRentalController = Ember.ObjectController.extend();
App.RandomGoodController = Ember.ObjectController.extend();
App.RandomEventController = Ember.ObjectController.extend();
