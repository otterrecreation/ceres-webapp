<!doctype html>
<html>
	<head>
        <% HTML::style( 'media/packages/bootstrap/css/bootstrap.min.css' ) %>
        <% HTML::style( 'media/packages/select2/select2.css' ) %>
        <% HTML::style( 'media/packages/select2-bootstrap/select2-bootstrap.css' ) %>
        <% HTML::style( 'media/packages/growl/jquery.growl.css' ) %>
        <% HTML::style( 'media/packages/font-awesome/css/font-awesome.min.css' ) %>
        <% HTML::style( 'media/packages/dropzone/css/basic.min.css' ) %>
        <% HTML::style( 'media/packages/dropzone/css/dropzone.min.css' ) %>
        <% HTML::style( 'media/packages/ckeditor/skins/moono/editor.css' ) %>
        <% HTML::style( 'media/packages/ckeditor/skins/moono/dialog.css' ) %>
        <% HTML::style( 'media/packages/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.css' ) %>
        <% HTML::style( 'media/css/main.min.css' ) %>
        
        <% HTML::script( 'media/packages/jquery/jquery.js' ) %>
        @if($settings->core_google_api_key)
        <% HTML::script( 'https://maps.googleapis.com/maps/api/js?key=' . $settings->core_google_api_key ) %>
        @else
        <% HTML::script( 'https://maps.googleapis.com/maps/api/js?key=' ) %>
        @endif
        <% HTML::script( 'media/packages/velocity/velocity.js' ) %>
        <% HTML::script( 'media/packages/growl/jquery.growl.js' ) %>
        <% HTML::script( 'media/packages/handlebars/handlebars.js' ) %>
        <% HTML::script( 'media/packages/ember/js/ember.prod.js' ) %>
        <% HTML::script( 'media/packages/ember-data/ember-data.js' ) %>
        <% HTML::script( 'media/packages/md5/js/md5.min.js' ) %>
        <% HTML::script( 'media/packages/bootstrap/js/bootstrap.min.js' ) %>
        <% HTML::script( 'media/packages/select2/select2.js' ) %>
        <% HTML::script( 'media/packages/masonry/js/masonry.pkgd.min.js' ) %>
        <% HTML::script( 'media/packages/imagesloaded/imagesloaded.js' ) %>
        <% HTML::script( 'media/packages/dropzone/js/dropzone.min.js' ) %>
        <% HTML::script( 'media/packages/ckeditor/ckeditor.js' ) %>
        <% HTML::script( 'media/packages/moment/moment.js' ) %>
        <% HTML::script( 'media/packages/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js' ) %>
        <% HTML::script( 'https://js.stripe.com/v2/' ) %>
        <script type="text/javascript">
			Stripe.setPublishableKey("<% Config::get('stripe.publishable_key') %>");
			
            var App = Ember.Application.create({
                LOG_TRANSITIONS: true
            });

            //MAP YOUR EMBER ROUTES HERE
            App.Router.map(function() {
                //FRONT-END ROUTES
                this.resource('home', { path: '/' });
                this.resource('rentals');
                this.resource('rentalItem', { path: 'rentals/:inventory_id' }, function() {
                    this.route('reserve');
                });
                this.resource('services', function() {
                    this.resource('service', { path: ':services_id' });
                });

                this.resource('contact');
//                this.resource('blog');
                //ServicesRoute
                //ServicesServiceRoute : if it was this.route('')
                //App.ServiceRoute
                

                this.resource('signup');
                this.resource('login');
                this.resource('account',function() {
					this.route('restock');
                	this.route('orders');
                	this.route('settings');
                    this.route('address', function() {
                        this.route('edit', { path: ':address_id' });
                        this.route('create');
                    });
                    this.route('billinginfo', function() {
                        this.route('edit', { path: ':billing_id' });
                        this.route('create');
                    });
                });
                
                //INVENTORY ROUTES
                this.resource('shop');
                this.resource('shopItem', { path: 'shop/:inventory_id'});
                
                //EVENTS ROUTES
                this.resource('events');
                this.resource('event', { path: 'events/:event_id' });
                
                this.resource('cart');

                //RESTOCK ROUTES
                this.resource('inventoryOrders', function() {
                   this.resource('inventoryOrder', { path: ':inventory_orders_id'});
                });                
                
                this.resource('blog');
                this.resource('singleBlog', { path: 'blog/:blog_id' });
                
                
                //ADMIN ROUTES (note: this.route maintains the namespace for nesting as apposed to this.resource)
                this.route('admin', function() {
                    this.route('dashboard', { path: '/' });
                    this.route('rentals');

                    this.route('account', function() {
                        this.route('orders');
                        this.route('settings');
                        this.route('address', function() {
                            this.route('edit', { path: ':address_id' });
                            this.route('create');
                        });
                        this.route('billinginfo', function() {
                            this.route('edit', { path: ':billing_id' });
                            this.route('create');
                        });
                    });
                                        
                    this.route('events', function() {
                        this.route('create');
                        this.route('event', { path: ':event_id'});
                    });
                    this.route('orders', function() {
                        this.route('create');
                        this.route('order', { path: ':orders_id' });
                    });
                    this.route('inventory', function() {
                        this.route('create');
                        this.route('item', { path: ':inventory_id' });
                    });
                    
                    this.route('inventoryOrders', function() {
                        this.route('inventoryOrder', { path: ':inventory_orders_id' });
                    });
                    
                    this.route('services', function() {
                        this.route('create');
                        this.route('settings');
                        this.route('service', { path: ':services_id' });
                    });

                    this.route('users', function() {
                        this.route('create');
                        this.route('user', { path: ':user_id' });
			            this.route('permissions');
                    });
                    this.route('media', function() {
                        this.route('upload');
                        this.route('createslideshow');
                        this.route('slideshow', { path: 'slideshow/:slideshow_id' });
                        this.route('item', { path: ':media_id' });
                    });
                    this.route('slideshows', function() {
                        this.route('create');
                        this.route('slideshow', { path: ':slideshow_id' }, function() {
                            this.route('createslide');
                            this.route('slide', { path: 'slides/:slide_id' });
                        });
                    });
                    this.route('blog', function() {
                        this.route('create');
                        this.route('item', { path: ':blog_id' });
                    });
                    this.route('settings', function() {
                        this.route('page', { path: 'page/:page_id' });
                    });
                });
            });
        </script>
		<!--IE fix-->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">

            var baseURL = "<% URL::to('/') %>";
            var rootURL = "<% ROOT_URL %>";
            var currentUser = JSON.parse('<% $currentUser or "[]" %>');

        </script>
        <% HTML::script( 'media/js/main.js' ) %>
	</head>
	<body>
		<!--main template of the site-->
		<script type="text/x-handlebars">

            {{#if isAdminRoute}}
                <div id="wrapper">
                    {{partial "admin_menu"}}
                    <div id="page-content-wrapper">
                        <div class="container-fluid">
                            {{ outlet 'admin' }}
                        </div>
                    </div>
                </div>
            {{else}}
                <div id="wrap">
                    {{partial "main_menu"}}
                    {{ outlet }}
                </div>
                {{partial "footer"}}
            {{/if}}
            
		</script>

        @include('ember::views.partials')
        @include('ember::components.components')
        
		<!--GET GENERAL PARTIALS FOR USE ACROSS OTHER PIECES-->
		@forelse($includes as $include)
		    @include($include)
		@empty
		<!--nothing to include-->
		@endforelse
	</body>
</html>
