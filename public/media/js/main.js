//remove the hashbang!
App.Router.reopen({ location: 'history', rootURL: window.rootURL });

//HANDLEBARS HELPERS

/*****************************************************************
******************************************************************   
*   Helper: Currency aka Money
*   Author: Sergio Tapia
*   Link: http://www.sergiotapia.me/emberjs-helper-to-format-money-values/
*
*   Description:
*   Money(currency) helper formats float values (0.01) into ($00.01)
*   safely for our project.
*****************************************************************/
Ember.Handlebars.helper('currency', function(value, options) 
{
      if (value === null) 
      {
        return new Ember.Handlebars.SafeString('$0.00');
      }
      formatted = parseFloat(value, 10).toFixed(2);
      return new Ember.Handlebars.SafeString('$' + formatted);
});

/*************************************************************************
******************************END currency********************************/

Ember.Handlebars.helper('thisYear', function(value, options)
{
    return new Date().getFullYear();
});

Ember.Handlebars.helper('relativeTime', function(value, options) {
    
    return moment(value).fromNow();
    
});

Ember.Handlebars.helper('moment', function(value, format, options) {
    
    if(typeof format === 'object') {
        format = 'MMMM Do YYYY, h:mm a';
    }
    
    return moment(value).format(format);
});

Ember.Handlebars.registerHelper('bind-style', function(options) {
    
    var fmt = Ember.String.fmt;
    var attrs = options.hash;

    Ember.assert("You must specify at least one hash argument to bindStyle", !!Ember.keys(attrs).length);

    var view = options.data.view;
    var ret = [];
    var style = [];

    // Generate a unique id for this element. This will be added as a
    // data attribute to the element so it can be looked up when
    // the bound property changes.
    var dataId = Ember.uuid();

    var attrKeys = Ember.keys(attrs).filter(function(item, index, self) {
        return (item.indexOf("unit") == -1) && (item !== "static");
    });

    // For each attribute passed, create an observer and emit the
    // current value of the property as an attribute.
    attrKeys.forEach(function(attr) {
          var property = attrs[attr];

          Ember.assert(fmt("You must provide an expression as the value of bound attribute." +
                         " You specified: %@=%@", [attr, property]), typeof property === 'string');

          var propertyUnit = attrs[attr+"-unit"] || attrs["unit"] || '';

          var lazyValue = view.getStream(property);
          var value = lazyValue.value();

          Ember.assert(fmt("Attributes must be numbers, strings or booleans, not %@", [value]), value == null || typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean');

          lazyValue.subscribe(view._wrapAsScheduled(function applyAttributeBindings() {
              var result = lazyValue.value();

              Ember.assert(fmt("Attributes must be numbers, strings or booleans, not %@", [result]),
                           result === null || result === undefined || typeof result === 'number' ||
                             typeof result === 'string' || typeof result === 'boolean');

              var elem = view.$("[data-bindattr-" + dataId + "='" + dataId + "']");

              Ember.assert("An style binding was triggered when the element was not in the DOM", elem && elem.length !== 0);

              elem.css(attr, result + "" + propertyUnit);
          }));

          if (attr === 'background-image' && typeof value === 'string' && value.substr(0, 4) !== 'url(') {
              value = 'url(' + value + ')';
          }

          style.push(attr+':'+value+propertyUnit+';'+(attrs["static"] || ''));
    }, this);

    // Add the unique identifier
    ret.push('style="' + style.join(' ') + '" data-bindAttr-' + dataId + '="' + dataId + '"');
    return new Ember.Handlebars.SafeString(ret.join(' '));
        
});;App.BootstrapDatetimepickerComponent = Ember.Component.extend({
    
    classNames: ['input-group', 'date'],
    
    stepping: 5,
    sideBySide: false,
    allowInputToggle: true, //normally datetimepicker isn't set to true. i don't agree with this default
    minDate: false, 
    maxDate: false,
    
    didInsertElement: function() {
        
        var self = this;
        
        Ember.$('#' + this.get('elementId')).datetimepicker({
            stepping: self.get('stepping'),
            sideBySide: self.get('sideBySide'),
            minDate: self.get('minDate'),
            maxDate: self.get('maxDate')
        });
        
        Ember.$('#' + self.get('elementId') + ' input').click(function(event){
           Ember.$('#' + self.get('elementId')).data("DateTimePicker").show();
        });
    }
});;App.CarouselSlideshowComponent = Ember.Component.extend({
    
    slideHeight: 200,
    backgroundSize: 'cover',
    
    //allows us to format the bootstrap slideshow properly
    indexedSlides: function() {
        var slides = this.get('slides');
        slides.forEach(function(slide, i) {
            console.log(i);
            slide.set('index', i);
            if(i === 0) {
                slide.set('isFirst', true);
            } else {
                slide.set('isFirst', false);
            }
        });
        
        return slides;
        
    }.property('slides'),
    
    didInsertElement: function() {
        
        var self = this;
        
        window.Carousel = Ember.$('.carousel').carousel();  //initialize the homepage slideshow
        
    }
});;App.CkEditorComponent = Ember.Component.extend({
    
    autoSaveInterval: 2, //in seconds
    autosave: true,
    
    didInsertElement: function() {
        
        var component = this;
        var autoSaveInterval = this.get('autoSaveInterval') * 1000;
        
//        CKEDITOR.plugins.add( 'emberImageSelect', {
//            init: function( editor ) {
//                // Plugin logic goes here...
//                editor.addCommand( 'emberImageSelectDialog', new CKEDITOR.dialogCommand( 'emberImageSelectDialog' ) );
//            }
//        });
        
        var ckEditor = CKEDITOR.replace('ck-editor-component', {
//            extraPlugins : 'emberImageSelect',
//            toolbar : [
//                ['Source', '-', 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
//                ['About','-','emberImageSelect']
//            ],
            on: {
                instanceReady: function() {
                    
                    var self = this;
                    // How to autosave
                    if(component.get('autosave')) {
                        var buffer = CKEDITOR.tools.eventsBuffer( autoSaveInterval, function() {
                            component.set('value', self.getData());
                        });         

                        this.on( 'change', buffer.input );
                    } else {
                        this.on( 'change', function() {
                            component.set('value', self.getData());
                        });
                    }
                }
            }
        });
        
        
    }
});;//START CUSTOM SELECT
App.CustomSelectComponent = Ember.Component.extend({
  tagName: "select",
  classNameBindings: [":custom-select"],
  attributeBindings: ['disabled', 'tabindex'],

  /**
   * Bound to the `disabled` attribute on the native <select> tag.
   *
   * @property disabled
   * @type Boolean
   * @default false
   */
  disabled: false,

  /**
   * Bound to the `tabindex` attribute on the native <select> tag.
   *
   * @property tabindex
   * @type Integer
   * @ default 1
   */
  tabindex: 1,

  /**
   * The collection of options for this select box. When options are
   * inserted into the dom, they will register themselves with their
   * containing `custom-select`
   *
   * @private
   * @property options
   */
  options: Ember.computed(function() {
    return Ember.A();
  }),

  /**
   * @private
   */
  setup: (function() {
    /* jshint unused:false */
    this.$().on('change', Ember.run.bind(this, function(e) {
      var option = this.get('options').find(function(option) {
        return option.$().is(':selected');
      });
      
      if(!Ember.isEmpty(this.get('object'))) {
        this.sendAction('action', option.get('value'), this.get('object'));
      } else {
        this.sendAction('action', option.get('value'), this);
      }
    }));
  }).on('didInsertElement'),

  /**
   * @private
   */
  teardown: (function() {
    this.$().off('change');
    //might be overkill, but make sure options can get gc'd
    this.get('options').clear();
  }).on('willDestroyElement'),

  /**
   * @private
   */
  registerOption: function(option) {
    this.get('options').addObject(option);
  },

  /**
   * @private
   */
  unregisterOption: function(option) {
    this.get('options').removeObject(option);
  }
});

//END CUSTOM SELECT

//START CUSTOM OPTION

var assert = Ember.assert;

/**
 * Used to wrap a native `<option>` tag and associate an object with
 * it that can be bound. It can only be used in conjuction with a
 * containing `custom-select` component
 *
 * @class App.CustomOptionComponent
 * @extends Ember.Component
 */
App.CustomOptionComponent = Ember.Component.extend({
  tagName: 'option',
  attributeBindings: ['selected', 'name', 'disabled'],
  classNameBindings: [':custom-option'],

  /**
   * The value associated with this option. When this option is
   * selected, the `custom-select` will fire its action with this
   * value.
   *
   * @property value
   * @type Object
   * @default null
   */
  value: null,

  /**
   * Property bound to the `selected` attribute of the native
   * `<option>` element. It is aware of the containing `Custom-select`'s
   * value and will mark itself if it is the same.
   *
   * @private
   * @property selected
   * @type Boolean
   */
  selected: Ember.computed('select.value', function() {
    return this.get('value') === this.get('select.value');
  }),

  /**
   * @private
   */
  registerWithSelect: (function() {
    var select = this.nearestOfType(App.CustomSelectComponent);
    assert("custom-option component declared without enclosing custom-select", !!select);
    this.set('select', select);
    select.registerOption(this);
  }).on('didInsertElement'),

  /**
   * @private
   */
  unregisterWithSelect: (function() {
    this.get('select').unregisterOption(this);
  }).on('willDestroyElement')
});

//END CUSTOM OPTION

;App.GoogleMapComponent = Ember.Component.extend({
    
    willInsertElement: function() {
        if(Ember.isEmpty(this.get('address')) && (Ember.isEmpty(this.get('lag')) || Ember.isEmpty('lng'))) {
            
            this.set('shouldRender', false);
        } else {
            
            this.set('shouldRender', true);
        }
    },
    
    didInsertElement: function() {
        
        if(this.get('shouldRender')) {
            var self = this;
            var $googleMap = Ember.$('#' + this.elementId);
                $googleMap.addClass('google-map');

            if(this.get('class')) {
                $googleMap.addClass(this.get('class'));
            }

            //let's make sure that we have included google maps
            if(typeof google !== 'undefined') {

                if(this.get('latLng') || (this.get('lat') && this.get('lng'))) {
                    if(!this.get('latLng')) {
                        this.set('latLng', { lat: this.get('lat'), lng: this.get('lng') });
                    }
                    
                    this.send('initializeMap');
                } else {
                    //we need to asynchronously
                    this.send('initializeMapWithAddress');
                }
            }
        }
    },
    
    actions: { 
        
        initializeMap: function() {
            var mapOptions = {
                center: this.get('latLng'),
                zoom: this.getWithDefault('zoom', 13)
            };

            var map = new google.maps.Map(document.getElementById(this.elementId), mapOptions);

            //add a marker
            var marker = new google.maps.Marker({
                position: this.get('latLng'),
                map: map,
                title: this.getWithDefault('title', '')
            });  
        },

        initializeMapWithAddress: function() {

            var self = this;
            
            var geocoder = new google.maps.Geocoder();

            geocoder.geocode(
                { address: this.get('address')},
                function(results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {
                        self.set('latLng', results[0].geometry.location);
                        self.send('initializeMap');
                    }
                }
            );

        }
    }
    
});;App.GravatarImageComponent = Ember.Component.extend({
  size: 200,
  email: '',

  gravatarUrl: function() {
    var email = this.get('email'),
        size = this.get('size');

    return 'http://www.gravatar.com/avatar/' + md5(email) + '?s=' + size;
  }.property('email', 'size')
});
;App.MultiPhotoUploaderComponent = Ember.Component.extend({
    
    didInsertElement: function() {
        var self = this;
        //initialize the dropzone
        Dropzone.options.adminMediaDropzone = false; //don't autoload dropzone. update it programatically
        
        //only allow image files
        var mediaDropzone = new Dropzone('#admin-media-dropzone', { url: baseURL + '/api-proxy/files', acceptedFiles: 'image/*' });
        
        mediaDropzone.on('success', function(response) {
            console.log(JSON.parse(response.xhr.response).file);
            self.get('targetObject.store').push('file', JSON.parse(response.xhr.response).file);
        });
        
        mediaDropzone.on('error', function(response) {
            console.log(response);
        });
    }
    
});;App.PhotoPickerComponent = Ember.Component.extend({
    
    photoPicked: function() {
//        console.log('photo picked changed');
        return this.get('targetObject.photoPicked');
    }.property('targetObject.photoPicked'),
    
    selectedMedia: function() {
        
        var files = this.get('media');
        var photoPicked = this.get('targetObject.photoPicked');
        
        if(typeof photoPicked !== 'undefined') {
            files.forEach(function(file) {
                if(photoPicked.get('id') == file.get('id')) {
                    file.set('isSelected', true);
                } else {
                    file.set('isSelected', false);
                }
            });
        }
        
        return files;
        
    }.property('media', 'targetObject.photoPicked'),
    
    click: function(e) {
        //make sure to only pass the photoPicked action when it's an input that is selected
        if(typeof Ember.$(e.target).attr('name') !== 'undefined') {
//            console.log(e.target);
            var file_id =  Ember.$(e.target).val();
            this.get('targetObject').set('photoPicked', this.get('targetObject.store').find('file', file_id)); 
        }
    }
});;/**
 * Ember select-2 component wrapping the jQuery select2 plugin while
 * respecting Ember data bindings and getter/setter methods on the content.
 *
 * Terminology:
 *  - Value: The currently selected value(s). Propagated to controllers etc.
 *    through the "value=..."" binding. Types:
 *    - Object: when using select-2 without any further configuration
 *    - Array of Objects: when using select-2 with "multiple=true"
 *    - Mixed: when using select-2 with "optionValuePath=..."
 *    - Array of Mixed: when using select-2 with "multiple=true" and
 *      "optionValuePath=..."
 *
 *  - Content: Array of Objects used to present to the user for choosing the
 *    selected values. "content" cannot be an Array of Strings, the Objects are
 *    expected to have an "id" and a property to be used as the label (by default,
 *    it is "text", but it can be overwritten it via "optionLabelPath"). These
 *    properties can be computed properties or just plain JavaScript values.
 */
var get = Ember.get;
var run = Ember.run;

App.Select2Component = Ember.Component.extend({
  tagName: "input",
  classNames: ["form-control"],
  classNameBindings: ["inputSize"],
  attributeBindings: ["style"],
  style: Ember.Handlebars.SafeString("display: hidden;"),

  // Bindings that may be overwritten in the template
  inputSize: "input-md",
  cssClass: null,
  optionIdPath: "id",
  optionValuePath: null,
  optionLabelPath: 'text',
  optionHeadlinePath: 'text',
  optionDescriptionPath: 'description',
  placeholder: null,
  multiple: false,
  allowClear: false,
  enabled: true,
  query: null,
  typeaheadSearchingText: 'Searching…',
  typeaheadNoMatchesText: 'No matches found',
  typeaheadErrorText: 'Loading failed',
  searchEnabled: true,
  minimumInputLength: null,
  maximumInputLength: null,

  // internal state
  _hasSelectedMissingItems: false,
  _hasPendingContentPromise: Ember.computed.alias('content.isPending'),
  _hasFailedContentPromise: Ember.computed.alias('content.isRejected'),
  _hasPendingValuePromise: Ember.computed.alias('value.isPending'),
  _hasFailedValuePromise: Ember.computed.alias('value.isRejected'),
  _typeaheadMode: Ember.computed.bool('query'),

  didInsertElement: function() {
    var self = this,
        options = {},
        optionIdPath = this.get('optionIdPath'),
        optionLabelPath = this.get('optionLabelPath'),
        optionHeadlinePath = this.get('optionHeadlinePath'),
        optionDescriptionPath = this.get('optionDescriptionPath'),
        content = this.get('content');


    // ensure select2 is loaded
    Ember.assert("select2 has to exist on Ember.$.fn.select2", typeof Ember.$.fn.select2 === "function");

    // setup
    options.placeholder = this.get('placeholder');
    options.multiple = this.get('multiple');
    options.allowClear = this.get('allowClear');
    options.minimumResultsForSearch = this.get('searchEnabled') ? 0 : -1 ;

    options.minimumInputLength = this.get('minimumInputLength');
    options.maximumInputLength = this.get('maximumInputLength');

    // override select2's default id fetching behavior
    options.id = (function(e) {
      return (e === undefined) ? null : get(e, optionIdPath);
    });

    // allowClear is only allowed with placeholder
    Ember.assert("To use allowClear, you have to specify a placeholder", !options.allowClear || options.placeholder);

    // search can't be disabled for multiple selection mode
    var illegalSearchInMultipleMode = options.multiple && !this.get('searchEnabled');
    Ember.assert("Search field can't be disabled for multiple selection mode", !illegalSearchInMultipleMode);

    /*
      Formatting functions that ensure that the passed content is escaped in
      order to prevent XSS vulnerabilities. Escaping can be avoided by passing
      Handlebars.SafeString as "text", "headline" or "description" values.
      Generates the html used in the dropdown list (and is implemented to
      include the description html if available).
     */
    options.formatResult = function(item) {
      if (!item) {
        return;
      }

      var output,
          id = get(item, optionIdPath),
          text = get(item, optionLabelPath),
          headline = get(item, optionHeadlinePath),
          description = get(item, optionDescriptionPath);

      if (item.children) {
        output = Ember.Handlebars.Utils.escapeExpression(headline);
      } else {
        output = Ember.Handlebars.Utils.escapeExpression(text);
      }

      // only for "real items" (no group headers) that have a description
      if (id && description) {
        output += " <span class=\"text-muted\">" +
          Ember.Handlebars.Utils.escapeExpression(description) + "</span>";
      }

      return output;
    };

    /*
      Generates the html used in the closed select input, displaying the
      currently selected element(s). Works like "formatResult" but
      produces shorter output by leaving out the description.
     */
    options.formatSelection = function(item) {
      if (!item) {
        return;
      }

      var text = get(item, optionLabelPath);

      // escape text unless it's passed as a Handlebars.SafeString
      return Ember.Handlebars.Utils.escapeExpression(text);
    };

    /*
      Provides a list of items that should be displayed for the current query
      term. Uses the default select2 matcher (which handles diacritics) with the
      Ember compatible getter method for optionLabelPath.
     */
    options.query = function(query) {
      var select2 = this;

      if (self.get('_typeaheadMode')) {
        var deferred = Ember.RSVP.defer('select2#query: ' + query.term);

        self.sendAction('query', query, deferred);

        deferred.promise.then(function(result) {
          var data = result;
          var more = false;

          if (result instanceof Ember.ArrayProxy) {
            data = result.toArray();
          } else if (!Array.isArray(result)) {
            if (result.data instanceof Ember.ArrayProxy) {
              data = result.data.toArray();
            } else {
              data = result.data;
            }
            more = result.more;
          }

          query.callback({
            results: data,
            more: more
          });
        }, function(reason) {
          query.callback({
            hasError: true,
            errorThrown: reason
          });
        });
      } else {
        Ember.assert("select2 has no content!", self.get('content'));

        var filteredContent = self.get("content").reduce(function(results, item) {
          // items may contain children, so filter them, too
          var filteredChildren = [];

          if (item.children) {
            filteredChildren = item.children.reduce(function(children, child) {
              if (select2.matcher(query.term, get(child, optionLabelPath)) || select2.matcher(query.term, get(child, optionHeadlinePath))) {
                children.push(child);
              }
              return children;
            }, []);
          }

          // apply the regular matcher
          if (select2.matcher(query.term, get(item, optionLabelPath)) || select2.matcher(query.term, get(item, optionHeadlinePath))) {
            // keep this item either if itself matches
            results.push(item);
          } else if (filteredChildren.length) {
            // or it has children that matched the term
            var result = Ember.$.extend({}, item, { children: filteredChildren });
            results.push(result);
          }
          return results;
        }, []);

        query.callback({
          results: filteredContent
        });
      }
    };

    /*
      Supplies the string used when searching for options, can be set via
      `typeaheadSearchingText`
     */
    options.formatSearching = function() {
      var text = self.get('typeaheadSearchingText');

      return Ember.String.htmlSafe(text);
    };

    /*
      Format the no matches message, substituting the %@ placeholder with the
      html-escaped user input
     */
    options.formatNoMatches = function(term) {
      var text = self.get('typeaheadNoMatchesText');
      if (text instanceof Ember.Handlebars.SafeString) {
        text = text.string;
      }

      term = Ember.Handlebars.Utils.escapeExpression(term);

      return Ember.String.htmlSafe(Ember.String.fmt(text, term));
    };

    /*
      Format the error message, substituting the %@ placeholder with the promise
      rejection reason
     */
    options.formatAjaxError = function(jqXHR, textStatus, errorThrown) {
      var text = self.get('typeaheadErrorText');

      return Ember.String.htmlSafe(Ember.String.fmt(text, errorThrown));
    };

    /*
      Maps "value" -> "object" when using select2 with "optionValuePath" set,
      and one time directly when setting up the select2 plugin even without "oVP".
      (but with empty value, which will just skip the method)
      Provides an object or an array of objects (depending on "multiple") that
      are referenced by the current select2 "val".
      When there are keys that can not be matched to objects, the select2 input
      will be disabled and a warning will be printed on the console.
      This is important in case the "content" has yet to be loaded but the
      "value" is already set and must not be accidentally changed because the
      inout cannot yet display all the options that are required.
      To disable this behaviour, remove those keys from "value" that can't be
      matched by objects from "content".
     */
    options.initSelection = function(element, callback) {
      var value = element.val(),
          content = self.get("content"),
          contentIsArrayProxy = Ember.ArrayProxy.detectInstance(content),
          multiple = self.get("multiple"),
          optionValuePath = self.get("optionValuePath");

      if (!value || !value.length) {
        return callback([]);
      }

      // this method should not be needed without the optionValuePath option
      // but make sure there is an appropriate error just in case.
      Ember.assert("select2#initSelection has been called without an \"" +
        "optionValuePath\" set.", optionValuePath);

      Ember.assert("select2#initSelection can not map string values to full objects " +
        "in typeahead mode. Please open a github issue if you have questions to this.",
        !self.get('_typeaheadMode'));


      var values = value.split(","),
          filteredContent = [];

      // for every object, check if its optionValuePath is in the selected
      // values array and save it to the right position in filteredContent
      var contentLength = get(content, 'length'),
          unmatchedValues = values.length,
          matchIndex;

      // START loop over content
      for (var i = 0; i < contentLength; i++) {
        var item = contentIsArrayProxy ? content.objectAt(i) : content[i];
        matchIndex = -1;

        if (item.children && item.children.length) {
          // take care of either nested data...
          for (var c = 0; c < item.children.length; c++) {
            var child = item.children[c];
            matchIndex = values.indexOf("" + get(child, optionValuePath));
            if (matchIndex !== -1) {
              filteredContent[matchIndex] = child;
              unmatchedValues--;
            }
            // break loop if all values are found
            if (unmatchedValues === 0) {
              break;
            }
          }
        } else {
          // ...or flat data structure: try to match simple item
          matchIndex = values.indexOf("" + get(item, optionValuePath));
          if (matchIndex !== -1) {
            filteredContent[matchIndex] = item;
            unmatchedValues--;
          }
          // break loop if all values are found
          if (unmatchedValues === 0) {
            break;
          }
        }
      }
      // END loop over content

      if (unmatchedValues === 0) {
        self.set('_hasSelectedMissingItems', false);
      } else {
        // disable the select2 element if there are keys left in the values
        // array that were not matched to an object
        self.set('_hasSelectedMissingItems', true);

        Ember.warn("select2#initSelection was not able to map each \"" +
          optionValuePath +"\" to an object from \"content\". The remaining " +
          "keys are: " + values + ". The input will be disabled until a) the " +
          "desired objects is added to the \"content\" array or b) the " +
          "\"value\" is changed.", !values.length);
      }

      if (multiple) {
        // return all matched objects
        return callback(filteredContent);
      } else {
        // only care about the first match in single selection mode
        return callback(filteredContent.get('firstObject'));
      }
    };

    /*
      Forward a custom css class to the components container and dropdown.
      The value will be read from the `cssClass` binding
     */
    options.containerCssClass = options.dropdownCssClass = function() {
      return self.get('cssClass') || '';
    };

    this._select = this.$().select2(options);

    this._select.on("change", run.bind(this, function() {
      // grab currently selected data from select plugin
      var data = this._select.select2("data");
      // call our callback for further processing
      this.selectionChanged(data);
    }));

    this.addObserver('content.[]', this.valueChanged);
    this.addObserver('content.@each.' + optionLabelPath, this.valueChanged);
    this.addObserver('content.@each.' + optionHeadlinePath, this.valueChanged);
    this.addObserver('content.@each.' + optionDescriptionPath, this.valueChanged);
    this.addObserver('value', this.valueChanged);

    // trigger initial data sync to set select2 to the external "value"
    this.valueChanged();

    // eventually disable input when content is PromiseProxy
    if (Ember.PromiseProxyMixin.detect(content)) {
      // enabling/siabling is done via binding to _hasPendingContentPromise
      // provide error for rejected promise, though.
      content.then(null, function (reason) {
        Ember.warn("select2: content promise was reject with reason " + reason +
          ". Recovering from this is not (yet) implemented.");
      });
    }

    this.watchDisabled();
  },

  /**
   * Teardown to prevent memory leaks
   */
  willDestroyElement: function() {
    // If an assertion caused the component not to render, we can't remove it from the dom.
    if(this._select) {
      this._select.off("change");
      this._select.select2("destroy");
    }

    this.removeObserver('content.[]', this.valueChanged);
    this.removeObserver(
      'content.@each.' + this.get('optionLabelPath'),
      this.valueChanged
    );
    this.removeObserver(
      'content.@each.' + this.get('optionHeadlinePath'),
      this.valueChanged
    );
    this.removeObserver(
      'content.@each.' + this.get('optionDescriptionPath'),
      this.valueChanged
    );
    this.removeObserver('value', this.valueChanged);
  },

  /**
   * Respond to selection changes originating from the select2 element. If
   * select2 is working with full objects just use them to set the value,
   * use the optionValuePath otherwise.
   *
   * @param  {String|Object} data   Currently selected value
   */
  selectionChanged: function(data) {
    var value,
        multiple = this.get("multiple"),
        optionValuePath = this.get("optionValuePath");

    // if there is a optionValuePath, don't set value to the complete object,
    // but only the property referred to by optionValuePath
    if (optionValuePath) {
      if (multiple) {
        // data is an array, so use getEach
        value = data.getEach(optionValuePath);
      } else {
        // treat data as a single object
        value = get(data, optionValuePath);
      }
    } else {
      value = data;
    }

    this.set("value", value);
    Ember.run.schedule('actions', this, function() {
      this.sendAction('didSelect', value, this);
    });
  },

  /**
   * Respond to external value changes. If select2 is working with full objects,
   * use the "data" API, otherwise just set the "val" property and let the
   * "initSelection" figure out which object was meant by that.
   */
  valueChanged: function() {
    var self = this,
        value = this.get("value"),
        optionValuePath = this.get("optionValuePath");

    if (Ember.PromiseProxyMixin.detect(value)) {
      // schedule re-setting value after promise is settled
      value.then(function(value) {
        if (value === null || value === undefined) {
          self._select.select2("val", null);
        }
      }, function(reason) {
        Ember.warn("select2: value promise was reject with reason " + reason +
          ". Recovering from this is not (yet) implemented.");
      });
    }

    if (optionValuePath) {
      // when there is a optionValuePath, the external value is a primitive value
      // so use the "val" method
      this._select.select2("val", value);
    } else {
      // otherwise set the full object via "data"
      this._select.select2("data", value);
    }
  },

  /**
   * Watch properties that determine the disabled state of the input.
   */
  watchDisabled: Ember.observer(
    '_hasSelectedMissingItems',
    '_hasPendingContentPromise',
    '_hasFailedContentPromise',
    '_hasPendingValuePromise',
    '_hasFailedValuePromise',
    'enabled',
    function() {
      var select = this._select,
          disabled = this.get('_hasSelectedMissingItems') ||
            this.get('_hasPendingContentPromise') ||
            this.get('_hasFailedContentPromise') ||
            this.get('_hasPendingValuePromise') ||
            this.get('_hasFailedValuePromise') ||
            !this.get('enabled');

      if (select) {
        Ember.run(function() {
          select.select2("readonly", disabled);
        });
      }
    }
  )
});

//END EMBER SELECT2

;App.SinglePhotoUploaderComponent = Ember.Component.extend({
    
    selectedMedia: function() {
        
        var files = this.get('media');
        var photoPicked = this.get('value');
//        console.log(photoPicked);
        
        if(!Ember.isNone(photoPicked)) {
            files.forEach(function(file) {
                if(photoPicked.get('id') == file.get('id')) {
                    file.set('isSelected', true);
                } else {
                    file.set('isSelected', false);
                }
            });
        }
        
        return files;
        
    }.property('media', 'value'),
    
    click: function(e) {
        //make sure to only pass the photoPicked action when it's an input that is selected
        if(!Ember.isNone(Ember.$(e.target).attr('name'))) {
//            console.log(e.target);
            var file_id =  Ember.$(e.target).val();
//            console.log(this.get('targetObject.store').find('file', file_id));
            var self = this;
            this.get('targetObject.store').find('file', file_id).then(function(file) {
                self.set('value', file);
            });
        }
    },
    
    didInsertElement: function() {
        
        $('.dropdown-menu a.removefromcart').click(function(e) {
            e.stopPropagation();
        });
        
        var self = this;
        //initialize the dropzone
        Dropzone.options.dropzoneSelector = false; //don't autoload dropzone. update it programatically
        
        //only allow image files
        var mediaDropzone = new Dropzone(
            '#dropzone-selector', 
            { 
                maxFiles: 1,
                url: baseURL + '/api-proxy/files', 
                acceptedFiles: 'image/*',
                clickable: '#dropzone-clickable',
                thumbnail: function() {},
                addedfile: function(file) {},
                thumbnail: function(file, dataUrl) {},
                uploadprogress: function(file, progress, bytesSent) {}
            }
        );
        
        //prevent more than one file from being loaded at a time in the preview
        mediaDropzone.on('addedfile', function() {
            if(this.files[1] != null) {
                this.removeFile(this.files[0]);
            }
        });
        
        mediaDropzone.on('success', function(response) {
            console.log(response);
            console.log(JSON.parse(response.xhr.response).file);
            var file = self.get('targetObject.store').push('file', JSON.parse(response.xhr.response).file);
            self.set('value', file);
        });
        
        mediaDropzone.on('error', function(response) {
            console.log(response);
        });
    }
    
});
;App.ApplicationController = Ember.ArrayController.extend({
    
    needs: ['cart', 'pages'],
    cart: Ember.computed.alias('controllers.cart'),
    pages: Ember.computed.alias('controllers.pages'),
    
    siteName: function() { 
        //now we can set the document title as well...
        var coreSiteName = this.get('model').filterBy('name', 'core_site_name').get('firstObject');
        
        //TODO: put this in a better spot to make the document title be dynamic based on this value changing
        if(!Ember.isEmpty(coreSiteName)) {
            document.title = coreSiteName.get('string_value');
        } else {
            document.title = 'Ceres Manager';
        }
        
        return coreSiteName; 
    
    }.property('model'),
    
    cartCount: function() {
        return this.get('cart.model.cartItems.length'); //just to get the count
    }.property('cart.model.cartItems'),
    
    currentUser: window.currentUser,
    
	canViewAdmin: function() {
		
		var permissions = this.get('currentUser.permissions');
		
		if(permissions) {
			//permissions exists so make sure one of them is view_admin for the current user
			return permissions.some(function(permission) {
				return permission === 'view_admin';
			});
		}
		
		return false;
		
	}.property('currentUser')

    
});



//Controllers not tied to any routes
App.InventoryCategoriesController = Ember.ArrayController.extend();
App.MediaController = Ember.ArrayController.extend();
App.PagesController = Ember.ArrayController.extend();
App.CurrentPageController = Ember.ObjectController.extend();
App.RentalCategoriesController = Ember.ArrayController.extend();
App.GoodCategoriesController = Ember.ArrayController.extend();

App.RandomRentalController = Ember.ObjectController.extend();
App.RandomGoodController = Ember.ObjectController.extend();
App.RandomEventController = Ember.ObjectController.extend();
;/*
 
    EMBER MIXINS GO HERE

 */;//EMBER MODEL SETTINGS AND INITIAL SETUP
var attr = DS.attr,
    hasMany = DS.hasMany,
    belongsTo = DS.belongsTo;


App.ApplicationSerializer = DS.RESTSerializer.extend({
    serializeIntoHash: function(hash, type, record, options) {
        Ember.merge(hash, this.serialize(record, options));
    }
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
    host: window.baseURL,
    namespace: 'api-proxy',

    ajax: function(url, type, hash) {
      if (Ember.isEmpty(hash)) hash = {};
      if (Ember.isEmpty(hash.data)) hash.data = {};

      //so we can spoof the request method so laravel knows how to interpret it
      if(type !== 'GET' && type !== 'POST') {
          hash.data._method = type;
          type = 'POST';
      }

      Ember.Logger.debug('DEBUG: Method type:', type);
      Ember.Logger.debug('DEBUG: Hash information: ', hash);

      return this._super(url, type, hash);
    },

    ajaxError: function(jqXHR) {

        console.log(jqXHR);
        var defaultAjaxError = this._super(jqXHR);
        if (jqXHR) {
            switch(jqXHR.status) {
                case 401:
                return jqXHR;
            }
        }
        return defaultAjaxError;
    }
});

//comment when not testing-------------------
/*App.ApplicationAdapter = DS.FixtureAdapter;
App.ApplicationAdapter = DS.FixtureAdapter.extend({
    queryFixtures: function(fixtures, query, type) {
        console.log('called from query fixtures');
        return fixtures.filter(function(fixture) {
            for(key in query) {
                if(fixture[key] !== query[key]) { return false; }
            }
            console.log(query);
            return true;
        });
    }
});*/

//help to override the faulty DS model
DS.Model.reopenClass({

    becameError: function() {
      alert('there was an error!');
    },

    becameInvalid: function(errors) {
      alert("Record was invalid because: #{errors}");
    }
});;App.ApplicationRoute = Ember.Route.extend({
    //allows us to set the title in the tab of the browser
    model: function() {
        return this.store.find('setting');
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('pages').set('model', this.store.find('page'));
        this.controllerFor('cart').set('model', this.store.find('cartItem'));
        
    },
    
    promptLogin: function(transition) {
        $(function() {
            $('#login-dropdown').dropdown('toggle');
        });
        
        var applicationController = this.controllerFor('application');
        applicationController.set('attemptedTransition', transition);
        this.transitionTo('home');
    },

    actions: {
        refresh: function() {
            this.refresh();
        },
        
        //open the admin up
        openAdmin: function() {
            this.send('closeAdmin');
            
            window.adminSide = window.open(rootURL + 'admin');
        },
        
        closeAdmin: function() {
            if(typeof window.adminSide !== 'undefined') {
                console.log('the frontend side has already been opened');
                
                if(typeof window.adminSide.close === 'function') {
                    window.adminSide.close();
                }
            } else {
                
                if(!Ember.isEmpty(window.opener)) {
                    console.log('the frontend side opened the admin side');
                    window.adminSide = window.opener;
                    window.adminSide.close();
                }
            }
        },
        
        frontendLogout: function() {
            
            this.send('closeAdmin');
            
            window.location = rootURL + 'api-proxy/logout'; //now that we've closed the children window we can logout
            
        },
        
        //catch all errors
        error: function(reason, transition) {
          
            switch(reason.status) {
                case 401:
                    //user is not logged in
                    this.promptLogin(transition);

                    break;
                case 403:

                    //token is good (could be client token) but the user is not allowed
                    this.redirectToHome(reason);

                    break;
                case 404:

                    //resource not found


                    break;
                case 405:

                    //the method used for the request is not allowed for the route requested


                    break;
                case 407:

                    //the api-proxy


                    break;
                case 440:

                    //login session has expired


                    break;
                case 498:

                    //the token must be an invalid one.


                    break;
                case 499:

                    //no token was submitted


                    break;
                default:

                    //unhandled error here


                    break;
            }
            
        }
    }
});;App.ApplicationView = Ember.View.extend({
	templateName: 'application',
	
	didInsertElement: function() {
		
	}
});

App.ModalView = Ember.View.extend(Ember.Evented, {
    
    routeOnClose: 'admin.inventory',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute(self.get('routeOnClose'));
        });
        
        this.get('controller').on('modalActionDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('modalActionDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
});

App.SlideoutView = Ember.View.extend({
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
});;App.AdminAccountController = Ember.ObjectController.extend({
	needs: ['userAddresses'],
	
	reset: function() {
		this.setProperties({
			password: '',
			password_confirmation: ''
		});
	},
	
	actions: {
		
		updateUser: function() {
			var self = this;
			var user = this.get('model');
			
			user.save().then(function(results) {
				console.log(results);
				console.log('user info updated successfully');
				self.reset();
			}, function(results) {
				console.log(results);
				user.rollback(); //if  saving didn't work
			});
			
		}
		
	}
});

App.AdminAccountAddressCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addAddress: function() {
		
			var self = this;
			var data = this.getProperties('recipient_name',
										  'address_line_1',
										  'address_line_2',
										  'city',
										  'state_province',
										  'zip',
										  'country');
			
			var userAddress = this.store.createRecord('userAddress', data);
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('addAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.destroyRecord();
			});
		
		}
	
	}
	
});

App.AdminAccountBillinginfoCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addBillinginfo: function(form) {
			
			var self = this;
			var $form = Ember.$('#form-add-billing-info');
			
			// Disable the submit button to prevent repeated clicks
    		var $submitForm = $form.find('button[type="submit"]').prop('disabled', true);
			
			Stripe.card.createToken($form, function(status, response) {
				  if (response.error) {
					// Show the errors on the form
					$form.find('.payment-errors').text(response.error.message);
					$submitForm.prop('disabled', false);
				  } else {
					// response contains id and card, which contains additional card details
					var token = response.id;
					
					var data = { stripeToken: token };
					Ember.$.post(rootURL + '/api-proxy/userBillingInfos', data).then(function(response) {
						self.store.push('userBillingInfo', response.userBillingInfo); //push to store to render on the view
						self.trigger('addBillingInfoDidSucceed');
					}, function(response) {
						$form.find('.payment-errors').text('error saving card. please try again');
						$submitForm.prop('disabled', false);
						console.log('error saving the new credit card information ' + response);
					});
					  
				  }
				
			});
			
		}
	
	}
	
});

App.AdminAccountAddressEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		updateAddress: function() {
		
			var self = this;
			var userAddress = this.get('model');
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.rollback();
			});
		
		}
	
	}
	
});

App.AdminAccountBillinginfoEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	formatted4: function() {
		return '•••• •••• •••• ' + this.get('last4');
	}.property('last4'),
	
	actions: {
	
		updateBillinginfo: function() {
			
			var self = this;
			var billingInfo = this.get('model');
			
			billingInfo.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateBillingInfoDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				billingInfo.rollback();
			});
			
		}
	
	}
	
});;;App.AdminAccountRoute = Ember.Route.extend({
    
    model: function() {
        
		var currentUser = this.controllerFor('application').get('currentUser');
		return this.store.find('user', currentUser.id);
        
    }
    
});

App.AdminAccountAddressEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userAddress', params.address_id);
	}
});

App.AdminAccountBillinginfoEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userBillingInfo', params.billing_id);
	}
});;App.AdminAccountBillinginfoCreateView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountAddressCreateView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountBillinginfoEditView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminAccountAddressEditView = Ember.View.extend(Ember.Evented, {
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.account');
        });
        
        this.get('controller').on('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});;App.AdminBlogController = Ember.ArrayController.extend({
    sortProperties: ['created_at'],
    sortAscending: false,
});

App.AdminBlogCreateController = Ember.ObjectController.extend(Ember.Evented, {
    
    item: null,
    
    init: function() {
        this._super();
        
        var self = this;
        
        //setup the autosaving feature...
        this.set('autosaveInterval', setInterval(function() {
            
            var item = self.get('item');
            
            if(Ember.isEmpty(item)) {
                var data = self.getProperties('title', 'is_active', 'page_content');
                var item = self.store.createRecord('blog', data);
                    item.save();
            } else {
                item.set('title', self.get('title'));
                item.set('is_active', self.get('is_active'));
                item.set('page_content', self.get('page_content'));
                item.save();
            }
            
            self.set('item', item);
            console.log('saving...');
        }, 2000));
    },
    
    is_active: false //by default the blog should be set to be published
    
});

App.AdminBlogItemController = Ember.ObjectController.extend(Ember.Evented, {
    
    init: function() {
        this._super();
        
        var self = this;
        
        //setup the autosaving feature...
        this.set('autosaveInterval', setInterval(function() {
            console.log('saving...');
            self.get('model').save();
        }, 2000));
    },
    
});;App.Page = DS.Model.extend({
    title: attr('string'),
    slug: attr('string'),
    show_title: attr('number'),
    is_active: attr('number'),
    is_home: attr('number'),
    page_content: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    slideshow: belongsTo('slideshow', { async: true })
});

App.Blog = DS.Model.extend({
    title: attr('string'),
    slug: attr('string'),
    is_active: attr('number'),
    page_content: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    previousBlog: attr('number'),
    nextBlog: attr('number'),
    
    author: belongsTo('user', { async: true }),
    last_modified_by: belongsTo('user', { async: true })
});
;App.AdminBlogRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('blog');
    }
    
});

App.AdminBlogCreateRoute = Ember.Route.extend({
    
    actions: {
        togglePublish: function() {
            var controller = this.get('controller');
            controller.set('is_active', !controller.get('is_active'));
        },
        
        willTransition: function(transition) {
            console.log('stop saving');
            //save one more time just in case
            clearInterval( this.get('controller').get('autosaveInterval') );
        }
    }
    
});

App.AdminBlogItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('blog', params.blog_id);
    },

    actions: {
        togglePublish: function() {
            var controller = this.get('controller');
            controller.set('is_active', !controller.get('is_active'));
            controller.get('model').save();
        },
        
        willTransition: function(transition) {
            console.log('stop saving');
            clearInterval( this.get('controller').get('autosaveInterval') );
        }
    }
    
});;App.AdminBlogCreateView = App.ModalView.extend({
    routeOnClose: 'admin.blog'
});

App.AdminBlogItemView = App.ModalView.extend({
    routeOnClose: 'admin.blog'
});;
;
;;;App.AdminEventsController = Ember.ArrayController.extend({
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['type', 'search'],    // Creates url Query ../?type='_____' will result in a search for type
    type: null,                         // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    filteredEvents: function() {
        var search = this.get('search');
        var searchableProperties = ['name', 'description'];
        var events = this.get('arrangedContent');  // creates array of models to be filtered.
        if(search) {
            events = events.filter(function(event) {
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(event.get(searchableProperties[i]));
                    if(event.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
                return false;
            });
        }
        return events;
    }.property('search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
    }
});

App.AdminEventsEventController = Ember.ObjectController.extend({
    
    needs: ['media'],
    media: Ember.computed.alias('controllers.media'),
    
    actions: {
        updateEvent: function() {
            var self = this;
            var item = this.get('model');
            
            item.set('start', new Date(this.get('start')));
            item.set('end', new Date(this.get('end')));
            item.set('registrationDeadline', new Date(this.get('registrationDeadline')));
            item.set('publicAt', new Date(this.get('publicAt')));
            
            item.save().then(function() {}, function(results) {
                console.log(results);
                item.rollback();
            });
            
        },
        addInstances: function() {
            var instanceCount = this.get('instanceCount');
            for(var i = 0; i < instanceCount; i++) {
                var newInstance = this.store.createRecord('eventsInstance', { events: this.get('model') });
                newInstance.save().then(function(results) {
                    console.log(results);
                }, function(results) {
                    console.log(results);
                    newInstance.destroyRecord();
                });
            }
        },
        deleteInstance: function(instance) {
            var confirmDelete = confirm("Are you sure you want to delete this event?");
            if (confirmDelete) {
                instance.deleteRecord();
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                    }, function(results) {
                        instance.rollback();
                    });
                }
            }
        },
        backToEvents: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        deleteEvent: function() {
            console.log('delete event...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this event?");
            if (confirmDelete) {
                this.send('backToEvents', function() {
                    var record = self.get('model'); //delete the event record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.events');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;

            this.send('backToEvents', function() {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.events');
            });
        }
    }
});

App.AdminEventsCreateController = App.AdminEventsEventController.extend(Ember.Evented, {
    
    needs: ['media'],
    media: Ember.computed.alias('controllers.media'),
    
    publicVisible: true,
    //function to reset all of the values in the modal when loaded...
    reset: function() {
        this.setProperties({
            alertAt: '',
            capacity: '',
            description: '',
            end: '',
            location: '',
            name: '',
            price: '',
            publicAt: '',
            registrationDeadline: '',
            start: ''
        });
    },
    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],
    actions: {
        saveEvent: function () {
            var self = this;
            //grab all relevant data for creating a new event
            var data = this.getProperties(
                'alertAt',
                'capacity',
                'description',
                'end',
                'location',
                'name',
                'file',
                'price',
                'publicVisible'
            );

            data.start = Ember.isEmpty(this.get('start')) ? null : new Date(this.get('start'));
            data.end = Ember.isEmpty(this.get('end')) ? null : new Date(this.get('end'));
            data.registrationDeadline = Ember.isEmpty(this.get('registrationDeadline')) ? null : new Date(this.get('registrationDeadline'));
            data.publicAt = Ember.isEmpty(this.get('publicAt')) ? null : new Date(this.get('publicAt'));

            console.log("Grabbing data from form...");
            self.send('saveEventWithData', data);
        },
        saveEventWithData: function(data) {
            console.log("Attempting to create a new event...");
            var self = this;
            var event = this.store.createRecord('event', data);
            event.save().then(function (event) {
                //now attach the item to the category
                console.log("Creating a new event succeeded.");
                self.trigger('addEventDidSucceed');
            }, function (results) {
                //TODO: this is where we want to highlight crap
                event.destroyRecord(); //failed. therefore we want to delete it from the store
                console.log("Creating a new event failed:\n" + results);
            });
        }
    }
});;App.EventsBase = DS.Model.extend({
    alertAt:                attr('number'),     // The number of registrants at which an alert will show for an event
    capacity:               attr('number'),     // The maximum amount of participants who can register for an event
    created_at:             attr('date'),       // The date an event was added to database, date created
    description:            attr('string'),     // The description of the event
    end:                    attr('date'),       // The end date/time of an event
    location:               attr('string'),     // The physical location of the event
    name:                   attr('string'),     // The name of event
    price:                  attr('number'),     // The price that a customer will pay at purchase
    publicAt:               attr('date'),       // The date an event becomes visible to the public
    publicVisible:          attr('boolean'),    // The overriding decider of an event's public visibility.
    registrationDeadline:   attr('date'),       // The date/time by which a customer can register for an event
    start:                  attr('date'),       // The start date/time of an event
    updated_at:             attr('date'),       // The date an event was last updated, date modified/updated
    
    file: belongsTo('file', { async: true }),
    
    //COMPUTED PROPERTIES FOR SORTING AND DISPLAYING
    //registrantCount: function() { return this.get('count'); }.property('count'),

    alertClass: function(registrantCount) {
        var dangerCount = isNaN(parseInt(this.get('alertAt'))) ? 0 : parseInt(this.get('alertAt'));
        var warningCount = dangerCount == 0 ? parseInt(this.get('alertAt')) / 3 : dangerCount / 2;
        //var registrantCount = parseInt(this.get('count'));

        if(registrantCount >= dangerCount) {
            return 'label-danger';
        } else if(registrantCount >= warningCount) {
            return 'label-warning';
        } else {
            return 'label-success';
        }

    }.property('alertAt', 'registrantCount')
});

App.Event = App.EventsBase.extend({});;App.AdminEventsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('event');
    }
});

App.AdminEventsEventRoute = Ember.Route.extend({

    model: function(params) {
        return this.store.find('event', params.event_id);
    },

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    }

});

App.AdminEventsCreateRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        
        controller.reset(); //reset the values when called
    }

});;App.AdminEventsEventView = Ember.View.extend({
    templateName: 'admin/events/event',
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
});

App.AdminEventsCreateView = Ember.View.extend(Ember.Evented, {
    templateName: 'admin/events/create',
    didInsertElement: function() {
        var self = this;
        Ember.$('#myModal').modal('show');
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.events');
        });
        this.get('controller').on('addEventDidSucceed', function() {
            self.send('closeModal');
        });
    },
    willClearRender: function() {
        this.get('controller').off('addEventDidSucceed', function() {
            self.send('closeModal');
        });
    },
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
});;App.AdminInventoryController = Ember.ArrayController.extend({
    
    //other controllers
    needs: ['inventoryCategories'],
    inventoryCategories: Ember.computed.alias('controllers.inventoryCategories'),
    
    inventoryTypes: ['good', 'rental', 'asset'],
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['type', 'search'],    // Creates url Query ../?type='_____' will result in a search for type
    type: null,                         // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    
    filteredInventory: function() {
        
        var type = this.get('type');    // retrieves type variable to filter by
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'id'];
        var items = this.get('arrangedContent');  // creats array of models to be filtered.
        
        if (type) {
            items = items.filterBy('type', type);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
    }.property('type', 'search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
        
    },

});



App.AdminInventoryItemController = Ember.ObjectController.extend({
    
   //other dependencies
   needs: ['inventoryCategories', 'media'],
   inventoryCategories: Ember.computed.alias('controllers.inventoryCategories'),
   media: Ember.computed.alias('controllers.media'),
    
   currentCategory: null,
    
   pricePerUnits: ['hour', 'day'],
    
   isItemGood: function() {
        return this.get('type') === 'good';
   }.property('type'),
    
   isItemRental: function() {
        return this.get('type') === 'rental';
   }.property('type'),
    
   isItemAsset: function() {
        return this.get('type') === 'asset';
   }.property('type'),
    
   itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
   }.property('model.currentState.stateName'),
    
   actions: {
        updatePublicVisible: function(value) {
            this.set('publicVisible', value);
        },

        changeInventoryCategory: function(value, object) {
            this.set('inventoryCategory', value);  
        }
   }
});




App.AdminInventoryCreateController = App.AdminInventoryItemController.extend(Ember.Evented, {
    
    inventoryTypes: ['good', 'rental', 'asset'], //legal inventory types for the dropdown
    type: 'good', //default inventory type for the dropdown
    
    pricePer: 'hour', //default pricePer value for the dropdown
    publicVisible: true,
    
    inventoryCategory: function() {
        return this.get('inventoryCategories').get('firstObject');
    }.property('inventoryCategories'),
    
    //function to reset all of the values in the modal when loaded...
    reset: function() {
        this.setProperties({
            name: '',
            price: '',
            newCategory: '',
            description: '',
            count: '',
            alertAt: ''
        });
    },
    
    actions: {
        updatePublicVisible: function(value) {
            this.set('publicVisible', value);
        },
        
        changeType: function(value) {
            console.log(value);
            this.set('type', value);
        },
        
        changeInventoryCategory: function(value, object) {
            console.log(value);
            this.set('inventoryCategory', object);  
        }
    }
});;App.InventoryBase = DS.Model.extend({
    //Name	Catagory ID	Description	SKU #	Price	Type	Count
    name:           attr('string'), // Name of item.
    description:    attr('string'), // Description of item.
    sku:            attr('string'), // ---still debatable---
    price:          attr('number'), // Price that a customer will pay at purchase.
    type:           attr('string'), // One of three; 'asset' 'rental' 'good'
    pricePer:       attr('string'), // One of two; 'hour' 'day'
    count:          attr('number'), // Quantity on hand of item.
    alertAt:        attr('number'), // Quantity of stock at which an aller will be displayed for a 'good'.
    publicVisible:  attr('number'), // Whether or not the the item should be show to the frontend or not
    created_at:     attr('date'),   // Date item was added to database, date created.
    updated_at:     attr('date'),   // Date item was last updated, date modified/updated.
    file:           belongsTo('file', { async: true }),
    /*************************************************************
    * Logic for getting an inventory quantity. If type is a 'good'
    * then grab count for that imtem, else look for quantity of
    * instantces.
    *************************************************************/
    //COMPUTED PROPERTIES FOR SORTING AND DISPLAYING SUB-ITEM DATA
    
    itemCount: function() {
        
        if(this.get('type') === 'good') {
            return this.get('count');
        } else {
            return this.get('inventoryInstance.length');
        }
        
    }.property('type', 'count', 'inventoryInstance.length'),
    
    alertClass: function() {
        
        var dangerCount = isNaN(parseInt(this.get('alertAt'))) ? 0 : parseInt(this.get('alertAt'));
        var warningCount = dangerCount == 0 ? 3 : dangerCount * 2;
        var itemCount = parseInt(this.get('itemCount'));
        
        if(dangerCount >= itemCount) {
            return 'label-danger';
        } else if(warningCount >= itemCount) {
            return 'label-warning';
        } else {
            return 'label-success';
        }
        
    }.property('alertAt', 'itemCount')
});

App.Inventory = App.InventoryBase.extend({

    //RELATIONSHIPS
    inventoryCategory: belongsTo('inventory-category', { async: true }),
    inventoryInstance: hasMany('inventory-instance', { async: true })
    
});


//INVENTORY
App.InventoryStatus = DS.Model.extend({
    status: attr('string'),
    inventoryInstance: hasMany('inventory-instance', {async: true}),
    created_at: attr('date'),
    updated_at: attr('date')
});


App.InventoryCondition = DS.Model.extend({
    condition: attr('string'),
    inventoryInstance: hasMany('inventory-instance', {async: true}),
    created_at: attr('date'),
    updated_at: attr('date')
});

App.InventoryCategoryBase = DS.Model.extend({
    category: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date')
});

App.InventoryCategory = App.InventoryCategoryBase.extend({
    inventory: hasMany('inventory', {async: true})
});


App.InventoryInstance = DS.Model.extend({
    publicVisible:  attr('number'), // Whether or not the the item should be show to the frontend or not
    inventory: belongsTo('inventory', { async: true }),
    inventoryStatus: belongsTo('inventory-status', { async: true }),
    inventoryCondition: belongsTo('inventory-condition', { async: true }),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    serial: function() {
        return this.get('inventory.id') + '-' + this.get('id');
    }.property('inventory.id', 'id')
});;App.AdminInventoryRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('inventory');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
    }
});

App.AdminInventoryItemRoute = Ember.Route.extend({

    model: function(params) {
        return this.store.find('inventory', params.inventory_id);
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        controller.set('currentCategory', model.get('inventoryCategory'));
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
        
    },
    
    actions: {
        updateInventory: function() {
            
            var self = this;
            var controller = this.get('controller');
            var item = controller.get('model');
            
            console.log(item);
            
            if(controller.get('newCategory')) {
                
                var newCategory = this.store.createRecord('inventoryCategory', { category: controller.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    
                    item.set('inventoryCategory', category);
                    item.save().then(function(results) {
                        self.set('newCategory', ''); //clear the new category value now that it's been added to the dropdown
                        console.log('update was successful');
                    }, function(results) {
                        item.rollback(); //save didn't work...don't persist any of the changes to ember
                    });
                    
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
                
            } else {
                
                item.set('inventoryCategory', controller.get('inventoryCategory'));    
                item.save().then(function(results) {
                    console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
            }
            
        },
       
       
        addInstances: function() {
            var controller = this.get('controller');
            var instanceCount = controller.get('instanceCount');
            for(var i = 0; i < instanceCount; i++) {
                var newInstance = this.store.createRecord('inventoryInstance', { inventory: controller.get('model') });
                newInstance.save().then(function(results) {
                    console.log(results);
                }, function(results) {
                    console.log(results);
                    newInstance.destroyRecord();
                });
            }
        },
       
       
       deleteInstance: function(instance) {
           var confirmDelete = confirm("Are you sure you want to delete this sub-item?");
           if (confirmDelete) {
               instance.deleteRecord();
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                    }, function(results) {
                            instance.rollback();
                    });
                }
            }
       },
       
        backToInventory: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteInventory: function() {
            
            var controller = this.get('controller');
            console.log('delete inventory...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this inventory item?");
            if (confirmDelete) {
                this.send('backToInventory', function() {
                    var record = controller.get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.replaceWith('admin.inventory');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;
            var controller = this.get('controller');
            
            this.send('backToInventory', function() {
                controller.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionTo('admin.inventory');
            });
        }
    }
    
});

App.AdminInventoryCreateRoute = Ember.Route.extend({
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        
        this.controllerFor('media');
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('inventoryCategories').set('model', this.store.find('inventoryCategory'));
        
        controller.reset(); //reset the values when called
    },
    
    actions: {
        saveItem: function() {
            
            var self = this;
            var controller = this.get('controller');
            //grab all relavent data for creating a new inventory item
            var data = controller.getProperties('type', 
                                                'name',
                                                'price',
                                                'description',
                                                'publicVisible',
                                                'count',
                                                'alertAt',
                                                'file');
            
            console.log(this.get('inventoryCategory'));
            
            if(data.type === 'rental') {
                data.pricePer = this.get('pricePer');
            }
            
            //create a new category or grab the existing one based on the categoryId
            if(this.get('newCategory')) {
                console.log('new category');
                var newCategory = this.store.createRecord('inventoryCategory', { category: controller.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    data.inventoryCategory = category;
                    self.send('saveItemWithData', data);
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
            } else {
                if(!this.get('inventoryCategory')) {
                    data.inventoryCategory = controller.get('inventoryCategories').get('firstObject');
                } else {
                    data.inventoryCategory = controller.get('inventoryCategory');
                }
                
                self.send('saveItemWithData', data);
            }
        },
        
        saveItemWithData: function(data) {
            var self = this;
            var item = this.store.createRecord('inventory', data);
            var controller = this.get('controller');
            
            item.save().then(function(item) {
                //now attach the item to the category
                controller.trigger('modalActionDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('inventory saved unsuccessfully');
                console.log(results);
            });
        }
    }
    
});;App.AdminInventoryItemView = App.SlideoutView.extend();

App.AdminInventoryCreateView = App.ModalView.extend();;App.AdminInventoryOrdersController = Ember.ArrayController.extend({

});



App.AdminInventoryOrderController = Ember.ObjectController.extend({

});



App.AdminInventoryOrderItemsController = Ember.ObjectController.extend({

});



App.AdminInventoryOrderCreateController = App.AdminInventoryOrderController.extend(Ember.Evented, {

});;App.InventoryOrder = DS.Model.extend({   
    
    po_number:      attr('string'), //
    shipping_cost:  attr('string'), //
    total_cost:     attr('string'), //
    ordered_at:     attr('date'),   //
    fulfilled_at:   attr('date'),   //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   //
    notes:          attr('string'), //
    
    //Relationships
    vendor:             belongsTo('inventory-order-vendor', { async: true }),
    order_status:       belongsTo('inventory-order-status', { async: true }),
    ordered_by:         belongsTo('user', { async: true }),
    entered_by:         belongsTo('user', { async: true }),
    last_modified_by:   belongsTo('user', { async: true }),
    
    inventoryOrderList: hasMany('inventory-order-item', { async: true }),

});


App.InventoryOrderStatus = DS.Model.extend({  
    
    created_at: attr('date'),   //
    updated_at: attr('date'),   //
    status:     attr('string'),
    
    //Relationships
    
});


App.InventoryOrderVendor = DS.Model.extend({  
    
    vendor_name:    attr('string'), //
    rep_name:       attr('string'), //
    phone:          attr('string'), //
    ext:            attr('string'), //
    account_number: attr('string'), //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   //
    
    //Relationships
    
});


App.InventoryOrderItem = DS.Model.extend({  
    
    item_cost:      attr('string'), //
    qty_ordered:    attr('string'), //
    qty_received:   attr('string'), //
    created_at:     attr('date'),   //
    updated_at:     attr('date'),   // 
    
    //Relationships
    inventory_id:   belongsTo('inventory', { async: true }), //
    order_id:       belongsTo('inventory-order', { async: true }), //
    intended_for:   belongsTo('user', { async: true }), //
    vendor_id:      belongsTo('inventory-order-vendor', { async: true }), //
    
    //Computed property
    orderItemReceived: function() {
        var isEntered = false;
        var itemCount = (this.get('qty_received'));

        
        if ( itemCount == '')
        {
            isEntered = false;
            
        } else {isEntered = true;}
        
        return isEntered;
        
    }.property('qty_received'),

});


;App.AdminInventoryOrdersRoute = Ember.Route.extend({
        
    model: function() {
        // the model will be pulled from the order model in models.js file
        return this.store.find('inventoryOrder'); 
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});


App.AdminInventoryOrderRoute = Ember.Route.extend({
    
    model: function(params) {

    // filter array inventory, by publicVisable set to 1(true) 
        return this.store.find('inventoryOrder',  params.inventory_orders_id );  
    },
    
    setupController: function(controller, model) {
                
        controller.set('model', model);
//        controller.set('inventoryOrderItems', this.store.find('inventoryOrderItem'));
  
        
    }
});


App.AdminInventoryOrderItemsRoute = Ember.Route.extend({
    
    
    model: function(params) {
        return this.store.find('inventoryOrderItem', params.inventoryOrder_id );
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
});

;App.AdminRestockOrderView = Ember.View.extend({
    
    templateName: 'admin/restock/order',
    
    didInsertElement: function() {
        console.log('in thing stuff');
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
    
});

App.AdminRestockCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/restock/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.restock');
        });
        
        this.get('controller').on('addItemDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addItemDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});;App.AdminMediaItemController = Ember.ObjectController.extend({
    itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
    }.property('model.currentState.stateName'),
});;App.File = DS.Model.extend({
    name: attr('string'),
    title: attr('string'),
    src: attr('string'),
    description: attr('string'),
    width: attr('string'),
    height: attr('string'),
    size: attr('string'),
    thumbnailSrc: attr('string'),
    extension: attr('string'),
    mime: attr('string'),
    altText: attr('string'),
    description: attr('string'),
    isImage: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //computed properties
    absoluteSrc: function() {
        return rootURL + '/' + this.get('src');
    }.property('src'),
    
    backgroundSrc: function() {
        return 'url(' + rootURL + '/' + this.get('src') + ') center center';
    }.property('src'),
    
    absoluteThumbnailSrc: function() {
        return rootURL + '/' + this.get('thumbnailSrc');
    }.property('thumbnailSrc'),
    
    backgroundThumbnailSrc: function() {
        return 'url(' + rootURL + '/' + this.get('thumbnailSrc') + ') center center';
    }.property('thumbnailSrc')
});;App.AdminMediaRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('file');
    },
    
    actions: {
        
        deleteMedia: function(item) {
            
            var confirmDelete = confirm('Are you sure you want to delete this file?');
            
            if(confirmDelete) {
                item.deleteRecord();
                if(item.get('isDeleted')) {
                    item.save().then(function(results) {
                        //item deleted successfully. no action necessary
                    }, function(results) {
                        item.rollback();
                    });
                }
            }
            
        },
        
        deleteSlideshow: function(item) {
            
            var confirmDelete = confirm('Are you sure you want to delete this slideshow?');
            
            if(confirmDelete) {
                item.deleteRecord();
                if(item.get('isDeleted')) {
                    item.save().then(function(results) {
    //                            console.log(results);
                    }, function(results) {
                        item.rollback();
                    });
                }
            }
        }
        
    }
    
});

App.AdminMediaItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('file', params.media_id);
    },
    
    actions: {
    
        deleteItem: function() {
            
            var self = this;
            
            confirmDelete = confirm('Are you sure you want to delete this file?');
            
            if(confirmDelete) {
                this.send('backToMedia', function() {
                    var item = self.currentModel; //delete the inventory record
                    item.deleteRecord();
                    if(item.get('isDeleted')) {
                        item.save().then(function(results) {
                            self.transitionTo('admin.media');
                        }, function(results) {
                            item.rollback();
                        });
                    }
                });
            }
        },
        
        saveItem: function() {
            
            var self = this;
            
            var item = this.currentModel;
            item.save().then(function(results) {
                console.log('update was successful');
            }, function(results) {
                console.log(results);
                item.rollback();
            });
            
        },
        
        backToMedia: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        doneEditing: function() {
            var self = this;
            
            this.send('backToMedia', function() {
                if(self.currentModel.get('isDirty')) {
                    self.currentModel.rollback(); //undo anything that hasn't been saved?
                }
                self.transitionTo('admin.media');
            });
        }
        
    }
    
});

App.AdminMediaCreateslideshowRoute = Ember.Route.extend({
    actions: {
        saveSlideshow: function() {
            var data = this.get('controller').getProperties('title');
            var item = this.store.createRecord('slideshow', data);
            item.save().then(function() {}, function(results) {
                item.rollback();
            });
        }
    }
});

App.AdminMediaSlideshowRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('slideshow', params.slideshow_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('media', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var data = {};
            var controller = this.get('controller');
            
            data.file = controller.get('newSlide');
            data.title = controller.get('newSlideTitle');
            data.caption = controller.get('newSlideCaption');
            data.slideshow = controller.get('model');;
            
            var item = this.store.createRecord('slide', data);
            
            item.save().then(function(results) {
                //item saved successfully
                console.log(results);
            }, function(results) {
                item.destroyRecord(); //backpedal
            });
        }
    }
});;App.AdminMediaUploadView = Ember.View.extend({
    
    didInsertElement: function() {
        
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.media');
        });
    },
    
    actions: {
        
        closeModal: function() {
            Ember.$('#myModal').modal('hide');
        }
        
    }
    
});

App.AdminMediaItemView = Ember.View.extend({
    
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
    
});;;//needs to be defined later. after the application controller is defined
App.VerifyAccessRoute = Ember.Route.extend({
    
    permissions: [],
    
    userHasPermissions: function() {
        var requiredPermissions = this.get('permissions');
        var currentUser = this.controllerFor('application').get('currentUser');
        
        if(typeof currentUser.permissions !== 'undefined') {
            
            var userPermissions = currentUser.permissions;
            
            return requiredPermissions.every(function(requiredPermission) {
                return userPermissions.some(function(userPermission) {
                    return userPermission === requiredPermission;
                });
            });
            
        }
        
        return false;
    },
    
    beforeModel: function(transition) {
        
        if(this.userHasPermissions()) {
            Ember.Logger.debug('user has valid permissions');
            return true;
        } else {
            
            var loginController = this.generateController('login');
            loginController.set('attemptedTransition', transition);
            
            $.growl.error({ title: "Error", message: "Redirect to home due to lack of permissions to that URL" });
            Ember.Logger.debug('user does not have valid permissions');
            
            this.transitionTo('login');
//            return false;
        }
    },
    
});

App.AdminRoute = App.VerifyAccessRoute.extend({
    
    permissions: ['view_admin'], //use some basic abilities needed for ALL admin routes
    
    activate: function() {
        this.controllerFor('application').set('isAdminRoute', true);
    },
    
    deactivate: function() {
        this.controllerFor('application').set('isAdminRoute', false);
    },
    
    actions: {
    
        openFrontend: function() {
            this.send('closeFrontend');
            
            window.frontendSide = window.open(rootURL);
        },
        
        closeFrontend: function() {
            if(typeof window.frontendSide !== 'undefined') {
                console.log('the frontend side has already been opened');
                
                if(typeof window.frontendSide.close === 'function') {
                    window.frontendSide.close();
                }
            } else {
                
                if(!Ember.isEmpty(window.opener)) {
                    console.log('the frontend side opened the admin side');
                    window.frontendSide = window.opener;
                    window.frontendSide.close();
                }
            }
        },
        
        adminLogout: function() {
            
            this.send('closeFrontend');
            
            window.location = rootURL + 'api-proxy/logout'; //now that we've closed the children window we can logout
            
        },
        
        //simply slides the admin menu in and out
        toggleAdminMenu: function() {
            $("#wrapper").toggleClass("toggled");
        }
    
    },
	
	renderTemplate: function() {
		this._super();
    	this.render({outlet: 'admin'});
  	}
});;App.AdminServicesController = Ember.ArrayController.extend({

    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['search'],
    search: null,
    searchQuery: null,

    filteredServices: function() {
            
            var search = this.get('search');
            var searchableProperties = ['name', 'description'];
            var items = this.get('arrangedContent');  // creats array of models to be filtered.
            
            if(search) {
                items = items.filter(function(item) {
                    var trueCount = 0;
                    
                    for(var i = 0; i < searchableProperties.length; i++) {
                        console.log(item.get(searchableProperties[i]));
                        if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                            trueCount++;
                        }
                    }
                
                    return trueCount > 0;
                });
            }
            
            return items;
        }.property('search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.

    actions: {

        search: function() {
            this.set('search', this.get('searchQuery'));
        },

        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
    }

});

App.AdminServicesServiceController = Ember.ObjectController.extend({

    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],

    actions: {

        backToServices: function(callback) 
        {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteServices: function() 
        {
            console.log('delete services...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this service?");
            if (confirmDelete) {
                this.send('backToServices', function() {
                    var record = self.get('model'); //delete the services record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.services');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() 
        {
            var self = this;
            
            this.send('backToServices', function() 
            {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.services');
            });
        },

        updateServices: function() 
        {        
            var self = this;
            var item = this.get('model');

            item.save().then(function(results) {
                console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
        }
    }
});

App.AdminServicesCreateController = App.AdminServicesServiceController.extend(Ember.Evented, {
    reset: function() {
        this.setProperties({
            name: '',
            price: '',
            description: '',
            hours: ''
        });
    },

    publicVisibility:  [{value: 0, label: 'Private'}, {value: 1, label: 'Public'}],

    actions: {
        saveService: function() {
            
            var self = this;
            //grab all relavent data for creating a new service
            var data = this.getProperties('name',
                                          'price',
                                          'description',
                                          'hours',
                                          'publicVisible');
                
            self.send('saveServiceWithData', data);
        },
        
        saveServiceWithData: function(data) {
            var self = this;
            var item = this.store.createRecord('service', data);
            
            item.save().then(function(item) {
                //now attach the item to the category
                self.trigger('addServiceDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('inventory saved unsuccessfully');
                console.log(results);
            });
        }
    }  

});

App.AdminServicesSettingsController = Ember.ObjectController.extend({

    reserveability:  [{value: 0, label: 'In Person Only'}, {value: 1, label: 'Online Reservable'}],

    actions: {
        saveServiceSettings: function() {
            
            var self = this;
            var item = this.get('model');
            item.save().then(function(response) { console.log(response); }, function(response) { console.log(response); });
            //grab all relavent data for creating a new service
            // var data = this.getProperties('availableServices',
            //                               'canReserve');
                
             self.send('saveServiceSettingsWithData', data);
        },

        saveServiceSettingsWithData: function(data) {
            var self = this;
            var item = this.store.updateRecord('service_settings', data);
            
            item.save().then(function(item) {
                //now attach the item to the category
                self.trigger('saveServiceSettingsDidSucceed');
//                console.log('inventory saved successfully');
//                console.log(results);
            }, function(results) {
                //TODO: this is where we want to highlight crap
                item.destroyRecord(); //failed. therefore we want to delete it from the store
//                console.log('settings saved unsuccessfully');
                console.log(results);
            });
        }
    }
});

App.AdminServicesRequestedController = Ember.ArrayController.extend({

    needs: ['cartItem'],
// will filter based on if a service and paid
    filteredRequestedServices: function() {

    }

});        ;App.AdminServicesRoute = Ember.Route.extend({

    model: function() {
        return this.store.find('service');
    }

});

App.AdminServicesServiceRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('service', params.services_id);
    },
    
    setupController: function(controller, model) {
        
        var self = this;
        
        controller.set('model', model);      
    }
    
});

App.AdminServicesCreateRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        controller.reset(); //reset the values when called
    }
    
});

App.AdminServicesSettingsRoute = Ember.Route.extend({

    model: function() {
        return this.store.find('servicesSetting', 1);
    },

    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});;App.AdminServicesServiceView = Ember.View.extend(Ember.Evented, {

	templateName:'admin/services/service',

	didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }

});

App.AdminServicesCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/services/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.services');
        });
        
        this.get('controller').on('addServiceDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addServiceDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AdminServicesSettingsView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/services/settings',

    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.services');
        });
        this.get('controller').on('saveServiceSettingsDidSucceed', function() {
            self.send('closeModal');
        });    
    },

    willClearRender: function() {
        this.get('controller').off('saveServiceSettingsDidSucceed', function() {
            self.send('closeModal');
        });
    },

    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }

});    ;App.AdminSettingsController = Ember.ArrayController.extend({
    
    needs: ['media', 'pages'],
    media: Ember.computed.alias('controllers.media'),
    pages: Ember.computed.alias('controllers.pages'),
    
    //GRAB EACH SETTING TO BE MODIFIED
    
    //General
    coreSiteName: function() { return this.get('model').filterBy('name', 'core_site_name').get('firstObject') }.property('model'),
    coreSiteLogo: function() { return this.get('model').filterBy('name', 'core_site_logo').get('firstObject') }.property('model'),
    
    //Contact Information
    corePhone: function() { return this.get('model').filterBy('name', 'core_phone').get('firstObject') }.property('model'),
    coreFax: function() { return this.get('model').filterBy('name', 'core_fax').get('firstObject') }.property('model'),
    coreEmail: function() { return this.get('model').filterBy('name', 'core_email').get('firstObject') }.property('model'),
    coreAddress: function() { return this.get('model').filterBy('name', 'core_address').get('firstObject') }.property('model'),
    
    //Hours of Operation
    coreHoursMonday: function() { return this.get('model').filterBy('name', 'core_hours_monday').get('firstObject') }.property('model'),
    coreHoursTuesday: function() { return this.get('model').filterBy('name', 'core_hours_tuesday').get('firstObject') }.property('model'),
    coreHoursWednesday: function() { return this.get('model').filterBy('name', 'core_hours_wednesday').get('firstObject') }.property('model'),
    coreHoursThursday: function() { return this.get('model').filterBy('name', 'core_hours_thursday').get('firstObject') }.property('model'),
    coreHoursFriday: function() { return this.get('model').filterBy('name', 'core_hours_friday').get('firstObject') }.property('model'),
    coreHoursSaturday: function() { return this.get('model').filterBy('name', 'core_hours_saturday').get('firstObject') }.property('model'),
    coreHoursSunday: function() { return this.get('model').filterBy('name', 'core_hours_sunday').get('firstObject') }.property('model'),
    
    //Payment Gateway
    corePublishableKey: function() { return this.get('model').filterBy('name', 'core_publishable_key').get('firstObject') }.property('model'),
    coreSecretKey: function() { return this.get('model').filterBy('name', 'core_secret_key').get('firstObject') }.property('model'),
    
    //Google API
    coreGoogleApiKey: function() { return this.get('model').filterBy('name', 'core_google_api_key').get('firstObject') }.property('model'),
    
    //FONTEND
    coreFrontendIsActive: function() { return this.get('model').filterBy('name', 'core_frontend_is_active').get('firstObject') }.property('model'),
    
    actions: {
        toggleActiveFrontend: function(value, object) {
            object.set('number_value', value).save().then(function() {}, function() { object.rollback(); });
        },
        
        toggleActivePage: function(value, object) {
            object.set('is_active', value).save().then(function() {}, function() { object.rollback(); }); 
        }
    }
    
});

App.AdminSettingsPageController = Ember.ObjectController.extend(Ember.Evented, {

});;App.Setting = DS.Model.extend({
    name: attr('string'),
    string_value: attr('string'),
    number_value: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date')
});;App.AdminSettingsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('setting');  
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
        this.controllerFor('pages').set('model', this.store.find('page'));
    },
    
    actions: {
        updateGeneral: function() {
            var controller = this.get('controller');
            
            var data = controller.getProperties('coreSiteName');
            
            this.send('saveSettings', data);
            
        },
        
        updateContact: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('corePhone',
                                                'coreFax',
                                                'coreEmail',
                                                'coreAddress');
            
            this.send('saveSettings', data);
        },
        
        updateHours: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('coreHoursMonday',
                                                'coreHoursTuesday',
                                                'coreHoursWednesday',
                                                'coreHoursThursday',
                                                'coreHoursFriday',
                                                'coreHoursSaturday',
                                                'coreHoursSunday');
            
            this.send('saveSettings', data);
        },
        
        updatePayment: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('corePublishableKey',
                                                'coreSecretKey');
            
            this.send('saveSettings', data);
        },
        
        updateGoogle: function() {
            var  controller = this.get('controller');
            var data = controller.getProperties('coreGoogleApiKey');
            
            this.send('saveSettings', data);
        },
        
        saveSettings: function(data) {
            for(var datum in data) {
                //data[datum] should be model instances
                data[datum].save().then(function() {}, function(response) {
                    data[datum].rollback();
                });
            }
            
            if(typeof window.frontendSide !== 'undefined') {
                this.send('closeFrontend'); //we want to close it so that it can be refreshed properly...
            }
        }
    }
    
});;App.AdminSettingsView = Ember.View.extend({
    
    didInsertElement: function() {
        
        $masonryContainer = Ember.$('.masonry-settings');
        $masonryContainer.imagesLoaded(function() {
            $masonryContainer.masonry({
              columnWidth: '.item',
              itemSelector: '.item'
            });
        });
    },
    
    willClearRender: function() {
        
    }
    
});

App.AdminSettingsPageView = App.ModalView.extend({ routeOnClose: 'admin.settings' });;App.AdminSlideshowsCreateController = Ember.ObjectController.extend(Ember.Evented, {

});

App.AdminSlideshowsSlideshowController = Ember.ObjectController.extend({
    
   needs: ['pages'],
   allPages: Ember.computed.alias('controllers.pages'),
   pagesToSync: null,
    
   itemState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'item ' + state[2];
   }.property('model.currentState.stateName'),
    
//   pagesToSync: function() {
//        return this.get('model.pages.content.content');
//   }.property('model.pages.content.content'),
    
   syncPages: function() {
       
       var pagesToSync = this.get('pagesToSync');
       var allPages = this.get('allPages');
       var slideshow = this.get('model');
       
       allPages.forEach(function(page) {
            if(pagesToSync.some(function(pageToSync) {
                return page.get('id') === pageToSync.get('id');
            })) {
                console.log('set ' + page.get('title') + ' to HAVE a slideshow');
                page.set('slideshow', slideshow).save();
            } else if(!Ember.isEmpty(page.get('slideshow')) && page.get('slideshow').get('id') === slideshow.get('id')) {
                console.log('set ' + page.get('title') + ' to NOT HAVE a slideshow');
                page.set('slideshow', null).save();
            }
            
       });
       
   }.observes('pagesToSync')
    
});

App.AdminSlideshowsSlideshowCreateslideController = Ember.ObjectController.extend(Ember.Evented, {
    needs: ['media'],
    media: Ember.computed.alias('controllers.media')
});

App.AdminSlideshowsSlideshowSlideController  = Ember.ObjectController.extend(Ember.Evented, {
    needs: ['media'],
    media: Ember.computed.alias('controllers.media')
});;App.Slideshow = DS.Model.extend({
    title: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),    
    
    //relationship
    slides: hasMany('slide', { async: true }),
    pages: hasMany('page', { async: true }),
    
    //computed properties
    collapseHeadingId: function() {
        return 'collapse-heading-' + this.get('id');
    }.property('id'),
    
    collapseId: function() {
        return 'collapse-' + this.get('id');
    }.property('id'),
    
    hashedId: function() {
        return '#' + this.get('collapseId');
    }.property('collapsedId')
});


App.Slide = DS.Model.extend({
    title: attr('string'),
    caption: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    slideshow: belongsTo('slideshow', { async: true }),
    file: belongsTo('file', { async: true })
});;App.AdminSlideshowsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('slideshow');
    } 
});

App.AdminSlideshowsCreateRoute = Ember.Route.extend({
    actions: {
        saveSlideshow: function() {
            var controller = this.get('controller');
            var data = controller.getProperties('title');
            
            var item = this.store.createRecord('slideshow', data);
            item.save().then(function() { 
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});

App.AdminSlideshowsSlideshowRoute = Ember.Route.extend(Ember.Evented, {
    model: function(params) {
        return this.store.find('slideshow', params.slideshow_id);
    },
    
    setupController: function(controller, model) {
        
        controller.set('model', model);
        controller.set('pagesToSync', model.get('pages.content.content'));
        
        //dependencies
        this.controllerFor('pages').set('model', this.store.find('page'));
    },
    
    actions: {
        
        addPage: function(value) {
            value.set('slideshow', this.get('controller').get('model')).save();
        },
        
        backToSlideshow: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteSlideshow: function() {
            console.log('delete slideshow...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this slideshow?");
            if (confirmDelete) {
                this.send('backToSlideshow', function() {
                    var record = self.get('controller').get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionTo('admin.slideshows');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        updateSlideshow: function() {
            
            var self = this;
            var item = this.get('controller').get('model');
            
            item.save().then(function(results) { 
                console.log('update was successful');
            }, function(results) {
                item.rollback(); //save didn't work...don't persist any of the changes to ember
            });
            
        },
       
       
       deleteSlide: function(instance) {
           var confirmDelete = confirm("Are you sure you want to delete this slide?");
           if (confirmDelete) {
               
               var self = this;
               
               instance.deleteRecord();
               
                if(instance.get('isDeleted')) {
                    instance.save().then(function(results) {
//                            console.log(results);
                        Ember.$.growl({ title: 'Note', message: 'Refresh your page or reopen the slideshow to reload the slideshow' });
//                        window.location.reload(); //hard refresh. TODO: make this not such an ugly solution
                    }, function(results) {
                            instance.rollback();
                    });
                }
            }
       },
        
        doneEditing: function() {
            var self = this;
            var item = this.get('controller').get('model');
            
            this.set('photoPicked', null);
            
            this.send('backToSlideshow', function() {
                item.rollback(); //undo anything that hasn't been saved
                self.transitionTo('admin.slideshows');
            });
        }
        
    }
});

App.AdminSlideshowsSlideshowCreateslideRoute = Ember.Route.extend({
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var controller = this.get('controller');
            var slideshow = this.modelFor('adminSlideshowsSlideshow');
            var data = {
                slideshow: slideshow,
                title: controller.get('newSlideTitle'),
                caption: controller.get('newSlideCaption'),
                file: controller.get('newSlide')
            };
            
            var item = this.store.createRecord('slide', data);
            item.save().then(function() {
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});

App.AdminSlideshowsSlideshowSlideRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('slide', params.slide_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('media').set('model', this.store.find('file'));
    },
    
    actions: {
        saveItem: function() {
            var controller = this.get('controller');
            var item = controller.get('model');
            
            item.save().then(function() {
                controller.trigger('modalActionDidSucceed');
            }, function() {
                item.rollback();
            });
        }
    }
});;App.AdminSlideshowsCreateView = App.ModalView.extend({
    
    routeOnClose: 'admin.slideshows'
    
});

App.AdminSlideshowsSlideshowCreateslideView = App.ModalView.extend({
    
    routeOnClose: 'admin.slideshows.slideshow'
    
});

App.AdminSlideshowsSlideshowSlideView = App.ModalView.extend({

    routeOnClose: 'admin.slideshows.slideshow'
    
});

App.AdminSlideshowsSlideshowView = App.SlideoutView.extend();;App.AdminUsersController = Ember.ArrayController.extend({
	
	needs: ['userRoles'],
    userRoles: Ember.computed.alias('controllers.userRoles'),
	
    sortProperties: ['name', 'created_at'], //default
    sortAscending: true,
    queryParams: ['role', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    role: null,                 // this is blank variable will store type to filter by
    search: null,
    searchQuery: null,
    
    filteredResults: function() {
        
        var role = this.get('role');    // retrieves type variable to filter by
        var search = this.get('search');
        var searchableProperties = ['name', 'username'];
        var users = this.get('arrangedContent');  // creats array of models to be filtered.
        
        if (role) {
			console.log(users);
			
			users = users.filter(function(user) {
				return user.get('role.id') === role;
			});
			
        }
        
        if(search) {
            users = users.filter(function(user) { 
				
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(user.get(searchableProperties[i]));
                    if(user.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return users;
    }.property('role', 'search' , 'arrangedContent'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        
        sortBy: function(property) {
            var currentProperties = this.get('sortProperties');
            this.set('sortProperties', [property, 'created_at']);
            
            if(property == currentProperties[0]) {
                this.set('sortAscending', !this.get('sortAscending'));
            }
        }
        
    },
    
    

});

App.AdminUsersUserController = Ember.ObjectController.extend({
    
   needs: ['adminUsers'],
   userRoles: Ember.computed.alias('controllers.adminUsers.userRoles'),
    
   pricePerUnits: ['hour', 'day'],
    
   publicVisibility:  [{value: 1, label: 'Public'}, {value: 0, label: 'Private'}], // Whether or not the the item should be show to the frontend or not
   isItemGood: function() {
        return this.get('model.type') === 'good';
   }.property('model.type'),
    
   isItemRental: function() {
        return this.get('model.type') === 'rental';
   }.property('model.type'),
    
   isItemAsset: function() {
        return this.get('model.type') === 'asset';
   }.property('model.type'),
    
   userState: function() {
        var state = this.get('model.currentState.stateName').split('.');
        return 'user ' + state[2];
   }.property('model.currentState.stateName'),
    
   actions: {
        updateUser: function() {
            
            var self = this;
            var item = this.get('model');
            
            if(this.get('newCategory')) {
                
                var newCategory = this.store.createRecord('inventoryCategory', { category: this.get('newCategory') });
                newCategory.save().then(function(category) {
                    console.log('new category saved successfully');
                    
                    item.set('inventoryCategory', category);
                    item.save().then(function(results) {
                        self.set('newCategory', ''); //clear the new category value now that it's been added to the dropdown
                        console.log('update was successful');
                    }, function(results) {
                        item.rollback(); //save didn't work...don't persist any of the changes to ember
                    });
                    
                }, function(results) {
                    console.log('new category saved unsuccessfully');
                    console.log(results);
                });
                
            } else {
                
                item.set('inventoryCategory', this.get('inventoryCategory.content'));    
                item.save().then(function(results) {
                    console.log('update was successful');
                }, function(results) {
                    item.rollback(); //save didn't work...don't persist any of the changes to ember
                });   
            }
            
        },
       
        backToUsers: function(callback) {
            Ember.$('.slideout').velocity({'opacity': 0}, 100);
            Ember.$('.slideout-content').velocity({ right: '-500px' }, callback, 200);
        },
        
        deleteUser: function() {
            console.log('delete inventory...');
            var self = this;
            var confirmDelete = confirm("Are you sure you want to delete this inventory item?");
            if (confirmDelete) {
                this.send('backToUsers', function() {
                    var record = self.get('model'); //delete the inventory record
                    record.deleteRecord();
                    if(record.get('isDeleted')) {
                        record.save().then(function(results) {
                            self.transitionToRoute('admin.users');
                        }, function(results) {
                            record.rollback();
                        });
                    }
                });
            }
        },
        
        doneEditing: function() {
            var self = this;
            
            this.send('backToUsers', function() {
                self.get('model').rollback(); //undo anything that hasn't been saved
                self.transitionToRoute('admin.users');
            });
        }
   }
});

App.AdminUsersPermissionsController = Ember.ArrayController.extend({
    
    needs: ['adminUsers'],
    userRoles: Ember.computed.alias('controllers.adminUsers.userRoles'),
    
	rolesPermissionsMap: function() {
        
		var allPermissions = this.get('model');
		var allRoles = this.get('userRoles');
        
        console.log(allRoles);
        
		var map = Ember.A();
		
		allPermissions.forEach(function(permission) {
			
			var permissionMap = {};
			permissionMap.permission_name = permission.get('display_name');
			permissionMap.permission_map = Ember.A();
            
			allRoles.forEach(function(role) {
                
				var permission_map = {};
				
				permission_map.disabled = role.get('name') === 'Super Admin';
				permission_map.nameAttr = "roles[" + role.get('id') + "][" + permission.get('id') + "]";
				permission_map.checked = role.get('perms').some(function(rolePermission) {
					return rolePermission.get('id') === permission.get('id');	
				});
				
				permissionMap.permission_map.push(permission_map);
				
			});
			
			map.push(permissionMap);

		});
        
		return map;
		
	}.property('userRoles.@each'),
	
	actions: {
		
		updatePermissions: function() {
			
			var permissionsMap = this.get('rolesPermissionsMap');
			
			var data = "";
			
			permissionsMap.forEach(function(permission) {
				permission.permission_map.forEach(function(role) {
					data += role.nameAttr + "=" + ( + role.checked ) + "&";
				});
			});
			
			Ember.$.post(rootURL + 'api-proxy/userPermissions', data).then(function(response) {
				console.log(response);
				console.log('permissions updated successfully');
			}, function(response) {
				console.log(response);
				console.log('failed to update permissions');
			});
		}
		
	}
	
});

App.UserRolesController = Ember.ArrayController.extend();
App.UserPermissionsController = Ember.ArrayController.extend();
App.UserAddressesController = Ember.ArrayController.extend();
App.UserBillablesController = Ember.ArrayController.extend();;App.User = DS.Model.extend({
    //properties
    name: attr('string'),
    username: attr('string'),
    password: attr('string'),
    password_confirmation: attr('string'),
	stripe_customer_id: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    addresses: hasMany('user-address', { async: true }),
	billingInfo: hasMany('user-billing-info', { async: true }),
	role: belongsTo('user-role', { async: true })
});

App.UserAddress = DS.Model.extend({
    //properties
	recipient_name: attr('string'),
    address_line_1: attr('string'),
    address_line_2: attr('string'),
	city: attr('string'),
    state_province: attr('string'),
    zip: attr('string'),
	country: attr('string'),
    created_at: attr('date'),
    updated_at: attr('date'),
    
    //relationships
    user: belongsTo('user', { async: true })
});

App.UserBillingInfo = DS.Model.extend({
	stripe_card_id: attr('string'),
	name: attr('string'),
	last4: attr('number'),
	brand: attr('string'),
	funding: attr('string'),
	exp_month: attr('number'),
	exp_year: attr('number'),
	created_at: attr('date'),
    updated_at: attr('date'),
	
	//TODO: determine if we need to have address info as well...
	
	//relationships
	user: belongsTo('user', { async: true })
});

App.UserRole = DS.Model.extend({
	name: attr('string'),
	created_at: attr('date'),
    updated_at: attr('date'),
	
	//relationships
	users: hasMany('user', { async: true }),
	perms: hasMany('user-permission', { async: true })
});

App.UserPermission = DS.Model.extend({
	name: attr('string'),
	display_name: attr('string'),
	created_at: attr('date'),
    updated_at: attr('date'),
    
	
	//relationships
	roles: hasMany('user-role', { async: true })
});;App.AdminUsersRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('user');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('userRoles').set('model', this.store.find('userRole'));
    }
    
});

App.AdminUsersUserRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('user', params.user_id);
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
    }
    
});

App.AdminUsersPermissionsRoute = Ember.Route.extend({
	
    model: function() {
        return this.store.find('userPermission');
    },
	
	setupController: function(controller, model) {
		controller.set('model', model);
	}
    
});;App.AdminUsersUserView = Ember.View.extend({
    
    templateName: 'admin/users/user',
    
    didInsertElement: function() {
        Ember.$('body').toggleClass('slideout-in');
        Ember.$('.slideout').velocity({'opacity': 1}, 100);
        Ember.$('.slideout-content').velocity({ right: '0px' }, { easing: [ 23, 8 ] });
    },
    
    willDestroyElement: function() {
        Ember.$('body').toggleClass('slideout-in');
    }
    
});

App.AdminUsersPermissionsView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'admin/users/permissions',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('admin.users');
        });
        
//        this.get('controller').on('addItemDidSucceed', function() {
//            self.send('closeModal');
//        });
    },
    
//    willClearRender: function() {
//        this.get('controller').off('addItemDidSucceed', function() {
//            self.send('closeModal');
//        });
//    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});;;App.AccountController = Ember.ObjectController.extend({
	needs: ['userAddresses'],
	
	reset: function() {
		this.setProperties({
			password: '',
			password_confirmation: ''
		});
	},
	
	actions: {
		
		updateUser: function() {
			var self = this;
			var user = this.get('model');
			
			user.save().then(function(results) {
				console.log(results);
				console.log('user info updated successfully');
				self.reset();
			}, function(results) {
				console.log(results);
				user.rollback(); //if  saving didn't work
			});
			
		}
		
	}
});

App.AccountAddressCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addAddress: function() {
		
			var self = this;
			var data = this.getProperties('recipient_name',
										  'address_line_1',
										  'address_line_2',
										  'city',
										  'state_province',
										  'zip',
										  'country');
			
			var userAddress = this.store.createRecord('userAddress', data);
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('addAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.destroyRecord();
			});
		
		}
	
	}
	
});

App.AccountBillinginfoCreateController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		addBillinginfo: function(form) {
			
			var self = this;
			var $form = Ember.$('#form-add-billing-info');
			
			// Disable the submit button to prevent repeated clicks
    		var $submitForm = $form.find('button[type="submit"]').prop('disabled', true);
			
			Stripe.card.createToken($form, function(status, response) {
				  if (response.error) {
					// Show the errors on the form
					$form.find('.payment-errors').text(response.error.message);
					$submitForm.prop('disabled', false);
				  } else {
					// response contains id and card, which contains additional card details
					var token = response.id;
					
					var data = { stripeToken: token };
					Ember.$.post(rootURL + '/api-proxy/userBillingInfos', data).then(function(response) {
						self.store.push('userBillingInfo', response.userBillingInfo); //push to store to render on the view
						self.trigger('addBillingInfoDidSucceed');
					}, function(response) {
						$form.find('.payment-errors').text('error saving card. please try again');
						$submitForm.prop('disabled', false);
						console.log('error saving the new credit card information ' + response);
					});
					  
				  }
				
			});
			
		}
	
	}
	
});

App.AccountAddressEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	actions: {
	
		updateAddress: function() {
		
			var self = this;
			var userAddress = this.get('model');
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateAddressDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.rollback();
			});
		
		}
	
	}
	
});

App.AccountBillinginfoEditController = Ember.ObjectController.extend(Ember.Evented, {
	
	formatted4: function() {
		return '•••• •••• •••• ' + this.get('last4');
	}.property('last4'),
	
	actions: {
	
		updateBillinginfo: function() {
			
			var self = this;
			var billingInfo = this.get('model');
			
			billingInfo.save().then(function(results) {
				//we can close the address
				console.log(results);
				self.trigger('updateBillingInfoDidSucceed');
			}, function(results) {
				console.log(results); //why did it fail?
				billingInfo.rollback();
			});
			
		}
	
	}
	
});;;App.AccountRoute = Ember.Route.extend({
    
    model: function() {
        var self = this;
        
		var currentUser = this.controllerFor('application').get('currentUser');
		
		return this.store.find('user', currentUser.id);
		
//        return Ember.$.get(window.rootURL + 'api-proxy/account').then(function(results) {
//            return results;
//        }, function(results) {
//            self.transitionTo('home'); //TODO: have error message when they try to get there
//        });
    },
    
    actions: {
    
        updateAccount: function() {
            var self = this;
            
            
            
        }
    
    }
    
});

App.AccountAddressEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userAddress', params.address_id);
	}
});

App.AccountBillinginfoEditRoute = Ember.Route.extend({
	model: function(params) {
		return this.store.find('userBillingInfo', params.billing_id);
	}
});;App.AccountBillinginfoCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/billinginfo/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountAddressCreateView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/address/create',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('addAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountBillinginfoEditView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/billinginfo/edit',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateBillingInfoDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});

App.AccountAddressEditView = Ember.View.extend(Ember.Evented, {
    
    templateName: 'account/address/edit',
    
    didInsertElement: function() {
        var self = this;
        
        Ember.$('#myModal').modal('show');
        
        $('#myModal').on('hidden.bs.modal', function (e) {
            self.controller.transitionToRoute('account');
        });
        
        this.get('controller').on('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    willClearRender: function() {
        this.get('controller').off('updateAddressDidSucceed', function() {
            self.send('closeModal');
        });
    },
    
    actions: {
        closeModal: function() {
            console.log('closed modal');
            Ember.$('#myModal').modal('hide');
        }
    }
    
});;App.BlogRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('blog');
    }
});

App.SingleBlogRoute = Ember.Route.extend({
    model: function(params) {
        return this.store.find('blog', params.blog_id );
    }
});;App.CartController = Ember.ArrayController.extend({
    
    resetAddress: function() {
        this.setProperties({
            recipient_name: '',
            address_line_1: '',
            address_line_2: '',
            city: '',
            state_province: '',
            zip: '',
            country: ''
        });
    },
    
    isCheckingOut: false,
    
    totalPrice: function() {
    
        var total = 0;
        
        this.get('model').forEach(function(cart) {
            total += cart.get('price');
        });
        
        return total;
    
    }.property('@each.price', '@each.quantity')
    
});;App.CartItem = DS.Model.extend({
    
    table_name: attr('string'),
    table_id: attr('number'),
    item_name: attr('string'),
    quantity: attr('number'),
    paid: attr('number'),
    price: attr('number'),
    created_at: attr('date'),
    updated_at: attr('date'),
    //relationships
    cart: attr('number')
    
});

//just so we don't break fixtures
App.CartItem.reopenClass({
    FIXTURES: [
        {
            id: 'fixture-1',
            table_name: 'inventory',
            item_name: 'item 1',
            quantity: 1,
            paid: 0,
            price: 50,
            cart: 1
        }
    ]
});;App.CartRoute = Ember.Route.extend({

    model: function() {
    
        return this.store.find('cartItem'); //we are grabbing the cart so we can then pull all of the items and do the realy stuff
    
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        if( !Ember.isEmpty(window.currentUser) ) {
            console.log('currentUser exists');
            controller.set('currentUser', this.store.find('user', window.currentUser.id));
        }
    },
    
    processNewAddress: function() {
        var controller = this.get('controller');
        
        
        
        return true;
    },
    
    processNewCard: function() {
        
    },
    
    actions: {
    
        removeItem: function(item) {
            item.deleteRecord();
            if(item.get('isDeleted')) {
                item.save().then(function(results) {
//                  console.log(results);
                }, function(results) {
                    console.log(results);
                    item.rollback();
                });
            }
        },
        
        anonymousCheckout: function() {
            console.log('checkout for anonymous user');
        },
        
        userCheckout: function() {
            
            var controller = this.get('controller');
            controller.set('isCheckingOut', true);
            
            var processNewAddress = false;
            var processNewCard = false;
            
            //time to process the address
            if( controller.get('recipient_name') ||
                controller.get('address_line_1') || 
                controller.get('address_line_2') || 
                controller.get('city') ||
                controller.get('state_province') ||
                controller.get('zip') ||
                controller.get('country') ) {

                if( !controller.get('recipient_name') ||
                    !controller.get('address_line_1') || 
                    !controller.get('address_line_2') || 
                    !controller.get('city') ||
                    !controller.get('state_province') ||
                    !controller.get('zip') ||
                    !controller.get('country') ) {
                
                    Ember.$.growl.error({ title: 'Error', message: 'The address you entered was not valid. Please check that you entered all of the fields'});
                    controller.set('isCheckingOut', false);
                    return; //stop. just stop.
                    
                } else {
                
                    processNewAddress = true;
                
                }
                
            } else if ( !controller.get('existingAddress') ) {
                Ember.$.growl.error({ title: 'Error', message: 'Please enter or choose an address'});
                controller.set('isCheckingOut', false);
                return; //stop. just stop.
            }
            
            //time to process the card...
            if( controller.get('card_holder') ||
                controller.get('card_number') ||
                controller.get('cvc') || 
                controller.get('exp_month') ||
                controller.get('exp_year') ) {
                
                if( !controller.get('card_holder') ||
                    !controller.get('card_number') ||
                    !controller.get('cvc') || 
                    !controller.get('exp_month') ||
                    !controller.get('exp_year') ) {
                
                    Ember.$.growl.error({ title: 'Error', message: 'We could not process your address correctly.'});
                    controller.set('isCheckingOut', false);
                    return; //stop. just stop.
                } else {
                    processNewCard = true;
                }
                
            } else if( !controller.get('existingCard') ) {
                Ember.$.growl.error({ title: 'Error', message: 'Please enter or choose payment information'});
                controller.set('isCheckingOut', false);
                return; //stop. just stop.
            }
            
            //now we can attempt to checkout.
            this.send('checkout');
            
        },
        
        saveNewAddress: function() {
            
            var controller = this.get('controller');
            
            var self = this;
			var data = controller.getProperties('recipient_name',
										        'address_line_1',
										        'address_line_2',
										        'city',
										        'state_province',
										        'zip',
										        'country');
			
			var userAddress = this.store.createRecord('userAddress', data);
			
			userAddress.save().then(function(results) {
				//we can close the address
				console.log(results);
                
                controller.set('existingAddress', userAddress);
                controller.resetAddress();
                
                Ember.$.growl.notice({ title: 'Success', message: 'New address added successfully' });
                
			}, function(results) {
				console.log(results); //why did it fail?
				userAddress.destroyRecord();
			});
        },
        
        checkout: function() {
        
        },
        
        flipCard: function() {
            Ember.$('.flipped-container .flipped-card').toggleClass('flipped');
        }
    
    }
    
});;;App.ContactController = Ember.ArrayController.extend({
    
    needs: ['currentPage'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    //basic contact information
    siteName: function() { return this.get('model').filterBy('name', 'core_site_name').get('firstObject'); }.property('model'),
    phone: function() { return this.get('model').filterBy('name', 'core_phone').get('firstObject'); }.property('model'),
    fax: function() { return this.get('model').filterBy('name', 'core_fax').get('firstObject'); }.property('model'),
    email: function() { return this.get('model').filterBy('name', 'core_email').get('firstObject'); }.property('model'),
    address: function() { return this.get('model').filterBy('name', 'core_address').get('firstObject'); }.property('model'),
    
    //Hours of Operation
    hoursMonday: function() { return this.get('model').filterBy('name', 'core_hours_monday').get('firstObject') }.property('model'),
    hoursTuesday: function() { return this.get('model').filterBy('name', 'core_hours_tuesday').get('firstObject') }.property('model'),
    hoursWednesday: function() { return this.get('model').filterBy('name', 'core_hours_wednesday').get('firstObject') }.property('model'),
    hoursThursday: function() { return this.get('model').filterBy('name', 'core_hours_thursday').get('firstObject') }.property('model'),
    hoursFriday: function() { return this.get('model').filterBy('name', 'core_hours_friday').get('firstObject') }.property('model'),
    hoursSaturday: function() { return this.get('model').filterBy('name', 'core_hours_saturday').get('firstObject') }.property('model'),
    hoursSunday: function() { return this.get('model').filterBy('name', 'core_hours_sunday').get('firstObject') }.property('model'),
    
});;App.ContactRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('setting');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 7));
    }
    
});;App.EventsController = Ember.ArrayController.extend({

    needs: ['currentPage'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    sortProperties: ['name'],

    queryParams: ['category', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    category: null,                 // this is blank variable will store type to filter by
    public: 1,                  // select public view items.
    
    filteredEvents: function() {
        //this.set('type', 'rental');
        
        var category = this.get('eventCategory.id');    // retrieves type variable to filter by
        this.set('category', category); //just to show it in the url
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'sku'];
        var items = this.get('model');  // creats array of models to be filtered.
        
        if (category) {
            items = items.filterBy('eventCategory.id', category);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
        
    }.property('eventCategory', 'search', 'model'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        }
    
    }
    
    
});;;App.EventsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('event');  // filter array inventory, by publicVisable set to 1(true)
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 4));
    }
    
});

App.EventRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('event', params.event_id);
    }
    
});;App.HomeController = Ember.ArrayController.extend({
    needs: ['currentPage', 'randomRental', 'randomGood', 'randomEvent'],
    currentPage: Ember.computed.alias('controllers.currentPage'),
    randomRental: Ember.computed.alias('controllers.randomRental'),
    randomGood: Ember.computed.alias('controllers.randomGood'),
    randomEvent: Ember.computed.alias('controllers.randomEvent')
});;;App.HomeRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 1));
        this.controllerFor('randomRental').set('model', this.store.find('rental', { random: true }));
        this.controllerFor('randomGood').set('model', this.store.find('good', { random: true }));
        this.controllerFor('randomEvent').set('model', this.store.find('event', { random: true }));
    }
});;;App.LoginRoute = Ember.Route.extend({
    actions: {
        login: function(data) {

            if(typeof data === 'undefined') {
                data = this.get('controller').getProperties('username', 'password');
            }
            
            var loginController = this.get('controller');

            Ember.$.post('api-proxy/login', data).then(function(response) {

                var attemptedTransition = loginController.get('attemptedTransition');
                
                if(attemptedTransition) {
                    console.log(attemptedTransition);
                    var attemptedUrl = attemptedTransition.intent.url;
                    
                    if(attemptedUrl.indexOf('/') === 0) {
                        attemptedUrl = attemptedUrl.substring(1);
                    }
                    
                    loginController.set('attemptedTransition', null);
                    
                    window.location = rootURL + attemptedUrl;
                    
                } else {
                    window.location = rootURL;   
                }
                
//                var attemptedTransition = self.get('attemptedTransition');
//                if (attemptedTransition) {
//                    attemptedTransition.retry();
//                    self.set('attemptedTransition', null);
//                } else {
//                    // Redirect to 'home' by default.
//                    self.transitionToRoute('home');
//                }
            }, function(response) {
                console.log(response);
                $.growl.error({ title: "Error", message: "Please check your username and password" });
            });
        }
    }
});;App.RentalsController = Ember.ArrayController.extend({

    
    //dependencies
    needs: ['rentalCategories', 'currentPage'],
    rentalCategories: Ember.computed.alias('controllers.rentalCategories'),
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    sortProperties: ['name'],

    queryParams: ['category', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    category: null,                 // this is blank variable will store type to filter by
    tableName: 'inventory',
    
    filteredInventory: function() {
        //this.set('type', 'rental');
        
        var category = this.get('rentalCategory.id');    // retrieves type variable to filter by
        this.set('category', category); //just to show it in the url
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'sku'];
        var items = this.get('model');  // creats array of models to be filtered.
        
        if (category) {
            items = items.filterBy('rentalCategory.id', category);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
        
    }.property('rentalCategory', 'search', 'model'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        }
    
    }
    
    
});

App.RentalItemController = Ember.ObjectController.extend({
    
    needs: ['cart'],
    
    tableName: 'inventory',
    
    actions: {
    
        addToCart: function() {
            
            var self = this;
            
//            console.log(this.get('controllers.cart').get('model').get('firstObject'));
            
            var item = this.get('model');
            
            if(item.get('count') > 0) {
                
                var data = {};
                    data.price = this.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = this.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                    $.growl.notice({ title: "Item Added", message: "Thank you for adding this item to your cart!" });
                }, function(response) {
                    $.growl.error({ title: "Item Not Added", message: "Unfortunately there was a problem adding this item to your cart. Please try again later" });
                });
                
            } else {
                $.growl.error({ title: "Error", message: "Unfortunately there are no more available. Please try ordering it again later." });
            }
            
        }
        
    }
});

App.RentalItemReserveController = Ember.ObjectController.extend(Ember.Evented, {
    
});;App.Rental = App.InventoryBase.extend({

    //RELATIONSHIPS
    rentalCategory: belongsTo('rental-category', { async: true })
    
}); //create another instance of inventory with a different endpoint for the api

App.RentalCategory = App.InventoryCategoryBase.extend({
    rentals: hasMany('rental', { async: true })
});

;;App.RentalsRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('rental');
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 2));
        this.controllerFor('rentalCategories').set('model', this.store.find('rentalCategory'));
    }
    
});

App.RentalItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('rental',  params.inventory_id );  // filter array inventory, by publicVisable set to 1(true) 
    }
    
});

App.RentalItemReserveRoute = Ember.Route.extend({
    
    actions: {
        
        checkReservation: function(item) {
            
            
            
        }
        
    }
    
});;App.RentalItemReserveView = App.ModalView.extend({
    routeOnClose: 'rentalItem'
});;App.ServicesController = Ember.ArrayController.extend({

    public: 1

});	

App.ServiceController = Ember.ObjectController.extend({

	needs: ['application', 'cart'],

	isCurrentUser: function() {
		return this.get('controllers.application.currentUser');
	}.property('controllers.application.currentUser'),

	tableName: 'services',

	actions: {
    
        addToCart: function() {
            
            var self = this;
            
//            console.log(this.get('controllers.cart').get('model').get('firstObject'));
            
            var item = this.get('model');
                
                var data = {};
                    data.price = this.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = this.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                }, function(response) {
                    console.log(response)
                }); 
        }
        
    }

});	;App.Service = DS.Model.extend({

	name: 		   attr('string'),
	description:   attr('string'),
	price: 		   attr('number'),
    hours:          attr('number'),
    publicVisible:  attr('number'),
	created_at:    attr('date'),  
	updated_at:    attr('date')

});

App.ServicesSetting = DS.Model.extend({

    availableServices:    attr('number'),
    canReserve:           attr('number'),
    created_at:           attr('date'),  
    updated_at:           attr('date')

});

App.Service.reopenClass({
    FIXTURES: [
        { id: 1,
            name: 'Tune Up',
            description: 'New break cables, break housing, shifter cables, shifter housing, and chain (if necessary)',
            price: 40.00,
            created_at: '',
            updated_at: ''},
            { id: 2,
            name: 'Hit Man',
            description: 'does it need one?',
            price: 1000.00,
            created_at: '',
            updated_at: ''}
    ]
});;App.ServicesRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('service');
    }     
});;App.ShopController = Ember.ArrayController.extend({

    //dependencies
    needs: ['goodCategories', 'currentPage'],
    goodCategories: Ember.computed.alias('controllers.goodCategories'),
    currentPage: Ember.computed.alias('controllers.currentPage'),
    
    sortProperties: ['name'],

    queryParams: ['category', 'search'],      // Creates url Query ../?type='_____' will result in a search for type
    category: null,                 // this is blank variable will store type to filter by
    tableName: 'inventory',                  // select public view items.
    
    filteredInventory: function() {
        //this.set('type', 'rental');
        
        var category = this.get('goodCategory.id');    // retrieves type variable to filter by
        this.set('category', category); //just to show it in the url
        var search = this.get('search');
        var searchableProperties = ['name', 'description', 'sku'];
        var items = this.get('model');  // creats array of models to be filtered.
        
        if (category) {
            items = items.filterBy('goodCategory.id', category);
        }
        
        if(search) {
            items = items.filter(function(item) {
                
                for(var i = 0; i < searchableProperties.length; i++) {
                    console.log(item.get(searchableProperties[i]));
                    if(item.get(searchableProperties[i]).toUpperCase().search(search.toUpperCase()) !== -1) {
                        return true;
                    }
                }
            
                return false;
            });
        }
        
        return items;
        
    }.property('goodCategory', 'search', 'model'), // property is a listener for real time updates, upon update reloads.
    
    actions: {
        
        search: function() {
            this.set('search', this.get('searchQuery'));
        },
        
        addToCart: function(item) {
            
            var self = this;
            if(item.get('count') > 0) {
                
                var data = {};
                    data.price = item.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = item.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                    $.growl.notice({ title: "Item Added", message: "Thank you for adding this item to your cart!" });
                }, function(response) {
                    $.growl.error({ title: "Item Not Added", message: "Unfortunately there was a problem adding this item to your cart. Please try again later" });
                });
                
            } else {
                $.growl.error({ title: "Error", message: "Unfortunately there are no more available. Please try ordering it again later." });
            }
            
        } 
    
    }
    
    
});

App.ShopItemController = Ember.ObjectController.extend({
    
    needs: ['cart'],
    
    tableName: 'inventory',
    
    actions: {
    
        addToCart: function() {
            
            var self = this;
            
//            console.log(this.get('controllers.cart').get('model').get('firstObject'));
            
            var item = this.get('model');
            
            if(item.get('count') > 0) {
                
                var data = {};
                    data.price = this.get('price');
                    data.table_name = this.get('tableName');
                    data.table_id = this.get('id');

                var cartItem = this.store.createRecord('cartItem', data);
                cartItem.save().then(function(response) {
                    console.log(response);
                }, function(response) {
                    console.log(response)
                });
                
            } else {
                $.growl.error({ title: "Error", message: "Unfortunately there are no more available. Please try ordering it again later." });
            }
            
        }
        
    }
});

App.InventoryCategoryController = Ember.ArrayController.extend({
    
    sortProperties: ['category'],
    sortAscending: true,
    
});;App.Good = App.InventoryBase.extend({
    goodCategory: belongsTo('good-category', { async: true })
}); //create another instance of inventory with a different endpoint for the api

App.GoodCategory = App.InventoryCategory.extend({
    goods: hasMany('good', {async: true})
});

;;App.ShopRoute = Ember.Route.extend({
    
    model: function() {
        return this.store.find('good');  // filter array inventory, by publicVisable set to 1(true) 
    },
    
    setupController: function(controller, model) {
        controller.set('model', model);
        
        //dependencies
        this.controllerFor('currentPage').set('model', this.store.find('page', 3));
        this.controllerFor('goodCategories').set('model', this.store.find('goodCategory'));
    }
    
});

App.ShopItemRoute = Ember.Route.extend({
    
    model: function(params) {
        return this.store.find('good',  params.inventory_id );  // filter array inventory, by publicVisable set to 1(true) 
    }
    
});;App.SignupController = Ember.ArrayController.extend({

    needs: ['application'],
    
    reset: function() {
        this.setProperties({
            name: '',
            username: '',
            password: '',
            password_confirmation: '',
        });
    },

    actions: {

        signup: function() {
            var self = this,
                data = this.getProperties('name', 'username', 'password', 'password_confirmation'),
                user = this.store.createRecord('user', data);

            user.save().then(function(response) {
                var applicationController = self.get('controllers.application');
                applicationController.set('currentUser', response.get('data'));
                self.transitionToRoute('home');
                
            }, function(response) {
                Ember.Logger.debug('user signup error:', response);
                self.set('errorMessage', response.messages);
            });
        }

    }

});
;;App.SignupRoute = Ember.Route.extend({

    setupController: function(controller, model) {
        this._super(controller, model);
        controller.reset(); //we should clear any values in here if it gets called again
    },
    
    actions: {
        error: function(reason, transition) {
            console.log('testing');
            return true;
        }
    }
    
});