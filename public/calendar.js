$(document).ready(function() 
{
    $("#starting").datepicker();
    $("#ending").datepicker();
    $("button").click(function() 
    {
    	var selected = $("#dropdown option:selected").text();
        var departing = $("#starting").val();
        var returning = $("#ending").val();
        if (departing === "" || returning === "") 
        {
			alert("Please select departing and returning dates.");
        } 
        else 
        {
			confirm("Would you like to go to " + selected + " on " + departing + " and return on " + returning + "?");
        }
    });
});